#!/bin/bash

# Read PostgreSQL connection from file

export $(cat .env | grep -v ^# | xargs)

# Load PostgreSQL IDs

PASSWORD_FILE="$HOME/.pgpass"

echo ""

if [ ! -f $PASSWORD_FILE ]; then
    printf "\e[0;31m~/.pgpass not found\e[m"
    exit
else
    printf "\e[0;36mLoading DB credentials from ~/.pgpass\e[m"
fi

#FILE_PERMISSION=`stat -c %a `$PASSWORD_FILE

#if [ $FILE_PERMISSION != 600 ]; then
#   printf "\e[0;31m~/.pgpass permission is not 600, PostgreSQL will ignore it\e[m"
#   exit
#fi

echo ""

CREDENTIALS_FOUND=false

# cat $PASSWORD_FILE |
while read line ; do
    IFS=':' read -a array <<< "$line"
    if [[ "$DB_HOST" = "${array[0]}"  && "$DB_USERNAME" = "${array[3]}" && ("$DB_DATABASE" = "${array[2]}" || "*" = "${array[2]}") ]];  then
        CREDENTIALS_FOUND=true
        break
    fi
done < $PASSWORD_FILE

CURRENT_DIR=`pwd`

if [ "$CREDENTIALS_FOUND" = "true" ]; then
    printf "\e[0;32mDB credentials was found\e[m\n"
    echo ""

    DONE_DIRECTORY="$CURRENT_DIR/database/migrations/.done"

    if [ ! -d "$DONE_DIRECTORY" ]; then
      mkdir $DONE_DIRECTORY
    fi

    for file in $CURRENT_DIR/database/migrations/*.sql
    do
        if [[ -f $file ]]; then
            SQL=`basename $file`
            DONE_MIGRATION=$CURRENT_DIR/database/migrations/.done/.$SQL
            if [[ ! -f $DONE_MIGRATION ]]; then
                ${PSQL_PATH}psql -U ${DB_USERNAME} -h ${DB_HOST} -d ${DB_DATABASE} -f $file
                touch $DONE_MIGRATION
            fi
        fi
    done

else
    printf "\e[0;31mDB credentials not found ! Aborting\e[m\n"
    echo "-----"
    cat ~/.pgpass
    echo "-----"
fi

echo ""
