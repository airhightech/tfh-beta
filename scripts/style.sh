#!/bin/bash

sass ./public/css/src/desktop.scss:./public/css/desktop.min.css --style compressed

sass ./public/css/src/mobile.scss:./public/css/mobile.min.css --style compressed

sass ./public/mobile/css/src/style.scss:./public/mobile/css/style.min.css --style compressed
