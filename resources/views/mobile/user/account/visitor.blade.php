@extends('mobile.layouts.auth')
<link href="/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="/js/pnotify/pnotify.custom.min.css" rel="stylesheet">
<style>
.member-profile-header .account-cover {
    position: relative;
    margin: -0;
    margin-bottom: 0px;
    height: 100px;
    background-color: #8ccddb;
    overflow: hidden;
}

.profile-picture {
    width: 100px;
    height: 100px;
    border: 4px solid #FFF;
    border-radius: 110px;
    position: relative;
    margin-left: 37%;
    float: left;
    margin-bottom: 20px;
    position: relative;
    overflow: hidden;
    background-color: #FF3B7C;
}
</style>

<?php

$profile = $user->getProfile();
$personalData = $user->getPersonalData();
$companyData = $user->getCompanyData();

?>

<div class="box">
  <div class="member-profile-header">
      <div class="account-cover">
          <img id="cover-img" src="{{ $profile->getCoverPicture() }}" class="img-responsive"/>
          <div class="profile-picture">
              <img id="profile-img" src="{{ $profile->getProfilePicture() }}" class="img-responsive"/>
          </div>
      </div>
    email : {{$user->email}}
    <table class="table setting-table">
        <tr>
            <td>{{ tr('member_profile.name-label', 'Name') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="name" value="{{ $profile->getName() }}" data-name="name"
                       class="setting-data-item"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.phone', 'Phone') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="phone" value="{{ $profile->phone }}"
                       class="small setting-data-item" data-name="phone"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.custom_address', 'Address') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <textarea rows="5" name="custom_address" data-name="custom_address" class="setting-data-item">{{ $profile->custom_address }}</textarea>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.birthdate', 'Birthday') }} </td>
            <td class="setting-data">
                <input type="text" id="birthdate" name="birthdate" value="{{ $personalData->getBirthdate() }}" class="birthdate small" data-name="birthdate"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.gender', 'Gender') }} </td>
            <td class="setting-data">
                <select name="gender" class="setting-data-item data-select" data-name="gender">
                    <option value="">Select a gender</option>
                    <option value="female" <?php echo $personalData->gender == 'female' ? 'selected' : ''; ?> > Female</option>
                    <option value="male" <?php echo $personalData->gender == 'male' ? 'selected' : ''; ?> > Male</option>
                </select>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.nationality', 'Nationality') }} </td>

            <td class="setting-data">
                <select name="nationality" class="setting-data-item data-select" data-name="nationality">
                    <option value="">Select a nationality</option>
                    <?php

                        $countries = config('countries.list');

                        foreach($countries as $country) {
                            ?>
                            <option value="{{ $country['code'] }}" <?php echo $personalData->nationality == $country['code'] ? 'selected' : ''; ?> > {{ $country['name'] }}</option>
                            <?php
                        }
                    ?>
                </select>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td></td>

            <td class="setting-data">
                <button id='save' type="button" class="btn btn-primary">Update</button>
            </td>
            <td class="action">
<!--                -->
            </td>
        </tr>
    </table>
</div>


@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet">
<script src="/libs/chosen/chosen.jquery.js"></script>
<script src="{{ __asset('/mobile/js/profile.js') }}"></script>
<script src="/libs/jquery-ui/jquery-ui.min.js"></script>
<script src="/js/pnotify/pnotify.custom.min.js"></script>
@endsection
