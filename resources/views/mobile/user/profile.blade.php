@extends('mobile.layouts.auth')

@section('content')
<div class="login-container">
    <div style="width: 70%;height: calc(100% - 40px);margin: 0 auto 40px;padding: 20px 0;">
        <img style="height:73px;display:block;margin: 11px auto;" src="/mobile/img/icon-logo.png"></img>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="/mobile/img/user.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input id='email' type="email" style="border:none;outline:none;" placeholder="E-mail" />
            </div>
        </div>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="/mobile/img/padlock.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input id="password" type="password" style="border:none;outline:none;" placeholder="Password" />
            </div>
        </div>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="/mobile/img/padlock.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input id="passwordconfirm" type="password" style="border:none;outline:none;" placeholder="Confirm Password" />
            </div>
        </div>
        <button id="submitLogin" style="height:32px;width:calc(100% - 22px);margin: 7px 11px;border:none;background-color:#4ec9f2;color:white;">Register</button>
    </div>
</div>
@endsection

@section('scripts')
<script src="/mobile/lib/jquery/dist/jquery.js"></script>
<script>
$('#submitLogin').click(function(){
  if($('#password').val() != $('#passwordconfirm').val()){
    alert('Password does not match');
  }
  else if($('#password').val() == '' || $('#passwordconfirm').val() == ''){
    alert('Please enter a password');
  }
  else {
    var data = {
      email: $('#email').val(),
      password : $('#password').val(),
      password_confirmation : $('#passwordconfirm').val(),
    };
    $.post( "/rest/auth/register", data, function(res) {
      alert('Please check your email');
  }).fail(function(res){
    //alert(res.email);
    alert(res.responseJSON.email[0]);
  });
  }
})
</script>
@endsection
