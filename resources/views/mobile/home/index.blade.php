@extends('mobile.layouts.default')

@section('content')
<link rel="stylesheet" type="text/css" href="/mobile/lib/bootstrap/dist/css/bootstrap.min.css">
<script src="/mobile/lib/angular/angular.js"></script>
<div class="body-container" ng-app="myApp" ng-controller="myController">
    <div style="width:100%;height:calc(100% - 63px);padding:9%;background:rgba(0, 0, 0, 0.53);background-image: linear-gradient(rgba(4, 62, 71, 0.53), rgba(4, 62, 71, 0.53)), url('/mobile/img/back.jpg');background-size: cover;" ng-if="selectedTab == 0">
        <form style="width:100%;height:100%;" id="inner-main" ng-submit="search()">
            <button type="button" class="big-button selected" ng-click="selectButton('rent')" id="rent">{{ tr('menubar.rent', 'Rent') }}</button>
            <div style="height:1px;float:left;" class="spacers"></div>
            <button type="button" class="big-button" ng-click="selectButton('buy')" id="buy">{{ tr('menubar.sale', 'Sale') }}</button>
            <button type="button" class="big-button" ng-click="selectButton('condo')" id="condo">{{ tr('menubar.condo-community', 'Condo community') }}</button>
            <div style="height:1px;float:left;" class="spacers"></div>
            <button type="button" class="big-button" ng-click="selectButton('new')" id="new">{{ tr('menubar.new-project', 'New project') }}</button>
            <input type="text" class="search-box" placeholder="{{ tr('home.input.search-placeholder', 'SEARCH') }}" id="search" />
            <input type="submit" class="search-input" value="">
        </form>
    </div>
    <div id="content0" style="visibility: hidden; width:100%;height:calc(100% - 63px);padding:7%;border-top:3px solid #dbdbdb;overflow-y:auto;" ng-if="selectedTab == 1">
        
        <div class="row home-banner" style="margin-bottom: 10px;">
            <div class="col-md-12 banner banner-home" data-location="home">            
            </div>        
        </div>
        
        <div style="height:100%;width:100%;">

            <?php
            foreach ($exclusive as $property) {

                $listing = $property->getListingData();
                $extended = $property->getExtendedData();
                $address = $property->getAddress();

                $images = $property->getImages();
                $image = false;

                if (count($images)) {
                    $image = array_shift($images);
                }

                $district = null;

                if ($address) {
                    $district = $address->getDistrict();
                }

                ?>
                <div class="listing-item">
                    <a href="/property/{{ $property->id }}">
                        <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="overflow: hidden;">
                        <?php
                            if ($image) {
                                ?>
                                <img src="{{ $image->getThumbUrl('310x250', true) }}" style="width:100%; "/>
                                <?php
                            } else {
                                ?>
                                <img src="/mobile/img/placeholder.png" style="width:100%; "></img>
                                <?php
                            }
                        ?>
                    </div>
                    <div class="listing-details">

                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/mobile/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">{{ $property->getTranslatable('name')}}</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/mobile/img/placeholder.png" style="width:100%; "></img>
                            </div>

                            <?php
                            if ($district) {
                                ?>
                            <div style="display:inline-block;width:54%;font-size:9px;">{{ $district->getTranslatable('name') }}</div>
                                <?php
                            }
                            ?>

                            <div style="display:inline-block;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; {{ number_format($property->getSalePrice()) }}</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">{{ str_limit($property->getCustomAddress(), $limit = 100, $end = '...') }}</div>
                    </div>
                    </a>

                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div id="content1" style="visibility: hidden;width:100%;height:calc(100% - 63px);padding:7%;border-top:3px solid #dbdbdb;overflow-y:auto;" ng-if="selectedTab == 2">
        
        <div class="row home-banner" style="margin-bottom: 10px;">
            <div class="col-md-12 banner banner-list-rent" data-location="list-rent">            
            </div>        
        </div>
        
        <div style="height:100%;width:100%;">
            <?php
            foreach ($featured_rent as $property) {

                $listing = $property->getListingData();
                $extended = $property->getExtendedData();
                $address = $property->getAddress();

                $images = $property->getImages();
                $image = false;

                if (count($images)) {
                    $image = array_shift($images);
                }

                $district = null;

                if ($address) {
                    $district = $address->getDistrict();
                }

                ?>
                <div class="listing-item">
                    <a href="/property/{{ $property->id }}">
                        <div class="listing-image" style="overflow: hidden;">
                        <?php
                            if ($image) {
                                ?>
                                <img src="{{ $image->getThumbUrl('310x250', true) }}" style="width:100%;"/>
                                <?php
                            } else {
                                ?>
                                <img src="/mobile/img/placeholder.png" style="width:100%;"></img>
                                <?php
                            }
                        ?>
                    </div>
                    <div class="listing-details">

                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/mobile/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">{{ $property->getTranslatable('name')}}</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/mobile/img/placeholder.png" style="width:100%; "></img>
                            </div>

                            <?php
                            if ($district) {
                                ?>
                            <div style="display:inline-block;width:54%;font-size:9px;">{{ $district->getTranslatable('name') }}</div>
                                <?php
                            }
                            ?>
                            <div style="display:inline-block;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; {{ number_format($property->getSalePrice()) }}</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">{{ str_limit($property->getCustomAddress(), $limit = 100, $end = '...') }}</div>
                    </div>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div id="content2" style="visibility: hidden;width:100%;height:calc(100% - 63px);padding:7%;border-top:3px solid #dbdbdb;overflow-y:auto;" ng-if="selectedTab == 3">
        
        <div class="row home-banner" style="margin-bottom: 10px;">
            <div class="col-md-12 banner banner-list-sale" data-location="list-sale">            
            </div>         
        </div>
        
        <div style="height:100%;width:100%;">
            <?php
            foreach ($featured_sale as $property) {

                $listing = $property->getListingData();
                $extended = $property->getExtendedData();
                $address = $property->getAddress();

                $images = $property->getImages();
                $image = false;

                if (count($images)) {
                    $image = array_shift($images);
                }

                $district = null;

                if ($address) {
                    $district = $address->getDistrict();
                }

                ?>
                <div class="listing-item">
                    <a href="/property/{{ $property->id }}">
                        <div class="listing-image" style="overflow: hidden;">
                            <?php
                                if ($image) {
                                    ?>
                                    <img src="{{ $image->getThumbUrl('310x250', true) }}" style="width:100%; "/>
                                    <?php
                                } else {
                                    ?>
                                    <img src="/mobile/img/placeholder.png" style="width:100%; "></img>
                                    <?php
                                }
                            ?>
                        </div>
                        <div class="listing-details">

                            <div style="display:block;width:100%;color:#000000;font-size:12px;">
                                <div style="display:inline-block;width:5%;">
                                    <img src="/mobile/img/home.png" style="width:100%;margin-top:-4px;"></img>
                                </div>
                                <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">{{ $property->getTranslatable('name')}}</div>
                            </div>
                            <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                                <div style="display:inline-block;width:5%;">
                                    <img src="/mobile/img/placeholder.png" style="width:100%; "></img>
                                </div>

                                <?php
                                if ($district) {
                                    ?>
                                <div style="display:inline-block;width:54%;font-size:9px;">{{ $district->getTranslatable('name') }}</div>
                                    <?php
                                }
                                ?>

                                <div style="display:inline-block;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; {{ number_format($property->getSalePrice()) }}</div>
                            </div>
                            <div style="font-size:9px;margin-top:-1px;">{{ str_limit($property->getCustomAddress(), $limit = 100, $end = '...') }}</div>
                        </div>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div style="width:100%;height:63px;background-color:white;display:flex;text-transform:uppercase;text-align: center;color: #4ec9f2;font-weight:bold;">
        <div class="home-tab" ng-click="selectTab(0)" id="tab0">
            <div class="tab-mark"></div>
            <div style="position: absolute;top: 50%;left: 50%;transform: translateX(-50%) translateY(-50%);width:100%;">{{ tr('home.options.exclusive', 'Exclusive Project') }}</div>
        </div>
        <div class="home-tab" ng-click="selectTab(1)" id="tab1">
            <div class="tab-mark"></div>
            <div style="position: absolute;top: 50%;left: 50%;transform: translateX(-50%) translateY(-50%);width:100%;">{{ tr('home.options.rent', 'FOR RENT') }}</div>
        </div>
        <div class="home-tab" ng-click="selectTab(2)" id="tab2">
            <div class="tab-mark"></div>
            <div style="position: absolute;top: 50%;left: 50%;transform: translateX(-50%) translateY(-50%);width:100%;">{{ tr('home.options.sale', 'SALE') }}</div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css">
<script src="/libs/autocomp/jquery.auto-complete.min.js"></script>
<script src="{{ __asset('/js/visitor/banner.js') }}"></script>
<script src="/mobile/homepage.js"></script>

@endsection
