@extends('mobile.layouts.auth')

@section('content')

<div class="login-container">
    <div style="width: 70%;height: calc(100% - 40px);margin: 0 auto 40px;padding: 20px 0;">
      <a href="/"><img style="height:73px;display:block;margin: 11px auto;" src="mobile/img/icon-logo.png"></img></a>
        <?php
        if (!Auth::check()) { ?>
        <button id="fb-login" style="height:34px;width:calc(100% - 22px);margin: 6px 11px;border:none;background-color:#184a6d;color:white;">{{ tr('nav_menu.sign-facebook', 'Log in with Facebook') }}</button>
        <button id="twitter-login" style="height:34px;width:calc(100% - 22px);margin: 6px 11px;border:none;background-color:#53bbd4;color:white;">{{ tr('nav_menu.sign-twitter', 'Log in with Twitter') }}</button>
        <button id="google-login" style="height:34px;width:calc(100% - 22px);margin: 6px 11px;border:none;background-color:#de4b39;color:white;">{{ tr('nav_menu.sign-google', 'Log in with Google+') }}</button>
        <div style="color:#b8b8b8;text-align:center;margin: 11px auto;">OR</div>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="mobile/img/user.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input id='email' type="email" style="border:none;outline:none;" placeholder="{{ tr('auth_login.email', 'E-mail') }}" />
            </div>
        </div>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="mobile/img/padlock.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input id="password" type="password" style="border:none;outline:none;" placeholder="{{ tr('auth_login.password', 'Password') }}" />
            </div>
        </div>
        <button id="submitLogin" style="height:32px;width:calc(100% - 22px);margin: 7px 11px;border:none;background-color:#4ec9f2;color:white;">{{ tr('user.login', 'Log in') }}</button>
        <div style="font-size:12px;color:#4ec9f2;margin: 10px auto; text-align:center;">
            <a href="/auth/register">{{ tr('user.createaccount', 'Create an account') }}</a>
        </div>
        <div style="font-size:12px;color:#4ec9f2;margin: 10px auto; text-align:center;">
            <a onclick="forgotPassword()">{{ tr('user.forgotpassword', 'Forgot password') }}</a>
        </div>
      <?php } else { ?>
          <button id="profile" style="height:32px;width:calc(100% - 22px);margin: 7px 11px;border:none;background-color:#4ec9f2;color:white;">{{ tr('user.myprofile', 'My Profile') }}</button>
          <button id="notice" style="height:32px;width:calc(100% - 22px);margin: 7px 11px;border:none;background-color:#4ec9f2;color:white;">{{ tr('user.notice', 'Notice') }}</button>
          <button id="loggout" style="height:32px;width:calc(100% - 22px);margin: 7px 11px;border:none;background-color:#4ec9f2;color:white;">{{ tr('user.logout', 'Logout') }}</button>
      <?php } ?>
    </div>
</div>
<div class="bottom-navigation">
        <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
        <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
    </div>
@endsection

@section('scripts')
<script>
    $(function(){
        $('#fb-login').on('click', function(){
            window.location = '/facebook-login';
        });
        $('#twitter-login').on('click', function(){
            window.location = '/auth/twitter';
        });
        $('#google-login').on('click', function(){
            window.location = '/auth/google';
        });
    });

    $('#profile').on('click', function(){
        window.location = '/user/profile';
    });

    $('#loggout').on('click', function(){
        window.location = '/auth/logout';
    });

    $('#notice').on('click', function(){
        window.location = '/user/notice';
    });

    var forgotPassword = function(){
      var email = prompt("Please enter your email", "");
      data = { email : email };
      $.post( "/rest/auth/forgot-password", data, function(data) {
        alert('Please check your email');
      });
    }
    </script>
@endsection
