@extends('mobile.layouts.default')

@section('content')
    <div class="main-menu">
        <input type="text" id="q" name="q" value="{{$q}}" class="search-box" placeholder="{{ tr('home.input.search-placeholder', 'SEARCH') }}">
        </input>
        <div class="options">
            <div class="row">
                <div class="col-xs-6 menu-item" id="option">
                    {{ tr('search_result.option', 'Option') }}
                </div>
                <div class="col-xs-6 menu-item" id="viewMap">
                    {{ tr('search_result.mapview', 'Map View') }}
                </div>
            </div>
        </div>
    </div>
<div>

  <div class="options-menu">
      <div class="row">
          <div class="col-xs-5">
              <label for="listing">{{ tr('search.listing.type', 'Listing Type') }}</label>
          </div>
          <div class="col-xs-7">
              <select name="listing" id="lt">
                  <option value="rent" selected>{{ tr('search.rent', 'Rent') }}</option>
                  <option value="sale">{{ tr('search.Sale', 'Sale') }}</option>
              </select>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-5">
              <label for="home">{{ tr('search.home.type', 'Home Type') }}</label>
          </div>
          <div class="col-xs-7">
              <select name="home" id="pt">
                  <option value="cd" selected>{{ tr('search.Condominium', 'Condominium') }}</option>
                  <option value="th" >{{ tr('search.Townhouse', 'Townhouse') }}</option>
                  <option value="ap" >{{ tr('search.Apartment', 'Apartment') }}</option>
                  <option value="dh" >{{ tr('search.Detached.House', 'Detached House') }}</option>
                  <option value="cdb" >{{ tr('search.Condo.Comunity', 'Condo Comunity') }}</option>
              </select>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-5">
              <label for="bed">{{ tr('search.Bedrooms', 'Bedrooms') }}</label>
          </div>
          <div class="col-xs-7">
              <select name="bed" id="bed">
                  <option value="" selected><option>
                  <option value="studio">{{ tr('search.Studio', 'Studio') }}</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4+">4+</option>
              </select>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-5">
              <label for="bath">{{ tr('search.Bathrooms', 'Bathrooms') }}</label>
          </div>
          <div class="col-xs-7">
              <select name="bath" id="bath">
                  <option value="" selected><option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4+">4+</option>
              </select>
          </div>
      </div>
      <div class="row">
          <div class="col-xs-4">
              <label for="new-project">{{ tr('search.New.Project', 'New Project') }}</label>
          </div>
          <div class="col-xs-7">
            <input type="checkbox" id='new-project' name="new-project" value="yes">
          </div>
      </div>
      <hr style="border-top-width:2px;margin:15px;" />
      <div class="row">
          <div class="col-xs-5">
              <label for="price">{{ tr('search.Price', 'Price') }}</label>
          </div>
          <div class="col-xs-7">
              <input type="number" id="pmin" name="pmin" value="" placeholder="{{ tr('search.Min.price', 'Min price') }}" />
                  -
              <input type="number" id="pmax" name="pmax" value="" placeholder="{{ tr('search.Max.price', 'Max price') }}" />
          </div>
      </div>
      <div class="row">
          <div class="col-xs-5">
              <label for="size">{{ tr('search.Size', 'Size') }}</label>
          </div>
          <div class="col-xs-7">
              <input type="number" id="smin" name="smin" value="" placeholder="{{ tr('search.Min.size', 'Min size') }}" />
                  -
              <input type="number" id="smax" name="smax" value="" placeholder="{{ tr('search.Max.size', 'Max size') }}" />
          </div>
      </div>
      <div class="row">
          <div class="col-xs-5">
              <label for="year">{{ tr('search.Year.Built', 'Year Built') }}</label>
          </div>
          <div class="col-xs-7">
              <input type="number" id="ymin" name="ymin" value="" placeholder="{{ tr('search.Min.year', 'Min year') }}" />
                  -
              <input type="number" id="ymax" name="ymax" value="" placeholder="{{ tr('search.Max.year', 'Max year') }}" />
          </div>
      </div>
      <div class="row apply">
          <div class="col-xs-12">
              <button class="apply-search-option">{{ tr('search.apply', 'APPLY') }}</button>
          </div>
      </div>
  </div>

</div>
    <div class="search-results">
@foreach ($properties as $property)
  <?php $prop = \App\Models\Property::find($property->id);
        $image = $prop->getMainImage();
  ?>
  <a href="/property/{{$property->id}}">
        <div class="property-item" data-year="" data-price="" data-beds="" data-baths="" data-size="" data-rental="" id="prop-{{$property->id}}">
            <?php if(isset($image)){?>
              <img id="img-list" src="/image/{{$image->filepath}}" class="prop-image img-responsive">
            <?php }
            else {
              ?>
                <img id="img-list" src="/img/default-property.png" class="prop-image img-responsive">
              <?php
            }
            ?>
            <?php
              $type = '';
              if($property->np == true){
                $type = "project";
              }
              elseif ($property->ptype == "cdb") {
                $type = "condo";
              }
              elseif ($property->ltype == "rent") {
                $type = "rent";
              }
              elseif ($property->ltype == "sale") {
                $type = "sale";
              }
              else{
                $type = 'condo';
              }
            ?>

            <button id="favorite-{{$property->id}}" data-idprop="{{$property->id}}" class="favorite-btn"><i class="favorite fa fa-heart-o"></i></button>
            <div class="details">
                <span class="listing-type <?php echo $type ?>"><i class="fa fa-circle"></i></span>  <span class="property">{{$prop->getTranslatable('name')}}</span><br>
                &lrm;<span class="listing-price">&lrm;฿ {{number_format($prop->getSalePrice())}}</span> |
                <span class="prop-only">
                        <span class="beds">{{$property->bedroom_num}}</span> <i class="fa fa-bed" aria-hidden="true"></i>
                        <span class="bath">{{$property->bathroom_num}}</span> <i class="fa fa-bath" aria-hidden="true"></i> |
                        <span class="size">{{$property->total_size}}</span>sqm
                    </span>
                <div class="summary">
                    <span class="address">{{$property->address}}</span>
                </div>
            </div>
        </div>
  </a>
@endforeach
    </div>

    @endsection

    @section('scripts')
    <link href="/mobile/js/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>

    <script src="/mobile/lib/jquery/dist/jquery.js"></script>
    <script src="/mobile/js/jquery-ui/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css">
    <script src="/libs/autocomp/jquery.auto-complete.min.js"></script>

    <script src="{{ __asset('/mobile/js/list.js') }}"></script>

    <script>
      var searchCriteria = <?php echo json_encode($all); ?>;
      var pleaseLogIn = "{{ tr('user.pleaselogin', 'Please Log in') }}";
    </script>

    <script>
        $(function() {
            $(".menu-item").click(function() {
                if($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                } else {
                    $(this).addClass("selected");
                }
            });
            $("#option").click(function() {
                if($(this).hasClass("selected")) {
                    $(".options-menu").css("display", "block");
                } else {
                    $(".options-menu").css("display", "none");
                }
            });
            $("#view").click(function() {
                if($(this).hasClass("selected")) {
                    $("#view").text("Map View");
                    $("#mapview").css("display", "none");
                    $("#listview").css("display", "block");
                } else {
                    $("#view").text("Map View");
                    $("#mapview").css("display", "block");
                    $("#listview").css("display", "none");
                }
            });
        });
    </script>
@endsection
