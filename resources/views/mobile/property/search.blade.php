@extends('mobile.layouts.default')

@section('content')

    <div class="main-menu">
        <input type="text" value="{{$query}}" id="q" name="q" value="" class="search-box" placeholder="{{ tr('home.input.search-placeholder', 'SEARCH') }}">
        </input>
        <div class="options">
            <div class="row">
                <div class="col-xs-6 menu-item" id="option">
                    {{ tr('search_result.option', 'Option') }}
                </div>
                <div class="col-xs-6 menu-item" id="viewList">
                    {{ tr('search_result.litview', 'List View') }}
                </div>
            </div>
        </div>
    </div>

    <div id="map" style="width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);">

    </div>

    <div class="options-menu">
        <div class="row">
            <div class="col-xs-5">
                <label for="listing">{{ tr('search.listing.type', 'Listing Type') }}</label>
            </div>
            <div class="col-xs-7">
                <select name="listing" id="lt">
                    <option value="rent" selected>{{ tr('search.rent', 'Rent') }}</option>
                    <option value="sale">{{ tr('search.Sale', 'Sale') }}</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="home">{{ tr('search.home.type', 'Home Type') }}</label>
            </div>
            <div class="col-xs-7">
                <select name="home" id="pt">
                    <option value="cd" selected>{{ tr('search.Condominium', 'Condominium') }}</option>
                    <option value="th" >{{ tr('search.Townhouse', 'Townhouse') }}</option>
                    <option value="ap" >{{ tr('search.Apartment', 'Apartment') }}</option>
                    <option value="dh" >{{ tr('search.Detached.House', 'Detached House') }}</option>
                    <option value="cdb" >{{ tr('search.Condo.Comunity', 'Condo Comunity') }}</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="bed">{{ tr('search.Bedrooms', 'Bedrooms') }}</label>
            </div>
            <div class="col-xs-7">
                <select name="bed" id="bed">
                    <option value="" selected><option>
                    <option value="studio">{{ tr('search.Studio', 'Studio') }}</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4+">4+</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="bath">{{ tr('search.Bathrooms', 'Bathrooms') }}</label>
            </div>
            <div class="col-xs-7">
                <select name="bath" id="bath">
                    <option value="" selected><option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4+">4+</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <label for="new-project">{{ tr('search.New.Project', 'New Project') }}</label>
            </div>
            <div class="col-xs-7">
              <input type="checkbox" id='new-project' name="new-project" value="yes">
            </div>
        </div>
        <hr style="border-top-width:2px;margin:15px;" />
        <div class="row">
            <div class="col-xs-5">
                <label for="price">{{ tr('search.Price', 'Price') }}</label>
            </div>
            <div class="col-xs-7">
                <input type="number" id="pmin" name="pmin" value="" placeholder="{{ tr('search.Min.price', 'Min price') }}" />
                    -
                <input type="number" id="pmax" name="pmax" value="" placeholder="{{ tr('search.Max.price', 'Max price') }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="size">{{ tr('search.Size', 'Size') }}</label>
            </div>
            <div class="col-xs-7">
                <input type="number" id="smin" name="smin" value="" placeholder="{{ tr('search.Min.size', 'Min size') }}" />
                    -
                <input type="number" id="smax" name="smax" value="" placeholder="{{ tr('search.Max.size', 'Max size') }}" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="year">{{ tr('search.Year.Built', 'Year Built') }}</label>
            </div>
            <div class="col-xs-7">
                <input type="number" id="ymin" name="ymin" value="" placeholder="{{ tr('search.Min.year', 'Min year') }}" />
                    -
                <input type="number" id="ymax" name="ymax" value="" placeholder="{{ tr('search.Max.year', 'Max year') }}" />
            </div>
        </div>
        <div class="row apply">
            <div class="col-xs-12">
                <button class="apply-search-option">{{ tr('search.apply', 'APPLY') }}</button>
            </div>
        </div>
    </div>



    <div id="listview" style="display:none;width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);">
        List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View
    </div>

    <div class="bottom-navigation">
        <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
        <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
    </div>

<div id="property-template" style="display: none;">
    <div class="property-item" data-year="" data-price="" data-beds="" data-baths="" data-size="" data-rental="">
        <img src="" class="prop-image img-responsive"/>
        <span class="img-count">2 images</span>
        <button class="favorite-btn"><i class="favorite fa"></i></button>
        <div class="details">
            <span class="listing-type"><i class="fa fa-circle"></i></span>  <span class="property"></span><br/>
            ‎<span class="listing-price"></span><br/>
            <div class="summary">
                <span class="prop-only">
                    <span class="beds"></span> <i class="fa fa-bed" aria-hidden="true"></i> |
                    <span class="bath"></span> <i class="fa fa-bath" aria-hidden="true"></i> |
                    <span class="size"></span>sqm |
                </span>
                <span class="address"></span>
            </div>
        </div>
    </div>
</div>
    @endsection

    @section('scripts')

    <script>
      var noResult = '{{ tr('search_result.noResult', 'No search result found') }}';
    </script>

    <link href="/mobile/js/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>

    <script src="/mobile/lib/jquery/dist/jquery.js"></script>
    <script src="/mobile/js/jquery-ui/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css">
    <script src="/libs/autocomp/jquery.auto-complete.min.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
    <script>
        $(function() {
            $(".menu-item").click(function() {
                if($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                } else {
                    $(this).addClass("selected");
                }
            });
            $("#option").click(function() {
                if($(this).hasClass("selected")) {
                    $(".options-menu").css("display", "block");
                } else {
                    $(".options-menu").css("display", "none");
                }
            });
            $("#view").click(function() {
                if($(this).hasClass("selected")) {
                    $("#view").text("Map View");
                    $("#mapview").css("display", "none");
                    $("#listview").css("display", "block");
                } else {
                    $("#view").text("Map View");
                    $("#mapview").css("display", "block");
                    $("#listview").css("display", "none");
                }
            });
        });
    </script>
    <script src="/mobile/js/infobox_packed.js"></script>
    <script src="/mobile/js/markerclusterer.js"></script>
    <script src="{{ __asset('/mobile/js/property.js') }}"></script>
    <script src="{{ __asset('/mobile/js/search.js') }}"></script>
    <script>
        var currentId = <?php echo isset($all['id']) ? $all['id'] : 'null'; ?>;
        var searchCriteria = <?php echo json_encode($all); ?>;
        var currentPoiData = <?php echo json_encode($place, true); ?>;
    </script>
@endsection
