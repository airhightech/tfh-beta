<div style="border:2px solid #0d8ea3;font-size: 10px;margin-bottom:15px;">
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.deposit', 'Deposit') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            ฿ {{ number_format($listing->deposite) }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.price-rent', 'Rental') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            ฿ {{ number_format($listing->listing_price) }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.psf', 'Price/Sqm') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            ฿ {{ number_format($extended->psf) }} /sqm
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.size', 'Size') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $extended->total_size }} sqm
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.bedrooms', 'Bedrooms') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $extended->bedroom_num }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.bathrooms', 'Bathrooms') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $extended->bathroom_num }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.floors', 'Floors') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $extended->floor_num }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.tower', 'Tower') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $building->number_of_tower }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.year-built', 'Year built') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $extended->year_built }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.developer', 'Developer') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $developer ? $developer->getName() : '' }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.property-id', 'Property ID') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $property->getRefCode() }}
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
            {{ tr('property.listed', 'Listed on') }}
        </div>
        <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
            {{ $property->getListedOn() }}
        </div>
    </div>
    
</div>