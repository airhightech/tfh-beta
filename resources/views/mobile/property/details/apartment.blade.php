@extends('mobile.layouts.default')

<?php
$address = $property->getAddress();
$latlng = $address->getLngLat();

$extended = $property->getExtendedData();
$listing = $property->getListingData();
$building = $property->getBuildingData();

$poster = $property->getPublisherProfile();
$developer = $building->getDeveloper();

$images = $property->getImages();

$currentUser = Auth::user();

$score = $property->getScore();
?>

@section('content')
<link rel="stylesheet" type="text/css" href="/mobile/css/swiper.css">
<link rel="stylesheet" type="text/css" href="/mobile/lib/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/mobile/lib/angularjs-slider/dist/rzslider.min.css">
<script src="https://maps.google.com/maps/api/js?key={{ config('google.browser_key')}}&libraries=places"></script>
<script src="/mobile/lib/angular/angular.js"></script>
<script src="/mobile/lib/angular-animate/angular-animate.js"></script>
<script src="/mobile/lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="/mobile/lib/ngmap/build/scripts/ng-map.js"></script>
<script src="/mobile/lib/lodash/lodash.js"></script>
<script src="/mobile/lib/angularjs-slider/dist/rzslider.js"></script>
<script src="/mobile/lib/chart.js/Chart.bundle.min.js"></script>
<script src="/mobile/lib/angular-chart.js/dist/angular-chart.js"></script>
<script src="/mobile/lib/jquery/dist/jquery.js"></script>

<div ng-app="myApp" ng-controller="myController" class="property-details"
     style="border-top:2px solid lightgray;width:100%;padding:20px 15px 50px;">
    <div style="font-size:16px;font-weight:bold;">{{ $property->getTitle()}}</div>
    <?php
    if ($property->ptype == 'cdb' || $property->new_project === true) {
        ?>
        <p class="ranking">
            {{ tr('property.ranking', 'RANKING')}} : {{ render_simple_rating($score, 5, true)}}
        </p>
    <?php } ?>
    <button id='add-favorite' type="button" class="favorite"><i class="fa fa-heart-o"></i></button>
    <a href="http://www.facebook.com/sharer.php?u=<?php echo url()->current(); ?>" target="_blank">
        <img style="height: 16px; width: 16px;" src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
    </a>
    <?php
      $text = '';
      if($property->new_project == true){
        $text = tr('property.starting-price', 'Starting price');
      }
      elseif ($property->ptype == "cdb") {
        $text = tr('property.starting-price', 'Starting price');
      }
      elseif ($listing->ltype == "rent") {
        $text = tr('property.price-rent', 'Rental');
      }
      elseif ($listing->ltype == "sale") {
        $text = tr('property.sale-price', 'Sale price');
      }
    ?>

<div style="text-align:right;height:20px;font-weight:bold;margin-bottom:5px;margin-top:-25px;">{{$text}} &#3647; {{ number_format($property->getStartingPrice())}}</div>


    <div style="height:150px" class="swiper-container">
        <div class="swiper-wrapper">
              <?php
              foreach ($images as $image) {
              ?>
                <div class="swiper-slide"><img src="{{ $image->getThumbUrl('400x400', true) }}" class="fit-size img-responsive"></div>
                <?php
              }
              ?>
        </div>
    <div class="swiper-pagination"></div>
  </div>

    <div class="agent-tabs" style="margin:15px 0;height:35px;">
        <div class="agent-tab selected" style="line-height:35px;border-width: 1px;font-size:11px;">
            Information
        </div>
        <?php
          if ($property->ptype == 'cdb' || $property->new_project === true) {
            ?>
            <div class="agent-tab" style="line-height:35px;border-width: 1px;font-size:11px;">
                Review
            </div>
            <?php } ?>
    </div>
    <div class="condo-tab-view information">
        <div class="condo-header">
            Details
        </div>
        <?php
        if ($listing->ltype == 'sale') {
            ?>
            @include('mobile.property.details.sale-details.apartment')
            <?php
        } else {
            ?>
            @include('mobile.property.details.rent-details.apartment')
            <?php
        }
        ?>

        <div class="condo-header">
            Key Feature
        </div>

        @include('mobile.property.details.common.key-feature')

        <div class="condo-header">
            Facilities
        </div>

        @include('mobile.property.details.common.facilities')

        <div class="condo-header">
            Details
        </div>
        <div style="padding:10px;">
            {{ $extended->getTranslatable('details')}}
        </div>
        <div class="condo-header">
            Address
        </div>
        <div style="padding:10px;">
            {{ $property->getCustomAddress()}}
        </div>


        @include('mobile.property.details.common.map')

        <?php
        if ($listing->ltype != 'rent') {
            ?>
                    @include('mobile.property.details.common.mortgage')
            <?php
        }
        ?>

    </div>

    @include('mobile.property.details.common.manager-review')

    <?php
    if ($listing->ltype != 'rent' && $listing->ltype != 'sale') {
        ?>
              @include('mobile.property.details.common.comments')
        <?php
    }
    ?>


            <a href="/agent/details/{{$property->publisher_id}}">
            <div class="condo-header">
                  Contact
            </div>
            </a>
</div>


@endsection

@section('scripts')
<script>
    var latlng = <?php echo json_encode($latlng); ?>;
    var pleaseLogIn = "{{ tr('user.pleaselogin', 'Please Log in') }}";
    </script>
<link rel="stylesheet" href="/libs/lightslider/css/lightslider.min.css" />
<script src="/libs/lightslider/js/lightslider.min.js"></script>
<script src="/mobile/condo.js"></script>
<script src="{{ __asset('/mobile/js/details.js') }}"></script>
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
<script src="{{ __asset('/js/visitor/mortgage.js') }}"></script>
<script>
    $(function() {
    $(".agent-tab").click(function() {
    if (!$(this).hasClass("selected")) {
    $(".agent-tab").removeClass("selected");
    $(this).addClass("selected");
    $(".condo-tab-view").css("display", "none");
    $("." + $(this).text().toLowerCase().trim()).css("display", "block");
    }
    });
    $(".condo-comments").click(function() {
    if ($(this).hasClass("selected")) {
    $(".condo-comments-view").css("display", "none");
    $(this).removeClass("selected");
    $(".condo-comments > div").removeClass("glyphicon-chevron-up");
    $(".condo-comments > div").addClass("glyphicon-chevron-down");
    } else {
    $(".condo-comments-view").css("display", "block");
    $(this).addClass("selected");
    $(".condo-comments > div").removeClass("glyphicon-chevron-down");
    $(".condo-comments > div").addClass("glyphicon-chevron-up");
    }
    });
    });
    var idProperty = <?php echo $property->id; ?>;
    </script>
@endsection
