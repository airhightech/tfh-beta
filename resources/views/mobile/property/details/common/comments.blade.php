<div class="condo-header condo-comments">
    Comments<div style="float:right;margin-right: 10px; margin-top:6px;font-size: 12px;" class="glyphicon glyphicon-chevron-down"></div>
</div>
<div class="condo-comments-view">
    <div style="width:100%;padding:10px;">
      <form id="formReview" action="/property/add-review" enctype="multipart/form-data" method="post">
        <div style="background-color:#F6FDFF;">
            <div style="">
                <div style="text-transform:uppercase;font-weight:bold;font-size:11px;margin-bottom:10px;">
                    Your Comment About This Condominium
                </div>
                <textarea id="review" name="review" style="width:100%;height:100px;margin-bottom:20px;resize:none;" ng-model="reply.review"></textarea>
                <div style="text-transform:uppercase;font-weight:bold;font-size:11px;margin-bottom:10px;">
                    Give Your Score Review
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div style="margin-bottom:5px;font-size:10px;">Condo Facilities</div>
                        <div style="margin-bottom:5px;height:35px;">
                            <fieldset id="facilities" class="rating" >
                              <input type="radio" value="5" id="starCondo5" name="rating[facilities]" /><label class = "full" for="starCondo5"></label>
                              <input type="radio" value="4" id="starCondo4" name="rating[facilities]" /><label class="full" for="starCondo4"></label>
                              <input type="radio" value="3" id="starCondo3" name="rating[facilities]" /><label class = "full" for="starCondo3"></label>
                              <input type="radio" value="2" id="starCondo2" name="rating[facilities]" /><label class="full" for="starCondo2"></label>
                              <input type="radio" value="1" id="starCondo1" name="rating[facilities]" /><label class = "full" for="starCondo1"></label>
                            </fieldset>
                        </div>
                        <div style="margin-bottom:5px;font-size:10px;">Room Design</div>
                        <div style="margin-bottom:5px;height:35px;">
                          <fieldset id="room" class="rating">
                            <input type="radio" value="5" id="starRoom5" name="rating[room]"  /><label class = "full" for="starRoom5"></label>
                            <input type="radio" value="4" id="starRoom4" name="rating[room]" /><label class="full" for="starRoom4"></label>
                            <input type="radio" value="3" id="starRoom3" name="rating[room]" /><label class = "full" for="starRoom3"></label>
                            <input type="radio" value="2" id="starRoom2" name="rating[room]" /><label class="full" for="starRoom2"></label>
                            <input type="radio" value="1" id="starRoom1" name="rating[room]" /><label class = "full" for="starRoom1"></label>
                          </fieldset>
                        </div>
                        <div style="margin-bottom:5px;font-size:10px;">Transport Accessibility</div>
                        <div style="margin-bottom:5px;height:35px;">
                          <fieldset id="transport" class="rating">
                            <input type="radio" value="5" id="starTransport5" name="rating[transport]" /><label class = "full" for="starTransport5"></label>
                            <input type="radio" value="4" id="starTransport4" name="rating[transport]" /><label class="full" for="starTransport4"></label>
                            <input type="radio" value="3" id="starTransport3" name="rating[transport]" /><label class = "full" for="starTransport3"></label>
                            <input type="radio" value="2" id="starTransport2" name="rating[transport]" /><label class="full" for="starTransport2"></label>
                            <input type="radio" value="1" id="starTransport1" name="rating[transport]" /><label class = "full" for="starTransport1"></label>
                          </fieldset>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div style="margin-bottom:5px;font-size:10px;">Condo Management</div>
                        <div style="margin-bottom:5px;height:35px;">
                          <fieldset id="management" class="rating">
                            <input type="radio" value="5" id="starManag5" name="rating[management]" /><label class = "full" for="starManag5"></label>
                            <input type="radio" value="4" id="starManag4" name="rating[management]" /><label class="full" for="starManag4"></label>
                            <input type="radio" value="3" id="starManag3" name="rating[management]" /><label class = "full" for="starManag3"></label>
                            <input type="radio" value="2" id="starManag2" name="rating[management]" /><label class="full" for="starManag2"></label>
                            <input type="radio" value="1" id="starManag1" name="rating[management]" /><label class = "full" for="starManag1"></label>
                          </fieldset>
                        </div>
                        <div style="margin-bottom:5px;font-size:10px;">Security</div>
                        <div style="margin-bottom:5px;height:35px;">
                          <fieldset id="security" class="rating">
                            <input type="radio" value="5" id="starSecurity5" name="rating[security]" /><label class = "full" for="starSecurity5"></label>
                            <input type="radio" value="4"  id="starSecurity4" name="rating[security]" /><label class="full" for="starSecurity4"></label>
                            <input type="radio" value="3"  id="starSecurity3" name="rating[security]" /><label class = "full" for="starSecurity3"></label>
                            <input type="radio" value="2"  id="starSecurity2" name="rating[security]" /><label class="full" for="starSecurity2"></label>
                            <input type="radio" value="1"  id="starSecurity1" name="rating[security]" /><label class = "full" for="starSecurity1"></label>
                          </fieldset>
                        </div>
                        <div style="margin-bottom:5px;font-size:10px;">Nearby Amenities</div>
                        <div style="margin-bottom:5px;height:35px;">
                          <fieldset id="amenities" class="rating">
                            <input type="radio" value="5" id="starAmenitie5" name="rating[amenities]" /><label class = "full" for="starAmenitie5"></label>
                            <input type="radio" value="4" id="starAmenitie4" name="rating[amenities]" /><label class="full" for="starAmenitie4"></label>
                            <input type="radio" value="3" id="starAmenitie3" name="rating[amenities]" /><label class = "full" for="starAmenitie3"></label>
                            <input type="radio" value="2" id="starAmenitie2" name="rating[amenities]" /><label class="full" for="starAmenitie2"></label>
                            <input type="radio" value="1" id="starAmenitie1" name="rating[amenities]" /><label class = "full" for="starAmenitie1"></label>
                          </fieldset>
                        </div>
                    </div>
                </div>
                    <div style="display:inline-block;font-size:20px;vertical-align:middle;" class="glyphicon glyphicon-upload"></div>
                    <label style="border:none;display:inline-block;color: #666e71;width:85px;background-color:inherit;font-size:10px;margin-top:5px;" id="filePhoto1" for="review_photo_1">Upload Picture 1</label>
                    <input type="file" name="review_photo_1" id="review_photo_1" style="display:none;" />
                    <div style="display:inline-block;font-size:20px;vertical-align:middle;" class="glyphicon glyphicon-upload"></div>
                    <label style="border:none;display:inline-block;color: #666e71;width:85px;background-color:inherit;font-size:10px;margin-top:5px;" id="filePhoto2" for="review_photo_2">Upload Picture 2</label>
                    <input type="file" name="review_photo_2" id="review_photo_2" style="display:none;" />
                    <div style="display:inline-block;font-size:20px;vertical-align:middle;" class="glyphicon glyphicon-upload"></div>
                    <label style="border:none;display:inline-block;color: #666e71;width:85px;background-color:inherit;font-size:10px;margin-top:5px;" id="filePhoto3" for="review_photo_3">Upload Picture 3</label>
                    <input type="file" name="review_photo_3" id="review_photo_3" style="display:none;" />
                <div class="row">
                    <div class="col-xs-12" style="text-align:center;font-size:16px;margin-bottom:10px;margin-top:10px;color:gray;font-size:10px;">
                        <input type="checkbox" style="margin-right:10px;" ng-model="reply.agree" /><span>I agree with ThaiFullHouse's Terms and Conditions</span>
                    </div>
                    <div class="col-xs-12" style="text-align:center;font-size:16px;margin-bottom:5px;margin-top:10px;color:gray;">
                      <input type="hidden" name="id" value="{{$property->id}}">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="postComment" type="submit" class="btn btn-primary" style="border-radius:0;background-color:#06B4FF;width: 80px;font-size:11px;" value="POST" />
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
<div class="condo-comments-view">
  @foreach ($reviews as $review)
  <?php
  $images = json_decode($review->images);
  ?>
    <div style="width: 100%;padding: 20px 5px 0px 5px;border-top: 1px solid #44B2CC;font-size:10px;">
        <div class="agent-comment">
            <div class="row">
                <div class="col-xs-12">
                    <div class="commenter-email" style="color:#0DB9F0;display:inline-block;">
                        {{$review->name}}
                    </div>
                    <div style="float:right;">Posted on {{$review->created_at}}</div>
                    <div style="margin:10px 0;padding-bottom:5px;">
                        {{$review->review}}
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div style="margin-bottom:5px;font-size:10px;">Condo Facilities</div>
                            <div style="margin-bottom:5px;height:35px;">
                                <fieldset class="rating-passive">
                                {{ render_rating_mobile('facilities', $review->getRatings()) }}
                                </fieldset>
                            </div>
                            <div style="margin-bottom:5px;font-size:10px;">Room Design</div>
                            <div style="margin-bottom:5px;height:35px;">
                                <fieldset class="rating-passive">
                                  {{ render_rating_mobile('room', $review->getRatings()) }}
                                </fieldset>
                            </div>
                            <div style="margin-bottom:5px;font-size:10px;">Transport Accessibility</div>
                            <div style="margin-bottom:5px;height:35px;">
                                <fieldset class="rating-passive">
                                  {{ render_rating_mobile('transport', $review->getRatings()) }}
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div style="margin-bottom:5px;font-size:10px;">Condo Management</div>
                            <div style="margin-bottom:5px;height:35px;">
                                <fieldset class="rating-passive">
                                {{ render_rating_mobile('management', $review->getRatings()) }}
                                </fieldset>
                            </div>
                            <div style="margin-bottom:5px;font-size:10px;">Security</div>
                            <div style="margin-bottom:5px;height:35px;">
                                <fieldset class="rating-passive">
                                {{ render_rating_mobile('security', $review->getRatings()) }}
                                </fieldset>
                            </div>
                            <div style="margin-bottom:5px;font-size:10px;">Nearby Amenities</div>
                            <div style="margin-bottom:5px;height:35px;">
                              <fieldset class="rating-passive">
                                {{ render_rating_mobile('amenities', $review->getRatings()) }}
                              </fieldset>
                            </div>
                        </div>
                    </div>
                    <div style="height:80px;width:100%;border-bottom: 1px solid #44B2CC;margin-bottom:20px;padding-bottom:10px;">
                      <?php
                      foreach($images as $image) {
                      ?>
                          <div style="float: left; width: calc((100% - 40px) / 3); height: 100%; background-image: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url(/image/{{$image}}); background-position: center center; background-size: cover; margin-right: 10px;"></div>
                      <?php
                      }
                      ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
