<?php
if ($listing->ltype == 'sale' || $property->ptype = 'cdb' || $property->new_project) {
    ?>
<div class="condo-header">
                Mortgage Payment Calculator
            </div>
    <div class="row mortgage-calculator">

        <?php
        $price = $property->getSalePrice();

        $log = round(log10($price));

        $loan = pow(10, $log);
        ?>
        <div class="col-sm-12">
            <p>
                <strong>{{ tr('property_mortgage.main-price', 'Principal price') }}</strong> 
                <span class="pull-right">฿ <?php echo number_format($property->getSalePrice()); ?></span>
            </p>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-sm-4">
                    <label style="margin-top: 9px;">{{ tr('property_mortgage.loan-amount', 'Loan amount') }}</label > 
                </div>
                <div class="col-sm-offset-2 col-sm-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">฿</span>
                        <input type="text" id="loan-value" name="loan" value="<?php echo $loan; ?>" class="form-control" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>        
            <p>
                <strong>{{ tr('property_mortgage.rate', 'Rate (ARPR)') }}</strong> 
                <span id="rate-value" class="pull-right"></span>
            </p>
            <p>
                <span>1%</span><span class="pull-right">10%</span>
            </p>
            <div id="rate-slider" class="flat-slider"></div>
            <p>
                <strong>{{ tr('property_mortgage.term', 'Loan term') }}</strong> 
                <span id="term-value" class="pull-right"></span>
            </p>
            <p>
                <span>1</span><span class="pull-right">30 {{ tr('property_mortgage.years', 'years') }}</span>
            </p>
            <div id="term-slider" class="flat-slider"></div>

        </div>
        <div class="col-sm-12" style="position: relative; margin-top: 15px;">
            <canvas id="mortgage-chart" width="400" height="400"></canvas>
            <div class="monthly-payment">
            <div class="content">
                <span>{{ tr('property_mortgage.monthly-payment', 'Your monthly payment') }}</span><br/>
                <strong id="monthly"></strong>
            </div>        
        </div>
        </div>
        

    </div>
    <script>
    <?php
    $price = $property->getSalePrice();

    if ($price > 0) {
        ?>
            var mainPrice = <?php echo $price; ?>;
        <?php
    } else {
        ?>
            var mainPrice = 0;
        <?php
    }
    ?>
    </script>
    <?php
}
?>

