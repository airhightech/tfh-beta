<div class="facility-line">

    <?php
    if (count($features)) {
        foreach ($features as $feature) {
            if (in_array($feature->id, $selected_features)) {
                ?>
                <div class="facility" ng-if="condo.facilities.gym">
                    <img src="{{ $feature->getThumbUrl('100x100', true, '') }}"></img>
                    <p>{{ $feature->getTranslatable('name') }}</p>
                </div>
                <?php
            }
        }
    }
    ?>
</div>
