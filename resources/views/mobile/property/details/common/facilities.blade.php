<div class="facility-line">

    <?php
    if (count($facilities)) {
        foreach ($facilities as $facility) {
            if (in_array($facility->id, $selected_facilities)) {
                ?>
                <div class="facility" ng-if="condo.facilities.gym">
                    <img src="{{ $facility->getThumbUrl('100x100', true, '') }}"></img>
                    <p>{{ $facility->getTranslatable('name') }}</p>
                </div>
                <?php
            }
        }
    }
    ?>
</div>
