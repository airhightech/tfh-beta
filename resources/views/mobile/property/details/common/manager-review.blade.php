<?php
$review = $property->getManagerReview();

$location_images = $review->getLocationImages();
$floorplan_images = $review->getFloorplanImages();
$facility_images = $review->getFacilityImages();

$ylink = $review->getYoutubeVideo();
?>

<div class="condo-tab-view {{ tr('property.review', 'Review') }}" style="display:none">
    <div class="condo-header">
        How To Get There
    </div>
    <div style="margin-top:5px;margin-bottom:10px;height:120px;">
        <?php
        if (count($location_images)) {
            ?>
            <img src="/image/{{ $location_images[0]->filepath }}" class="img-responsive"/>
            <?php
        }
        ?>
    </div>
    <div class="details">{{ $review->getTranslatable('comment_loc') }}</div>
    <div class="condo-header">
        Facilities &amp; Landscape
    </div>
    <div style="margin-top:5px;margin-bottom:10px;height:120px;">
        <?php
        if (count($facility_images)) {
            ?>
            <img src="/image/{{ $facility_images[0]->filepath }}" class="img-responsive"/>
            <?php
        }
        ?>
    </div>
    <div class="details">{{ $review->getTranslatable('comment_fac') }}</div>
    <div class="condo-header">
        Floor Plans
    </div>
    <div style="margin-top:5px;margin-bottom:10px;height:120px;">
        <?php
        if (count($floorplan_images)) {
            ?>
            <img src="/image/{{ $floorplan_images[0]->filepath }}" class="img-responsive"/>
            <?php
        }
        ?>
    </div>
    <div class="details">{{ $review->getTranslatable('comment_pla') }}</div>
    <div class="condo-header">
        Project Video
    </div>
    <div class="video-container">
        <?php
        if ($ylink) {
            ?>
            <iframe src="https://www.youtube.com/embed/{{ $ylink }}?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            <?php
        }
        ?>
    </div>
    <div class="condo-header">
        Overview
    </div>
    <div class="details" style="margin-top: 5px;">{{ $review->getTranslatable('overview') }}</div>
</div>
