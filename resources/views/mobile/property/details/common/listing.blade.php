<?php
$properties = $property->getListedProperties();
$count = $property->countListedProperties();
?>
<div class="condo-header condo-listing-rent">
    {{ tr('property.for-rent-property', 'For rent') }} (<?php echo array_get($count, 'rent', 0); ?>)<div style="float:right;margin-right: 10px; margin-top:6px;font-size: 12px;" class="glyphicon glyphicon-chevron-down"></div>
</div>
<div class="condo-listing-rent-view">
    <div style="width:100%;padding:10px;">
      <div class="property-list">
          <?php
          foreach ($properties as $_property) {
            $_listing = $_property->getListingData();
            $_extended = $_property->getExtendedData();
            $img = $_property->getMainImage();
              if($_listing->ltype == "rent"){
              ?>
              <div class="row listing-area">
                <div class="col-xs-3">
                  <?php
                  if ($img) {
                      ?>
                      <img src="{{ $img->getThumbUrl('300x300', true) }}" class="img-responsive"/>
                      <?php
                  } else {
                      ?>
                      <img src="http://placehold.it/300x300?text=No+image" class="img-responsive"/>
                      <?php
                  }
                  ?>
                </div>
                <div class="col-xs-8">
                  <a href="/property/{{ $_property->id }}">
                      <span class="name"><?php echo $_property->getTranslatable('name'); ?></span>
                      <span class="address"><?php echo $_property->getCustomAddress(); ?></span>
                      <span class="price">
                          &#3647; <?php echo number_format($_listing->listing_price); ?>
                          <?php echo $_listing->ltype == 'rent' ? ' / month' : ''; ?>
                      </span>
                  </a>
                </div>
            </div>
              <?php
            }
          }
          ?>
      </div>
    </div>
</div>


<div class="condo-header condo-listing-sale">
    {{ tr('property.for-sale-property', 'For sale') }} (<?php echo array_get($count, 'sale', 0); ?>)<div style="float:right;margin-right: 10px; margin-top:6px;font-size: 12px;" class="glyphicon glyphicon-chevron-down"></div>
</div>
<div class="condo-listing-sale-view">
    <div style="width:100%;padding:10px;">
      <div class="property-list">
          <?php
          foreach ($properties as $_property) {
            $_listing = $_property->getListingData();
            $_extended = $_property->getExtendedData();
            $img = $_property->getMainImage();
              if($_listing->ltype == "sale"){
              ?>
              <div class="row listing-area">
                <div class="col-xs-3">
                  <?php
                  if ($img) {
                      ?>
                      <img src="{{ $img->getThumbUrl('300x300', true) }}" class="img-responsive"/>
                      <?php
                  } else {
                      ?>
                      <img src="http://placehold.it/300x300?text=No+image" class="img-responsive"/>
                      <?php
                  }
                  ?>
                </div>
                <div class="col-xs-8">
                  <a href="/property/{{ $_property->id }}">
                      <span class="name"><?php echo $_property->getTranslatable('name'); ?></span>
                      <span class="address"><?php echo $_property->getCustomAddress(); ?></span>
                      <span class="price">
                          &#3647; <?php echo number_format($_listing->listing_price); ?>
                          <?php echo $_listing->ltype == 'rent' ? ' / month' : ''; ?>
                      </span>
                  </a>
                </div>
            </div>
              <?php
            }
          }
          ?>
      </div>
    </div>
</div>
