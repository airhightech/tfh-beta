<div class="condo-header">
    Map
</div>
<div id="property-map" class="map-container">

</div>


<?php
$list = $property->getNearbyPlaces();

$places = [];

foreach ($list as $place) {
    $places[$place->type][] = $place;
}

if (count($places)) {
    ?>
    <div class="nearby-places">
        <?php
        foreach ($places as $type => $_list) {
            ?>
            <div style="display:block;margin:10px 0;height:40px;">
                <div style="display: inline-block;float:left;width:130px;height:40px;text-transform:capitalize;background-color:#6BA9B6;color:white;text-align: center;vertical-align: middle;line-height:40px;font-size: 11px;">

                    <?php
                    switch ($type) {
                        case 'bts':
                        case 'mrt':
                        case 'apl':
                            ?>
                            <i class="fa fa-bus"></i>
                            <?php
                            break;
                        case 'bank':
                            ?>
                            <i class="fa fa-bank"></i>
                            <?php
                            break;
                        case 'dpt-store':
                            ?>
                            <i class="fa fa-shopping-cart"></i>
                            <?php
                            break;
                        case 'hospital':
                            ?>
                            <i class="fa fa-h-square"></i>
                            <?php
                            break;
                        case 'school':
                            ?>
                            <i class="fa fa-graduation-cap"></i>
                            <?php
                            break;
                        default:
                            ?>
                            <i class="fa fa-map-pin"></i>
                            <?php
                            break;
                    }
                    ?>
                    (Total {{ count($_list)}})
                </div>

            <select class='poi-list' style="display: inline-block;float:left;width:calc(100% - 130px);height:40px;padding-left:20px;font-size: 11px;" ng-model="selectedItem[$index]" ng-change="goToLocation(selectedItem[$index])">
                <option value="0">{{ tr('property.select-poi', 'Select a POI')}}</option>
                <?php
                foreach ($_list as $place) {
                    $names = json_decode($place->name, true);
                    ?>
                    <option value="{{ $place->id}}"
                            data-lat="{{ $place->lat}}"
                            data-lng="{{ $place->lng}}"
                            data-type="{{ $place->type }}">{{ trget($names) }} ({{ __distance(round($place->distance)) }}m)</option>
                            <?php
                        }
                        ?>
            </select>
                </div>
        <?php
    }
    ?>
    </div>
    <?php
}
