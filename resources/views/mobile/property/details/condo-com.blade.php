@extends('mobile.layouts.default')

<?php
$address = $property->getAddress();
$latlng = $address->getLngLat();

$extended = $property->getExtendedData();
$listing = $property->getListingData();
$building = $property->getBuildingData();

$poster = $property->getPublisherProfile();
$developer = $building->getDeveloper();

$images = $property->getImages();

$currentUser = Auth::user();

$score = $property->getScore();
?>

@section('content')
<link rel="stylesheet" type="text/css" href="/mobile/css/swiper.css">
<link rel="stylesheet" type="text/css" href="/mobile/lib/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/mobile/lib/angularjs-slider/dist/rzslider.min.css">
<script src="https://maps.google.com/maps/api/js?key={{ config('google.browser_key')}}&libraries=places"></script>
<script src="/mobile/lib/angular/angular.js"></script>
<script src="/mobile/lib/angular-animate/angular-animate.js"></script>
<script src="/mobile/lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="/mobile/lib/ngmap/build/scripts/ng-map.js"></script>
<script src="/mobile/lib/lodash/lodash.js"></script>
<script src="/mobile/lib/angularjs-slider/dist/rzslider.js"></script>
<script src="/mobile/lib/chart.js/Chart.bundle.min.js"></script>
<script src="/mobile/lib/angular-chart.js/dist/angular-chart.js"></script>
<script src="/mobile/lib/jquery/dist/jquery.js"></script>

<div ng-app="myApp" ng-controller="myController" class="property-details"
     style="border-top:2px solid lightgray;width:100%;padding:20px 15px 50px;">
    <div style="font-size:16px;font-weight:bold;">{{ $property->getTitle()}}</div>
    <?php
    if ($property->ptype == 'cdb' || $property->new_project === true) {
        ?>
        <p class="ranking">
            {{ tr('property.ranking', 'RANKING')}} : {{ render_simple_rating($score, 5, true)}}
        </p>
    <?php } ?>
    <button id='add-favorite' type="button" class="favorite"><i class="fa fa-heart-o"></i></button>
    <a href="http://www.facebook.com/sharer.php?u=<?php echo url()->current(); ?>" target="_blank">
        <img style="height: 16px; width: 16px;" src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
    </a>
    <?php
      $text = '';
      if($property->new_project == true){
        $text = tr('property.starting-price', 'Starting price');
      }
      elseif ($property->ptype == "cdb") {
        $text = tr('property.starting-price', 'Starting price');
      }
      elseif ($listing->ltype == "rent") {
        $text = tr('property.price-rent', 'Rental');
      }
      elseif ($listing->ltype == "sale") {
        $text = tr('property.sale-price', 'Sale price');
      }
    ?>

<div style="text-align:right;height:20px;font-weight:bold;margin-bottom:5px;margin-top:-25px;">{{$text}} &#3647; {{ number_format($property->getStartingPrice())}}</div>


    <div style="height:150px" class="swiper-container">
        <div class="swiper-wrapper">
              <?php
              foreach ($images as $image) {
              ?>
                <div class="swiper-slide"><img src="{{ $image->getThumbUrl('400x400', true) }}" class="fit-size img-responsive"></div>
                <?php
              }
              ?>
        </div>
    <div class="swiper-pagination"></div>
  </div>

    <div class="agent-tabs" style="margin:15px 0;height:35px;">
        <div class="agent-tab selected" style="line-height:35px;border-width: 1px;font-size:11px;">
            {{ tr('property.information', 'Information') }}
        </div>
        <?php
          if ($property->ptype == 'cdb' || $property->new_project === true) {
            ?>
            <div class="agent-tab" style="line-height:35px;border-width: 1px;font-size:11px;">
                {{ tr('property.review', 'Review') }}
            </div>
            <?php } ?>
    </div>
    <div class="condo-tab-view {{ tr('property.information', 'Information') }}">
        <div class="listing-highlights">
            <div class="listing-highlight">
                <img src="/mobile/img/money.png"></img>
                <br/>
                <br/>
                <p>{{tr('property.starting-price', 'Sales Price Start From')}}</p>
                <h4>{{ number_format($property->getStartingPrice())}}</h4>
            </div>
            <div class="listing-highlight">
                <img src="/mobile/img/circular-clock.png"></img>
                <br/>
                <br/>
                <p>{{tr('property.office-opening-hours', 'Office Operating Hours')}}</p>
                <h4><?php echo $extended->office_hours; ?></h4>
            </div>
            <div class="listing-highlight">
                <img src="/mobile/img/crane.png"></img>
                <br/>
                <br/>
                <p>{{tr('property.year-built', 'Year Built')}}</p>
                <h4><?php echo $extended->year_built; ?></h4>
            </div>
            <div class="listing-highlight">
                <img src="/mobile/img/building-3.png"></img>
                <br/>
                <br/>
                <p>{{tr('property.tower', 'Tower')}}</p>
                <h4><?php echo $building->number_of_tower; ?></h4>
            </div>
            <div class="listing-highlight">
                <img src="/mobile/img/lift.png"></img>
                <br/>
                <br/>
                <p>{{tr('property.floors', 'Floors')}}</p>
                <h4><?php echo $extended->total_floors; ?></h4>
            </div>
        </div>
        <div class="condo-header">
            {{ tr('property.details', 'Details') }}
        </div>
        <div style="border:2px solid #0d8ea3;font-size: 10px;margin-bottom:15px;">
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:10px 0 3px 25px;">
                    {{ tr('property.total-units', 'Total units') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:10px 0 3px 15px;">
                    {{ $building->total_units}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:3px 0 3px 25px;">
                    {{ tr('property.room-type', 'Room type') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:3px 0 3px 15px;">
<?php
$room_types = $property->getRoomTypes();
if (count($room_types)) {
    foreach ($room_types as $room_type) {
      ?>
      <p>
          <?php echo $room_type->name; ?> : <?php echo $room_type->size; ?> sqm
      </p>
      <?php
    }
}
?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:3px 0 3px 25px;">
                    {{ tr('property.parking-space', 'Parking space') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:3px 0 3px 15px;">
                    {{ $extended->parking_lot}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:3px 0 3px 25px;">
                    {{ tr('property.website', 'Website') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:3px 0 3px 15px;">
                    {{ $extended->website}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:3px 0 3px 25px;">
                    {{ tr('property.total-floors', 'Total floors') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:3px 0 3px 15px;">
                    {{ $extended->total_floors}}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:3px 0 3px 25px;">
                    {{ tr('property.district', 'District') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:3px 0 3px 15px;">
<?php
if ($address) {
    echo $address->getDistrictName();
}
?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="font-weight:bold;padding:3px 0 10px 25px;">
                    {{ tr('property.contact', 'Contact') }}
                </div>
                <div class="col-xs-7" style="border-left:2px solid #0d8ea3;padding:3px 0 10px 15px;">
                    {{ $extended->contact_person}}
                </div>
            </div>
        </div>
            @include('mobile.property.details.common.listing')

        <div class="condo-header">
            {{ tr('property.facilities', 'Facilities') }}
        </div>

        @include('mobile.property.details.common.facilities')

        <div class="condo-header">
            {{ tr('property.details', 'Details') }}
        </div>
        <div style="padding:10px;">
            {{ $extended->getTranslatable('details')}}
        </div>
        <div class="condo-header">
            {{ tr('property.address', 'Address') }}
        </div>
        <div style="padding:10px;">
            {{ $property->getCustomAddress()}}
        </div>


        @include('mobile.property.details.common.map')

        <?php
        if ($listing->ltype != 'rent') {
            ?>
                    @include('mobile.property.details.common.mortgage')
            <?php
        }
        ?>

    </div>

    @include('mobile.property.details.common.manager-review')

    <?php
    if ($listing->ltype != 'rent' && $listing->ltype != 'sale') {
        ?>
              @include('mobile.property.details.common.comments')
        <?php
    }
    ?>

</div>


@endsection

@section('scripts')
<script>
    var latlng = <?php echo json_encode($latlng); ?>;
    var pleaseLogIn = "{{ tr('user.pleaselogin', 'Please Log in') }}";
</script>
<link rel="stylesheet" href="/libs/lightslider/css/lightslider.min.css" />
<script src="/libs/lightslider/js/lightslider.min.js"></script>
<script src="/mobile/condo.js"></script>
<script src="{{ __asset('/mobile/js/details.js') }}"></script>
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.1/js/swiper.min.js"></script>
<script src="{{ __asset('/js/visitor/mortgage.js') }}"></script>
<script>
    $(function() {
    $(".agent-tab").click(function() {
      if (!$(this).hasClass("selected")) {
        $(".agent-tab").removeClass("selected");
        $(this).addClass("selected");
        $(".condo-tab-view").css("display", "none");
        $("." + $(this).text().trim()).css("display", "block");
      }
    });
    $(".condo-comments").click(function() {
    if ($(this).hasClass("selected")) {
    $(".condo-comments-view").css("display", "none");
    $(this).removeClass("selected");
    $(".condo-comments > div").removeClass("glyphicon-chevron-up");
    $(".condo-comments > div").addClass("glyphicon-chevron-down");
    } else {
    $(".condo-comments-view").css("display", "block");
    $(this).addClass("selected");
    $(".condo-comments > div").removeClass("glyphicon-chevron-down");
    $(".condo-comments > div").addClass("glyphicon-chevron-up");
    }
    });

    $(".condo-listing-rent").click(function() {
      if ($(this).hasClass("selected")) {
        $(".condo-listing-rent-view").css("display", "none");
        $(this).removeClass("selected");
        $(".condo-listing-rent > div").removeClass("glyphicon-chevron-up");
        $(".condo-listing-rent > div").addClass("glyphicon-chevron-down");
      }
      else {
        $(".condo-listing-rent-view").css("display", "block");
        $(this).addClass("selected");
        $(".condo-listing-rent > div").removeClass("glyphicon-chevron-down");
        $(".condo-listing-rent > div").addClass("glyphicon-chevron-up");
        }
    });

    $(".condo-listing-sale").click(function() {
      if ($(this).hasClass("selected")) {
        $(".condo-listing-sale-view").css("display", "none");
        $(this).removeClass("selected");
        $(".condo-listing-sale > div").removeClass("glyphicon-chevron-up");
        $(".condo-listing-sale > div").addClass("glyphicon-chevron-down");
      }
      else {
        $(".condo-listing-sale-view").css("display", "block");
        $(this).addClass("selected");
        $(".condo-listing-sale > div").removeClass("glyphicon-chevron-down");
        $(".condo-listing-sale > div").addClass("glyphicon-chevron-up");
        }
    });

    });
    var idProperty = <?php echo $property->id; ?>;
    </script>
@endsection
