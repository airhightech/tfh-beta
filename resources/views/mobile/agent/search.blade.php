@extends('mobile.layouts.agent')

@section('content')
<div class="main-menu">
    <form action="/agent/search" method="get" style="margin-bottom: 6px;">
        <div class="input-group">
            <input id="q" type="text" class="form-control" name="nam" value="{{ array_get($search, 'nam')}}" placeholder="Enter a location, a freelance agent or a real estate agency name">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </form>
    <div class="options">
        <div class="row">
            <div class="col-xs-6 menu-item selected" id="freelance">{{ tr('agent.freelance', 'Freelance') }}</div>
            <div class="col-xs-6 menu-item" id="realestate">{{ tr('agent.realestate', 'Real Estate') }}</div>
        </div>
    </div>
</div>

<div id="freelancepage" style="width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);padding:10px 10px;background-color:#DDEDED;overflow-y:auto;">
    <?php
    foreach ($agents as $agent) {
        $profile = $agent->getProfile();
        $score = $profile->getRating();
        if($agent->ctype == 'agent'){
        ?>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:10px;">
            <a href="/agent/details/<?php echo $agent->id; ?>">
                <div class="row" style="height:100%;padding:10px 8px;">
                    <div class="col-xs-3" style="height:100%;padding-right:0;">
                        <div style="width: 100%;height:75px;background-size:cover;background-position:center;">
                            <img src="<?php echo $profile->getProfilePicture(); ?>" class="img-responsive"/>
                        </div>

                        <?php
                        if ($agent->agent_verified) {
                            ?>
                            <div class="agent-search-verified">
                                <div class="glyphicon glyphicon-ok"></div>{{ tr('agent.verified', 'Verified') }} 
                            </div>
                            <?php
                        }
                        ?>

                    </div>
                    <div class="col-xs-9">
                        <div style="color:#085978;font-weight:bold;font-size:13px;"><?php echo $profile->getName(); ?></div>
                        <div style="color:gray;font-size:9px;">
                            <?php
                            $postings = $agent->countProperty(true);
                            if ($postings > 1) {
                                echo '(', $postings, ' postings)';
                            } else if ($postings == 1) {
                                echo '(1 posting)';
                            } else {
                                echo '(No posting)';
                            }
                            ?>
                        </div>
                        <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div>
                            <div style="display:inline-block;font-size:9px;"> <?php echo $profile->getTags(); ?></div></div>
                        <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div>
                            <div style="display:inline-block;font-size:9px;"> <?php echo $agent->countReview(); ?> reviews</div></div>
                        <div style="font-size:9px;"><?php echo $profile->introduction; ?></div>
                        <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                        <div style="position:absolute;top:0px;right:15px;">
                            {{ render_simple_rating($score, 5, true) }}
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <?php
        }
    }
    ?>

    <div class="agent-list-pagination">
        <?php echo $agents->appends($search)->links(); ?>
    </div>
</div>

<div id="realestatepage" style="width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);padding:10px 10px;background-color:#DDEDED;overflow-y:auto;">
  <?php
  foreach ($agents as $agent) {
      $profile = $agent->getProfile();
      $score = $profile->getRating();
      if($agent->ctype == 'company'){
      ?>
      <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:10px;">
          <a href="/agent/details/<?php echo $agent->id; ?>">
              <div class="row" style="height:100%;padding:10px 8px;">
                  <div class="col-xs-3" style="height:100%;padding-right:0;">
                      <div style="width: 100%;height:75px;background-size:cover;background-position:center;">
                          <img src="<?php echo $profile->getProfilePicture(); ?>" class="img-responsive"/>
                      </div>

                      <?php
                      if ($agent->agent_verified) {
                          ?>
                          <div class="agent-search-verified">
                              <div class="glyphicon glyphicon-ok"></div>Verified
                          </div>
                          <?php
                      }
                      ?>


                  </div>
                  <div class="col-xs-9">
                      <div style="color:#085978;font-weight:bold;font-size:13px;"><?php echo $profile->getName(); ?></div>
                      <div style="color:gray;font-size:9px;">
                          <?php
                          $postings = $agent->countProperty(true);
                          if ($postings > 1) {
                              echo '(', $postings, ' postings)';
                          } else if ($postings == 1) {
                              echo '(1 posting)';
                          } else {
                              echo '(No posting)';
                          }
                          ?>
                      </div>
                      <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div>
                          <div style="display:inline-block;font-size:9px;"> <?php echo $profile->getTags(); ?></div></div>
                      <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div>
                          <div style="display:inline-block;font-size:9px;"> <?php echo $agent->countReview(); ?> reviews</div></div>
                      <div style="font-size:9px;"><?php echo $profile->introduction; ?></div>
                      <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                      <div style="position:absolute;top:0px;right:15px;">
                          {{ render_simple_rating($score, 5, true) }}
                      </div>
                  </div>
              </div>
          </a>
      </div>
      <?php
      }
  }
  ?>

  <div class="agent-list-pagination">
      <?php echo $agents->appends($search)->links(); ?>
  </div>
</div>

<div class="bottom-navigation">
    <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
    <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
</div>
@endsection
@section('scripts')
<script>
    $(function () {
        $("#freelance").click(function () {
            if (!$(this).hasClass("selected")) {
                $(this).addClass("selected");
                $("#realestate").removeClass("selected");
                $("#freelancepage").css("display", "block");
                $("#realestatepage").css("display", "none");
            }
        });
        $("#realestate").click(function () {
            if (!$(this).hasClass("selected")) {
                $(this).addClass("selected");
                $("#freelance").removeClass("selected");
                $("#freelancepage").css("display", "none");
                $("#realestatepage").css("display", "block");
            }
        });
    });
</script>
@endsection
