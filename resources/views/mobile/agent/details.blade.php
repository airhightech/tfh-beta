@extends('mobile.layouts.agent')

<?php
$profile = $user->getProfile();
$personalData = $user->getPersonalData();
$companyData = $user->getCompanyData();
$countProperty = $user->countProperty(true);
$score = $profile->getRating();
?>


@section('content')

    <div ng-app="myApp" ng-controller="myController">
        <div class="agent-header">
            <div class="agent-logo-container">
                <img id="cover-img" src="{{ $profile->getCoverPicture() }}" class="img-responsive"/>
                <div class="agent-circle">
                    <img id="profile-img" src="{{ $profile->getProfilePicture() }}" class="img-responsive"/>
                </div>
            </div>
            <div class="agent-title">
                <div>
                    <div class="agent-badge glyphicon glyphicon-user"></div>
                    <span class="agent-name">{{ $profile->getName() }}</span>
                </div>
                <div style="margin-bottom:10px;">
                    <div ng-if="true">
                        <div class="agent-search-verified" style="float:left;margin-left: calc(50% - 70px);margin-top:0;margin-right:25px;">
                            <div class="glyphicon glyphicon-ok"></div>Verified
                        </div>
                    </div>
                    <div class="agent-posting">
                        <?php
                if ($countProperty > 0) {
                    ?>
                    ({{ tr('member_profile.profile-count', 'Posting') }} {{ $countProperty }})
                    <?php
                }
                ?>
                    </div>
                </div>
                <div style="margin-top:20px;width:100px;height:1px;margin-left:120px;font-size:16px;color:gold;text-align:center;">
                    {{ render_simple_rating($score) }}
                </div>
            </div>
            <div class="agent-tabs">
                <div class="agent-tab selected" data-ref="#about-tab">
                   {{ tr('agent.about', 'About') }}
                </div>
                <div class="agent-tab middle" data-ref="#items-tab">
                    {{ tr('agent.items', 'Items') }}
                </div>
                <div class="agent-tab" data-ref="#reviews-tab">
                   {{ tr('agent.review', 'Review') }}
                </div>
            </div>
            <div id="about-tab" class="agent-tab-view about">
                <?php
                    if ($user->ctype == 'agent') {
                        ?>
                        @include('mobile.agent.details.agent')
                        <?php
                    } else if($user->ctype == 'company'){
                        ?>
                        @include('mobile.agent.details.company')
                        <?php
                    } else if($user->ctype == 'developer') {
                        ?>
                        @include('mobile.agent.details.developer')
                        <?php
                    }
                ?>
            </div>
            <div id="items-tab" class="agent-tab-view items">
                @include('mobile.agent.details.items')
            </div>
            <div id="reviews-tab" class="agent-tab-view review">
                @include('mobile.agent.details.reviews')
            </div>
            <div style="width:100%;padding: 0 20px;">
                <div class="agent-contact">
                    <p>{{ tr('property.contact', 'Contact') }}</p>
                    <form id="form-message" name="form">
                        <label for="name">{{ tr('message.user-name', 'Name') }}</label><input name="sender_name" type="text" placeholder="{{ tr('message.user-name', 'Name') }}" ng-model="contact.name" />
                        <label for="tel">{{ tr('message.user-tel', 'Your phone number') }}</label><input name="sender_phone" type="tel" placeholder="{{ tr('message.user-tel', 'Your phone number') }}" ng-model="contact.phone" />
                        <label for="email">{{ tr('message.user-email', 'E-mail') }}</label><input name="sender_email" type="email" placeholder="{{ tr('message.user-email', 'E-mail') }}" ng-model="contact.email" />
                        <label for="message">{{ tr('property.message', 'Message') }}</label><textarea name="message" placeholder="{{ tr('property.message', 'Message') }}" ng-model="contact.message"></textarea>
                        <input class="news" name="subscribe" type="checkbox" ng-model="contact.newsletter" /><label class="news" for="news">{{ tr('property.send-newsletter', 'You want to receive our newsletter') }}</label>
                        <input type="hidden" name="agent_id" value="{{ $user->id }}">
                        <button id="send-message" type="button" class="submit btn btn-primary" name="submit" ng-click="submitMessage()"><i class="glyphicon glyphicon-ok"></i>{{ tr('agent.submit', 'Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-navigation">
        <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
        <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
    </div>

@endsection
<?php
$labels = [
    tr('property.apartment', 'Apartment'),
    tr('property.condo', 'Condominium'),
    tr('property.single-house', 'Single house'),
    tr('property.townhouse', 'Townhouse')
];
$groupedPropertyCount = $user->countGroupedProperty(true);
$count = array_merge(['ap' => 0, 'cd' => 0, 'sh' => 0, 'th' => 0], $groupedPropertyCount);
?>

@section('scripts')
<link rel="stylesheet" href="/libs/rating/min/rating.css" />
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script>
    var labels = <?php echo json_encode($labels); ?>;
    var propertyData = <?php echo json_encode(array_values($count)); ?>;
    var agentId = {{ $user->id }};
    $(function() {
        $(".agent-tab").click(function() {
            if(!$(this).hasClass("selected")) {
                $(".agent-tab").removeClass("selected");
                $(this).addClass("selected");
                $(".agent-tab-view").css("display", "none");
                $("." + $(this).text().toLowerCase().trim()).css("display", "block");
            }
        });
        $(".list-button").click(function() {
            if($(this).hasClass("selected")) {
                $(this).removeClass("selected");
            } else {
                $(this).addClass("selected");
            }
        });
    });
</script>
<script src="/mobile/js/agent.js"></script>
@endsection
