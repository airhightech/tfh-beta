<div class="row">
    <div class="col-xs-5 gray">
        {{ tr('agent.agentname', 'Agent name') }}
    </div>
    <div class="col-xs-7 black">
        {{ $profile->getName() }}
    </div>
</div>
<div class="row">
    <div class="col-xs-5 gray">
        {{ tr('agent.agentid', 'Agent ID') }}
    </div>
    <div class="col-xs-7 black">
        {{ $user->getPublicId() }}
    </div>
</div>
<div class="row">
    <div class="col-xs-5 gray">
        {{ tr('agent.email', 'E-mail') }}
    </div>
    <div class="col-xs-7 black">
        {{ $user->email }}
    </div>
</div>
<div class="row">
    <div class="col-xs-5 gray">
        {{ tr('agent.tel', 'Tel') }}
    </div>
    <div class="col-xs-7 black">
        {{ $personalData->phone }}
    </div>
</div>
<div class="row">
    <div class="col-xs-5 gray">
        {{ tr('agent.Website', 'Website') }}
    </div>
    <div class="col-xs-7 black">
        {{ $profile->website }}
    </div>
</div>
<div class="row">
    <div class="col-xs-5 gray">
        {{ tr('agent.language', 'Language ability') }}
    </div>
    <div class="col-xs-7 black">
        {{ $personalData->languages }}
    </div>
</div>
