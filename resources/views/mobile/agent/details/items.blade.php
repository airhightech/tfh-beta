<?php
$stat = array_merge(['ap' => 0, 'cd' => 0, 'th' => 0, 'sh' => 0], $user->countGroupedProperty(true));

$total = $stat['ap'] + $stat['cd'] + $stat['th'] + $stat['sh'];

$lstat = array_merge(['rent' => 0, 'sale' => 0], $user->countListingStats());

$ltotal = $lstat['rent'] + $lstat['sale'];

$stat_sale = 0;
$stat_rent = 0;

if ($ltotal > 0) {
    $stat_sale = 100 * $lstat['sale'] / $ltotal;
    $stat_rent = 100 * $lstat['rent'] / $ltotal;
}
?>

<div class="overview">
    <div class="col-xs-7 left">
        <canvas id="property-canvas" class="chart chart-doughnut" chart-data="data" chart-labels="labels" chart-colors="backgroundColor"></canvas>
        <div style="width:100%;padding:30px 0;font-size:10px;">
            <?php
            if ($user->ctype !== 'developer') {
                ?>
                <div class="rental-stat">
                    <div class="progress">
                        <div class="progress-bar progress-bar-sale" style="width: {{ $stat_sale}}%">
                            <span>{{ round($stat_sale)}}%</span>
                        </div>
                        <div class="progress-bar progress-bar-rent" style="width: {{ $stat_rent}}%">
                            <span>{{ round($stat_rent)}}%</span>
                        </div>
                    </div>
                    <p class="labels">
                        <span>{{ tr('property.sale', 'Sale')}}</span><span class="pull-right">{{ tr('property.rent', 'Rent')}}</span>
                    </p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="col-xs-5 right">
        <div class="onefourth house">
            <div class="col-xs-5" style="height:100%;">
                <img src="/mobile/img/detachedhouse.png" style="height:80%;filter:invert(100%);"></img>
            </div>
            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                <div>{{ tr('manager_listing.filter-property-house', 'Single house') }}</div>
                <div>{{ $stat['sh']}}</div>
            </div>
        </div>
        <div class="onefourth townhouse">
            <div class="col-xs-5" style="height:100%;">
                <img src="/mobile/img/townhouse.png" style="height:80%;filter:invert(100%);"></img>
            </div>
            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                <div>{{ tr('manager_listing.filter-property-townhouse', 'Townhouse') }}</div>
                <div>{{ $stat['th']}}</div>
            </div>
        </div>
        <div class="onefourth apartment">
            <div class="col-xs-5" style="height:100%;">
                <img src="/mobile/img/apartment.png" style="height:80%;filter:invert(100%);"></img>
            </div>
            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                <div>{{ tr('manager_listing.filter-property-apartment', 'Apartment') }}</div>
                <div>{{ $stat['ap']}}</div>
            </div>
        </div>
        <div class="onefourth condo">
            <div class="col-xs-5" style="height:100%;">
                <img src="/mobile/img/condo.png" style="height:80%;filter:invert(100%);"></img>
            </div>
            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                <div>{{ tr('manager_listing.filter-property-condo', 'Condo') }}</div>
                <div>{{ $stat['cd']}}</div>
            </div>
        </div>
    </div>
</div>
<div class="listing-header">
    <div class="list-item">
        <button id="btn-rent" class="list-button selected">Rent</button>
    </div>
    <div class="list-item">
        <button id="btn-sale" class="list-button">Buy</button>
    </div>
    <div id="ptype" class="list-item" style="float:right;margin-right:2px;">
        <select class="list-select">
            <option value="">{{ tr('search.listing.type', 'Listing Type') }}</option>
            <option value="sh">{{ tr('manager_listing.filter-property-house', 'Single house') }}</option>
            <option value="th">{{ tr('manager_listing.filter-property-townhouse', 'Townhouse') }}</option>
            <option value="ap">{{ tr('manager_listing.filter-property-apartment', 'Apartment') }}</option>
            <option value="cd">{{ tr('manager_listing.filter-property-condo', 'Condo') }}</option>
        </select>
    </div>
</div>
<div class="list-items">

    <div class="property-list">
        <?php
        $properties = $user->getProperties(10);
        foreach ($properties as $property) {
            $img = $property->getMainImage();
            $listing = $property->getListingData();
            $extended = $property->getExtendedData();
            ?>
            <div data-ltype="{{$listing->ltype}}" data-ptype="{{$property->ptype}}" class="property-item">
                <h2 style="font-size:14px;margin:5px auto;">{{ $property->getTranslatable('name')}}</h2>
                <p style="padding:0;font-size:13px;">{{ $property->getCustomAddress()}}</p>
                <div style="color:white;margin-bottom:15px; position: relative;">
                    <div style="position:relative; height:150px; overflow: hidden;">
                        <?php
                        if ($img) {
                            ?>
                            <img src="/image/{{ $img->filepath}}" class="img-responsive"/>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="meta">
                        <div style="display:flex;align-items: center;font-size:8px;width:100%;">
                            <p style="flex:42;margin:0 auto;font-size:14px;font-weight:bold;">
                                <span class="price">฿ {{ number_format($listing->listing_price) }}</span>
                                <span class="others">
                                    | {{ $extended->bedroom_num }} beds | {{ $extended->bathroom_num }} baths | {{ $extended->total_size }} sqm
                                </span>
                            </p>
                        </div>
                        <div class="address">
                            <p style="">{{ $property->getCustomAddress()}}</p>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>
    </div>

</div>
