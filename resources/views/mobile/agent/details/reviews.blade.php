<div class="agent-comments">
    <div style="width:100%;">
        <p style="display:table;margin: 10px 10px 0;text-transform:uppercase;font-weight:bold;font-size:12px;">{{ tr('agent_reviews.write-comments', 'Write comments') }}</p>
        <div style="font-size:10px;padding: 10px;" class="agent-commenting">
            <div style="clear:both;height:10px;">
                <div style="float:left;height:20px;">{{ tr('agent_reviews.give-recommendation', 'Give your recommendation') }}</div>
                <div style="float:left;margin-top:-11px;margin-left: 30px;height:20px;">
                    <fieldset class="rating-blue">
                        <input type="radio" value="10" ng-click="reply.rating = 10" id="star51" name="rating1" value="5" /><label class = "full" for="star51" title="Awesome - 5 stars"></label>
                        <input type="radio" value="9" ng-click="reply.rating = 9" id="star41half" name="rating1" value="4 and a half" /><label class="half" for="star41half" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" value="8" ng-click="reply.rating = 8" id="star41" name="rating1" value="4" /><label class = "full" for="star41" title="Pretty good - 4 stars"></label>
                        <input type="radio" value="7" ng-click="reply.rating = 7" id="star31half" name="rating1" value="3 and a half" /><label class="half" for="star31half" title="Meh - 3.5 stars"></label>
                        <input type="radio" value="6" ng-click="reply.rating = 6" id="star31" name="rating1" value="3" /><label class = "full" for="star31" title="Meh - 3 stars"></label>
                        <input type="radio" value="5" ng-click="reply.rating = 5" id="star21half" name="rating1" value="2 and a half" /><label class="half" for="star21half" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" value="4" ng-click="reply.rating = 4" id="star21" name="rating1" value="2" /><label class = "full" for="star21" title="Kinda bad - 2 stars"></label>
                        <input type="radio" value="3" ng-click="reply.rating = 3" id="star11half" name="rating1" value="1 and a half" /><label class="half" for="star11half" title="Meh - 1.5 stars"></label>
                        <input type="radio" value="2" ng-click="reply.rating = 2" id="star11" name="rating1" value="1" /><label class = "full" for="star11" title="Sucks big time - 1 star"></label>
                        <input type="radio" value="1" ng-click="reply.rating = 1" id="starhalf1" name="rating1" value="half" /><label class="half" for="starhalf1" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                </div>
            </div>
            <br/>
            <br/>
            <p>{{ tr('agent_review.title-label', 'Title') }}</p>
            <input id="review-title" placeholder="Title" type="text" />
            <p>{{ tr('agent_review.experience-label', 'Describe in details your experience with this agent') }}</p>
            <textarea id="review-comment" placeholder="Your comment" style="resize:none;width:100%;height:60px;" ng-model="reply.user.message"></textarea>
            <br/>
            <div style="height:30px;">
                <button id="send-review" class="btn" style="float:right;width:110px;background-color:#9d9d9d;font-size:10px;height:30px;" ng-click="sendReview()">{{ tr('button.send-review', 'SEND THE REVIEW') }}</button>
                <!--
                <div style="display:inline-block;font-size:20px;vertical-align:middle;" class="glyphicon glyphicon-upload"></div>
                <button style="border:none;display:inline-block;color: #666e71;height:30px;width:85px;background-color:inherit;">Upload Picture</button>
                -->
            </div>
        </div>
    </div>

    <div style="width: 100%;padding: 20px 5px 0px 5px;border-top: 1px solid #44B2CC;font-size:10px;">
          @foreach ($reviews as $review)
        <div class="agent-comment" ng-repeat="comment in comments.slice(((currentPage - 1) * itemsPerPage), (((currentPage - 1) * itemsPerPage) + itemsPerPage))">
            <div class="row">
                <div class="col-xs-2">
                    <div style="width:100%;padding: 90%;" back-img="@{{comment.user.image}}"></div>
                </div>
                <div class="col-xs-10">
                    <div class="commenter-email">
                        {{$review->name}}
                    </div>
                    <div>
                        {{ render_simple_rating($review->rating) }}
                    </div>
                    <div style="margin:10px 0 20px 0;border-bottom: 1px solid #44B2CC;padding-bottom:10px;">
                          {{$review->comment}}
                    </div>
                </div>
            </div>
        </div>
                @endforeach
        <div id="comments-end" style="display: table;margin: 20px auto;" items-per-page="itemsPerPage" uib-pagination boundary-links="true" max-size="maxSize" rotate="true" total-items="totalItems" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
    </div>
</div>
