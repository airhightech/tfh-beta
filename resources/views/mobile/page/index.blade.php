@extends('mobile.layouts.default')

@section('content')

<div class="row" style="padding: 15px;">
    <div class="col-xs-12 title">
        <h2 class="top-title">{{ $page->getTranslatable('title') }}</h2>            
    </div>

    <div class="col-xs-12 content">
        {!! $page->getTranslatable('html') !!} 
        <p class="text-muted">
            {{ tr('page.updated-on', 'Updated on') }} : <?php echo date('d/m/Y', strtotime($page->updated_at)); ?>
        </p>
    </div>
</div>

@endsection