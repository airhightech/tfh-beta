<script src="/mobile/lib/jquery/dist/jquery.js"></script>
<ul class="dropped">
    <!--
    <li class="lang">
        <span>English</span>
        <button class="btn btn-lang pull-right"><i class="fa fa-chevron-right"></i></button>
    </li>
    -->
    <li class="lang">
        <span id="zh" class="lang-flag zh"></span>
        <span id="ja" class="lang-flag ja"></span>
        <span id="ko" class="lang-flag ko"></span>
        <span id="en" class="lang-flag en"></span>
        <span id="th" class="lang-flag th"></span>
    </li>
    <li><a class="btn btn-menu btn-block" href="/search?lt=rent">{{ tr('home.options.rent', 'RENT') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/search?lt=sale">{{ tr('home.options.sale', 'SALE') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/search?np=yes">{{ tr('home.options.new-project', 'NEW PROJECT') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/search?pt=cdb">{{ tr('home.options.condo', 'CONDO COMMUNITY') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/agent/search">{{ tr('menubar.find-agent', 'Find agent') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/page/advertise">{{ tr('menubar.advertise-with-us', 'Advertise with us') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/page/about-us">{{ tr('footer.about-us', 'About us') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/page/contact-us">{{ tr('footer.contact-us', 'Contact us') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/page/careers">{{ tr('footer.careers', 'Careers') }}</a></li>
</ul>

<script>
    $(function() {
        $(".lang-flag").click(function() {
            if(!$(this).hasClass("active")) {
                $(".lang-flag").removeClass("active");
                $(this).addClass("active");
            }
        });
        $('.lang-flag.th').on('click', function(){
            window.location = '/?hl=th';
        });
        $('.lang-flag.en').on('click', function(){
            window.location = '/?hl=en';
        });
        $('.lang-flag.ja').on('click', function(){
            window.location = '/?hl=ja';
        });
        $('.lang-flag.ko').on('click', function(){
            window.location = '/?hl=ko';
        });
        $('.lang-flag.zh').on('click', function(){
            window.location = '/?hl=zh';
        });

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $(document).ready(function () {
            var lang = getUrlParameter('hl');
            if(lang != undefined) {
                $(".lang-flag").removeClass("active");
                $(".lang-flag." + lang).addClass("active");
            }
        });
    });
</script>
