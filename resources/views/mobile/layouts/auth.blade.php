<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>ThaiFullHouse</title>

        <!-- Bootstrap -->
        <link href="/mobile/css/bootstrap.min.css" rel="stylesheet">
        <link href="/mobile/css/font-awesome.min.css" rel="stylesheet">
        <link href="/mobile/css/style.min.css?t=<?php echo filemtime(public_path() . '/mobile/css/style.min.css'); ?>" rel="stylesheet">
        <link href="/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet"> 
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="mobile-container">
            @yield('content')
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/mobile/js/jquery-3.1.1.min.js"></script>
        <script src="{{ __asset('/mobile/js/mobile.js') }}"></script>
        <script>
            var currentHL = '<?php echo \App::getLocale(); ?>';
            var userIsOnline = <?php echo Auth::check() ? 'true' : 'false'; ?>;
            var currentUserId = <?php echo Auth::check() ? Auth::user()->id : '0'; ?>;
            var onOfflineMessage = 'Please login';
        </script>
        @yield('scripts')
        @include('desktop.layouts.google-analytics')
    </body>
</html>
