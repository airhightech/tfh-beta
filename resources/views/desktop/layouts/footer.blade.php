
    <footer>
        <div class="row">
            <div class="col-md-3">
                <h5>Properties for Sales</h5>
                <ul class="list-unstyled">
                    <li><a href="/search?lt=sale&pt=dh">Houses for Sales</a></li>
                    <li><a href="/search?lt=sale&pt=co">Condos for Sales</a></li>
                    <li><a href="/search?lt=sale&pt=th">Townhouses for Sales</a></li>
                    <li><a href="/search?lt=sale&pt=ap">Apartments for Sales</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Properties for Rent</h5>
                <ul class="list-unstyled">
                    <li><a href="/search?lt=rent&pt=dh">Houses for Rent</a></li>
                    <li><a href="/search?lt=rent&pt=co">Condos for Rent</a></li>
                    <li><a href="/search?lt=rent&pt=th">Townhouses for Rent</a></li>
                    <li><a href="/search?lt=rent&pt=ap">Apartments for Rent</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Search Condos by Price</h5>
                <ul class="list-unstyled">
                    <li><a href="/search?lt=sale&pt=co&pmin=1000000&pmax=5000000">Condos for sales 1-5M.</a></li>
                    <li><a href="/search?lt=sale&pt=co&pmin=5000000&pmax=10000000">Condos for sales 6-10M.</a></li>
                    <li><a href="/search?lt=sale&pt=co&pmin=11000000&pmax=15000000">Condos for sales 11-15M.</a></li>
                    <li><a href="/search?lt=sale&pt=co&pmin=16000000&pmax=">Condos for sales 16M+</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5>Search Houses by Price</h5>
                <ul class="list-unstyled">
                    <li><a href="/search?lt=sale&pt=dh&pmin=5000000&pmax=10000000">Houses for sales 5-10M.</a></li>
                    <li><a href="/search?lt=sale&pt=dh&pmin=11000000&pmax=15000000">Houses for sales 11-15M.</a></li>
                    <li><a href="/search?lt=sale&pt=dh&pmin=16000000&pmax=20000000">Houses for sales 16-20M.</a></li>
                    <li><a href="/search?lt=sale&pt=dh&pmin=21000000&pmax=">Houses for sales 21M+</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?php
                
                $developers = \App\Models\User::where('ctype', 'developer')
                            ->where('verified', true)->inRandomOrder()->get();
                
                ?>
<!--                <h5>Developers</h5>
                <div class="developer-list">
                    <ul class="list-unstyled">
                        <?php
                            foreach($developers as $developer) {
                                $profile = $developer->getProfile();
                                ?>
                        <li><a href="/agent/{{ $developer->id }}">{{ $profile->getName() }}</a></li>
                                <?php
                            }
                        ?>
                    </ul>
                </div>-->
            </div>
            <div class="col-md-3">
<!--                <h5>Popular areas in Bangkok</h5>-->
            </div>
            <div class="col-md-3">
                <h5><a href="/page/advertise">{{ tr('menubar.advertise-with-us', 'Advertise with us') }}</a></h5>
            </div>
            <div class="col-md-3">
                <h5>Company Info</h5>
                <ul class="list-unstyled">
                    <li><a href="/page/about-us">{{ tr('footer.about-us', 'About us') }}</a></li>
                    <li><a href="/page/contact-us">{{ tr('footer.contact-us', 'Contact us') }}</a></li>
                    <li><a href="/page/privacy">{{ tr('footer.privacy', 'Privacy Policy') }}</a></li>
                    <li><a href="/page/terms">{{ tr('footer.terms', 'Terms and Conditions') }}</a></li>
                    <li><a href="/page/careers">{{ tr('footer.careers', 'Careers') }}</a></li>
                    <li><a href="/auth/register">{{ tr('footer.register', 'Register') }}</a></li>
                    <li><a href="/auth/forgot-password">{{ tr('footer.request-passwordd', 'Forgot password') }}</a></li>       
                    
                </ul>
            </div>
        </div>
    </footer>
    <footer class="address">
        <div class="row">
            <div class="col-md-6">
                <img src="/img/logo-small.png" class="bottom-logo"/>
            </div>
            <div class="col-md-6">
                <h3>Wonderpons Co.,Ltd</h3>
                <p>
                    <?php echo nl2br(__dbconf('footer_contact', true)); ?>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="nav nav-pills footer-nav">
                    <li role="presentation"><a href="/page/about-us">{{ tr('footer.about-us', 'About us') }}</a></li>
                    <li role="presentation"><a href="/auth/register">{{ tr('footer.register', 'Register') }}</a></li>
                    <li role="presentation"><a href="/page/privacy">{{ tr('footer.privacy', 'Privacy Policy') }}</a></li>
                    <li role="presentation"><a href="/page/terms">{{ tr('footer.terms', 'Terms and Conditions') }}</a></li>
                    <li role="presentation"><a href="/page/contact-us">{{ tr('footer.contact-us', 'Contact us') }}</a></li>
                    <li role="presentation"><a href="/page/careers">{{ tr('footer.careers', 'Careers') }}</a></li>
                    <li role="presentation"><a href="#">Copyright &copy; Wonderpons Co., Ltd.</a></li>
                </ul>
            </div>
        </div>
    </footer>

