<div class="container">
    <ol class="breadcrumb top-breadcrumb">
        @yield('breadcrumb')
        <?php
        if (Auth::check()) {
            $user = Auth::user();

            if ($user->ctype == 'visitor') {
                ?>
                <li class="free-listing-link pull-right"><a href="/auth/register/type">{{ tr('breadcrumb.free-listing', 'Free listing') }}</a></li>
                <?php
            } else {
                ?>
                <li class="free-listing-link pull-right"><a href="/member/listing/select-type">{{ tr('breadcrumb.free-listing', 'Free listing') }}</a></li>
                <?php
            }
        } else {
            ?>
            <li class="free-listing-link pull-right"><a href="/auth/register/type" class="free-listing">{{ tr('breadcrumb.free-listing', 'Free listing') }}</a></li>
                <?php
            }
            ?>             
    </ol>
</div>