<div class="container mainmenu-container">
    <a href="/"><img src="/img/logo-small.png" class="logo"/></a>
    <ul class="nav nav-pills main-nav">
        <li role="presentation"><a href="/search?lt=sale" class="main">{{ tr('menubar.sale', 'SALE') }}</a></li>
        <li role="presentation"><a href="/search?lt=rent" class="main">{{ tr('menubar.rent', 'Rent') }}</a></li>
        <li role="presentation"><a href="/search?np=yes" class="main">{{ tr('menubar.new-project', 'New project') }}</a></li>
        <li role="presentation"><a href="/agent/search" class="main">{{ tr('menubar.find-agent', 'Find agent') }}</a></li>
        <li role="presentation"><a href="/search?pt=cdb" class="main">{{ tr('menubar.condo-community', 'Condo community') }}</a></li>

        <?php
        if (Auth::check()) {
            $user = Auth::user();
            $profile = $user->getProfile();

            $new_messages = $user->countNewMessage();

            if ($user->ctype == 'manager') {
                ?>
                <li role="presentation" class="pull-right">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><strong>{{ $profile->getShortName() }}</strong> <i class="fa fa-chevron-down"></i></a>
                    <div class="dropdown-menu account-dropdown" aria-labelledby="drop3">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="/manager/dashboard">{{ tr('menu.manager-dashboard', 'Manager Dashboard') }}</a></li>  
                            <li class="list-group-item"><a href="/member/account">{{ tr('menu.setting', 'Setting profile') }}</a></li>
                            <li class="list-group-item"><a href="/member/listing">{{ tr('menu.listing', 'My listings') }}</a></li>
                            <li class="list-group-item"><a href="/member/purchase">{{ tr('menu.purchase', 'My purchase') }}</a></li>
                            <li class="list-group-item"><a href="/member/message">{{ tr('menu.messages', 'Message') }}</a>
                                <?php
                                if ($new_messages > 0) {
                                    ?>
                                    <span class="badge">{{ $new_messages }}</span>
                                    <?php
                                }
                                ?>
                            </li>
                            <li class="list-group-item"><a href="/member/favorite">{{ tr('menu.favorites', 'Favorites') }}</a></li>
                            <li class="list-group-item"><a href="/member/notice">{{ tr('menu.notices', 'Notice') }}</a></li>
                            
                            <li class="list-group-item"><a href="/auth/logout">{{ tr('menu.logout', 'Logout') }}</a></li>
                        </ul>
                    </div>
                </li>
                <?php
            } else if (in_array($user->ctype, ['editor', 'translator', 'accountant'])) {
                ?>
                <li role="presentation" class="pull-right">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><strong>{{ $profile->getShortName() }}</strong> <i class="fa fa-chevron-down"></i></a>
                    <div class="dropdown-menu account-dropdown" aria-labelledby="drop3">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="/manager/dashboard">{{ tr('menu.manager-dashboard', 'Manager Dashboard') }}</a></li>                            
                            <li class="list-group-item"><a href="/auth/logout">{{ tr('menu.logout', 'Logout') }}</a></li>
                        </ul>
                    </div>
                </li>
                <?php
            } else if ($user->ctype == 'visitor') {
                ?>
                <li role="presentation" class="pull-right">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><strong>{{ $profile->getShortName() }}</strong> <i class="fa fa-chevron-down"></i></a>
                    <div class="dropdown-menu account-dropdown" aria-labelledby="drop3">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="/member/account">{{ tr('menu.setting', 'Setting profile') }}</a></li>                            
                            <li class="list-group-item"><a href="/member/message">{{ tr('menu.messages', 'Message') }}</a>
                                <?php
                                if ($new_messages > 0) {
                                    ?>
                                    <span class="badge">{{ $new_messages }}</span>
                                    <?php
                                }
                                ?>
                            </li>
                            <li class="list-group-item"><a href="/member/favorite">{{ tr('menu.favorites', 'Favorites') }}</a></li>
                            <li class="list-group-item"><a href="/auth/logout">{{ tr('menu.logout', 'Logout') }}</a></li>
                        </ul>
                    </div>
                </li>
                <?php
            } else {
                ?>
                <li role="presentation" class="pull-right">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><strong>{{ $profile->getShortName() }}</strong> <i class="fa fa-chevron-down"></i></a>
                    <div class="dropdown-menu account-dropdown" aria-labelledby="drop3">
                        <ul class="list-group">
                            <li class="list-group-item"><a href="/member/account">{{ tr('menu.setting', 'Setting profile') }}</a></li>
                            <li class="list-group-item"><a href="/member/listing">{{ tr('menu.listing', 'My listings') }}</a></li>
                            <li class="list-group-item"><a href="/member/purchase">{{ tr('menu.purchase', 'My purchase') }}</a></li>
                            <li class="list-group-item"><a href="/member/message">{{ tr('menu.messages', 'Message') }}</a>
                                <?php
                                if ($new_messages > 0) {
                                    ?>
                                    <span class="badge">{{ $new_messages }}</span>
                                    <?php
                                }
                                ?>
                            </li>
                            <li class="list-group-item"><a href="/member/favorite">{{ tr('menu.favorites', 'Favorites') }}</a></li>
                            <li class="list-group-item"><a href="/member/notice">{{ tr('menu.notices', 'Notice') }}</a></li>
                            <li class="list-group-item"><a href="/auth/logout">{{ tr('menu.logout', 'Logout') }}</a></li>
                        </ul>
                    </div>
                </li>
                <?php
            }
            ?>
            <?php
        } else {
            include resource_path('views/desktop/layouts/auth-dropdown.blade.php');
        }
        ?>
    </ul>
</div>


