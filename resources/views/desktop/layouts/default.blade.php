<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        @yield('custom-header')
        
        <title>Thaifullhouse - {{ $page_title or '' }}</title>

        <!-- Bootstrap -->
        <link href='https://fonts.googleapis.com/css?family=Signika:400,600,300' rel='stylesheet' type='text/css'>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/font-awesome.min.css" rel="stylesheet"> 
        <link href="/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet"> 
        <link rel="stylesheet" href="/libs/social/stylesheets/arthref.css">
        <link href="{{ __asset('/css/desktop.min.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
        if ((location.hostname === "thaifullhouse.com" || location.hostname === "www.thaifullhouse.com") && screen.width <= 699) {
            document.location = "http://m.thaifullhouse.com/";
        }
        </script>
    </head>
    <body>
        @include('desktop.layouts.topbar')     

        @include('desktop.layouts.menubar') 

        @include('desktop.layouts.breadcrumb') 

        @yield('content')     

        <div class="container">
            @include('desktop.layouts.footer')
        </div>  

        <div id="default-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-default" role="document">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
                </div>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
                    </div>
                    <div class="modal-body default-modal-body">

                    </div>
                </div>
            </div>
        </div>
        <script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
        
        <script src="/libs/jquery-ui/jquery-ui.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/pnotify/pnotify.custom.min.js"></script>

        <script src="/libs/social/javascripts/socialShare.min.js"></script>
        <script src="/libs/social/javascripts/socialProfiles.min.js"></script>
        <script src="{{ __asset('/js/app.js') }}"></script>
        <script src="{{ __asset('/js/form.js') }}"></script>
        <script>
            var currentHL = '<?php echo \App::getLocale(); ?>';
            var userIsOnline = <?php echo Auth::check() ? 'true' : 'false'; ?>;
            var currentUserId = <?php echo Auth::check() ? Auth::user()->id : '0'; ?>;
            var default_lat = <?php echo config('google.default_lat'); ?>;
            var default_lng = <?php echo config('google.default_lng'); ?>;
            var default_zl = <?php echo config('google.default_zl'); ?>;
        </script>

        @yield('scripts')

        <script>
            var _trans = <?php echo App\Helpers\Translation::getFrontTranslations(); ?>;
        </script>
        @include('desktop.layouts.google-analytics')
    </body>
</html>