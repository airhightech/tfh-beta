<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Thaifullhouse - {{ $page_title or '' }}</title>

        <!-- Bootstrap -->
        <link href='https://fonts.googleapis.com/css?family=Signika:400,600,300' rel='stylesheet' type='text/css'>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/font-awesome.min.css" rel="stylesheet"> 
        <link href="/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet"> 
        <link href="/js/pnotify/pnotify.custom.min.css" rel="stylesheet">
        <link href="{{ __asset('/css/desktop.min.css') }}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
        if ((location.hostname === "thaifullhouse.com" || location.hostname === "www.thaifullhouse.com") && screen.width <= 699) {
            document.location = "http://m.thaifullhouse.com/";
        }
        </script>
    </head>
    <body>
        @include('desktop.layouts.topbar')     
        
        @include('desktop.layouts.menubar') 
        
        @include('desktop.layouts.breadcrumb') 
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('desktop.member.header')
                </div>
                <div class="col-md-3">
                    @include('desktop.member.sidebar')
                </div>
                <div class="col-md-9 manager-content member-content">
                    @yield('content')                    
                </div>
            </div>
        </div>
        <div class="container">
        @include('desktop.layouts.footer')
        </div>
        
        <div id="loading-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-default" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
                    </div>
                    <div class="modal-body default-modal-body">
                        <i class="fa fa-spinner fa-spin fa-fw"></i> {{ tr('manager.loading', 'Loading ...') }}
                    </div>
                </div>
            </div>
        </div>
        
        <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-default" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
                    </div>
                    <div class="modal-body default-modal-body">
                        <p>
                            {{ tr('modal.delete-confirm', 'Do you really want to delete ?') }}
                        </p>                        
                        <button type="button" class="btn btn-default">{{ tr('button.cancel', 'Cancel') }}</button>
                        <button type="button" class="btn btn-danger pull-right" onClick="performDelete();">{{ tr('button.delete', 'Delete') }}</button>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="/js/jquery-3.1.1.min.js"></script>
        <script src="/libs/jquery-ui/jquery-ui.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/pnotify/pnotify.custom.min.js"></script>
        <script>
            var supportedLangs = <?php echo json_encode(\App\Helpers\Language::getActiveCodes()); ?>;
            PNotify.prototype.options.styling = "bootstrap3";
            PNotify.prototype.options.delay = 2000;
        </script>
        <script src="{{ __asset('/js/app.js') }}"></script>
        <script src="{{ __asset('/js/form.js') }}"></script>
        
        @yield('scripts')
        
        <script>
            var _trans = <?php echo App\Helpers\Translation::getFrontTranslations(); ?>;
        </script>
        @include('desktop.layouts.google-analytics')
    </body>
</html>