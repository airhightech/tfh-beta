<li role="presentation" class="pull-right">
    <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><?php echo tr('nav_menu.login-register', 'LOGIN') ?></a>
    <div class="dropdown-menu session-dropdown" aria-labelledby="drop3">
        <div class="auth-box">
            <div class="social-btn-container">
                <p>
                    <a href="/auth/facebook" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> <?php echo tr('nav_menu.sign-facebook', 'Login with Facebook') ?></a>
                </p>
                <p>
                    <a href="/auth/twitter" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> <?php echo tr('nav_menu.sign-twitter', 'Login with Twitter') ?></a>
                </p>
                <p>
                    <a href="/auth/google" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus"></i> <?php echo tr('nav_menu.sign-google', 'Login with Google') ?></a>
                </p>
            </div>

            <p class="text-center or-separator-container">
                <span class="or-separator">or</span>
            </p>

            <form method="post" id="login-form" action="/rest/auth/login">                
                <div class="form-group">
                    <label for="email"><?php echo tr('nav_menu.email', 'Email') ?></label>
                    <input type="text" class="form-control" name="email" value="" placeholder="" required>
                    <p class="input-error email hidden-error"></p>
                </div>
                <div class="form-group">
                    <label for="password"><?php echo tr('nav_menu.password', 'Password') ?></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="" required>
                    <p class="input-error password hidden-error"></p>
                </div>
<!--                <div class="checkbox">
                    <label>
                        <input type="checkbox"> <?php echo tr('nav_menu.remember-me', 'Remember me') ?>
                    </label>
                </div>-->
                <p class="form-error hidden-error text-danger">
                    </p>
                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <button type="button" class="btn btn-default submit-ajax-form" data-target="login-form">
                    <?php echo tr('nav_menu.sign-in', 'Sign in') ?> <i class="fa fa-spinner fa-spin fa-fw"></i>
                </button>
                <p class="account-link">
                    <a href="/auth/forgot-password"><?php echo tr('nav_menu.forgot-password', 'Forgot password') ?></a>
                </p>
                <p class="text-right account-link">
                    <a href="#" class="auth-mode-switcher"><?php echo tr('nav_menu.dont-have-account', 'Create an account') ?></a>
                </p>
            </form>
        </div>
        <div class="auth-box" style="display: none;">
            <div class="social-btn-container">
                <p>
                    <a href="/auth/facebook" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> <?php echo tr('nav_menu.sign-facebook', 'Login with Facebook') ?></a>
                </p>
                <p>
                    <a href="/auth/twitter" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> <?php echo tr('nav_menu.sign-twitter', 'Login with Twitter') ?></a>
                </p>
                <p>
                    <a href="/auth/google" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus"></i> <?php echo tr('nav_menu.sign-google', 'Login with Google') ?></a>
                </p>
            </div>

            <p class="text-center or-separator-container">
                <span class="or-separator">OR</span>
            </p>

            <form method="post" id="registration-form" action="/rest/auth/register">
                <div class="alert alert-danger error-box" >
                    <ul id="registration-errors-list" class="list-unstyled">
                    </ul>
                </div>
                <div class="form-group">
                    <label for="email"><?php echo tr('nav_menu.email', 'Email') ?></label>
                    <input type="text" class="form-control" name="email" value="" placeholder="">
                    <p class="input-error email hidden-error"></p>
                </div>
                <div class="form-group">
                    <label for="password"><?php echo tr('nav_menu.password', 'Password') ?></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="">
                    <p class="input-error password hidden-error"></p>
                </div>
                <div class="form-group">
                    <label for="email"><?php echo tr('nav_menu.password-confirm', 'Confirm password') ?></label>
                    <input type="password" class="form-control" name="password_confirmation" value="" placeholder="">
                    <p class="input-error password_confirmation hidden-error"></p>
                </div>
                <p class="form-error hidden-error text-danger">
                    </p>

                <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                <button type="button" class="btn btn-default submit-ajax-form"
                        data-target="registration-form"><?php echo tr('nav_menu.sign-up', 'Sign Up') ?> <i class="fa fa-spinner fa-spin fa-fw"></i></button>

                <p class="text-right account-link">
                    <a href="#" class="auth-mode-switcher"><?php echo tr('nav_menu.have-an-account', 'Already have an account ?') ?></a>
                </p>
            </form>
        </div>

    </div>
</li>