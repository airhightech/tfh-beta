<div class="topbar">
    <nav class="navbar navbar-blue navbar-static-top">
        <div class="container">
            <div id="navbar" class="collapse navbar-collapse">
                @include('desktop.layouts.socialbars')
                <ul class="nav navbar-nav navbar-right navbar-lang">
                    @include('desktop.layouts.languages')
<!--                    <li><a href="#"><i class="fa fa-search"></i></a></li>-->
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
</div>