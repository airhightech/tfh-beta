<?php
$active = 'popup';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_popup.static-page-list', 'Homepage popups') }}</h3>

<div class="toolbar">

    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/popup/create" class="btn btn-sm btn-blue">{{ tr('manager_popup.create', 'Add new popup') }}</a>
        </li>
    </ul>

</div>

<table class="table table-list">
    <thead>
        <tr>                
            <th>{{ tr('manager_popup.title-col', 'Title') }}</th>    
            <th>{{ tr('manager_popup.link-col', 'Link to') }}</th>    
            <th class="small-col">{{ tr('manager_popup.start-col', 'Start') }}</th> 
            <th class="small-col">{{ tr('manager_popup.end-col', 'End') }}</th> 
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($popups as $link => $popup) {
            ?>
            <tr>
                <td>{{ $popup->title }}</td>
                <td>{{ $popup->related_link }}</td>
                <td>{{ $popup->getHRDate('start_time') }}</td>
                <td>{{ $popup->getHRDate('end_time') }}</td>
                <td class="text-center"><a href="/manager/popup/edit/{{ $popup->id }}"><i class="fa fa-pencil"></i></a></td>
            </tr>
            <?php
        }
        ?>

    </tbody>
</table>

@endsection