<?php
$active = 'pages';
$langs = \App\Helpers\Language::getActives();
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_popup.static-page-list', 'Homepage popups') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/popup/list">{{ tr('manager_page.back-to-list', 'Back to list') }}</a>
        </li>
    </ul>
</div>

<form action="/manager/popup/create" method="post" id="page-form">

    <div class="form-group">
        <label for="title" class="control-label">{{ tr('manager_page.title-label', 'Title') }} <span class="text-danger">*</span></label>
        <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" placeholder="">
    </div>
    
    <div class="form-group">
        <label for="related_link" class="control-label">{{ tr('manager_page.link-label', 'Link') }} <span class="text-danger">*</span></label>
        <input type="text" class="form-control" id="related_link" name="related_link" value="{{ old('related_link') }}" placeholder="">
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
                <label for="start_time" class="control-label">{{ tr('manager_page.start-label', 'Start') }} <span class="text-danger">*</span></label>
                <input type="text" class="form-control datetime" id="start_time" name="start_time" value="{{ old('start_time') }}" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="end_time" class="control-label">{{ tr('manager_page.end-label', 'End') }} <span class="text-danger">*</span></label>
                <input type="text" class="form-control datetime" id="end_time" name="end_time" value="{{ old('end_time') }}" placeholder="">
            </div>
        </div>
    </div>

    <h4><?php echo tr('manager_page.html-label', 'Content'); ?> <span class="text-danger">*</span></h4>

    <div class="form-group">
        <div class="">
            <ul id="html-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#details-{{ $code }}" aria-controls="settings" role="tab" 
                           data-toggle="tab" data-content="#html-{{ $code }}" data-editor="#editor-{{ $code }}">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}" >
                        <div id="editor-{{ $code }}" class="notes"></div>
                        <textarea id="html-{{ $code }}" name="content[{{ $code }}]" class="hidden"></textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span class="sr-only"></span>
        </div>
    </div>
    <button type="button" class="btn btn-primary submit-ajax-form" data-target="page-form">{{ tr('button.save', 'SAVE') }}</button>
</form>
@endsection

@section('scripts')
<link href="/libs/summernote/summernote.css" rel="stylesheet">
<script src="/libs/summernote/summernote.js"></script>
<script src="{{ __asset('/js/manager/popup.js') }}"></script>
@endsection