<?php
$active = 'place';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_account.place.title', 'List of places') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/place/list">{{ tr('manager_property.back', 'List view') }}</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-9">
        <div id="place-map" class="place-map">            
        </div>
        <div class="place-editor">
            <form id="place-editor-form">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="name">{{ tr('manager_place.name', 'Place name') }}</label>
                        </div>
                        <?php echo __trfields('name'); ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group" style="margin-top: 38px;">
                            <label for="name">{{ tr('manager_place.type', 'Type') }}</label>
                            <select class="form-control" id="type" name="type">
                                <?php
                                foreach ($supported as $type) {
                                    ?>
                                    <option value="{{ $type }}">{{ tr('places.' . $type, $type) }}</option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>                        
                    </div>
                    <div class="col-md-8 place-actions">
                        <button id="place-delete-confirm" type="button" class="btn btn-default">
                            {{ tr('button.delete', 'DELETE') }}
                        </button>
                        <button id="place-save" type="button" class="btn btn-primary pull-right">
                            {{ tr('button.save', 'SAVE') }}
                        </button>
                    </div>
                </div>
                <input type="hidden" id="lng" name="lng" value=""/>
                <input type="hidden" id="lat" name="lat" value=""/>
                <input type="hidden" id="place-id" name="id" value=""/>
            </form>
        </div>
    </div>
    <div class="col-md-3">
        <div class="place-filters">
            <h4>{{ tr('manager_place.place-filters', 'Place filters') }}</h4>
            <form>

                <?php
                foreach ($supported as $type) {
                    ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="type[]" value="{{ $type }}" checked class="place-filter"> {{ tr('places.' . $type, $type) }}
                        </label>
                    </div>
                    <?php
                }
                ?>
                <button id="place-filter-apply" type="button" class="btn btn-default">
                    {{ tr('button.apply-filters', 'Apply filters') }}
                </button>
            </form>
        </div>
    </div>
</div>

<div id="place-modal" class="modal fade account-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body account-details">
                <strong class="text-danger">{{ tr('manager_place.confirm-delete', 'Do you really want to delete this place ?') }}</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ tr('button.cancel', 'CANCEL') }}</button>
                <button id="place-delete" type="button" class="btn btn-danger">{{ tr('button.delete', 'DELETE') }}</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/markerclusterer.js"></script>
<script src="{{ __asset('/js/maps/place.js') }}"></script>
<script src="{{ __asset('/js/manager/place.js') }}"></script>
@endsection