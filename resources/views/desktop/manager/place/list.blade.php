<?php
$active = 'place';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_account.place.title', 'List of places') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/place/index">{{ tr('manager_property.back', 'Map view') }}</a>
        </li>
    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ tr('manager_place.name-col', 'Name') }}</th>
            <th class="small-col text-center">{{ tr('manager_place.type-col', 'Type') }}</th>
            <th class="icon-col"></th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($places as $place) {
            ?>
            <tr>
                <td>{{ $place->getTranslatable('name') }}</td>
                <td class="text-center">{{ $place->type }}</td>
                <td class="text-center">
                    <a href="#" class="edit-place" data-id="{{ $place->id }}"><i class="fa fa-pencil"></i></a>
                </td>
                <td class="text-center">
                    <a href="#" class="delete-place" data-id="{{ $place->id }}"><i class="fa fa-times text-danger"></i></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div id="place-modal" class="modal fade account-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('manager_place.update-place', 'Update place') }}</h4>
            </div>
            <div class="modal-body place-details">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-primary save-place">{{ tr('button.update', 'UPDATE') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="delete-place-modal" class="modal fade account-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('manager_place.delete-place', 'Delete place') }}</h4>
            </div>
            <div class="modal-body delete-place-details">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-danger remove-place">{{ tr('button.delete', 'DELETE') }}</button>
            </div>
        </div>
    </div>
</div>

{{ $places->appends($request->all())->links() }}

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/place-list.js') }}"></script>
@endsection