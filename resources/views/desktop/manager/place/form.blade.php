<form id="place-update-form" action="/rest/place/update" method="post">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">{{ tr('manager_place.name', 'Place name') }}</label>
            </div>
            <?php echo __trfields('name', $place, false, 3, true); ?>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="name">{{ tr('manager_place.type', 'Type') }}</label>
                <select class="form-control" id="type" name="type">
                    <?php
                    foreach ($supported as $type) {
                        ?>
                        <option value="{{ $type }}" <?php echo $place->type == $type ? 'selected' : ''; ?>>{{ tr('places.' . $type, $type) }}</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="{{ $place->id }}"/>
</form>