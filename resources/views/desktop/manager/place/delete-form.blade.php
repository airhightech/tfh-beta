<form id="place-delete-form" action="/rest/place/delete" method="post">
    <div class="row">
        <div class="col-md-12">
            <p class="text-danger" style="margin-top: 10px;">
                {{ tr('manager_place.delete-message', 'Do you really want to delete this place ') }} :
                                                          <strong>{{ $place->getTranslatable('name') }}</strong>
            </p>
        </div>
    </div>
    <input type="hidden" name="id" value="{{ $place->id }}"/>
</form>