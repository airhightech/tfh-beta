
@section('title', 'Page Title')

@section('content')
<h3>{{ $title }} ({{ $properties->total() }})</h3>
<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation" class="input">  
            <form class="form-inline" method="get" action="/manager/{{ $action_link }}/list">
                <div class="form-group">
                    <input type="text" name="q" value="{{ $request->input('q') }}" 
                           class="form-control search" 
                           placeholder="{{ tr('manager_property.search-placeholder', 'Search property by name') }}"/>
                </div>
                <button type="submit" class="btn">{{ tr('button.search', 'Search') }}</button>
            </form>
        </li>

        <?php
        $q = trim($request->input('q'));
        if (isset($action_link) && !empty($q)) {
            ?>
            <li role="presentation">
                <a href="/manager/{{ $action_link }}/list">{{ tr('manager_account.reset-search', 'Reset search') }}</a>
            </li>
            <?php
        }
        ?>

        <?php
        if (isset($action_link) && $create_text !== false) {
            ?>
            <li role="presentation" class="pull-right">
                <a href="/manager/{{ $action_link }}/create" class="btn btn-sm btn-blue">
                    <i class="fa fa-plus"></i> {{ $create_text }} 
                </a>
            </li>
            <?php
        }
        ?>

    </ul>
</div>

<?php
if (isset($show_filter)) {
    ?>
    <table class="table table-filter">
       
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-property', 'Property type') }}
            </td>
            <td>
                <input type="radio" name="pt" value="all" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'all' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="pt" value="cdb" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'cdb' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-condo', 'Condo') }}
            </td>
            <td>
                <input type="radio" name="pt" value="th" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'th' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-townhouse', 'Townhouse') }}
            </td>
            <td>
                <input type="radio" name="pt" value="sh" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'sh' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-house', 'Single house') }}
            </td>
            <td>
                <input type="radio" name="pt" value="ap" 
                       class="filter" <?php echo array_get($filters, 'property') == 'ap' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-apartment', 'Apartment') }}
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-class', 'Class') }}
            </td>
            <td>
                <input type="radio" name="class" value="all" 
                       class="filter" <?php echo array_get($filters, 'class') == 'all' ? 'checked' : ''; ?> >
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="class" value="standard" 
                       class="filter" <?php echo array_get($filters, 'class') == 'standard' ? 'checked' : ''; ?> >
                {{ tr('manager_listing.filter-class-standard', 'Standard') }}
            </td>
            <td>
                <input type="radio" name="class" value="featured" 
                       class="filter" <?php echo array_get($filters, 'class') == 'featured' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-class-featured', 'Featured') }}
            </td>
            <td>
                <input type="radio" name="class" value="exclusive" 
                       class="filter" <?php echo array_get($filters, 'class') == 'exclusive' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-class-exclusive', 'Exclusive') }}
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-status', 'Status') }}
            </td>
            <td>
                <input type="radio" name="status" value="all" 
                       class="filter" <?php echo array_get($filters, 'status') == 'all' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="status" value="expired" 
                       class="filter" <?php echo array_get($filters, 'status') == 'expired' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-status-expired', 'Expired') }}
            </td>
            <td>
                <input type="radio" name="status" value="present" 
                       class="filter" <?php echo array_get($filters, 'status') == 'present' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-status-present', 'Present') }}
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <?php
}
?>

<table class="table table-list">
    <thead>
        <tr>
            <th class="icon-col"></th>
            <th>{{ tr('manager_property.name-col', 'Name') }}</th>
            <th class="small-col">{{ tr('manager_project.class-col', 'Class') }}</th>
            <th class="small-col text-center">{{ tr('manager_project.views-col', 'Views') }}</th>
            <th class="small-col text-center">{{ tr('manager_project.upgrade-col', 'Upgrade') }}</th>
            <th>{{ tr('manager_property.district-col', 'District') }}</th>
            <?php
            if (isset($new_project) && $new_project == true) {
                ?>
                <th class="small-col text-center">{{ tr('manager_property.start-col', 'Start') }}</th>  
                <th class="small-col text-center">{{ tr('manager_property.end-col', 'End') }}</th>
                <?php
            }
            ?>
            <th class="small-col text-center">{{ tr('manager_property.photo-col', 'Photos') }}</th>            
            <th class="icon-col"></th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($properties as $property) {

            $address = $property->getAddress();
            $listing = $property->getListingData();
            $district = null;

            if ($address) {
                $district = $address->getDistrict();
            }
            ?>
            <tr>
                <td class="text-center"><input type="checkbox" name="active" value="YES"  onclick="togglePropertyStatus({{ $property->id }})" <?php echo $property->published ? 'checked' : ''; ?>/></td>
                <td>
                    <a href="/property/{{ $property->id }}" target="_blank">{{ $property->getTranslatable('name') }}</a>
                </td>
                <td>{{ ucfirst($listing->listing_class) }}</td>
                <td class="text-center">{{ $property->clicks + 0 }}</td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-white edit-listing" data-id="{{ $property->id }}">
                        <i class="fa fa-pencil"></i>
                    </button>
                </td>
                <td>{{ $district ? $district->getTranslatable('name') : '' }}</td>

                <?php
                if (isset($new_project) && $new_project == true) {
                    $building = $property->getBuildingData();
                    ?>
                    <td class="text-center"><?php echo date('d/m/Y', strtotime($building->project_date_start)); ?></td>  
                    <td class="text-center"><?php echo date('d/m/Y', strtotime($building->project_date_end)); ?></td>
                    <?php
                }
                ?>

                <td class="text-center">{{ $property->countImages() }}</td>

                <td class="text-center">
                    <?php
                    if (isset($listing) && $listing === true) {
                        ?>
                        <button type="button" class="btn btn-xs btn-white edit-listing" data-id="{{ $property->id }}">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <?php
                    } else {
                        
                        if ($property->ptype == 'cdb') {
                            $action_link = 'project/condo'; 
                        } else if ($property->ptype == 'ap') {
                            $action_link = 'project/apartment';
                        } else if ($property->ptype == 'th') {
                            $action_link = 'project/townhouse';
                        } else if ($property->ptype == 'sh') {
                            $action_link = 'project/house';
                        }
                        ?>
                        <div class="dropdown">
                            <button class="btn btn-xs btn-white"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                <li>
                                    <a href="/manager/{{ $action_link }}/info/{{ $property->id }}">
                                        {{ tr('manager_property.info-tab', 'Info') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="/manager/{{ $action_link }}/details/{{ $property->id }}">
                                        {{ tr('manager_property.details-tab', 'Details') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="/manager/{{ $action_link }}/media/{{ $property->id }}">
                                        {{ tr('manager_property.media-tab', 'Media') }}
                                    </a>
                                </li>
                                <?php
                                if ($property->ptype == \App\Models\Property::TYPE_APARTMENT_BUILDING ||
                                        $property->ptype == \App\Models\Property::TYPE_CONDO_BUILDING ||
                                        $property->new_project) {
                                    ?>
                                    <li>
                                        <a href="/manager/{{ $action_link }}/review/{{ $property->id }}">
                                            {{ tr('manager_property.review-tab', 'Review') }}
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>                            
                                <li>
                                    <a href="/manager/{{ $action_link }}/places/{{ $property->id }}">
                                        {{ tr('manager_property.places-tab', 'Places') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>

                </td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-danger delete-listing" data-id="{{ $property->id }}">
                        <i class="fa fa-times"></i>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div id="listing-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body modal-listing">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-primary" 
                        onclick="submitGenericForm('listing-update-form')">{{ tr('button.update', 'UPDATE') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="listing-modal-delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body modal-listing-delete">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-danger" 
                        onclick="submitGenericForm('listing-delete-form')">{{ tr('button.update', 'DELETE') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

{{ $properties->appends($request->all())->links() }}

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/listing.js') }}"></script>
<script>

var updatefilter = function () {
    var fproperty = $('.filter[name=pt]:checked').val();
    var fclass = $('.filter[name=class]:checked').val();
    var fstatus = $('.filter[name=status]:checked').val();

    window.location = '/manager/project/list?pt=' + fproperty + '&class=' + fclass + '&status=' + fstatus;

};


$(function () {
    $('.filter').on('click', function () {

        console.log(this);

        updatefilter();
    });
});
</script>
@endsection