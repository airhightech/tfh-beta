@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$tab = 'places';

$list = $property->getNearbyPlaces();

$places = [];

$lang = \App::getLocale();

foreach ($list as $place) {
    $places[$place->type][] = $place;
}

?>

@section('content')

<h3>{{ $title }} : {{ $property->getTranslatable('name') }}</h3>

@include('desktop.manager.property.tab')

<h4>{{ tr('manager_property.current-places-title', 'Current places') }}</h4>

<div class="row place-listing">
    <div class="col-md-12">

        <h5>{{ tr('manager_property.places-bank-title', 'Banks') }}</h5>

        <?php
        $_list = array_get($places, 'bank');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ tr('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ tr('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ tr('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ tr('manager_property.places-stores-title', 'Stores &amp; Department stores') }}</h5>
        
        <?php
        $_list = array_get($places, 'dept-store');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ tr('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ tr('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ tr('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ tr('manager_property.places-schools-title', 'Schools &amp; Universities') }}</h5>
        
        <?php
        $_list = array_get($places, 'school');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ tr('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ tr('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ tr('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ tr('manager_property.places-hospital-title', 'Hospitals &amp; Pharmacies') }}</h5>
        
        <?php
        $_list = array_get($places, 'hospital');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ tr('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ tr('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ tr('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ tr('manager_property.places-trains-title', 'Train stations') }}</h5>
        
        <?php
        $_list = array_merge(array_get($places, 'bts', []), array_get($places, 'mrt', []), array_get($places, 'apl', []));

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ tr('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ tr('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ tr('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

    </div>
</div>

@endsection