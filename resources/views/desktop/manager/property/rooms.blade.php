<?php

$rooms = $property->getRoomTypes();

$i = 1;

foreach ($rooms as $room) {
    ?>
    <div class="form-group room-row-{{ $i }}">
        <label for="inputEmail3" class="col-sm-2 control-label">{{ tr('manager_property.room-type', 'Type #') }} {{ $i }}</label>
        <div class="col-sm-2">
            <input type="text" class="form-control room_name" id="room_name_{{ $room->id }}" name="name" value="{{ $room->name }}" 
                   placeholder="{{ tr('manager_property.room-name', 'Room name') }}" onkeyup="showUpdate('#update-button-{{ $i }}')">
        </div>
        <div class="col-sm-2">
            <input type="text" class="form-control room_size" id="room_size_{{ $room->id }}" name="size" value="{{ $room->size }}" 
                   placeholder="{{ tr('manager_property.room-size', 'Size (sqm)') }}" onkeyup="showUpdate('#update-button-{{ $i }}')">
        </div>
        <div class="col-sm-2">
            <button type="button" id="update-button-{{ $i }}" class="btn btn-room text-primary" onclick="updateRoom({{ $room->id }})" style="display: none;"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-room text-danger" onclick="deleteRoomType({{ $room->id }})"><i class="fa fa-trash"></i></button>
        </div>

    </div>
    <?php
    $i++;
}
?>