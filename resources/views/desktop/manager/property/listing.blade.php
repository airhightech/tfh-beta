<h3>{{ $title }} ({{ $properties->total() }})</h3>

<?php
if (isset($show_filter)) {
    ?>
    <table class="table table-filter">
        <tr>
            <td class="filter-label">{{ tr('manager_listing.filter-type', 'Filters') }}</td>
            <td class="filter-all">
                <input type="radio" name="lt" value="all" class="filter" <?php echo array_get($filters, 'lt') == 'all' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td class="filter">
                <input type="radio" name="lt" value="rent" 
                       class="filter" <?php echo array_get($filters, 'lt') == 'rent' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-type-rent', 'Rent') }}
            </td>
            <td class="filter">
                <input type="radio" name="lt" value="sale" 
                       class="filter" <?php echo array_get($filters, 'lt') == 'sale' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-type-sale', 'Sale') }}
            </td>
            <td class="filter"></td>
            <td class="filter"></td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-property', 'Property type') }}
            </td>
            <td>
                <input type="radio" name="pt" value="all" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'all' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="pt" value="cd" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'cd' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-condo', 'Condo') }}
            </td>
            <td>
                <input type="radio" name="pt" value="th" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'th' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-townhouse', 'Townhouse') }}
            </td>
            <td>
                <input type="radio" name="pt" value="sh" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'sh' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-house', 'Single house') }}
            </td>
            <td>
                <input type="radio" name="pt" value="ap" 
                       class="filter" <?php echo array_get($filters, 'property') == 'ap' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-apartment', 'Apartment') }}
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-class', 'Class') }}
            </td>
            <td>
                <input type="radio" name="class" value="all" 
                       class="filter" <?php echo array_get($filters, 'class') == 'all' ? 'checked' : ''; ?> >
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="class" value="standard" 
                       class="filter" <?php echo array_get($filters, 'class') == 'standard' ? 'checked' : ''; ?> >
                {{ tr('manager_listing.filter-class-standard', 'Standard') }}
            </td>
            <td>
                <input type="radio" name="class" value="featured" 
                       class="filter" <?php echo array_get($filters, 'class') == 'featured' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-class-featured', 'Featured') }}
            </td>
            <td>
                <input type="radio" name="class" value="exclusive" 
                       class="filter" <?php echo array_get($filters, 'class') == 'exclusive' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-class-exclusive', 'Exclusive') }}
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-status', 'Status') }}
            </td>
            <td>
                <input type="radio" name="status" value="all" 
                       class="filter" <?php echo array_get($filters, 'status') == 'all' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="status" value="expired" 
                       class="filter" <?php echo array_get($filters, 'status') == 'expired' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-status-expired', 'Expired') }}
            </td>
            <td>
                <input type="radio" name="status" value="present" 
                       class="filter" <?php echo array_get($filters, 'status') == 'present' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-status-present', 'Present') }}
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <?php
}
?>

<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation" class="input">  
            <form class="form-inline" method="get" action="/manager/{{ $action_link }}/list">
                <div class="form-group">
                    <input type="text" name="q" value="{{ $request->input('q') }}" 
                           class="form-control search" 
                           placeholder="{{ tr('manager_property.search-placeholder', 'Search property by name') }}"/>
                </div>
                <button type="submit" class="btn">{{ tr('button.search', 'Search') }}</button>
            </form>
        </li>

<?php
$q = trim($request->input('q'));
if (!empty($q)) {
    ?>
            <li role="presentation">
                <a href="/manager/{{ $action_link }}/list">{{ tr('manager_account.reset-search', 'Reset search') }}</a>
            </li>
    <?php
}
?>

    </ul>
</div>
<table class="table table-list">
    <thead>
        <tr>
            <th class="text-center">{{ tr('manager_project.id-col', 'ID') }}</th>
            <th>{{ tr('manager_project.type-col', 'Type') }}</th>
            <th>{{ tr('manager_project.price-col', 'Price') }}</th>
            <th>{{ tr('manager_project.property-col', 'Property') }}</th>
            <th class="small-col">{{ tr('manager_project.class-col', 'Class') }}</th>
            <th class="small-col text-center">{{ tr('manager_project.views-col', 'Views') }}</th>
            <th class="text-center">{{ tr('manager_project.start-col', 'Start') }}</th>
            <th class="text-center">{{ tr('manager_project.start-col', 'End') }}</th>
            <th class="icon-col">{{ tr('manager_project.active-col', 'Active') }}</th>
            <th class="icon-col"></th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
<?php
foreach ($properties as $property) {
    $listing = $property->getListingData();
    $address = $property->getAddress();

    $province = null;

    if ($address) {
        $province = $address->getProvince();
    }   
    ?>
            <tr>
                <td class="text-center">
                    <a href="/property/{{ $property->id }}" target="_blank">
                        {{ $property->id }}
                    </a>
                </td><!-- id -->
                <td>{{ $listing->getTypeName() }}</td><!-- type -->
                <td class="text-right">฿ {{ number_format($listing->listing_price) }}</td><!-- price -->
                <td>{{ $property->getTranslatable('name') }}</td><!-- province -->
                <td>{{ ucfirst($listing->listing_class) }}</td><!-- class -->
                <td class="text-center">{{ $property->clicks + 0 }}</td>
                <td class="text-center">{{ date('d/m/Y', strtotime($property->publication_date)) }}</td><!-- start -->    
                <td class="text-center">{{ date('d/m/Y', strtotime($listing->expiration_date)) }}</td><!-- start --> 
                <td class="text-center">
                    <input type="checkbox" name="published" value="YES" <?php echo $property->published ? 'checked' : ''; ?> 
                           class="listing-visibility" data-id="{{ $property->id }}" onclick="togglePropertyStatus({{ $property->id }})"/>
                </td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-white edit-listing" data-id="{{ $property->id }}">
                        <i class="fa fa-pencil"></i>
                    </button>
                </td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-danger delete-listing" data-id="{{ $property->id }}">
                        <i class="fa fa-times"></i>
                    </button>
                </td>
            </tr>
    <?php
}
?>
    </tbody>
</table>

{{ $properties->appends($request->all())->links() }}

<div id="listing-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body modal-listing">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-primary" 
                        onclick="submitGenericForm('listing-update-form')">{{ tr('button.update', 'UPDATE') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div id="listing-modal-delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body modal-listing-delete">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-danger" 
                        onclick="submitGenericForm('listing-delete-form')">{{ tr('button.update', 'DELETE') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>