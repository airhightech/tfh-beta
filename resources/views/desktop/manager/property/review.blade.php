
@section('title', 'Page Title')

<?php
$tab = 'reviews';
$review = $property->getManagerReview();
?>

<h3>{{ $title }} : {{ $property->getTranslatable('name') }}</h3>

@include('desktop.manager.property.tab')

<h4 class="blue">{{ tr('manager_project.review.how-to-get-there-title', 'How to get there')  }}</h4>

<div class="review-media-list">
    <?php
    $images1 = $review->getLocationImages();

    if (count($images1)) {
        foreach ($images1 as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="#" class="rm-media" onclick="showDeleteImage('#delete-{{ $image->id }}');">
                    <i class="fa fa-times"></i>
                </a>
                <div id="delete-{{ $image->id }}" class="rm-links">
                    <p>{{ tr('manager_property.delete-image', 'Delete ?') }}</p>
                    <a href="/rest/property/delete-image-review/{{ $image->id }}" class="text-danger silent-link">
                        {{ tr('manager_property.delete-image-yes', 'YES') }}
                    </a>
                    <a href="#" class="text-primary"
                       onclick="cancelDeleteImage('#delete-{{ $image->id }}');">{{ tr('manager_property.delete-image-no', 'NO') }}</a>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>

<div id="location-preview" class="media-preview">
</div>
<div id="size-error-1" class="alert alert-danger size-error hidden-error text-center">
    {{ tr('media.image-size-too-big', 'Image size are too big, please respect max size') }} : {{ max_size() }}<br/>
    {{ tr('media.send-image-one-by-one', 'You can also send the image one by one') }}
</div>

<div class="progress push-below">
    <div id="progress-1" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>
<form id="upload-location-images" method="post"
      action="/rest/property/review-upload" data-progress="#progress-1">
    <div class="row">
        <div class="col-md-4">
            <label class="btn btn-sm btn-default upload-trigger" 
                   data-target="#location-image">
                {{ tr('manager_project.review.add-image', 'SELECT IMAGES') }}       
                <input type="file" id="location-image" name="images[]" class="hidden image-input" multiple
                       data-target="#location-preview" data-error="size-error-1"/>
                {{ csrf_field() }}
                <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" />
                <input type="hidden" name="id" value="{{ $review->id }}"/>
                <input type="hidden" name="domain" value="location"/>
            </label> 
            <p class="text-danger">(JPEG or PNG only)<br/>
            Max size: {{ max_size() }}</p>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-sm btn-primary" 
                    onclick="submitGenericForm('upload-location-images')">
                {{ tr('button.send-image', 'UPLOAD') }}
            </button>
        </div>
    </div>
</form>

<h5>{{ tr('manager_project.review.details-title', 'Details')  }}</h5>

<div class="review-form-container push-below row">
    <form id="location-review" action="/rest/property/review" method="post" class="col-md-8">        
        
        {{ __trfields('comment_loc', $review, true) }}
        
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $review->id }}"/>
        <button type="button" class="btn btn-primary"
                onclick="submitGenericForm('location-review')">{{ tr('button.submit', 'Submit') }}</button>
    </form>
</div>

<h4 class="blue">{{ tr('manager_project.review.facility-landscape-title', 'Facility landscape')  }}</h4>

<div class="review-media-list">
    <?php
    $images2 = $review->getFacilityImages();

    if (count($images2)) {
        foreach ($images2 as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="#" class="rm-media" onclick="showDeleteImage('#delete-{{ $image->id }}');">
                    <i class="fa fa-times"></i>
                </a>
                <div id="delete-{{ $image->id }}" class="rm-links">
                    <p>{{ tr('manager_property.delete-image', 'Delete ?') }}</p>
                    <a href="/rest/property/delete-image-review/{{ $image->id }}" class="text-danger silent-link">
                        {{ tr('manager_property.delete-image-yes', 'YES') }}
                    </a>
                    <a href="#" class="text-primary"
                       onclick="cancelDeleteImage('#delete-{{ $image->id }}');">{{ tr('manager_property.delete-image-no', 'NO') }}</a>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>

<div id="facility-preview" class="media-preview">
</div>
<div id="size-error-2" class="alert alert-danger size-error hidden-error text-center">
    {{ tr('media.image-size-too-big', 'Image size are too big, please respect max size') }} : {{ max_size() }}<br/>
    {{ tr('media.send-image-one-by-one', 'You can also send the image one by one') }}
</div>
<div class="progress push-below">
    <div id="progress-2" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>

<form id="upload-facility-images" method="post"
      action="/rest/property/review-upload" data-progress="#progress-2">
    <div class="row">
        <div class="col-md-4">
            <label class="btn btn-sm btn-default upload-trigger" 
                   data-target="#location-image">
                {{ tr('manager_project.review.add-image', 'SELECT IMAGES') }}        
                <input type="file" id="location-image" name="images[]" class="hidden image-input" multiple
                       data-target="#facility-preview" data-error="size-error-2"/>
                {{ csrf_field() }}
                <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" />
                <input type="hidden" name="id" value="{{ $review->id }}"/>
                <input type="hidden" name="domain" value="facility"/>
            </label> 
            <p class="text-danger">(JPEG or PNG only)<br/>
            Max size: {{ max_size() }}</p>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-sm btn-primary" 
                    onclick="submitGenericForm('upload-facility-images')">
                {{ tr('button.send-image', 'UPLOAD') }}
            </button>
        </div>
    </div>
</form>

<h5>{{ tr('manager_project.review.details-title', 'Details')  }}</h5>

<div class="review-form-container push-below row">
    <form id="facility-review" action="/rest/property/review" method="post" class="col-md-8">        
        
        {{ __trfields('comment_fac', $review, true) }}
        
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $review->id }}"/>
        <button type="button" class="btn btn-primary"
                onclick="submitGenericForm('facility-review')">{{ tr('button.submit', 'Submit') }}</button>
    </form>
</div>

<h4 class="blue">{{ tr('manager_project.review.floor-plan-title', 'Floorplan')  }}</h4>

<div class="review-media-list">
    <?php
    $images3 = $review->getFloorplanImages();

    if (count($images3)) {
        foreach ($images3 as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="#" class="rm-media" onclick="showDeleteImage('#delete-{{ $image->id }}');">
                    <i class="fa fa-times"></i>
                </a>
                <div id="delete-{{ $image->id }}" class="rm-links">
                    <p>{{ tr('manager_property.delete-image', 'Delete ?') }}</p>
                    <a href="/rest/property/delete-image-review/{{ $image->id }}" class="text-danger silent-link">
                        {{ tr('manager_property.delete-image-yes', 'YES') }}
                    </a>
                    <a href="#" class="text-primary"
                       onclick="cancelDeleteImage('#delete-{{ $image->id }}');">{{ tr('manager_property.delete-image-no', 'NO') }}</a>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>

<div id="floorplan-preview" class="media-preview">
</div>

<div id="size-error-3" class="alert alert-danger size-error hidden-error text-center">
    {{ tr('media.image-size-too-big', 'Image size are too big, please respect max size') }} : {{ max_size() }}<br/>
    {{ tr('media.send-image-one-by-one', 'You can also send the image one by one') }}
</div>

<div class="progress push-below">
    <div id="progress-3" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>

<form id="upload-floorplan-images" method="post"
      action="/rest/property/review-upload" data-progress="#progress-2">
    <div class="row">
        <div class="col-md-4">
            <label class="btn btn-sm btn-default upload-trigger" 
                   data-target="#floorplan-image">
                {{ tr('manager_project.review.add-image', 'SELECT IMAGES') }}       
                <input type="file" id="location-image" name="images[]" class="hidden image-input" multiple
                       data-target="#floorplan-preview" data-error="size-error-3"/>
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $review->id }}"/>
                <input type="hidden" name="domain" value="floorplan"/>
            </label> 
            <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" />
            <p class="text-danger">(JPEG or PNG only)<br/>
            Max size: {{ max_size() }}</p>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-sm btn-primary" 
                    onclick="submitGenericForm('upload-floorplan-images')">
                {{ tr('button.send-image', 'UPLOAD') }}
            </button>
        </div>
    </div>
</form>

<h5>{{ tr('manager_project.review.details-title', 'Details')  }}</h5>

<div class="review-form-container push-below row">
    <form id="floorplan-review" action="/rest/property/review" method="post" class="col-md-8">        
        
        {{ __trfields('comment_pla', $review, true) }}
        
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $review->id }}"/>
        <button type="button" class="btn btn-primary"
                onclick="submitGenericForm('floorplan-review')">{{ tr('button.submit', 'Submit') }}</button>
    </form>
</div>

<h4 class="blue">{{ tr('manager_project.review.video-title', 'Video')  }}</h4>

<div class="review-form-container push-below row">
    <form id="video-review" action="/rest/property/review" method="post" class="col-md-8">        
        
        <div class="form-group">
            <input type="text" class="form-control" name="video_link" value="{{ $review->video_link }}">
        </div>
        
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $review->id }}"/>
        <button type="button" class="btn btn-primary"
                onclick="submitGenericForm('video-review')">{{ tr('button.submit', 'Submit') }}</button>
    </form>
</div>

<h4 class="blue">{{ tr('manager_project.review.overview-title', 'Overview')  }}</h4>

<div class="review-form-container push-below row">
    <form id="overview-review" action="/rest/property/review" method="post" class="col-md-8">        
        
        {{ __trfields('overview', $review, true) }}
        
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $review->id }}"/>
        <button type="button" class="btn btn-primary"
                onclick="submitGenericForm('overview-review')">{{ tr('button.submit', 'Submit') }}</button>
    </form>
</div>

@section('scripts')
<script src="{{ __asset('/js/manager/uploader.js') }}"></script>
<script src="{{ __asset('/js/manager/review.js') }}"></script>
@endsection