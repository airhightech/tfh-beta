@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'house-project';
$title = tr('manager_property.house-project-create', 'Create new house project');

$create = true;
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.create')

@endsection