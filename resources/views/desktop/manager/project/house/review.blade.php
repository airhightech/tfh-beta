@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'house-project';
$title = tr('manager_property.house-project-review', 'House review');
$tab_link = 'project/house';

?>

@section('content')

@include('desktop.manager.property.review')

@endsection