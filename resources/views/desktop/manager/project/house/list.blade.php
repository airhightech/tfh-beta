@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'house-project';
$title = tr('manager_property.house-projects', 'House projects');
$create_text = tr('manager_property.house-create', 'Create new house');
$action_link = 'project/house';
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.list')

@endsection