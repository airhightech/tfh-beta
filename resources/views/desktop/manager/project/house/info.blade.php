@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'house-project';
$title = tr('manager_property.house-project-info', 'House info');
$tab_link = 'project/house';

$new_project = true;

?>

@section('content')

@include('desktop.manager.property.info')

@endsection