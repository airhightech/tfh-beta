@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-project';
$title = tr('manager_property.townhouse-project-places', 'Townhouse places');
$tab_link = 'project/townhouse';

?>

@section('content')

@include('desktop.manager.property.places')

@endsection