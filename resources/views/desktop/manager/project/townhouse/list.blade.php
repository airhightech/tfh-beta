@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-project';
$title = tr('manager_property.townhouse-projects', 'Townhouse projects');
$create_text = tr('manager_property.townhouse-create', 'Create new Townhouse');
$action_link = 'project/townhouse';
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.list')

@endsection