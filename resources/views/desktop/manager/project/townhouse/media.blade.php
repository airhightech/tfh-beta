@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-project';
$title = tr('manager_property.townhouse-project-media', 'Townhouse Media');
$tab_link = 'project/townhouse';

?>

@section('content')

@include('desktop.manager.property.media')

@endsection