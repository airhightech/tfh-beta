@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-project';
$title = tr('manager_property.townhouse-project-details', 'Townhouse details');
$tab_link = 'project/townhouse';

?>

@section('content')

@include('desktop.manager.property.details')

@endsection