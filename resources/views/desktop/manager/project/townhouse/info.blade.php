@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-project';
$title = tr('manager_property.townhouse-project-info', 'Townhouse info');
$tab_link = 'project/townhouse';

$new_project = true;

?>

@section('content')

@include('desktop.manager.property.info')

@endsection