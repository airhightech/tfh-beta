@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-project';
$title = tr('manager_property.townhouse-project-create', 'Create new townhouse project');

$create = true;
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.create')

@endsection