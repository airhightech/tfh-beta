@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-project';
$title = tr('manager_property.apartment-project-info', 'Apartment info');
$tab_link = 'project/apartment';
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.info')

@endsection