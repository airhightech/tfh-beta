@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-project';
$title = tr('manager_property.apartment-project-media', 'Apartment Media');
$tab_link = 'project/apartment';

?>

@section('content')

@include('desktop.manager.property.media')

@endsection