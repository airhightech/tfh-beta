@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-project';
$title = tr('manager_property.apartment-projects', 'Apartment projects');
$action_link = 'project/apartment';
$create_text = tr('manager_property.apartment-create', 'Create new apartment');
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.list')

@endsection