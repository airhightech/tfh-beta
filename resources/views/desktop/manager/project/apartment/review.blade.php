@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-project';
$title = tr('manager_property.apartment-project-review', 'Apartment review');
$tab_link = 'project/apartment';

?>

@section('content')

@include('desktop.manager.property.review')

@endsection