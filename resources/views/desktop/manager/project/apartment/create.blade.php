@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-project';
$title = tr('manager_property.apartment-project-create', 'Create new apartment project');

$create = true;
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.create')

@endsection