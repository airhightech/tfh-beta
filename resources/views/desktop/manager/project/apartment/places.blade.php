@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-project';
$title = tr('manager_property.apartment-project-places', 'Apartment places');
$tab_link = 'project/apartment';

?>

@section('content')

@include('desktop.manager.property.places')

@endsection