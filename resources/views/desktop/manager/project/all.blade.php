@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'all-project';
$title = tr('manager_property.apartment-projects', 'All projects');
$action_link = 'project';
$create_text = false;
$new_project = true;
$show_filter = true;

?>

@section('content')

@include('desktop.manager.property.list')

@endsection