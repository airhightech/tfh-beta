@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-project';
$title = tr('manager_property.info-condo-project', 'Condo info');
$tab_link = 'project/condo';
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.info')

@endsection