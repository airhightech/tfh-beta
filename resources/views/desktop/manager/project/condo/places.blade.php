@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-project';
$title = tr('manager_property.condo-place-title', 'Condo places');
$tab_link = 'project/condo';

?>

@section('content')

@include('desktop.manager.property.places')

@endsection