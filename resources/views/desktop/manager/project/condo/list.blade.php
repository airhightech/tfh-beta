@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-project';
$title = tr('manager_property.condo-project-list', 'Condo projects');
$action_link = 'project/condo';
$create_text = tr('manager_property.condo-create', 'Create new condo');
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.list')

@endsection