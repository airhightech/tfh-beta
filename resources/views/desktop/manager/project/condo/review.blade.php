@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-project';
$title = tr('manager_property.condo-review-title', 'Condo review');
$tab_link = 'project/condo';

?>

@section('content')

@include('desktop.manager.property.review')

@endsection