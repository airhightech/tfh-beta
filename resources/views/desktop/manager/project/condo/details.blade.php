@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-project';
$title = tr('manager_property.create-condo-community', 'Create new condo');
$tab_link = 'project/condo';

?>

@section('content')

@include('desktop.manager.property.details')

@endsection