@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-project';
$title = tr('manager_property.condo-project-create', 'Create new condo project');

$create = true;
$new_project = true;

?>

@section('content')

@include('desktop.manager.property.create')

@endsection
