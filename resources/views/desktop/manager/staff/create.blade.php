<h4>{{ tr('manager_staff.create-title', 'Create a staff') }}</h4>
<form id="create-staff-form" action="/manager/staff/create" method="post">
    <div class="form-group">
        <label for="name">{{ tr('manager_staff.name-label', 'Name') }}</label>
        <input type="text" class="form-control" id="name" name="name" value="">
    </div>
    <div class="form-group">
        <label for="email">{{ tr('manager_staff.email-label', 'Email') }}</label>
        <input type="text" class="form-control" id="email" name="email" value="">
    </div>
    <div class="form-group">
        <label for="password">{{ tr('visitor_registration.owner.password', 'Password') }}</label>
        <input type="password" class="form-control" id="password" name="password" value="">
    </div>
    <div class="form-group">
        <label for="password_confirmation">{{ tr('visitor_registration.owner.password-confirmation', 'Re-password') }}</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="">
    </div>
    <div class="form-group">
        <label for="email">{{ tr('manager_staff.role-label', 'Role') }}</label>
        <select class="form-control" id="role" name="role">
            <?php
                foreach($roles as $role => $name) {
                    ?>
                    <option value="{{ $role }}">{{ $name }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
    <div class="alert alert-danger error-container" style="display: none;">
        {{ tr('manager_staff.user-exists', 'User exists') }}
    </div>
</form>