<div class="alert alert-danger">
    <p>
        Delete <strong>{{ $user->name }}</strong> ?
    </p>
    <p>
        <a href="/manager/staff/remove/{{ $user->id }}" class="btn btn-danger">{{ tr('form.delete', 'Delete') }}</a>
    </p>
</div>