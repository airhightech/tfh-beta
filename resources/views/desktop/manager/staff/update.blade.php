<h4>{{ tr('manager_staff.update-title', 'Update staff') }}</h4>
<form id="update-staff-form" action="/manager/staff/update/{{ $user->id }}" method="post">
    <div class="form-group">
        <label for="name">{{ tr('manager_staff.name-label', 'Name') }}</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
    </div>
    <div class="form-group">
        <label for="email">{{ tr('manager_staff.email-label', 'Email') }}</label>
        <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" readonly>
    </div>
    <div class="form-group">
        <label for="email">{{ tr('manager_staff.role-label', 'Role') }}</label>
        <select class="form-control" id="role" name="role">
            <?php
                foreach($roles as $role => $name) {
                    ?>
                    <option value="{{ $role }}" <?php echo $user->ctype == $role ? 'selected' : ''; ?> >{{ $name }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
</form>