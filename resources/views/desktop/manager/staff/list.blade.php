@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ tr('manager_staff.list-title', 'List of staff') }}</h3>
<div class="toolbar">

    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <button type="button" class="btn btn-sm btn-blue" data-toggle="modal" data-target="#create-staff">
                {{ tr('manager_staff.create', 'Create') }}
            </button>
        </li>
    </ul>

</div>

<table class="table table-list">
    <thead>
        <tr>            
            <th class="tiny-col text-center">ID</th>
            <th>
                <?php
                sortable_col(
                        tr('manager_staff.name-col', 'Name'), '/manager/staff/list', 'name', 'asc', $sortby
                );
                ?>
            </th>
            <th class="medium-col">
                <?php
                sortable_col(
                        tr('manager_staff.role-col', 'Role'), '/manager/staff/list', 'role', 'asc', $sortby
                );
                ?>
            </th>
            <th class="icon-col"></th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
<?php
foreach ($staffs as $user) {
   //$user = $staff->getUser();
    ?>
            <tr>
                <td class="text-center">{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ tr('manager_staff.role-' . $user->ctype, $user->ctype) }}</td>
                <td class="text-center">
                    <button class="btn btn-xs btn-white" 
                            data-toggle="modal" data-target="#update-staff" data-id="{{ $user->id }}">
                        <i class="fa fa-cog"></i>
                    </button>
                </td>
                <td class="text-center">
                    <button class="btn btn-xs btn-white" 
                            data-toggle="modal" data-target="#delete-staff" data-id="{{ $user->id }}">
                        <i class="fa fa-times text-danger"></i>
                    </button>
                </td>
            </tr>
    <?php
}
?>
    </tbody>
</table>

{{ $staffs->links() }}

<div class="modal fade model-modal" id="create-staff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div id="create-body" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('form.close', 'Close') }}</button>
                <button type="button" class="btn btn-primary" onclick="submitGenericForm('create-staff-form');">{{ tr('form.create', 'Create') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade model-modal" id="update-staff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div id="update-body" class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('form.close', 'Close') }}</button>
                <button type="button" class="btn btn-primary" onclick="submitGenericForm('update-staff-form');">{{ tr('form.update', 'Update') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade model-modal" id="delete-staff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div id="delete-body" class="modal-body">
                ...
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>

    window.onAjaxFormSuccess = function (data, xhr) {
        window.location = '?resort=true';
    };
    
    window.onAjaxFormError = function (xhr) {
        if (xhr.status === 422) {
            $('.error-container').show();
        }
    };

    $(function () {
        
        $('#create-staff').on('show.bs.modal', function (e) {
            $('#create-body').load('/manager/staff/create');
        });

        $('#update-staff').on('show.bs.modal', function (event) {
            var id = $(event.relatedTarget).data('id');
            $('#update-body').load('/manager/staff/update/' + id);
        });
        
        $('#delete-staff').on('show.bs.modal', function (event) {
            var id = $(event.relatedTarget).data('id');
            $('#delete-body').load('/manager/staff/delete/' + id);
        });
        
    });
</script>
@endsection