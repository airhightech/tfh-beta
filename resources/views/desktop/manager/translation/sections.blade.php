@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')

<?php
$active = 'translation';

$langs = \App\Helpers\Language::getActives();
?>

<h3>{{ tr('manager.translation.list-title', 'Translation list') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills"> 
        <li role="presentation" style="min-width: 100px;">
            <span class="input-label">{{ tr('manager.translation.select-section', 'Select a section') }}</span>
        </li>
        <li role="presentation" class="input"> 
            <select class="form-control" id="section" name="section" style="min-width: 250px;">
                <option value="">{{ tr('manager.translation.select-placeholder', 'Select a section/page') }}</option>
                <?php
                foreach ($sections as $section) {
                    ?>
                    <option value="{{ $section }}" <?php echo $currentSection == $section ? 'selected' : ''; ?> >{{ trans('__section.' . $section) }}</option>
                    <?php
                }
                ?>
            </select>

        </li>
        <?php
        if ($currentSection) {
            ?>
            <li role="presentation" class="input pull-right">
                <a href="/manager/translation/publish?section={{ $currentSection }}" class="btn btn-sm btn-primary">
                    {{ tr('manager_translation.publish-changes', 'Publish changes') }}
                </a>
            </li>
            <?php
        }
        ?>
    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <?php
            foreach ($langs as $code => $lang) {
                ?>
                <th class="lang-col text-center">{{ $lang }}</th>
                <?php
            }
            ?>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($translations as $translation) {
            ?>
            <tr>
                <?php
                foreach ($langs as $code => $lang) {
                    $text = json_decode($translation->translation);

                    if ($text->$code) {
                        ?>
                        <td class="lang-col">
                            <div class="trans">
                                {{ $text->$code }}
                            </div>                            
                        </td>
                        <?php
                    } else {
                        ?>
                        <td class="lang-col danger">
                            <div class="trans">
                                {{ $translation->label }}
                            </div>                            
                        </td>
                        <?php
                    }
                }
                ?>
                <td class="text-center">
                    <button class="btn btn-xs btn-white translation-updater" data-id="{{ $translation->id }}"
                            data-toggle="modal" data-target="#translation-modal">
                        <i class="fa fa-language text-primary"></i>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div class="modal fade" id="translation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="save-translation" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>

    $(function () {

        $('#section').on('change', function () {
            window.location = '/manager/translation/list?section=' + $(this).val();
        });

        $('#translation-modal').on('show.bs.modal', function (event) {
            var tid = $(event.relatedTarget).data('id');
            console.log(tid);
            $('.modal-body').html('Loading translation ...');
            $('.modal-body').load('/manager/translation/edit/' + tid);
        });

        $('#save-translation').on('click', function () {

            var form = document.getElementById('translation-form');
            var data = new FormData(form);

            $.ajax({
                url: '/manager/translation/update',
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data, status, xhr) {

                    window.location.reload(true);
                },
                error: function (xhr, status, error) {

                }

            });

        });

    });

</script>
@endsection
