<?php
$active = 'facility';
$img = $feature->getThumbUrl('100x100', true);
?>

@extends('desktop.layouts.manager')


@section('content')

<h3>{{ tr('manager_feature.update-title', 'Update feature') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/feature/list"><i class="fa fa-long-arrow-left"></i> {{ tr('manager_feature.back', 'Back to list of feature') }}</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="clearfix">
            <form id="create-feature" method="post" action="/manager/feature/update">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">
                    <label for="name">{{ tr('manager_feature.name', 'Feature name') }}</label>
                </div>

                <?php echo __trfields('name', $feature); ?>

                <div class="form-group">
                    <label title="Upload image file" for="facility_icon" class="btn btn-default">
                        <input type="file" accept="image/*" name="image" id="facility_icon" class="hide image-input" 
                               data-target="#icon-preview" data-error="size-error">
                        Upload an icon
                    </label>
                </div>

                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $feature->id }}"/>
                <button type="button" class="btn btn-primary pull-right" onclick="submitGenericForm('create-feature');">{{ tr('button.update', 'UPDATE') }}</button>
            </form>
        </div>
    </div>
    <div class="col-md-offset-1 col-md-4">
        <div class="form-group">
            <label for="name">{{ tr('manager_feature.icon-preview', 'Icon preview') }}</label>
        </div>
        <div id="icon-preview" class="icon-preview">
            <?php
            if ($img) {
                ?>
                <img src="{{ $img }}" class="img-responsive"/>
                <?php
            }
            ?>
        </div>
        <div id="size-error" class="alert alert-danger size-error hidden-error text-center">
            {{ tr('media.image-size-too-big', 'Image size are too big, please respect max size') }} : {{ max_size() }}<br/>
            {{ tr('media.send-image-one-by-one', 'You can also send the image one by one') }}
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/uploader.js') }}"></script>
@endsection
