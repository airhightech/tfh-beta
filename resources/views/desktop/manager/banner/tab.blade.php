<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/banner/list">{{ tr('manager_banner.back-to-list', 'Back to banner list') }}</a>
        </li>
    </ul>
</div>

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'info' ? 'class="active"' : ''; ?> >
        <a href="/manager/banner/info/{{ $banner->id }}">{{ tr('manager_banner.info-tab', 'Info') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'image' ? 'class="active"' : ''; ?>>
        <a href="/manager/banner/image/{{ $banner->id }}">{{ tr('manager_banner.image-tab', 'Images') }}</a>
    </li>
</ul>