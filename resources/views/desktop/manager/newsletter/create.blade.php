<?php
$active = 'newsletter';
$langs = \App\Helpers\Language::getActives();
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_notice.create-title', 'Create a newsletter') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/newsletter/list">{{ tr('manager_newsletter.back-link', 'Back to list') }}</a>
        </li>
    </ul>
</div>

<form action="/manager/newsletter/create" method="post" id="newsletter-form">
    <h4><?php echo tr('manager_page.title-label', 'Object'); ?> <span class="text-danger">*</span></h4>
    <?php echo __trfields('subject'); ?>
    <h4><?php echo tr('manager_page.title-label', 'Send at'); ?> <span class="text-danger">*</span></h4>
    <div>
        <div class="input-group">
            <input type="text" class="form-control" id="send_time" name="send_time" value="{{ old('send_time') }}" placeholder="">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    </div>
    <h4><?php echo tr('manager_page.title-label', 'Content'); ?> <span class="text-danger">*</span></h4>
        <div class="form-group">
            <div class="">
                <ul id="mail_body-tab" class="nav nav-tabs" role="tablist">
                    <?php
                    foreach ($langs as $code => $lang) {
                        ?>
                        <li role="presentation">
                            <a href="#details-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab" data-content="#mail_body-{{ $code }}">{{ $lang }}</a>
                        </li>
                        <?php

                    }
                    ?>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <?php
                    foreach ($langs as $code => $lang) {
                        ?>
                        <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}">
                            <div class="notes">{!! $template !!}</div>
                            <textarea id="mail_body-{{ $code }}" name="mail_body[{{ $code }}]" class="hidden">{!! $template !!}</textarea>
                        </div>
                        <?php

                    }
                    ?>
                </div>
            </div>
        </div>
        {{ csrf_field() }}
        <button type="submit" type="button" class="btn btn-primary">{{ tr('button.create', 'CREATE') }}</button>
</from>
@endsection

@section('scripts')
<link href="/libs/summernote/summernote.css" rel="stylesheet">
<script src="/libs/summernote/summernote.js"></script>
<script src="{{ __asset('/js/manager/newsletter.js') }}"></script>
@endsection
