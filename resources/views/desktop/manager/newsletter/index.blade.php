<?php
$active = 'newsletter';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_notice.list-title', 'Newsletter management') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/newsletter/create">{{ tr('manager_newsletter.create-link', 'Create a newsletter') }}</a>
        </li>
    </ul>
</div>

LIST OF NEWSLETTER

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ tr('manager_page.name-col', 'Subject') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>

      <?php
      foreach ($newsletters as $newsletter) {
      ?>
        <tr>
            <td> {{ $newsletter->getTranslatable('subject') }}</td>
            <td class="text-center"><a href="/manager/newsletter/edit?id={{ $newsletter->id }}"><i class="fa fa-pencil"></i></a></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
</table>


@endsection

@section('content')
<script src="{{ __asset('/js/manager/newsletter.js') }}"></script>
@endsection
