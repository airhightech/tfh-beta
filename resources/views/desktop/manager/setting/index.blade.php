@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php 
$tab = 'basic'; 
?>

@section('content')

<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

<div class="manager-content">    

    @include('desktop.manager.setting.tab')
    
    <h4>{{ tr('manager.setting.social-network-title', 'Social network') }}</h4>

    <div class="form-container row">
        
        
        <div class="col-md-7">
            
            <form class="form-horizontal" method="post" action="/manager/setting/update">
                
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="facebook_link" class="col-sm-4">{{ tr('manager.setting.facebook-link-label', 'Facebook link') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="facebook_link" value="{{ array_get($settings, 'facebook_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="instagram_link" class="col-sm-4">{{ tr('manager.setting.instagram-link-label', 'Instagram link') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="instagram_link" value="{{ array_get($settings, 'instagram_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="twitter_link" class="col-sm-4">{{ tr('manager.setting.twitter-link-label', 'Twitter link') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="twitter_link" value="{{ array_get($settings, 'twitter_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="google_link" class="col-sm-4">{{ tr('manager.setting.google-link-label', 'Google link') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="google_link" value="{{ array_get($settings, 'google_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-primary">{{ tr('button.update', 'Update') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
@endsection