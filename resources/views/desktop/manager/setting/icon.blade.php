@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$tab = 'icon';

?>

@section('content')
<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

@include('desktop.manager.setting.tab')

<h4>{{ tr('manager_setting.homepage-icons', 'Homepage icons') }}</h4>

<div class="row">
    <div class="col-md-3">
        <h5>{{ tr('home.hero.search.title', 'SEARCH FASTER') }}</h5>
        <p style="background-color: #555;">
            <img src="{{ __icon('hero_search', '/img/home/searching.png') }}" class="img-responsive">
        </p>
        <form method="post" action="/manager/setting/icon" enctype="multipart/form-data">
            <div class="form-group">
                <label title="Upload image file" for="hero_search_icon" class="btn btn-default">
                    <input id="hero_search_icon" type="file" accept="image/*" name="icon" class="hide image-input">
                    Select an icon
                </label>
                <a href="/manager/setting/icon/delete?code=hero_search" class="btn btn-danger pull-right">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <input type="hidden" name="code" value="hero_search"/>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">{{ tr('button.upload', 'UPLOAD') }}</button>
        </form>
    </div>
    <div class="col-md-3">
        <h5>{{ tr('home.hero.free.title', 'FREE POSTING') }}</h5>
        <p style="background-color: #555;">
            <img src="{{ __icon('hero_free', '/img/home/free.png') }}" class="img-responsive">
        </p>
        <form method="post" action="/manager/setting/icon" enctype="multipart/form-data">
            <div class="form-group">
                <label title="Upload image file" for="hero_free_icon" class="btn btn-default">
                    <input id="hero_free_icon" type="file" accept="image/*" name="icon" class="hide image-input">
                    Select an icon
                </label>
                <a href="/manager/setting/icon/delete?code=hero_free" class="btn btn-danger pull-right">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <input type="hidden" name="code" value="hero_free"/>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">{{ tr('button.upload', 'UPLOAD') }}</button>
        </form>
    </div>
    <div class="col-md-3">
        <h5>{{ tr('home.hero.info.title', 'FULL INFORMATION') }}</h5>
        <p style="background-color: #555;">
            <img src="{{ __icon('hero_info', '/img/home/thumbs-up.png') }}" class="img-responsive">
        </p>
        <form method="post" action="/manager/setting/icon" enctype="multipart/form-data">
            <div class="form-group">
                <label title="Upload image file" for="hero_info_icon" class="btn btn-default">
                    <input id="hero_info_icon" type="file" accept="image/*" name="icon" class="hide image-input">
                    Select an icon
                </label>
                <a href="/manager/setting/icon/delete?code=hero_info" class="btn btn-danger pull-right">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <input type="hidden" name="code" value="hero_info"/>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">{{ tr('button.upload', 'UPLOAD') }}</button>
        </form>
    </div>
    <div class="col-md-3">
        <h5>{{ tr('home.hero.agents.title', 'BEST AGENTS') }}</h5>
        <p style="background-color: #555;">
            <img src="{{ __icon('hero_agents', '/img/home/trophy.png') }}" class="img-responsive">
        </p>
        <form method="post" action="/manager/setting/icon" enctype="multipart/form-data">
            <div class="form-group">
                <label title="Upload image file" for="hero_agents_icon" class="btn btn-default">
                    <input id="hero_agents_icon" type="file" accept="image/*" name="icon" class="hide image-input">
                    Select an icon
                </label>
                <a href="/manager/setting/icon/delete?code=hero_agents" class="btn btn-danger pull-right">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <input type="hidden" name="code" value="hero_agents"/>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">{{ tr('button.upload', 'UPLOAD') }}</button>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/setting.js') }}"></script>
@endsection