

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'basic' ? 'class="active"' : ''; ?> >
        <a href="/manager/setting/index">{{ tr('manager_setting.basic-tab', 'Basic setting') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'contact' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/contact">{{ tr('manager_setting.contact-tab', 'Contact') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'bank' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/bank">{{ tr('manager_setting.bank-tab', 'Bank accounts') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'image' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/image">{{ tr('manager_setting.image-tab', 'Images') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'purchase' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/purchase">{{ tr('manager_setting.purchase-tab', 'Purchase') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'icon' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/icon">{{ tr('manager_setting.icon-tab', 'Icons') }}</a>
    </li>
</ul>