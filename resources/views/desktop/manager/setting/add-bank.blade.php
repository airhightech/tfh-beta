@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php $tab = 'bank'; ?>

@section('content')
<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

<div class="manager-content">
    @include('desktop.manager.setting.tab')

    <div class="toolbar">
        <ul class="nav nav-pills">
            <li role="presentation" class="pull-right">
                <a href="/manager/setting/bank"><i class="fa fa-long-arrow-left"></i> {{ tr('manager.setting.back-bank', 'Back to list') }}</a>
            </li>
        </ul>
    </div>
    <div class="form-container row">
        <div class="col-md-5">
            <form method="post" action="/manager/setting/add-bank">
                <div class="form-group">
                    <label>{{ tr('manager.setting.bank.bank-label', 'Bank') }} <span class="text-danger">*</span></label>
                    <?php
                    $banks = \App\Models\System\Bank::getBankList();
                    ?>
                    <select class="form-control" name="code">
                        <option value="-">{{ tr('manager.setting.bank.bank-select', 'Select a bank') }}</option>
                        <?php
                        foreach ($banks as $code => $bank) {
                            if (old('bank') == $code) {
                                ?>
                                <option value="{{ $code }}" selected>{{ $bank }}</option>
                                <?php
                            } else {
                                ?>
                                <option value="{{ $code }}">{{ $bank }}</option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>{{ tr('manager.setting.bank.name-label', 'Name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label>{{ tr('manager.setting.bank.account-no-label', 'Account #') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="account_no" value="{{ old('account_no') }}">
                </div>
                <div class="form-group">
                    <label>{{ tr('manager.setting.bank.branch-label', 'Branch') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="branch" value="{{ old('branch') }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">{{ tr('button.add-button', 'Add bank') }}</button>
            </form>
        </div>
        <div class="col-md-7">
            <p>
                <span class="text-danger">*</span> {{ tr('form.required-fields-msg', 'Required fields') }}
            </p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection