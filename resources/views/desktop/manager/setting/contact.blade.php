@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'contact';

$langs = \App\Helpers\Language::getActives();
?>

@section('content')
<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

@include('desktop.manager.setting.tab')

<h4>{{ tr('manager_setting.default-contact-message', 'Default contact message') }}</h4>

<form method="post" action="/manager/setting/contact">
    <div class="form-group">
        <div class="">
            <ul id="details-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#details-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php
                $details = $default_message ? json_decode($default_message, true) : [];

                foreach ($langs as $code => $lang) {
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}">
                        <textarea class="form-control" rows="7" name="details[{{ $code }}]">{{ old('address.' . $code, array_get($details, $code)) }}</textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <div class="form-group clearfix">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary pull-right">{{ tr('button.update', 'UPDATE') }}</button>
    </div>
</form>

<h4>{{ tr('manager_setting.default-contact-footer', 'Footer contact') }}</h4>

<form method="post" action="/manager/setting/footer">
    <div class="form-group">
        <div class="">
            <ul id="footer-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#footer-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php
                $details = $footer ? json_decode($footer, true) : [];

                foreach ($langs as $code => $lang) {
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="footer-{{ $code }}">
                        <textarea class="form-control" rows="7" name="footer[{{ $code }}]">{{ old('address.' . $code, array_get($details, $code)) }}</textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <div class="form-group clearfix">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary pull-right">{{ tr('button.update', 'UPDATE') }}</button>
    </div>
</form>

<h4>{{ tr('manager_setting.line-qrcode', 'Line QR-Code') }}</h4>

<div class="row">
    <div class="col-md-9">
        <div class="manager-setting-preview">
            <?php
            if ($line_qrcode) {
                ?>
                <img src="/thumb/400x100/{{ $line_qrcode }}" class="img-responsive"/>
                <a class="delete" href="/manager/setting/image-remove?domain=line_qrcode"><i class="fa fa-times"></i></a>
                <?php
            }
            ?>                        
        </div>
    </div>
    <div class="col-md-3">
        <p class="text-right">
            <label for="line_qrcode" class="btn btn-upload btn-primary">{{ tr('manager_setting.select-an-image', 'Select an image') }}</label>
            <input type="file" id="line_qrcode" name="image" 
                   data-token="line_qrcode" data-progress="#line-progress" class="setting-image hidden"/>
        </p>
    </div>
</div>
<div class="">

</div>

<div class="progress" style="margin-top: 25px;">
    <div id="line-progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"></span>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/setting.js') }}"></script>
<script>

$(function () {
    $('#details-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#details-tab a:first').tab('show');


    $('#footer-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#footer-tab a:first').tab('show');

});
</script>

@endsection
