@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'purchase';
?>

@section('content')

<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

@include('desktop.manager.setting.tab')

<form id="condo-info-form" action="/manager/setting/purchase" method="post">
    <div class="form-container row">
        <div class="col-md-6">
            <h4>{{ tr('manager_setting.featured-step-1-text', 'Featured Step 1') }}</h4>
            {{ __trfields('featured_step_1', $featured_step_1) }}
            <h4>{{ tr('manager_setting.exclusive-step-1-text', 'Exclusive Step 1') }}</h4>
            {{ __trfields('exclusive_step_1', $exclusive_step_1) }}
            
        </div>
        <div class="col-md-6">
            <h4>{{ tr('manager_setting.featured-step-2-text', 'Featured Step 2') }}</h4>
            {{ __trfields('featured_step_2', $featured_step_2) }}
            <h4>{{ tr('manager_setting.exclusive-step-2-text', 'Exclusive Step 2') }}</h4>
            {{ __trfields('exclusive_step_2', $exclusive_step_2) }}
        </div>
        <div class="col-md-12">
            <div class="form-group clearfix">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary pull-right">{{ tr('button.update', 'UPDATE') }}</button>
            </div>
        </div>
    </div>
</form>

@endsection