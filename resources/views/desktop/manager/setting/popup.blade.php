@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'popup';

$langs = \App\Helpers\Language::getActives();
?>

@section('content')
<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

@include('desktop.manager.setting.tab')

<h4>{{ tr('manager_setting.homepage-popup-message', 'Homepage message') }}</h4>

<form method="post" action="/manager/setting/popup">
    <div class="form-group">
        <div class="">
            <ul id="details-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#details-{{ $code }}" aria-controls="settings" role="tab" 
                           data-toggle="tab" data-content="#html-{{ $code }}">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php

                foreach ($langs as $code => $lang) {
                    $html = isset($homepage_popup[$code]) ? $homepage_popup[$code] : '';
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}">
                        <div class="notes">{!! $html !!}</div>
                        <textarea id="html-{{ $code }}" name="popup[{{ $code }}]" class="hidden">{!! $html !!}</textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <div class="form-group clearfix">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary pull-right">{{ tr('button.update', 'UPDATE') }}</button>
    </div>
</form>

@endsection

@section('scripts')
<link href="/libs/summernote/summernote.css" rel="stylesheet">
<script src="/libs/summernote/summernote.js"></script>
<script src="{{ __asset('/js/manager/setting.js') }}"></script>
<script src="{{ __asset('/js/manager/popup-setting.js') }}"></script>
<script>

$(function () {
    $('#details-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('#details-tab a:first').tab('show');

});
</script>

@endsection
