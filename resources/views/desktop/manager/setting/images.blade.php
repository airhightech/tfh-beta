@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'image';

$langs = \App\Helpers\Language::getActives();

$_langs = [
    'rent' => tr('home.options.rent', 'RENT')
    , 'sale' => tr('home.options.sale', 'SALE')
    , 'new_project' => tr('home.options.new-project', 'NEW PROJECT')
    , 'condo' => tr('home.options.condo', 'CONDO')];
?>

@section('content')
<h3>{{ tr('manager.setting.list-title', 'Settings') }}</h3>

@include('desktop.manager.setting.tab')

<h4>{{ tr('manager_setting.homepage-background', 'Homepage background') }}</h4>

<ul id="home-image-tab" class="nav nav-tabs" role="tablist">
    <?php
    foreach ($_langs as $code => $lang) {
        ?>
        <li role="presentation"><a href="#home-image-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a></li>
        <?php
    }
    ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <?php
    $home_image = json_decode($home_image, true);

    foreach ($_langs as $code => $lang) {

        $image = array_get($home_image, $code);
        ?>
        <div role="tabpanel" class="tab-pane" id="home-image-{{ $code }}">
            <div class="row">
                <div class="col-md-9">
                    <div class="manager-setting-preview">
                        <?php
                        if ($image) {
                            ?>
                            <img src="/thumb/400x100/{{ $image }}" class="img-responsive"/>
                            <a class="delete" href="/manager/setting/image-remove?domain=home_image&lang={{ $code }}"><i class="fa fa-times"></i></a>
                            <?php
                        }
                        ?>                        
                    </div>
                </div>
                <div class="col-md-3">
                    <p>
                        <label for="home-input-{{ $code }}" class="btn btn-upload">{{ tr('manager_setting.select-an-image', 'Select an image') }}</label>
                        <input type="file" id="home-input-{{ $code }}" name="image" 
                               data-token="home_image" data-lang="{{ $code }}" data-progress="#home-progress" class="setting-image hidden"/>
                        <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" /> 
                    </p>
                    <p class="text-danger">
                        Image dimension: 2048 x 400<br/> 
                        JPEG or PNG only<br/>
                        Max size: {{ max_size() }}
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="progress" style="margin-top: 25px;">
    <div id="home-progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"></span>
    </div>
</div>

<h4>{{ tr('manager_setting.buy-package', 'Exclusive package images') }}</h4>

<ul id="exclusive-image-tab" class="nav nav-tabs" role="tablist">
    <?php
    foreach ($langs as $code => $lang) {
        ?>
        <li role="presentation">
            <a href="#exclusive-image-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a>
        </li>
        <?php
    }
    ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <?php
    $exclusive_image = json_decode($exclusive_image, true);

    foreach ($langs as $code => $lang) {

        $image = array_get($exclusive_image, $code);
        ?>
        <div role="tabpanel" class="tab-pane" id="exclusive-image-{{ $code }}">
            <div class="row">
                <div class="col-md-9">
                    <div class="manager-setting-preview">
                        <?php
                        if ($image) {
                            ?>
                            <img src="/thumb/400x100/{{ $image }}" class="img-responsive"/>
                            <a class="delete" href="/manager/setting/image-remove?domain=exclusive_image&lang={{ $code }}"><i class="fa fa-times"></i></a>
                            <?php
                        }
                        ?>                        
                    </div>
                </div>
                <div class="col-md-3">
                    <p>
                        <label for="exclusive-input-{{ $code }}" class="btn btn-upload">{{ tr('manager_setting.select-an-image', 'Select an image') }}</label>
                        <input type="file" id="exclusive-input-{{ $code }}" name="image" 
                               data-token="exclusive_image" data-lang="{{ $code }}" data-progress="#exclusive-progress" class="setting-image hidden"/>
                        <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" /> 
                    </p>
                    <p class="text-danger">
                        Image dimension: 940 x 450<br/> 
                        JPEG or PNG only<br/>
                        Max size: {{ max_size() }}
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="progress" style="margin-top: 25px;">
    <div id="exclusive-progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"></span>
    </div>
</div>

<h4>{{ tr('manager_setting.buy-package', 'Featured package images') }}</h4>

<ul id="featured-image-tab" class="nav nav-tabs" role="tablist">
    <?php
    foreach ($langs as $code => $lang) {
        ?>
        <li role="presentation"><a href="#featured-image-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a></li>
        <?php
    }
    ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <?php
    $featured_image = json_decode($featured_image, true);

    foreach ($langs as $code => $lang) {

        $image = array_get($featured_image, $code);
        ?>
        <div role="tabpanel" class="tab-pane" id="featured-image-{{ $code }}">
            <div class="row">
                <div class="col-md-9">
                    <div class="manager-setting-preview">
                        <?php
                        if ($image) {
                            ?>
                            <img src="/thumb/400x100/{{ $image }}" class="img-responsive"/>
                            <a class="delete" href="/manager/setting/image-remove?domain=featured_image&lang={{ $code }}"><i class="fa fa-times"></i></a>
                            <?php
                        }
                        ?>                        
                    </div>
                </div>
                <div class="col-md-3">
                    <p>
                        <label for="featured-input-{{ $code }}" class="btn btn-upload">{{ tr('manager_setting.select-an-image', 'Select an image') }}</label>
                        <input type="file" id="featured-input-{{ $code }}" name="image" 
                               data-token="featured_image" data-lang="{{ $code }}" data-progress="#featured-progress" class="setting-image hidden"/>
                        <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" /> 
                    </p>
                    <p class="text-danger">
                        Image dimension: 940 x 450<br/>
                        JPEG or PNG only<br/>
                        Max size: {{ max_size() }}
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="progress" style="margin-top: 25px;">
    <div id="featured-progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"></span>
    </div>
</div>


<h4>{{ tr('manager_setting.condo-community', 'Condo community') }}</h4>
<p class='manager-help'>
    {{ tr('manager_setting.condo-community-text', 'Condo community image on home page') }}
</p>


<ul id="condo-image-tab" class="nav nav-tabs" role="tablist">
    <?php
    foreach ($langs as $code => $lang) {
        ?>
        <li role="presentation"><a href="#condo-image-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a></li>
        <?php
    }
    ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <?php
    $condo_image = json_decode($condo_image, true);

    foreach ($langs as $code => $lang) {

        $image = array_get($condo_image, $code);
        ?>
        <div role="tabpanel" class="tab-pane" id="condo-image-{{ $code }}">
            <div class="row">
                <div class="col-md-9">
                    <div class="manager-setting-preview">
                        <?php
                        if ($image) {
                            ?>
                            <img src="/thumb/400x100/{{ $image }}" class="img-responsive"/>
                            <a class="delete" href="/manager/setting/image-remove?domain=condo_image&lang={{ $code }}"><i class="fa fa-times"></i></a>
                            <?php
                        }
                        ?>                        
                    </div>
                </div>
                <div class="col-md-3">
                    <p>
                    <label for="condo-input-{{ $code }}" class="btn btn-upload">{{ tr('manager_setting.select-an-image', 'Select an image') }}</label>
                    <input type="file" id="condo-input-{{ $code }}" name="image" 
                           data-token="condo_image" data-lang="{{ $code }}" data-progress="#condo-progress" class="setting-image hidden"/>
                    <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" /> 
                    </p>
                    <p class="text-danger">
                        Image dimension: 992 x 267<br/>
                        JPEG or PNG only<br/>
                        Max size: {{ max_size() }}
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="progress" style="margin-top: 25px;">
    <div id="condo-progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"></span>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/setting.js') }}"></script>
@endsection