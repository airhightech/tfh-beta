@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php $tab = 'bank'; ?>

@section('content')
<h3>{{ tr('manager_setting.list-title', 'Settings') }}</h3>

<div class="manager-content">
    @include('desktop.manager.setting.tab')

    <div class="toolbar">
        <ul class="nav nav-pills">
            <li role="presentation">
                <a href="/manager/setting/add-bank">{{ tr('manager_setting.add-bank', 'Add bank') }}</a>
            </li>
            <li role="presentation" class="pull-right">
                <a href="/manager/setting/bank"><i class="fa fa-long-arrow-left"></i> {{ tr('manager_setting.back-bank', 'Back to list') }}</a>
            </li>
        </ul>
    </div>
    <div class="form-container row">
        <div class="col-md-5">
            <form method="post" action="/manager/setting/update-bank/{{ $bank->id }}">
                <div class="form-group">
                    <label>{{ tr('manager_setting.bank.bank-label', 'Bank') }} <span class="text-danger">*</span></label>
                    <?php
                    $banks = \App\Models\System\Bank::getBankList();
                    ?>
                    <select class="form-control" name="code">
                        <option value="-">{{ tr('manager_setting.bank.bank-select', 'Please select a bank') }}</option>
                        <?php
                        foreach ($banks as $code => $name) {
                            if (old('bank', $bank->code) == $code) {
                                ?>
                                <option value="{{ $code }}" selected>{{ $name }}</option>
                                <?php
                            } else {
                                ?>
                                <option value="{{ $code }}">{{ $name }}</option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>{{ tr('manager_setting.bank.name-label', 'Name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" value="{{ old('name', $bank->name) }}">
                </div>
                <div class="form-group">
                    <label>{{ tr('manager_setting.bank.account-no-label', 'Account #') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="account_no" value="{{ old('account_no', $bank->account_no) }}">
                </div>
                
                <div class="form-group">
                    <label>{{ tr('manager_setting.bank.branch-label', 'Branch') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="branch" value="{{ old('branch', $bank->branch) }}">
                </div>
                
                <p class="text-danger">
                    <span class="text-danger">*</span> {{ tr('form.required-fields-msg', 'Required fields') }}
                </p>
                
                {{ csrf_field() }}
                
                <button type="submit" class="btn btn-primary">{{ tr('button.update-button', 'Update') }}</button>
            </form>
        </div>
        <div class="col-md-offset-2 col-md-5">

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            
            <div class="danger-zone">
                <p>
                    {{ tr('manager_setting.bank.delete-message', 'Delete this bank ?') }}
                </p>
                <a href="/manager/setting/delete-bank/{{ $bank->id }}" class="btn btn-danger">
                    {{ tr('manager_setting.bank.delete-link', 'Delete') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection