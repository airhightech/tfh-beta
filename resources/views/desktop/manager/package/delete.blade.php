@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ tr('manager_package.delete-title', 'Delete package') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/list">{{ tr('manager_package.back-to-list', 'Back to list') }}</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <h3>{{ tr('manager_package.details-title', 'Details') }}</h3>
        <table class="table model-details">
            <tr>
                <td>{{ tr('manager_package.name', 'Name') }}</td>
                <td>{{ $package->getTranslatable('name') }}</td>
            </tr>
            <tr>
                <td>{{ tr('manager_package.type', 'Type') }}</td>
                <td>{{ $package->type }}</td>
            </tr>
            <tr>
                <td>{{ tr('manager_package.listing-count', 'Listing count') }}</td>
                <td>{{ $package->listing_count }}</td>
            </tr>
            <tr>
                <td>{{ tr('manager_package.free-listing-count', 'Free listing count') }}</td>
                <td>{{ $package->free_listing_count }}</td>
            </tr>
            <tr>
                <td>{{ tr('manager_package.normal-price', 'Normal price') }}</td>
                <td>{{ number_format($package->normal_price) }}</td>
            </tr>
            <tr>
                <td>{{ tr('manager_package.discounted-price', 'Discounted price') }}</td>
                <td>{{ number_format($package->discounted_price) }}</td>
            </tr>
        </table>
        <div class="alert alert-danger">
            {{ tr('manager_package.details-confirmation-question', 'Do you really want to delete this package ?') }}
        </div>
        <div>
            <a href="/manager/package/list" class="btn btn-default">{{ tr('button.cancel', 'Cancel') }}</a>
            <a href="/manager/package/do-delete/{{ $package->id }}" class="btn btn-danger pull-right">{{ tr('button.delete', 'Delete') }}</a>
        </div>
    </div>
</div>

@endsection