@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ tr('manager_package.list-title', 'Package list') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/create-featured">{{ tr('manager_package.create-featured', 'Create featured') }}</a>
        </li>
        <li role="presentation">
            <a href="/manager/package/create-exclusive">{{ tr('manager_package.create-exclusive', 'Create exclusive') }}</a>
        </li>
    </ul>
</div>
<table class="table table-list">
    <thead>
        <tr>
            <th class="small-col">{{ tr('manager_package.col-type', 'Type') }}</th>
            <th>{{ tr('manager_package.col-name', 'Name') }}</th>            
            <th class="small-col text-center">{{ tr('manager_package.col-listing', 'Listings') }}</th>
            <th class="small-col text-center">{{ tr('manager_package.col-discount', 'Discount') }}</th>
            <th class="small-col text-right">{{ tr('manager_package.col-price', 'Price') }}</th>
            <th class="small-col text-center">{{ tr('manager_package.col-free-listing', 'Free listings') }}</th>
            <th class="small-col text-center">{{ tr('manager_package.col-period', 'Period') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <?php
    if (count($packages)) {
        foreach ($packages as $package) {
            $discount = (int) (100 * $package->discounted_price / $package->normal_price);
            ?>
            <tr>
                <td>{{ strtoupper($package->type) }}</td>
                <td>
                    <a href="/manager/package/edit-{{ $package->type }}/{{ $package->id }}">{{ $package->getTranslatable('name') }}</a>
                </td>   
                <td class="text-center">
                    {{ number_format($package->listing_count) }}
                </td>
                <td class="text-center">
                    <?php
                        if($discount > 0) {
                            echo (100 - $discount), '%';
                        }
                        else {
                            echo '-';
                        }
                    ?>
                </td>
                <td class="text-right">
                    <?php
                    if ($package->discounted_price > 0) {
                        echo number_format($package->discounted_price);
                    } else {
                        echo number_format($package->normal_price);
                    }
                    ?>
                </td>
                <td class="text-center">
                    {{ number_format($package->free_listing_count) }}
                </td>
                <td class="text-center">
                    {{ $package->duration }} 
                </td>
                <td class="icon-col">
                    <a href="/manager/package/edit-{{ $package->type }}/{{ $package->id }}"><i class="fa fa-pencil"></i></a>
                </td> 
            </tr>
            <?php
        }
    }
    ?>
</table>
@endsection