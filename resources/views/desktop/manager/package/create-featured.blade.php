@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ tr('manager_package.create-featured-title', 'Create featured') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/list">{{ tr('manager_package.back-to-list', 'Back to list') }}</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-container">
            <form method="post" action="/manager/package/save-featured">
                <div class="form-group">
                    <label for="name">{{ tr('manager_package.name', 'Names') }} <span class="text-danger">*</span></label>
                    {{ __trfields('name') }}
                </div>
                <div class="form-group">
                    <label for="listing_count">{{ tr('manager_package.listing-count', 'Listing count') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="listing_count" value="{{ old('listing_count') }}">
                </div>
                <div class="form-group">
                    <label for="normal_price">{{ tr('manager_package.normal-price', 'Normale price') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="normal_price" value="{{ old('normal_price') }}">
                </div>
                <div class="form-group">
                    <label for="discounted_price">{{ tr('manager_package.discounted-price', 'Discounted price') }}</label>
                    <input type="number" class="form-control" name="discounted_price" value="{{ old('discounted_price') }}">
                </div>
                <div class="form-group">
                    <label for="period">{{ tr('manager_package.duration', 'Duration') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="duration" value="{{ old('duration') }}">
                </div>
                <p>
                    <span class="text-danger">*</span> {{ tr('form.required-fields-msg', 'required fields') }}
                </p>
                <button type="submit" class="btn btn-primary">{{ tr('button.create', 'Create') }}</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </div>
</div>

@endsection