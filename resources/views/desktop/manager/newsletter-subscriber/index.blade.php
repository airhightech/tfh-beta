<?php
$active = 'newsletter-subscription';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_notice.list-title', 'NEWSLETTER SUBSCRIPTION') }}</h3>

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ tr('manager_page.name-col', 'Email') }}</th>
            <th>{{ tr('manager_page.name-col', 'Language') }}</th>
        </tr>
    </thead>
    <tbody>

      <?php
      foreach ($subscribers as $subscriber) {
      ?>
        <tr>
            <td> {{ $subscriber->email }}</td>
            <td> {{ $subscriber->prefered_lang }}</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
</table>


@endsection

@section('content')
@endsection
