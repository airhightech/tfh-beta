@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php
$active = 'all-listing';
$title = tr('manager_property.property-listing-all', 'Listing management');
$action_link = 'listing/all';
$create_text = false;
$listing = true;

$show_filter = true;
?>

@section('content')

@include('desktop.manager.property.listing')

@endsection

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet"> 
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script src="{{ __asset('/js/manager/listing.js') }}"></script>

<script>

var updatefilter = function () {

    var ftype = $('.filter[name=lt]:checked').val();
    var fproperty = $('.filter[name=pt]:checked').val();
    var fclass = $('.filter[name=class]:checked').val();
    var fstatus = $('.filter[name=status]:checked').val();

    window.location = '/manager/listing/all?lt=' + ftype + '&pt=' + fproperty + '&class=' + fclass + '&status=' + fstatus;

};


$(function () {
    $('.filter').on('click', function () {

        console.log(this);

        updatefilter();
    });
});
</script>
@endsection