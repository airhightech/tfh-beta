@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'apartment-listing';
$title = tr('manager_property.apartment-listing-list', 'Apartment listings');
$action_link = 'listing/apartment';
$create_text = false;
$listing = true;

?>

@section('content')

@include('desktop.manager.property.listing')

@endsection

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet"> 
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script src="{{ __asset('/js/manager/listing.js') }}"></script>
@endsection