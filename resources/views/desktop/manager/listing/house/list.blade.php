@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'house-listing';
$title = tr('manager_property.house-listing-list', 'House listings');
$action_link = 'house/condo';
$create_text = false;
$listing = true;
?>

@section('content')

@include('desktop.manager.property.listing')

@endsection

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet"> 
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script src="{{ __asset('/js/manager/listing.js') }}"></script>
@endsection