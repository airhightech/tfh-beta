<form id="listing-update-form" action="/rest/property/update-listing" method="post" class="listing-update-form">
    <h4 class="blue">#{{ $property->id }} : {{ $property->getTranslatable('name') }}</h4>
    <div class="form-group">
        <label for="listing_class">{{ tr('manager_property.class-label', 'Listing class') }}</label>
        <select class="form-control" name="listing_class">
            <option value="">-</option>
            <option value="standard" <?php echo $listing->listing_class == 'standard' ? 'selected' : ''; ?> >
                {{ tr('manager_property.class-standard', 'Standard') }}
            </option>
            <option value="featured" <?php echo $listing->listing_class == 'featured' ? 'selected' : ''; ?> >
                {{ tr('manager_property.class-featured', 'Featured') }}
            </option>
            <option value="exclusive" <?php echo $listing->listing_class == 'exclusive' ? 'selected' : ''; ?> >
                {{ tr('manager_property.class-exclusive', 'Exclusive') }}
            </option>
        </select>
    </div>
    <!--    
    <div class="form-group">
            <label for="property_status">{{ tr('manager_property.status-label', 'Status') }}</label>
            <select class="form-control" name="property_status">
                <option value="">-</option>
                <option value="present" <?php echo $property->status == 'present' ? 'selected' : ''; ?>>{{ tr('manager_property.status-present', 'Present') }}</option>
                <option value="expired" <?php echo $property->status == 'expired' ? 'selected' : ''; ?>>{{ tr('manager_property.status-expired', 'Expired') }}</option>
            </select>
        </div>
    -->
    <?php
    if ($property->ptype == 'cd') {
        $building_id = $property->belongsToBuilding();
        ?>
        <div class="form-group">
            <label for="publication_date">{{ tr('manager_property.condo', 'Condo community') }}</label>
            <select class="form-control building_id" id="building_id"  name="building_id">
                <?php
                foreach ($condos as $condo) {
                    ?>
                    <option value="{{ $condo->id }}" <?php echo $condo->id == $building_id ? 'selected' : ''; ?> >
                        <?php echo $condo->getTranslatable('name'); ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <?php
    }
    ?>
    <div class="form-group">
        <label for="publication_date">{{ tr('manager_property.start-label', 'Publication data') }}</label>
        <div class="input-group">
            <input type="text" id="from" name="publication_date" value="{{ date('d/m/Y', strtotime($property->publication_date)) }}" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
        </div>
    </div>
    <div class="form-group">
        <label for="expiration_date">{{ tr('manager_property.end-label', 'Expiration date') }}</label>
        <div class="input-group">
            <input type="text" id="to" name="expiration_date" value="{{ date('d/m/Y', strtotime($listing->expiration_date)) }}" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
        </div>
    </div>



    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $property->id }}"/>
</form>