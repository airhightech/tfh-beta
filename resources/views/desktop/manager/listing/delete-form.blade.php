<form id="listing-delete-form" action="/rest/property/delete-listing" method="post" class="listing-update-form">
    <h4 class="blue">#{{ $property->id }} : {{ $property->getTranslatable('name') }}</h4>
    
    <div class="alert alert-danger">
        {{ tr('manager_property.delete-listing', 'Do you really want to delete this property ?') }}
    </div>



    {{ csrf_field() }}
    <input type="hidden" name="id" value="{{ $property->id }}"/>
</form>