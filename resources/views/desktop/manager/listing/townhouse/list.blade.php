@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'townhouse-listing';
$title = tr('manager_property.townhouse-listing-list', 'Townhouse listings');
$action_link = 'townhouse/condo';
$create_text = false;
$listing = true;
?>

@section('content')

@include('desktop.manager.property.listing')

@endsection

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet"> 
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script src="{{ __asset('/js/manager/listing.js') }}"></script>
@endsection