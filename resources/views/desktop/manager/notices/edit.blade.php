<?php
$active = 'notice';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_notice.create-title', 'Create a notice') }}</h3>

<div class="manager-content">    
    <form id="notice-create-form" action="/rest/notice/update" method="post">
        <div class="form-group">
            <label for="type">{{ tr('manager_notice.type', 'Type') }}</label>
            <input type="text" class="form-control" id="type" name="type" value="{{ $notice->type }}">
        </div>
        <div class="form-group">
            <div class="notice">{!! $notice->message !!}</div>
            <textarea id="message" name="message" class="hidden">{{ $notice->message }}</textarea>
        </div>
        <?php
        if ($notice->sent == false) {
            ?>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="send" value="yes"> {{ tr('manager_notice.send-after-submit', 'Send after submit') }}
                </label>
            </div>
            <?php
        }
        ?>
        <input type="hidden" name="id" value="{{ $notice->id }}"/>
        <button type="button" class="btn btn-primary submit-ajax-form" data-target="notice-create-form">Submit</button>
    </form>
</div>

@endsection

@section('scripts')
<link href="/libs/summernote/summernote.css" rel="stylesheet">
<script src="/libs/summernote/summernote.js"></script>
<script src="{{ __asset('/js/manager/notice.js') }}"></script>
@endsection