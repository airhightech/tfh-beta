<?php
$active = 'notice';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_notice.list-title', 'List of notices') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation" class="pull-right">
            <a href="/manager/notice/create">{{ tr('manager_notice.create-link', 'Create a notice') }}</a>
        </li>

    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="check-col"></th>
            <th class="medium-col">{{ tr('manager_notice.type-col', 'Type') }}</th>
            <th class="tiny-col text-center">{{ tr('manager_notice.date-col', 'Date') }}</th>
            <th>{{ tr('manager_notice.news-col', 'News') }}</th>            
            <th class="check-col"></th>
        </tr>
    </thead>
    <tbody class="sortable">
        <?php
        foreach ($notices as $notice) {
            ?>
            <tr>
                <td class="text-center">
                    <?php
                    if ($notice->sent == false) {
                        ?>
                        <a href="/manager/notice/send/{{ $notice->id }}" class="text-danger"><i class="fa fa-send"></i></a>
                        <?php
                    }
                    ?>
                </td>
                <td>
                    {{ $notice->type }}
                </td>
                <td>
                    <?php
                    echo date('d/m/Y', strtotime($notice->created_at))
                    ?>
                </td>
                <td><div>{!! $notice->message !!}</div></td>
                <td class="text-center">
                    <?php
                    if ($notice->sent == false) {
                        ?>
                        <a href="/manager/notice/edit/{{ $notice->id }}"><i class="fa fa-pencil"></i></a>
                        <?php
                    }
                    ?>
                    
                </td>                
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $notices->links() }}

@endsection