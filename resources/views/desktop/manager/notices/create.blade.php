<?php
$active = 'notice';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_notice.create-title', 'Create a notice') }}</h3>

<div class="manager-content">    
    <form id="notice-create-form" action="/rest/notice/create" method="post">
        <div class="form-group">
            <label for="type">{{ tr('manager_notice.type', 'Type') }}</label>
            <input type="text" class="form-control" id="type" name="type" value="">
        </div>
        <div class="form-group">
            <div class="notice"></div>
            <textarea id="message" name="message" class="hidden"></textarea>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="send" value="yes"> {{ tr('manager_notice.send-after-submit', 'Send after submit') }}
            </label>
        </div>
        <button type="button" class="btn btn-primary submit-ajax-form" data-target="notice-create-form">Submit</button>
    </form>
</div>

@endsection

@section('scripts')
<link href="/libs/summernote/summernote.css" rel="stylesheet">
<script src="/libs/summernote/summernote.js"></script>
<script src="{{ __asset('/js/manager/notice.js') }}"></script>
@endsection