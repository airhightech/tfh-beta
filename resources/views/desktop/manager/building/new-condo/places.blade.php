@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'new-condo-community';
$title = tr('manager_property.new-condo-places', 'New condo places');
$tab_link = 'new-condo';

?>

@section('content')

@include('desktop.manager.building.generic.places')

@endsection