@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-community';
$title = tr('manager_property.create-condo-community', 'Create new condo');

$create = true;
$ptype = 'cdb';

?>

@section('content')

@include('desktop.manager.building.generic.create')

@endsection