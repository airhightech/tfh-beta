@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'new-condo-community';
$title = tr('manager_property.create-condo-community', 'New Condo community');
$action_link = 'new-condo';
$create_text = false;

?>

@section('content')

@include('desktop.manager.building.generic.list')

@endsection