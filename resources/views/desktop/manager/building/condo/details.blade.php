@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-community';
$title = tr('manager_property.condo-details', 'Condo details');
$tab_link = 'condo';

?>

@section('content')

@include('desktop.manager.building.generic.details')

@endsection