@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-community';
$title = tr('manager_property.condo-reviews', 'Condo reviews');
$tab_link = 'condo';

?>

@section('content')

@include('desktop.manager.building.generic.review')

@endsection