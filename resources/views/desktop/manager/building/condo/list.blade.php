@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php

$active = 'condo-community';
$title = tr('manager_property.create-condo-community', 'Condo community');
$action_link = 'condo';
$create_text = tr('manager_property.create-condo-community', 'Create new condo');

?>

@section('content')

@include('desktop.manager.building.generic.list')

@endsection