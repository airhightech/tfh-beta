<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/{{ $tab_link }}/list"><i class="fa fa-long-arrow-left"></i> {{ tr('manager_property.back', 'Back') }}</a>
        </li>
    </ul>
</div>

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'info' ? 'class="active"' : ''; ?> >
        <a href="/manager/{{ $tab_link }}/info/{{ $property->id }}">{{ tr('manager_property.info-tab', "Info") }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'details' ? 'class="active"' : ''; ?>>
        <a href="/manager/{{ $tab_link }}/details/{{ $property->id }}">{{ tr('manager_property.details-tab', 'Details') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'media' ? 'class="active"' : ''; ?>>
        <a href="/manager/{{ $tab_link }}/media/{{ $property->id }}">{{ tr('manager_property.media-tab', 'Media') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'places' ? 'class="active"' : ''; ?>>
        <a href="/manager/{{ $tab_link }}/places/{{ $property->id }}">{{ tr('manager_property.places-tab', 'Places') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'reviews' ? 'class="active"' : ''; ?>>
        <a href="/manager/{{ $tab_link }}/review/{{ $property->id }}">{{ tr('manager_property.review-tab', 'Reviews') }}</a>
    </li>
<!--    <li role="presentation" <?php echo $tab == 'listing' ? 'class="active"' : ''; ?>>
        <a href="/manager/{{ $tab_link }}/listing/{{ $property->id }}">{{ tr('manager_property.listing-tab', 'Listing') }}</a>
    </li>-->
</ul>