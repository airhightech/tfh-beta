@section('title', 'Page Title')

<?php
$tab = 'details';
$extended = $property->getExtendedData();
$building = $property->getBuildingData();
?>

<h3>{{ $title }} : {{ $property->getTranslatable('name') }}</h3>

<div class="manager-content">     

    @include('desktop.manager.building.tab')

    <div class="form-container row">
        <div class="col-md-12">

            <h4>{{ tr('manager_property.general-info', 'General info') }}</h4>
            <form id="general-info-form" class="form-horizontal" action="/rest/property/details-info" method="post">
                <div class="form-group">
                    <label for="starting_sales_price" class="col-sm-2 control-label">{{ tr('manager_property.start-sales-price', 'Sales price start') }}</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="starting_sales_price" value="{{ $extended->starting_sales_price }}">
                    </div>

                    <label for="total_units" class="col-sm-2 control-label">{{ tr('manager_property.total-units', 'Total units') }}</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="total_units" value="{{ $building->total_units }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="office_hours" class="col-sm-2 control-label">{{ tr('manager_property.office-hours', 'Office hours') }}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="office_hours" value="{{ $extended->office_hours }}">
                    </div>
                    <label for="psf" class="col-sm-2 control-label">{{ tr('manager_property.psf', 'Price/Sqm') }}</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="psf" value="{{ $extended->psf }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="year_built" class="col-sm-2 control-label">{{ tr('manager_property.year-built', 'Year built') }}</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="year_built" value="{{ $extended->year_built }}" placeholder="">
                    </div>

                    <label for="website" class="col-sm-2 control-label">{{ tr('manager_property.webite', 'Website') }}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="website" value="{{ $extended->website }}" placeholder="">
                    </div>

                </div>
                <div class="form-group">
                    <label for="number_of_tower" class="col-sm-2 control-label">{{ tr('manager_property.number-tower', 'Number of tower') }}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="number_of_tower" value="{{ $building->number_of_tower }}">
                    </div>
                    <label for="parking_lot" class="col-sm-2 control-label">{{ tr('manager_property.parking-lot', 'Parking lot') }}</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="parking_lot" value="{{ $building->parking_lot }}">
                    </div>

                </div>
                <div class="form-group">
                    <label for="total_floors" class="col-sm-2 control-label">{{ tr('manager_property.total-floors', 'Total floors') }}</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="total_floors" value="{{ $extended->total_floors }}">
                    </div>

                    <label for="contact_person" class="col-sm-2 control-label">{{ tr('manager_property.contact-person', 'Contact') }}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="contact_person" value="{{ $extended->contact_person }}">
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" onclick="submitGenericForm('general-info-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>
            </form>
            <h4>{{ tr('manager_property.room-types', 'Room types') }}</h4>
            <form class="form-horizontal" action="/manager/condo/add-room/{{ $property->id }}" method="post" id="room-types-form">
                <div id="room-type-container">
                    @include('desktop.manager.property.rooms')
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">{{ tr('manager_property.new room', 'New room') }}</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="room_name" name="name" value="" placeholder="Name">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="room_size" name="size" value="" placeholder="Size (sqm)">
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" onclick="addRoomType()"><i class="fa fa-save"></i></button>
                    </div>
                </div>
            </form>
            <h4>Facilities</h4>
            <form id="update-facility-form" class="form-horizontal" action="/rest/property/update-facilities" method="post">

                <?php
                if (count($facilities)) {
                    foreach ($facilities as $facility) {
                        ?>
                        <div class="checkbox col-md-4">
                            <label>
                                <input type="checkbox" name="facilities[]" value="{{ $facility->id }}" <?php echo in_array($facility->id, $property_facilities) ? 'checked' : ''; ?> > {{ $facility->getTranslatable('name') }}
                            </label>
                        </div>
                        <?php
                    }
                }
                ?>

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" 
                                onclick="submitGenericForm('update-facility-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>

            </form>
            
            <h4>Nearby Transportation</h4>

            <form id="nearby-info-form" class="form-horizontal" action="/rest/property/nearby-info" method="post">

                @include('desktop.member.posting.generic.nearby-transportation')

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" 
                                onclick="submitGenericForm('nearby-info-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>

            </form>

            <form id="other-details-form" class="form-horizontal" action="/rest/property/other-details" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <h4>{{ tr('manager_property.extended-custom-address', 'Custom address') }}</h4>
                        {{ __trfields('custom_address', $extended, true) }}
                    </div>
                    <div class="col-md-7">
                        <h4>{{ tr('manager_property.extended-details', 'Details') }}</h4>
                        {{ __trfields('details', $extended, true) }}
                    </div>
                </div>


                <div class="form-group clearfix">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" onclick="submitGenericForm('other-details-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet">
<script src="{{ __asset('/js/manager/rooms.js') }}">
</script>
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script>
    $(function () {
        $(".bts").chosen({max_selected_options: 4});
        $(".mrt").chosen({max_selected_options: 4});
        $(".apl").chosen({max_selected_options: 4});
    });
</script>
@endsection
