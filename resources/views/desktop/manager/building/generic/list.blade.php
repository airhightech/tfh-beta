
@section('title', 'Page Title')

@section('content')
<h3>{{ $title }}</h3>


<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation" class="input">  
            <form class="form-inline" method="get" action="/manager/{{ $action_link }}/list">
                <div class="form-group">
                    <input type="text" name="q" value="{{ $request->input('q') }}" 
                           class="form-control search" 
                           placeholder="{{ tr('manager_property.search-placeholder', 'Search property by name') }}"/>
                </div>
                <button type="submit" class="btn">{{ tr('button.search', 'Search') }}</button>
            </form>
        </li>

        <?php
        $q = trim($request->input('q'));
        if (!empty($q)) {
            ?>
            <li role="presentation">
                <a href="/manager/{{ $action_link }}/list">{{ tr('manager_account.reset-search', 'Reset search') }}</a>
            </li>
            <?php
        }
        ?>

        <?php
        if ($create_text !== false) {
            ?>
            <li role="presentation" class="pull-right">
                <a href="/manager/{{ $action_link }}/create" class="btn btn-sm btn-blue">
                    <i class="fa fa-plus"></i> {{ $create_text }} 
                </a>
            </li>
            <?php
        }
        ?>

    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="icon-col"></th>
            <th>{{ tr('manager_property.name-col', 'Name') }}</th>
            <th>{{ tr('manager_property.area-col', 'District') }}</th>
            <th class="small-col text-center">{{ tr('manager_project.views-col', 'Views') }}</th>
            <th class="small-col text-center">{{ tr('manager_property.listed-col', 'Listed') }}</th>
            <th class="small-col text-center">{{ tr('manager_property.photo-col', 'Photos') }}</th>            
            <th class="icon-col"></th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($properties as $property) {
            $address = $property->getAddress();
            $district = null;

            if ($address) {
                $district = $address->getDistrict();
            }
            ?>
            <tr>
                <td class="text-center">
                    <input type="checkbox" name="active" value="YES" <?php echo $property->published ? 'checked' : ''; ?> 
                           class="property-status" data-id="{{ $property->id }}"/>
                </td>
                <td>{{ $property->getTranslatable('name') }}</td>
                <td>{{ $district ? $district->getTranslatable('name') : '' }}</td>
                <td class="text-center">{{ $property->clicks + 0 }}</td>
                <td class="text-center">{{ $property->countImages() }}</td>
                <td class="text-center">{{ $property->countImages() }}</td>
                <td class="text-center">
                    <?php
                    if (isset($listing) && $listing === true) {
                        ?>
                        <button type="button" class="btn btn-xs btn-white edit-listing" data-id="{{ $property->id }}">
                            <i class="fa fa-pencil"></i>
                        </button>
                        <?php
                    } else {
                        ?>
                        <div class="dropdown">
                            <button class="btn btn-xs btn-white"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                <li>
                                    <a href="/manager/{{ $action_link }}/info/{{ $property->id }}">
                                        {{ tr('manager_property.info-tab', 'Info') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="/manager/{{ $action_link }}/details/{{ $property->id }}">
                                        {{ tr('manager_property.details-tab', 'Details') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="/manager/{{ $action_link }}/media/{{ $property->id }}">
                                        {{ tr('manager_property.media-tab', 'Media') }}
                                    </a>
                                </li>
                                <?php
                                if ($property->ptype == \App\Models\Property::TYPE_APARTMENT_BUILDING ||
                                        $property->ptype == \App\Models\Property::TYPE_CONDO_BUILDING ||
                                        $property->new_project) {
                                    ?>
                                    <li>
                                        <a href="/manager/{{ $action_link }}/review/{{ $property->id }}">
                                            {{ tr('manager_property.review-tab', 'Review') }}
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>                            
                                <li>
                                    <a href="/manager/{{ $action_link }}/places/{{ $property->id }}">
                                        {{ tr('manager_property.places-tab', 'Places') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <?php
                    }
                    ?>
                </td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-danger delete-listing" data-id="{{ $property->id }}">
                        <i class="fa fa-times"></i>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div id="listing-modal-delete" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-default" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body modal-listing-delete">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-danger" 
                        onclick="submitGenericForm('listing-delete-form')">{{ tr('button.update', 'DELETE') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

{{ $properties->appends($request->all())->links() }}

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/listing.js') }}"></script>
<script src="{{ __asset('/js/manager/property.js') }}"></script>
@endsection