@section('title', 'Page Title')

<?php

//$new_project = isset($new_project) ? $new_project : false;

?>

@section('content')

<h3>{{ $title }}</h3>

<div class="manager-content">    
    <form id="property-create-form" action="/rest/property/create" method="post">

        <div class="form-container row">
            <div class="col-md-6">

                <h4><?php echo tr('manager_property.create-name-label', 'Condo names'); ?> <span class="text-danger">*</span></h4>

                {{ __trfields('name') }}

                <h4><?php echo tr('manager_property.custom-address-label', 'Custom address'); ?></h4>

                {{ __trfields('custom_address') }}

                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> 
                <input type="hidden" name="ptype" value="<?php echo $ptype; ?>"> 
                <input type="hidden" name="new_project" value="<?php echo $new_project ? 'true' : 'false'; ?>">
                <button type="button" class="btn btn-primary" onclick="submitGenericForm('property-create-form')">Submit</button>

            </div>
            
            <div class="col-md-6">
                
                <h4><?php echo tr('manager_property.address-label', 'Property location'); ?></h4>
                
                <div class="form-group">
                    <label for="">{{ tr('manager_property.province-label', 'Province') }}</label>
                    <select class="form-control" id="province_id" name="province_id">
                        <option value="0">{{ tr('manager_property.province-select', 'Select a province') }}</option>
                        <?php
                        foreach ($provinces as $province) {
                            ?>
                            <option value="{{ $province->id }}">
                                <?php echo $province->getTranslatable('name'); ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">{{ tr('manager_property.district-label', 'District') }}</label>
                    <select class="form-control" id="district_id" name="district_id" disabled>
                            <option value="0">{{ tr('manager_property.province-select', 'Please select a province') }}</option>
                    </select>                    
                    <span class="help-block district_loading generic_data_loading">
                        {{ tr('loading.district-list', 'Loading district') }}<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    </span>
                </div>
                <div class="form-group">
                    <label for=""><?php echo tr('manager_property.area-label', 'Area'); ?></label>
                    <select class="form-control" id="area_id" name="area_id" disabled>
                            <option value="0">{{ tr('manager_property.district-select', 'Please select a district') }}</option>
                    </select>
                    <span class="help-block area_loading generic_data_loading">
                        {{ tr('loading.area-list', 'Loading area') }}<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    </span>
                </div>
                
                <div class="form-group">
                    <label for=""><?php echo tr('manager_property.postal-code-label', 'Postal code'); ?></label>
                    <input type="text" class="form-control" id="postal_code" name="postal_code" value="{{ old('postal_code') }}" placeholder="">
                </div>
                
                <div class="form-group">
                    <label for=""><?php echo tr('manager_property.latitude-label', 'Latitude'); ?></label>
                    <input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat') }}" placeholder="">
                </div>
                <div class="form-group">
                    <label for=""><?php echo tr('manager_property.longitude-label', 'Longitude'); ?></label>
                    <input type="text" class="form-control" id="lng" name="lng" value="{{ old('lng') }}" placeholder="">
                </div>
            </div>
            
            <div class="col-md-12">
                <div id="map-picker" 
                     style="width: 100%; height: 400px; margin-top: 25px; margin-bottom: 15px;"></div>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="area" name="area" placeholder="Type the area name ...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="clear-area-btn" type="button">Clear</button>
                        </span>
                    </div>

                </div>

            </div>
        </div>

    </form>

</div>

@endsection

<?php

$lat = 13.746318;
$lng = 100.534875;
?>

@section('scripts')
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&key={{ config('google.browser_key') }}'></script>
<script src="/js/lp/locationpicker.jquery.js"></script>
<script src="{{ __asset('/js/visitor/address.js') }}"></script>
<script src="{{ __asset('/js/manager/property.js') }}"></script>
<script>

    var lat0 = <?php echo $lat; ?>;
    var lng0 = <?php echo $lng; ?>;

</script>
@endsection
