@extends('desktop.layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'media';
$images = $property->getImages(); 
?>

@section('content')

<h3>{{ $title }} : {{ $property->getTranslatable('name') }}</h3>

@include('desktop.manager.building.tab')

<h3>{{ tr('manager.condo.current-media-title', 'Current media') }}</h3>

<div class="review-media-list">
    <?php

    if (count($images)) {
        foreach ($images as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="#" class="rm-media" data-target="#delete-{{ $image->id }}">
                    <i class="fa fa-times"></i>
                </a>
                <div id="delete-{{ $image->id }}" class="rm-links">
                    <p>{{ tr('manager_property.delete-image', 'Delete ?') }}</p>
                    <a href="/rest/property/delete-image/{{ $image->id }}" class="text-danger silent-link">
                        {{ tr('manager_property.delete-image-yes', 'YES') }}
                    </a>
                    <a href="#" class="text-primary rm-media-cancel" data-target="#delete-{{ $image->id }}">{{ tr('manager_property.delete-image-no', 'NO') }}</a>
                </div>
                
            </div>
            <?php
        }
    }
    ?>
</div>

<h3>{{ tr('manager.condo.media-upload-title', 'Upload new images') }}</h3>

<div id="preview" class="media-preview">
</div>
<div id="size-error" class="alert alert-danger size-error hidden-error text-center">
    {{ tr('media.image-size-too-big', 'Image size are too big, please respect max size') }} : {{ max_size() }}<br/>
    {{ tr('media.send-image-one-by-one', 'You can also send the image one by one') }}
</div>
<div class="progress push-below">
    <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>

<form id="upload-images" method="post" enctype="multipart/form-data"
      action="/rest/property/upload" data-progress="#progress">
    <div class="row">
        <div class="col-md-4">
            <label class="btn btn-sm btn-default upload-trigger">
                {{ tr('manager_project.review.add-image', 'SELECT IMAGES') }}       
                <input type="file" id="location-image" name="images[]" class="hidden image-input" multiple
                       data-target="#preview" data-error="size-error"/>
                {{ csrf_field() }}
                <input type="hidden" name="MAX_FILE_SIZE" value="{{ file_upload_max_size() }}" /> 
                <input type="hidden" name="id" value="{{ $property->id }}"/>
            </label> 
            <p class="text-danger">(JPEG or PNG only)<br/>
            Max size: {{ max_size() }}</p>
        </div>
        <div class="col-md-4">
            <button type="button" class="btn btn-sm btn-primary" 
                    onclick="submitGenericForm('upload-images')">
                {{ tr('button.send-image', 'UPLOAD') }}
            </button>
        </div>
    </div>
</form>

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/uploader.js') }}"></script>
@endsection