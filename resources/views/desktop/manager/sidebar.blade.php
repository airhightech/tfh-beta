<?php
$user = Auth::user();
?>

<div class="list-group manager-sidebar">

    <a href="/manager/dashboard" class="list-group-item <?php echo $active == 'dashboard' ? 'active' : ''; ?>">
        {{ tr('manager_sidebar.dashboard', 'Dashboard') }}
    </a>    

    <?php
    if ($user->ctype == 'manager' || $user->ctype == 'editor' || $user->ctype == 'accountant') {
        ?>
        <h5>{{ tr('manager_sidebar.member-management', 'Member management') }}</h5>

        <a href="/manager/visitor/list" class="list-group-item <?php echo $active == 'visitors' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.visitors', 'General member') }} 
        </a>
        <a href="/manager/owner/list" class="list-group-item <?php echo $active == 'owners' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.owners', 'Owners') }} 
        </a>
        <a href="/manager/agent/list" class="list-group-item <?php echo $active == 'agents' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.agents', 'Agents') }} 
        </a>
        <a href="/manager/developer/list" class="list-group-item <?php echo $active == 'developers' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.developers', 'Land developers') }} 
        </a>
        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'editor') {
        ?>
        <h5>{{ tr('manager_sidebar.property-management', 'Property management') }}</h5>    

        <a href="/manager/condo/list" class="list-group-item <?php echo $active == 'condo-community' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.condo_management', 'Condo Community') }}
        </a>

        <a href="/manager/facility/list" class="list-group-item <?php echo $active == 'facility' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.condo_facilities', 'Condo facilities') }}
        </a> 
        <a href="/manager/feature/list" class="list-group-item <?php echo $active == 'feature' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.property_features', 'Property features') }}
        </a>
        <a href="/manager/optional-feature/list" class="list-group-item <?php echo $active == 'optional-feature' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.optional_features', 'Optional features') }}
        </a>   

        <h5>{{ tr('manager_sidebar.new-project', 'New project') }}</h5>

        <a href="/manager/project/list" class="list-group-item <?php echo $active == 'all-project' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.all_project', 'All projects') }}
        </a>

        <a href="/manager/project/condo/list" class="list-group-item <?php echo $active == 'condo-project' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.condo_project', 'Condominium') }}
        </a>
        <a href="/manager/project/apartment/list" class="list-group-item <?php echo $active == 'apartment-project' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.apartment_project', 'Apartment') }}
        </a>
        <a href="/manager/project/house/list" class="list-group-item <?php echo $active == 'house-project' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.house_project', 'Single house') }}
        </a>
        <a href="/manager/project/townhouse/list" class="list-group-item <?php echo $active == 'townhouse-project' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.townhouse_project', 'Townhouse') }}
        </a>

        <h5>{{ tr('manager_sidebar.customer-data', 'Customer data') }}</h5>

        <a href="/manager/new-condo/list" class="list-group-item <?php echo $active == 'new-condo-community' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.new_condo_list', 'New condo community') }}
        </a>
        <a href="/manager/listing/all" class="list-group-item <?php echo $active == 'all-listing' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.all_listing', 'All listings') }}
        </a>
        <a href="/manager/listing/condo/list" class="list-group-item <?php echo $active == 'condo-listing' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.condo_list', 'Condo') }}
        </a>
        <a href="/manager/listing/apartment/list" class="list-group-item <?php echo $active == 'apartment-listing' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.apartment_list', 'Apartment') }}
        </a>
        <a href="/manager/listing/house/list" class="list-group-item <?php echo $active == 'house-listing' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.house_list', 'Single house') }}
        </a>
        <a href="/manager/listing/townhouse/list" class="list-group-item <?php echo $active == 'townhouse-listing' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.townhouse_list', 'Townhouse') }}
        </a>

        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'accountant') {
        ?>
        <a href="/manager/order/list" class="list-group-item <?php echo $active == 'order' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.orders', 'Orders') }}
        </a> 
        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'editor') {
        ?>
        <h5>{{ tr('manager_sidebar.geographic-data', 'Geographic data') }}</h5>

        <a href="/manager/province/list" class="list-group-item <?php echo $active == 'location' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.provinces', 'Provinces') }}
        </a>
        <a href="/manager/place/index" class="list-group-item <?php echo $active == 'place' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.points', 'Places') }} 
        </a>    

        <h5>{{ tr('manager_sidebar.other-data', 'Other data') }}</h5>

        <a href="/manager/popup/list" class="list-group-item <?php echo $active == 'popup' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.popups', 'Popups') }} 
        </a>    
        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'accountant') {
        ?>
        <a href="/manager/banner/list" class="list-group-item <?php echo $active == 'banner' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.banners', 'Banners') }} 
        </a>
        <a href="/manager/package/list" class="list-group-item <?php echo $active == 'package' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.package', 'Packages') }}
        </a>
        <?php
    }

    if ($user->ctype == 'manager') {
        ?>
        <a href="/manager/staff/list" class="list-group-item <?php echo $active == 'staff' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.staff', 'Staff') }}
        </a>
        <a href="/manager/setting/index" class="list-group-item <?php echo $active == 'setting' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.settings', 'Settings') }}
        </a>
        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'editor') {
        ?>
        <a href="/manager/page/list" class="list-group-item <?php echo $active == 'pages' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.static', 'Pages') }}
        </a>
        <a href="/manager/notice/list" class="list-group-item <?php echo $active == 'notice' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.notice', 'Notices') }}
        </a>
        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'translator') {
        ?>
        <a href="/manager/translation/list" class="list-group-item <?php echo $active == 'translation' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.translation', 'Translations') }}
        </a>
        <?php
    }

    if ($user->ctype == 'manager' || $user->ctype == 'editor') {
        ?>
        <a href="/manager/newsletter/list" class="list-group-item <?php echo $active == 'newsletter' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.newsletter', 'Newsletter') }}
        </a>
        <a href="/manager/newsletter-subscription/list" class="list-group-item <?php echo $active == 'newsletter-subscription' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.newsletter-subscriber', 'Newsletter Subscription') }}
        </a>
        <a href="/manager/comment/list" class="list-group-item <?php echo $active == 'comment' ? 'active' : ''; ?>">
            {{ tr('manager_sidebar.comment', 'Comments') }}
        </a>
        <?php
    }
    ?>

</div>
