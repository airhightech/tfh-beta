<?php
$active = 'comment';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_comment.comments.list-title', 'List of reviews') }}</h3>

<ul class="nav nav-tabs">
    <li role="presentation" class="{{ __select($type . '-' . $status, 'property-PENDING', 'active') }}">
        <a href="/manager/comment/list?type=property">{{ tr('manager_comment.property-pending-tab', 'Property reviews (Pending)') }}</a>
    </li>
    <li role="presentation" class="{{ __select($type . '-' . $status, 'agent-PENDING', 'active') }}">
        <a href="/manager/comment/list?type=agent">{{ tr('manager_comment.agent-pending-tab', 'Agent reviews (Pending)') }}</a>
    </li>
    <li role="presentation" class="{{ __select($type . '-' . $status, 'property-APPROVED', 'active') }}">
        <a href="/manager/comment/list?type=property&status=APPROVED">{{ tr('manager_comment.property-approved-tab', 'Property reviews (Approved)') }}</a>
    </li>
    <li role="presentation" class="{{ __select($type . '-' . $status, 'agent-APPROVED', 'active') }}">
        <a href="/manager/comment/list?type=agent&status=APPROVED">{{ tr('manager_comment.agent-approved-tab', 'Agent reviews (Approved)') }}</a>
    </li>
</ul>

<table class="table">
    <thead>
        <tr>
            <th>{{ tr('manager_comment.name-col', 'Name') }}</th>
            <th>{{ tr('manager_comment.user-col', 'User') }}</th>
            <th>{{ tr('manager_comment.date-col', 'Date') }}</th>
            <th class="icon-col"></th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach($comments as $comment) {
            $user = $comment->getUser();
            $profile = $user->getProfile();
            ?>
            <tr>
                <td>{{ $comment->getText() }}</td>
                <td>{{ $profile->getName() }}</td>
                <td>{{ $comment->created_at }}</td>
                <td>
                    <a href="{{ $comment->getLink() }}" target="_blank">
                        <i class="fa fa-mail-forward"></i>
                    </a>
                </td>
                <td><a href="#" class="edit-comment" 
                       data-id="{{ $comment->id }}"
                           data-type="{{ $type }}"><i class="fa fa-cog"></i></a></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
        
</table>

{{ $comments->appends($request->all())->links() }}

@include('desktop.manager.comment.modal')

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/comments.js') }}"></script>
@endsection
