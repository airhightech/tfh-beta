<div id="comment-modal" class="modal fade comment-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('manager_comment.approve', 'Approve comment') }}</h4>
            </div>
            <div class="modal-body comment-details">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left delete-comment">{{ tr('button.delete', 'DELETE') }}</button>
                
                <button type="button" class="btn btn-success approve-comment">{{ tr('button.mark-as-read', 'MARK AS READ') }}</button>
                
                <button type="button" class="btn btn-primary update-comment">{{ tr('button.update', 'UPDATE') }}</button>
            </div>
        </div>
    </div>
</div>