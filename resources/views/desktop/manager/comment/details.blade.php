<div class="row">
    <div class="col-md-8">
        <form id="comment-form" action="/manager/comment/update" method="post">
            <div class="form-group">
                <label for="comment">{{ tr('manager_comment.comment', 'Comment') }}</label>
                <textarea name="comment" class="form-control" rows="5">{{ $comment->getText() }}</textarea>
            </div>
            <div class="form-group">
                <label for="comment">{{ tr('manager_comment.comment', 'Images') }}</label>
                <div class="row">
                    <?php
                    $images = $comment->getImages();
                    if (count($images)) {
                        foreach ($images as $image) {
                            ?>
                            <div class="col-md-4">
                                <img src="/image/{{ $image }}" class="img-responsive"/>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            {{ csrf_field() }}
            <input type="hidden" id="op" name="op" value=""/>
            <input type="hidden" name="type" value="{{ $type }}"/>
            <input type="hidden" name="id" value="{{ $comment->id }}"/>
        </form>
    </div>
    <div class="col-md-4">
        <?php
        $user = $comment->getUser();
        $profile = $user->getProfile();
        ?>
        <h5>{{ tr('manager_comment.user-col', 'User') }}</h5>
        <p>
            {{ $profile->getName() }}
        </p>
        <p>
            {{ $user->email }}
        </p>
    </div>
</div>



