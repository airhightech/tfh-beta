<?php
$active = 'order';
$user = $purchase->getUser();
$data = $purchase->getData();
$package = $data->getPackage();
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_order.order-list', 'List of orders') }} : #{{ $user->getPublicId() }}</h3>

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ tr('manager_order.id-col', 'Invoice') }}</th>
            <th>{{ tr('manager_order.package-col', 'Package') }}</th>
            <th class="text-center">{{ tr('manager_order.listing-unit-col', 'Listing') }}</th>
            <th class="text-right">{{ tr('manager_order.price-col', 'Price') }}</th>
            <th class="text-center">{{ tr('manager_order.status-col', 'Status') }}</th>
            <th class="text-center">{{ tr('manager_order.payment-col', 'Payment') }}</th>
            <th class="icon-col text-center"></th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <td>{{ $purchase->invoice_id }}</td><!-- invoice_id -->
            <td>{{ $package->getTranslatable('name') }}</td>
            <td class="text-center">{{ $data->listing_count }}</td>
            <td class="text-right">{{ number_format($purchase->getAmount(), 2) }}</td>
            <td class="text-center">{{ $purchase->payment_status }}</td>
            <td class="text-center">{{ ucfirst($purchase->payment_method) }}</td>
            <td class="text-center">

            </td>
        </tr>
    </tbody>
</table>

<div class="row order-customer-setting">
    <div class="col-md-6">
        <h5 class="text-center"><i class="fa fa-circle"></i> {{ tr('manager_order.card-info-title', 'Customer Card') }}</h5>
        <?php
        if ($status == 'account_updated') {
            ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ tr('manager_order.account-updated', 'Customer account updated') }}</p>
            </div>
            <?php
        }
        ?>
        <div class="customer-account">
            <form class="form-horizontal" action="/manager/order/account/{{ $purchase->id }}" method="post">


            </form>
        </div>
    </div> 
    <div class="col-md-6">
        <h5 class="text-center"><i class="fa fa-circle"></i> {{ tr('manager_order.bank-info-title', 'Bank info') }}</h5>        
        <div class="customer-account">
            <form class="form-horizontal" id="update-purchase" action="/manager/order/update" method="post">
                <div class="form-group">
                    <label for="bank" class="col-sm-4 control-label">{{ tr('manager_order.setting-bank', 'Bank setting') }}</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="bank_id" name="bank_id">
                            <?php
                            foreach ($banks as $bank) {
                                ?>
                                <option value="{{ $bank->id }}" <?php echo $purchase->bank_id == $bank->id ? 'selected' : ''; ?> >{{ $bank->name }}</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank" class="col-sm-4 control-label">{{ tr('manager_order.payment-status', 'Payment status') }}</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="payment_status" name="payment_status">
                            <option value="pending" <?php echo $purchase->payment_status == 'pending' ? 'selected' : ''; ?> >Pending</option>
                            <option value="paid" <?php echo $purchase->payment_status == 'paid' ? 'selected' : ''; ?> >Paid</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_amount" class="col-sm-4 control-label">{{ tr('manager_order.setting-last_amount', 'Last amount') }}</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="last_amount" name="last_amount" value="{{ ($purchase->updated_amount) }}">
                    </div>
                </div>
                {{ csrf_field() }}
                <input type="hidden" value="{{ $purchase->id }}" name="id"/>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="button" class="btn btn-dark submit-ajax-form" data-target="update-purchase">{{ tr('button.update', 'Update') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<h4>{{ tr('manager.order.history-title', 'Purchase history') }}</h4>
<?php
if (count($purchases)) {
    ?>
    <table class="table table-list">
        <thead>
            <tr>
                <th>{{ tr('manager_order.id-col', 'Invoice') }}</th>
                <th>{{ tr('manager_order.package-col', 'Package') }}</th>
                <th class="text-center">{{ tr('manager_order.listing-unit-col', 'Listing') }}</th>
                <th class="text-right">{{ tr('manager_order.price-col', 'Price') }}</th>
                <th class="text-center">{{ tr('manager_order.status-col', 'Status') }}</th>
                <th class="text-center">{{ tr('manager_order.payment-col', 'Payment') }}</th>
                <th class="icon-col text-center"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($purchases as $_purchase) {
                $user = $_purchase->getUser();
                $paysbuy = $_purchase->getPaybuyPayment();
                $data = $_purchase->getData();

                if ($data) {
                    $package = $data->getPackage();
                    ?>
                    <tr>
                        <td>{{ $_purchase->invoice_id }}</td>
                        <td>{{ $_purchase->package_name }}</td>
                        <td class="text-center">{{ $_purchase->listing_count }}</td>
                        <td class="text-right">{{ number_format($_purchase->getAmount(), 2) }}</td>
                        <td class="text-center">{{ $_purchase->payment_status }}</td>
                        <td class="text-center">
                            <?php
                            if ($_purchase->payment_method == 'paysbuy') {
                                if ($paysbuy != null) {
                                    echo $paysbuy->getPaymentMethod();
                                } else {
                                    echo ucfirst($_purchase->payment_method);
                                }
                            } else if ($_purchase->payment_method == 'bank') {
                                echo ucfirst($_purchase->payment_method);
                            }
                            ?>
                        </td>
                        <td class="text-center">
                            <a href="/manager/order/details/{{ $_purchase->id }}"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>

    {{ $purchases->links() }}
    <?php
}
?>


@endsection