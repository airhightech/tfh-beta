<?php
$active = 'order';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_order.order-list', 'List of orders') }}</h3>

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ tr('manager_order.id-col', 'Invoice') }}</th>
            <th>{{ tr('manager_order.user-col', 'User') }}</th>
            <th>{{ tr('manager_order.package-col', 'Package') }}</th>
            <th class="text-center">{{ tr('manager_order.listing-unit-col', 'Listing') }}</th>
            <th class="text-right">{{ tr('manager_order.price-col', 'Price') }}</th>
            <th class="text-center">{{ tr('manager_order.status-col', 'Status') }}</th>
            <th class="text-center">{{ tr('manager_order.payment-col', 'Payment') }}</th>
            <th class="icon-col text-center"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($purchases as $purchase) {
            $user = $purchase->getUser();
            $paysbuy = $purchase->getPaybuyPayment();
            $data = $purchase->getData();

            if ($data) {
                $package = $data->getPackage();

                if ($package) {
                    ?>
                    <tr>
                        <td>{{ $purchase->invoice_id }}</td><!-- invoice_id -->
                        <td>{{ $user->getPublicId() }}</td>
                        <td>{{ $package->getTranslatable('name') }}</td>
                        <td class="text-center">{{ $package->listing_count }}

                            <?php
                            if ($package->free_listing_count > 0) {
                                echo '+', $package->free_listing_count;
                            }
                            ?>
                        </td>
                        <td class="text-right">{{ number_format($purchase->getAmount(), 2) }}</td>
                        <td class="text-center">{{ $purchase->payment_status }}</td>
                        <td class="text-center">
                            <?php
                            if ($purchase->payment_method == 'paysbuy') {
                                if ($paysbuy != null) {
                                    echo $paysbuy->getPaymentMethod();
                                } else {
                                    echo ucfirst($purchase->payment_method);
                                }
                            } else if ($purchase->payment_method == 'bank') {
                                echo ucfirst($purchase->payment_method);
                            }
                            ?>
                        </td>
                        <td class="text-center">
                            <a href="/manager/order/details/{{ $purchase->id }}"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    <?php
                }
            }
        }
        ?>
    </tbody>
</table>

{{ $purchases->links() }}

@endsection 