<?php
$active = 'location';
?>

@extends('desktop.layouts.manager')

@section('content')

<div class="row">
    <div class="col-md-5">
        <h3>{{ tr('manager_location.update-area-title', 'Update area') }}</h3>
        <form action="/rest/area/update" method="post" id="update-area-form">
            <h4 class="dark-title"><?php echo tr('manager_location.area-name-label', 'Area names'); ?></h4>
            {{ __trfields('name', $area) }}
            <input type="hidden" name="id" value="{{ $area->id }}"/>
            <button type="button" class="btn btn-primary submit-ajax-form" data-target="update-area-form">Submit</button>
        </form>
    </div>
    <div class="col-md-7">
        <h3>{{ tr('manager_location.update-area-location', 'Area location') }}</h3>
        <div id="map" class="location-map">            
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script>    
var domain = 'area';
var id = {{ $area->id }};
var lat = <?php echo $area->lat() ? $area->lat() : 'null'; ?>;
var lng = <?php echo $area->lng() ? $area->lng() : 'null'; ?>;
var zl = <?php echo $area->zoom_level ? $area->zoom_level : '15'; ?>;

</script>
<script src="{{ __asset('/js/manager/location.js') }}"></script>
@endsection
