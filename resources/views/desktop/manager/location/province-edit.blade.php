<?php
$active = 'location';
?>

@extends('desktop.layouts.manager')

@section('content')



<div class="row">
    <div class="col-md-5">
        <h3>{{ tr('manager_location.update-province-title', 'Update province') }}</h3>
        <form action="/rest/province/update" method="post" id="update-province-form">
            <h4 class="dark-title"><?php echo tr('manager_location.province-name-label', 'Province names'); ?></h4>
            {{ __trfields('name', $province) }}
            <input type="hidden" name="id" value="{{ $province->id }}"/>
            <button type="button" class="btn btn-primary submit-ajax-form" data-target="update-province-form">Submit</button>
        </form>
    </div>
    <div class="col-md-7">
        <h3>{{ tr('manager_location.update-province-location', 'Province location') }}</h3>
        <div id="map" class="location-map">            
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script>    
var domain = 'province';
var id = {{ $province->id }};
var lat = <?php echo $province->lat() ? $province->lat() : 'null'; ?>;
var lng = <?php echo $province->lng() ? $province->lng() : 'null'; ?>;
var zl = <?php echo $province->zoom_level ? $province->zoom_level : '15'; ?>;

</script>
<script src="{{ __asset('/js/manager/location.js') }}"></script>
@endsection

