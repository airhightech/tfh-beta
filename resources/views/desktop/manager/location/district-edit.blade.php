<?php
$active = 'location';
?>

@extends('desktop.layouts.manager')

@section('content')



<div class="row">
    <div class="col-md-5">
        <h3>{{ tr('manager_location.update-district-title', 'Update district') }}</h3>
        <form action="/rest/district/update" method="post" id="update-district-form">
            <h4 class="dark-title"><?php echo tr('manager_location.district-name-label', 'District names'); ?></h4>
            {{ __trfields('name', $district) }}
            <input type="hidden" name="id" value="{{ $district->id }}"/>
            <button type="button" class="btn btn-primary submit-ajax-form" data-target="update-district-form">Submit</button>
        </form>
    </div>
    <div class="col-md-7">
        <h3>{{ tr('manager_location.update-district-location', 'District location') }}</h3>
        <div id="map" class="location-map">            
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script>    
var domain = 'district';
var id = {{ $district->id }};
var lat = <?php echo $district->lat() ? $district->lat() : 'null'; ?>;
var lng = <?php echo $district->lng() ? $district->lng() : 'null'; ?>;
var zl = <?php echo $district->zoom_level ? $district->zoom_level : '15'; ?>;

</script>
<script src="{{ __asset('/js/manager/location.js') }}"></script>
@endsection
