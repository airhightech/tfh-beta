<?php
$active = 'location';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_location.province-title', 'Province list') }}</h3>

<div class="row">
    <div class="col-md-7">
        <table class="table table-list" id="province-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center small-col">Districts</th>
                    <th class="tiny-col text-center">Properties</th>
                    <th class="icon-col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($provinces as $province) {
                    ?>
                    <tr>                        
                        <td>
                            <a href="/manager/district/list/{{ $province->id }}">
                                {{ $province->getTranslatable('name') }}
                            </a>
                        </td>
                        <td class="text-center">{{ $province->getDistrictCount() }}</td>   
                        <td class="text-center">{{ $province->countProperties() }}</td>
                        <td class="icon-col text-center">
                            <a href="/manager/province/edit/{{ $province->id }}"><i class="fa fa-pencil"></i></a>
                        </td>   
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

        {{ $provinces->links() }}
    </div>
    <div class="col-md-5">
        <form action="/rest/province/create" method="post" id="create-province-form">
            <h4 class="dark-title"><?php echo tr('manager_property.property-name-label', 'Property names'); ?></h4>

            {{ __trfields('name') }}

            <button type="button" class="btn btn-primary submit-ajax-form" data-target="create-province-form">Submit</button>
        </form>
    </div>
</div>



@endsection