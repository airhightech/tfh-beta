<?php
$active = 'location';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_location.area-title', 'Area list') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">   
        <li role="presentation">
            <a href="/manager/province/list">{{ tr('manager_location.back-province-list', 'Province list') }} &raquo;</a>
        </li>
        <li role="presentation">
            <a href="/manager/district/list/{{ $province->id }}">{{ $province->getTranslatable('name') }} &raquo;</a>
        </li>        
        <li role="presentation">
            <a hre="#" class="text-muted">{{ $district->getTranslatable('name') }}</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-7">
        <table class="table table-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="tiny-col text-center">Properties</th>
                    <th class="icon-col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($areas as $area) {
                    ?>
                    <tr>                        
                        <td>
                            <a href="/manager/area/list/{{ $area->id }}">
                                {{ $area->getTranslatable('name') }}
                            </a>
                        </td> 
                        <td class="text-center">{{ $area->countProperties() }}</td>
                        <td class="icon-col text-center">
                            <a href="/manager/area/edit/{{ $area->id }}"><i class="fa fa-pencil"></i></a>
                        </td>   
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        {{ $areas->links() }}
    </div>
    <div class="col-md-5">
        <form action="/rest/area/create" method="post" id="create-area-form">
            <h4 class="dark-title"><?php echo tr('manager_location.area-name-label', 'Area names'); ?></h4>

            {{ __trfields('name') }}
            <input type="hidden" name="district_id" value="{{ $district->id }}"/>
            <button type="button" class="btn btn-primary submit-ajax-form" data-target="create-area-form">Submit</button>
        </form>
    </div>
</div>



@endsection