<?php
$active = 'location';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_location.district-title', 'District list') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation">
            <a href="/manager/province/list">{{ tr('manager_location.back-province-list', 'Province list') }} &raquo;</a>
        </li>
        <li role="presentation">
            <a href="#" class="text-muted">{{ $province->getTranslatable('name') }}</a>
        </li> 
    </ul>
</div>

<div class="row">
    <div class="col-md-7">
        <table class="table table-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center small-col">Districts</th>
                    <th class="tiny-col text-center">Properties</th>
                    <th class="icon-col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($districts as $district) {
                    ?>
                    <tr>                        
                        <td>
                            <a href="/manager/area/list/{{ $district->id }}">
                                {{ $district->getTranslatable('name') }}
                            </a>
                        </td>
                        <td class="text-center">{{ $district->getAreaCount() }}</td>  
                        <td class="text-center">{{ $district->countProperties() }}</td>
                        <td class="icon-col text-center">
                            <a href="/manager/district/edit/{{ $district->id }}"><i class="fa fa-pencil"></i></a>
                        </td>   
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

        {{ $districts->links() }}
    </div>
    <div class="col-md-5">
        <form action="/rest/district/create" method="post" id="create-district-form">
            <h4 class="dark-title"><?php echo tr('manager_location.district-name-label', 'District names'); ?></h4>
            {{ __trfields('name') }}
            <button type="button" class="btn btn-primary submit-ajax-form" data-target="create-district-form">Submit</button>
        </form>
    </div>
</div>



@endsection