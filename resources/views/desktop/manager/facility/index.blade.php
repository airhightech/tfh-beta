<?php
$active = 'facility';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_facility.list-title', 'List of facilities') }}</h3>

<div class="row">
    <div class="col-md-9">
        <table class="table table-list">
            <thead>
                <tr>
                    <th class="icon-col"></th>
                    <th class="tiny-col"></th>
                    <th>{{ tr('manager_facility.name-col', 'Name') }}</th>
                    <th class="check-col"></th>
                </tr>
            </thead>
            <tbody class="sortable">
                <?php
                foreach ($facilities as $facility) {
                    $img = $facility->getThumbUrl('48x48', true);
                    ?>
                    <tr class="facility-items" data-id="{{ $facility->id }}">
                        <td class="text-center">
                             <i class="fa fa-arrows-v text-muted"></i>
                        </td>
                        <td>
                            <?php
                            if ($img) {
                                ?>
                                <img src="{{ $img }}" class="img-responsive"/>
                                <?php
                            }
                            ?>
                        </td>
                        <td>{{ $facility->getTranslatable('name') }}</td>
                        <td class="text-center">
                            <a href="/manager/facility/details/{{ $facility->id }}"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-3">
        <a href="/manager/facility/create" class="btn btn-default pull-right">{{ tr('manager_facility.create-link', 'Create new facility') }}</a>
    </div>
</div>

@endsection

@section('scripts')
<script src="/libs/jquery-ui/jquery-ui.min.js"></script>
<script>
$(document).ready(function () {
    $("tbody.sortable").sortable({
        stop: function (event, ui) {
            var data = [];

            $.each($(".facility-items"), function (index, value) {
                data.push($(value).data('id'));

            });
            console.log(data);

            $.post('/manager/facility/order', {ranks: data}, function (data) {

            });
        }
    });
    $("tbody.sortable").disableSelection();
});
</script>
@endsection

