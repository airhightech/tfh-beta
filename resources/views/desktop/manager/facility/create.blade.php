<?php
$active = 'facility';
?>

@extends('desktop.layouts.manager')


@section('content')

<h3>{{ tr('manager_facility.create-title', 'Create new facility') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/facility/list"><i class="fa fa-long-arrow-left"></i> {{ tr('manager_facility.back', 'Back to list of facilities') }}</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="clearfix">
            <form id="create-facility" method="post" action="/manager/facility/create">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="form-group">
                    <label for="name">{{ tr('manager_facility.name', 'Facility name') }}</label>
                </div>

                <?php echo __trfields('name'); ?>

                <div class="form-group">
                    <label title="Upload image file" for="facility_icon" class="btn btn-default">
                        <input type="file" accept="image/*" name="image" id="facility_icon" class="hide image-input" 
                               data-target="#icon-preview" data-error="size-error">
                        Upload an icon
                    </label>
                </div>

                {{ csrf_field() }}

                <button type="button" class="btn btn-primary pull-right" onclick="submitGenericForm('create-facility');">{{ tr('button.create', 'CREATE') }}</button>
            </form>
        </div>
    </div>
    <div class="col-md-offset-1 col-md-4">
        <div class="form-group">
            <label for="name">{{ tr('manager_facility.icon-preview', 'Icon preview') }}</label>
        </div>
        <div id="icon-preview" class="icon-preview">

        </div>
        <div id="size-error" class="alert alert-danger size-error hidden-error text-center">
            {{ tr('media.image-size-too-big', 'Image size are too big, please respect max size') }} : {{ max_size() }}<br/>
            {{ tr('media.send-image-one-by-one', 'You can also send the image one by one') }}
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/uploader.js') }}"></script>
@endsection
