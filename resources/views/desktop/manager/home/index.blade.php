<?php
$active = 'dashboard';

?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_dashboard.title', 'Site statistic') }}</h3>

<h4>Period</h4>
<div class="form-group">
  <div class="row">
    <div class="col-md-4">
      <div class="input-group">
         <input type="text" class="form-control" id="period_from" name="period_from" value="<?php echo date('d/m/Y'); ?>" placeholder="">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
      </div>
    </div>
    <div class="col-md-4">
      <div class="input-group">
        <input type="text" class="form-control" id="period_to" name="period_to" value="<?php echo date('d/m/Y', strtotime("+1 days")); ?>" placeholder="">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
      </div>
    </div>
    <button id="search-period" type="button" class="btn btn-primary">Search</button>
  </div>
</div>

<div class="summary-stat">
    <h4>{{ tr('manager_dashboard.summary', 'Summary') }}</h4>
    <div class="row">
        <div class="col-md-3">
            {{ tr('manager_dashboard.total-listing', 'Total listings') }}
        </div>
        <div class="col-md-3 value">
            {{ $total_listing }} {{ tr('manager_dashboard.listing', 'listings') }}
        </div>
        <div class="col-md-3">
            {{ tr('manager_dashboard.total-comments', 'Total comments') }}
        </div>
        <div class="col-md-3 value">
            {{ $total_comments }} {{ tr('manager_dashboard.comments', 'comments') }}
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            {{ tr('manager_dashboard.total-member', 'Total members') }}
        </div>
        <div class="col-md-3 value">
            {{ $total_members }} {{ tr('manager_dashboard.members', 'members') }}
        </div>
    </div>
</div>

<div class="stat-box total-listing">
    <h4>Total Listing</h4>
    <div id="listing-total-listing" id="filter-total-listing">
      <label class="label-filter">Listing</label>
      <label class="radio-inline">
        <input type="radio" value="rent" for-chart="total_listing" name="radiolistingtotal">Rent
      </label>
      <label class="radio-inline">
        <input type="radio" value="sale" for-chart="total_listing" name="radiolistingtotal">Sale
      </label>
      <label class="radio-inline">
        <input type="radio"  value="np" for-chart="total_listing" name="radiolistingtotal">New Project
      </label>
    </div>
    <div id="showby-total-listing" class="filter-area">
      <label class="label-filter">Show By</label>
      <label class="radio-inline">
        <input checked="checked" type="radio" value="weekly" for-chart="total_listing" name="radioshowbylistingtotal">Weekly
      </label>
      <label class="radio-inline">
        <input type="radio" value="monthly" for-chart="total_listing" name="radioshowbylistingtotal">Monthly
      </label>
      <label class="radio-inline">
        <input type="radio"  value="yearly" for-chart="total_listing" name="radioshowbylistingtotal">Yearly
      </label>
    </div>
    <div class="row">
        <div class="col-md-9">
            <canvas id="total_listing" width="400" height="400"></canvas>
        </div>
        <div class="col-md-3">
            <div class="chart-legend">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-stop condo"></i>{{ tr('property.condo', 'Condo') }}</li>
                    <li><i class="fa-li fa fa-stop apartment"></i>{{ tr('property.apartment', 'Apartment') }}</li>
                    <li><i class="fa-li fa fa-stop house"></i>{{ tr('property.house', 'House') }}</li>
                    <li><i class="fa-li fa fa-stop townhouse"></i>{{ tr('property.townhouse', 'Towhnouse') }}</li>
                    <li><i class="fa-li fa fa-stop new-project"></i>{{ tr('property.new-project', 'New project') }}</li>
                </ul>
            </div>
        </div>
        <div class="summary-chart">
          <h5>Summary</h5>
          <ul class="fa-ul">
              <li>Total Listing : {{ $total_listing }}</li>
              <li>Total Condo Listing : {{ __arr($totalListingData, 'cd', 0) }}</li>
              <li>Total Apartment Listing : {{ __arr($totalListingData, 'ap', 0) }}</li>
              <li>Total House Listing : {{ __arr($totalListingData, 'sh', 0) }}</li>
              <li>Total Townhouse Listing : {{ __arr($totalListingData, 'th', 0) }}</li>
          </ul>
        </div>
    </div>
</div>


<div class="stat-box total-listing">
    <h4>Total Member</h4>
    <div id="showby-total-member" class="filter-area">
      <label class="label-filter">Show By</label>
      <label class="radio-inline">
        <input type="radio" value="weekly" for-chart="total_member" name="radiototalmember">Weekly
      </label>
      <label class="radio-inline">
        <input type="radio" value="monthly" for-chart="total_member" name="radiototalmember">Monthly
      </label>
      <label class="radio-inline">
        <input type="radio"  value="yearly" for-chart="total_member" name="radiototalmember">Yearly
      </label>
    </div>
    <div class="row">
        <div class="col-md-9">
            <canvas id="total_member" width="400" height="400"></canvas>
        </div>
        <div class="col-md-3">
            <div class="chart-legend">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-stop user"></i>User</li>
                    <li><i class="fa-li fa fa-stop owner"></i>Owner</li>
                    <li><i class="fa-li fa fa-stop developer"></i>Land Developer</li>
                    <li><i class="fa-li fa fa-stop freelance"></i>Freelance Agent</li>
                    <li><i class="fa-li fa fa-stop real-estate"></i>Real Estate Agent</li>
                </ul>
            </div>
        </div>
        <div class="summary-chart">
          <h5>Summary</h5>
          <ul class="fa-ul">
            <li>Total Member : {{$total_members}}</li>
            <li>Total User : {{$totalMemberData['visitor']}}</li>
            <li>Total Owner : {{$totalMemberData['owner']}}</li>
            <li>Total Land Developer : {{$totalMemberData['developer']}}</li>
            <li>Total Freelance Agent : {{$totalMemberData['agent']}}</li>
            <li>Total Real Estate Agent : {{$totalMemberData['company']}}</li>
          </ul>
        </div>
    </div>
</div>

<div class="stat-box total-listing">
    <h4>Total Comment</h4>
    <div id="showby-total-comments" class="filter-area">
      <label class="label-filter">Show By</label>
      <label class="radio-inline">
        <input type="radio" value="weekly" for-chart="total_comments" name="radiototalcomment">Weekly
      </label>
      <label class="radio-inline">
        <input type="radio" value="monthly" for-chart="total_comments" name="radiototalcomment">Monthly
      </label>
      <label class="radio-inline">
        <input type="radio"  value="yearly" for-chart="total_comments" name="radiototalcomment">Yearly
      </label>
    </div>
    <div class="row">
        <div class="col-md-9">
            <canvas id="total_comments" width="400" height="400"></canvas>
        </div>
        <div class="col-md-3">
            <div class="chart-legend">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-stop condo"></i>Condo</li>
                    <li><i class="fa-li fa fa-stop apartment"></i>House</li>
                    <li><i class="fa-li fa fa-stop house"></i>Apartment</li>
                    <li><i class="fa-li fa fa-stop townhouse"></i>Townhouse</li>
                </ul>
            </div>
        </div>
        <div class="summary-chart">
          <h5>Summary</h5>
          <ul class="fa-ul">
              <li>Total Comment : {{$total_comments}}</li>
              <li>Condo : {{ __arr($totalCommentData, 'cd', 0)}} </li>
              <li>House : <?php if (array_key_exists('sh', $totalCommentData)) { echo $totalCommentData['sh']; } else { echo '0'; } ?></li>
              <li>Apartment : <?php if (array_key_exists('ap', $totalCommentData)) { echo $totalCommentData['ap']; } else { echo '0'; } ?></li>
              <li>Townhouse : <?php if (array_key_exists('th', $totalCommentData)) { echo $totalCommentData['th']; } else { echo '0'; } ?> </li>
          </ul>
        </div>
    </div>
</div>

<div class="stat-box total-listing">
    <h4>Member's prefered language</h4>
    <div id="showby-prefered-language" class="filter-area">
      <label class="label-filter">Show By</label>
      <label class="radio-inline">
        <input type="radio" value="weekly" for-chart="prefered_language" name="radiolanguage">Weekly
      </label>
      <label class="radio-inline">
        <input type="radio" value="monthly" for-chart="prefered_language" name="radiolanguage">Monthly
      </label>
      <label class="radio-inline">
        <input type="radio"  value="yearly" for-chart="prefered_language" name="radiolanguage">Yearly
      </label>
    </div>
    <div class="row">
        <div class="col-md-9">
            <canvas id="prefered_language" width="400" height="400"></canvas>
        </div>
        <div class="col-md-3">
            <div class="chart-legend">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-stop th"></i>Thai</li>
                    <li><i class="fa-li fa fa-stop en"></i>English</li>
                    <li><i class="fa-li fa fa-stop ja"></i>Japanese</li>
                    <li><i class="fa-li fa fa-stop zh"></i>Chinese</li>
                    <li><i class="fa-li fa fa-stop ko"></i>Korean</li>
                </ul>
            </div>
        </div>
        <div class="summary-chart">
          <h5>Summary</h5>
          <ul class="fa-ul">
              <li>Total Thai : <?php if (array_key_exists('th', $totalPreferedLanguage)) { echo $totalPreferedLanguage['th']; } else { echo '0'; } ?> </li>
              <li>Total English : <?php if (array_key_exists('en', $totalPreferedLanguage)) { echo $totalPreferedLanguage['en']; } else { echo '0'; } ?> </li>
              <li>Total Japanese : <?php if (array_key_exists('ja', $totalPreferedLanguage)) { echo $totalPreferedLanguage['ja']; } else { echo '0'; } ?></li>
              <li>Total Chinese : <?php if (array_key_exists('za', $totalPreferedLanguage)) { echo $totalPreferedLanguage['za']; } else { echo '0'; } ?></li>
              <li>Total Korean : <?php if (array_key_exists('ko', $totalPreferedLanguage)) { echo $totalPreferedLanguage['ko']; } else { echo '0'; } ?></li>
          </ul>
        </div>
    </div>
</div>

<!--<div class="stat-box total-listing">
    <h4>Listing number by province and districts</h4>
    <div id="showby-total-listing-province" class="filter-area">
      <label class="label-filter">Show By</label>
      <label class="radio-inline">
        <input type="radio" value="weekly" for-chart="total_listing_province" name="radionumberbyprovince">Weekly
      </label>
      <label class="radio-inline">
        <input type="radio" value="monthly" for-chart="total_listing_province" name="radionumberbyprovince">Monthly
      </label>
      <label class="radio-inline">
        <input type="radio"  value="yearly" for-chart="total_listing_province" name="radionumberbyprovince">Yearly
      </label>
    </div>
    <div class="row">
        <div class="col-md-9">
            <canvas id="total_listing_province" width="400" height="400"></canvas>
        </div>
        <div class="col-md-3">
            <div class="chart-legend">
                <ul class="fa-ul">
                    <li><i class="fa-li fa fa-stop rent"></i>Rent</li>
                    <li><i class="fa-li fa fa-stop sale"></i>Sale</li>
                    <li><i class="fa-li fa fa-stop new-proj"></i>New Project</li>
                </ul>
            </div>
        </div>
        <div class="summary-chart">
          <h5>Summary</h5>
          <ul class="fa-ul">
              <li>Total Listing :  </li>
              <li>Total Condo Listing : </li>
              <li>Total Apartment Listing :</li>
              <li>Total House Listing :</li>
              <li>Total Townhouse Listing : </li>
          </ul>
        </div>
    </div>
</div>-->

@endsection

@section('scripts')
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script src="{{ __asset('/js/manager/dashboard.js') }}"></script>
@endsection
