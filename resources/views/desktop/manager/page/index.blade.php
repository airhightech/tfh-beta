<?php
$active = 'pages';
?>

@extends('desktop.layouts.manager')

@section('content')

<h3>{{ tr('manager_page.static-page-list', 'Static page') }}</h3>

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ tr('manager_page.name-col', 'Name') }}</th>     
            <th>{{ tr('manager_page.link-col', 'Link') }}</th>    
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $pages = [
            'about-us' => tr('page.about-us', 'About us'),
            'privacy' => tr('page.privacy', 'Privacy policy'),
            'terms' => tr('page.terms', 'Terms and Conditions'),
            'contact-us' => tr('page.contact-us', 'Contact Us'),
            'careers' => tr('page.careers', 'Careers'),
            'advertise' => tr('page.advertise', 'Advertise with us')
        ];

        foreach ($pages as $link => $name) {
            ?>
            <tr>
                <td>{{ $name }}</td>
                <td><a href="/page/{{ $link }}" target="_blank">/page/{{ $link }}</a></td>
                <td class="text-center"><a href="/manager/page/edit?page={{ $link }}"><i class="fa fa-pencil"></i></a></td>
            </tr>
            <?php
        }
        ?>

    </tbody>
</table>

@endsection