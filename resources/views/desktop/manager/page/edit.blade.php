<?php
$active = 'pages';
$langs = \App\Helpers\Language::getActives();
?>

@extends('desktop.layouts.manager')

@section('content')
<h3>{{ $page->sku }} : {{ $page->getTranslatable('title') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/page/list">{{ tr('manager_page.back-to-list', 'Back to list') }}</a>
        </li>
    </ul>
</div>

<form action="/manager/page/update" method="post" id="page-form">

    <h4><?php echo tr('manager_page.title-label', 'Title'); ?> <span class="text-danger">*</span></h4>

    {{ __trfields('title', $page) }}

    <h4><?php echo tr('manager_page.html-label', 'Content'); ?> <span class="text-danger">*</span></h4>

    <div class="form-group">
        <div class="">
            <ul id="html-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#details-{{ $code }}" aria-controls="settings" role="tab" 
                           data-toggle="tab" data-content="#html-{{ $code }}" data-editor="#editor-{{ $code }}">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}" >
                        <div id="editor-{{ $code }}" class="notes">{!! $page->getTranslatable('html', $code) !!}</div>
                        <textarea id="html-{{ $code }}" name="html[{{ $code }}]" class="hidden">{!! $page->getTranslatable('html', $code) !!}</textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span class="sr-only"></span>
        </div>
    </div>

    <input type="hidden" name="id" value="{{ $page->id }}">
    <button type="button" class="btn btn-primary submit-ajax-form" data-target="page-form">{{ tr('button.update', 'UPDATE') }}</button>
    <a href="/page/{{ $page->sku }}" class="btn btn-link pull-right" target="_blank">{{ tr('manager_page.preview', 'Page preview') }}</a>
</form>
@endsection

@section('scripts')
<link href="/libs/summernote/summernote.css" rel="stylesheet">
<script src="/libs/summernote/summernote.js"></script>
<script src="{{ __asset('/js/manager/page.js') }}"></script>
@endsection