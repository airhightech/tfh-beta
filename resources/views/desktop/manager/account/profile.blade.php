<?php
$pdata = $user->getPersonalData();
$cdata = $user->getCompanyData();
$profile = $user->getProfile();

$profile_photo = $profile->getProfilePicture();

if (empty($pdata->firstname) && empty($pdata->lastname)) {
    $pdata->updateFromOptions(['name' => $user->name]);
    $pdata->save();
}
?>

<form id="account-details" method="post" action="/manager/account/update">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <?php
                if ($profile->haveProfilePicture()) {
                    ?>
                    <div class="profile-picture">
                        <img id="profile-img" src="{{ $profile_photo }}" class="img-responsive"/>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="form-group">
                <label for="name">{{ tr('manager_account.user-firstname', 'Firstname') }}</label>
                <input type="text" class="form-control" name="firstname" value="{{ $pdata->firstname }}">
            </div>

            <div class="form-group">
                <label for="name">{{ tr('manager_account.user-lastname', 'Lastname') }}</label>
                <input type="text" class="form-control" name="lastname" value="{{ $pdata->lastname }}">
            </div>

            <div class="form-group">
                <label for="email">{{ tr('manager_account.user-email', 'Email') }}</label>
                <input type="text" class="form-control" name="email" value="{{ $user->email }}">
            </div>

            <div class="form-group">
                <label for="name">{{ tr('manager_account.gender', 'Gender') }}</label>
                <select name="gender" class="form-control" data-name="gender">
                    <option value="">Select a gender</option>
                    <option value="female" <?php echo $pdata->gender == 'female' ? 'selected' : ''; ?> > Female</option>
                    <option value="male" <?php echo $pdata->gender == 'male' ? 'selected' : ''; ?> > Male</option>
                </select>
            </div>

            <div class="row">
                <div class="col-md-6">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="active" value="true" <?php echo __select($user->active, true, 'checked'); ?> > {{ tr('manager_account.user-active', 'Active') }}
                        </label>
                    </div>

                </div>
                <?php
                if ($user->isAgent()) {
                    ?>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="verified" value="true"
                                       <?php echo __select($user->verified, true, 'checked'); ?> > {{ tr('manager_account.user-verified', 'Verified') }}
                            </label>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>


        </div>
        <div class="col-md-4">
            <?php
            if ($user->ctype == 'company') {
                ?>
                <div class="form-group">
                    <label for="company_name">{{ tr('visitor_registration.realestate.company-name', 'Company name') }}</label>
                    <input type="text" class="form-control" name="company_name" value="{{ $cdata->company_name }}" >
                </div>
                <div class="form-group">
                    <label for="company_registration">{{ tr('visitor_registration.realestate.company-registration', 'Company registration') }}</label>
                    <input type="text" class="form-control" name="company_registration" value="{{ $cdata->company_registration }}" >
                </div>
                <?php
            }
            ?>

            <div class="form-group">
                <label for="phone">{{ tr('manager_account.phone', 'Phone') }}</label>
                <input type="text" class="form-control" name="phone" value="{{ $profile->phone }}">
            </div>

            <div class="form-group">
                <label for="line_id">{{ tr('manager_account.line_id', 'Line ID') }}</label>
                <input type="text" class="form-control" name="line_id" value="{{ $profile->line_id }}">
            </div>

            <div class="form-group">
                <label for="phone">{{ tr('manager_account.address', 'Address') }}</label>
                <textarea rows="5" name="custom_address" data-name="custom_address" class="form-control">{{ $profile->custom_address }}</textarea>
            </div>

        </div>
        <div class="col-md-4">
            <?php
            if ($user->isAgent()) {
                $max = 10;
                ?>
                <div class="form-group">
                    <label for="name">{{ tr('manager_account.citizen-id', 'Citizen ID') }}</label>
                    <input type="text" class="form-control" name="citizen_id" value="{{ $pdata->citizen_id }}" disabled>
                </div>        
                <div class="form-group">
                    <label for="name">{{ tr('manager_account.listing-level', 'Listing level') }}</label>
                    <select class="form-control" name="listing_level">
                        <option value="0">{{ tr('manager_account.select-listing-level', 'Select listing level') }}</option>
                        <?php
                        for ($i = 1; $i <= $max; $i++) {
                            ?>
                            <option value="{{ $i }}" <?php echo __select($user->listing_level, $i, 'selected'); ?>>{{ $i }}</option>
                            <?php
                        }
                        ?>
                    </select>
                    <p class="help-block">{{ tr('manager_account.listing-level-help', 'Agent with small level will appear in first places on search result') }}</p>
                </div>
                <?php
            }
            ?>
            <div class="form-group">
                <label for="phone">{{ tr('manager_account.languages', 'Language ability') }}</label>
                <p class="form-control-static">                    
                    <?php
                    $supported = explode(',', $profile->languages);
                    $languages = config('countries.languages');
                    foreach ($languages as $code => $lang) {
                        if (in_array($code, $supported)) {
                            echo $lang, ', ';
                        }
                    }
                    ?>                    
                </p>
            </div>

            <div class="form-group">
                <label for="phone">{{ tr('manager_account.nationality', 'Nationality') }}</label>
                <p class="form-control-static">                    
                    <?php
                    $countries = config('countries.list');
                    echo isset($countries[$pdata->nationality]) ? $countries[$pdata->nationality] : '';
                    ?>                    
                </p>
            </div>

        </div>

    </div>

    <input type="hidden" name="id" value="{{ $user->id }}"/>
</form>