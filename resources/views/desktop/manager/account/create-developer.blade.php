@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')

<?php
$active = 'developers';
?>

<h3>{{ tr('manager_account.developer.list-title', 'Create a land developer') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">           


        <li role="presentation" class="pull-right">
            <a href="/manager/developer/list">{{ tr('manager_account.back', 'Back to list') }}</a>
        </li>

    </ul>
</div>

<div class="">

    <form id="user-registration-form" action="/rest/register/developer" method="post">

        <div class="row registration-form-container registration-form">

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="company_name">{{ tr('visitor_registration.register.developer.company-name', 'Company name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" placeholder="" required>
                    <p class="input-error company_name hidden-error"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="company_registration">{{ tr('visitor_registration.register.developer.company-registration', 'Company Registration') }}</label>
                    <input type="text" class="form-control" name="company_registration" value="{{ old('company_registration') }}" placeholder="">
                    <p class="input-error company_registration hidden-error"></p>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="firstname">{{ tr('visitor_registration.register.developer.firstname', 'First name') }}</label>
                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="">
                    <p class="input-error firstname hidden-error"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">{{ tr('visitor_registration.register.developer.lastname', 'Last name') }}</label>
                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="">
                    <p class="input-error lastname hidden-error"></p>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="phone">{{ tr('visitor_registration.register.developer.telephone', 'Tel.') }} </label>
                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="mobile">{{ tr('visitor_registration.register.developer.mobile', 'Mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="">
                    <p class="input-error mobile hidden-error"></p>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="line_id">{{ tr('visitor_registration.register.developer.line-id', 'Line Id') }}</label>
                    <input type="text" class="form-control" name="line_id" value="{{ old('line_id') }}" placeholder="">
                </div>
            </div>
        </div>

    <!--        <div class="row registration-form with-marging-top">
                <div class="col-md-offset-2 col-md-4">
                    <div class="form-group">
                        <label for="email">{{ tr('visitor_registration.register.developer.email', 'ID (E-mail)') }} <span class="text-danger">*</span></label>
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="" required>
                        <p class="input-error email hidden-error"></p>
                    </div>
                </div>
            </div>
            <div class="row registration-form">
                <div class="col-md-offset-2 col-md-4">
                    <div class="form-group">
                        <label for="password">{{ tr('visitor_registration.register.developer.password', 'Password') }} <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" name="password" value="" placeholder="" required>
                        <p class="input-error password hidden-error"></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="password_confirmation">{{ tr('visitor_registration.register.developer.password-confirmation', 'Re-password') }} <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" name="password_confirmation" value="" placeholder="" required>
                        <p class="input-error password_confirmation hidden-error"></p>
                    </div>
                </div>
            </div>-->
        <div class="row registration-form with-marging-top register-action">

            <div class="col-md-offset-3 col-md-6 text-center with-marging-top">
                {{ csrf_field() }}
                <button type="button" data-target="user-registration-form" class="btn btn-gray submit-ajax-form">{{ tr('visitor_registration.register.developer.button-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>

</div>

@endsection