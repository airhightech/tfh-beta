<div id="account-modal" class="modal fade account-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('manager_account.update-user', 'Update user') }}</h4>
            </div>
            <div class="modal-body account-details">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-primary save-account">{{ tr('button.update', 'UPDATE') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="account-delete-modal" class="modal fade account-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('manager_account.delete-user', 'Delete user') }}</h4>
            </div>
            <div class="modal-body delete-account-details">
                Do you really want to delete <strong id="user-name"></strong>?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ tr('button.close', 'CLOSE') }}</button>
                <button type="button" class="btn btn-danger delete-user-account">{{ tr('button.delete', 'DELETE') }}</button>
            </div>
        </div>
    </div>
</div>