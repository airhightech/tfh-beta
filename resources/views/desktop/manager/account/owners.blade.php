@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')

<?php
$active = 'owners';
?>

<h3>{{ tr('manager_account.owner.list-title', 'List of property owners') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation" class="input">  
            <form class="form-inline" method="get" action="/manager/owner/list">
                <div class="form-group">
                    <input type="text" name="q" value="{{ $request->input('q') }}" 
                           class="form-control search" placeholder="{{ tr('manager_agent.search-placeholder', 'Search agent by name, email or ID') }}"/>
                </div>
                <button type="submit" class="btn">{{ tr('button.search', 'Search') }}</button>
            </form>
        </li>

        <?php
        $q = trim($request->input('q'));
        if (!empty($q)) {
            ?>
            <li role="presentation">
                <a href="/manager/owner/list">{{ tr('manager_account.reset-search', 'Reset search') }}</a>
            </li>
            <?php
        }
        ?>

    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="icon-col">ID</th>
            <th>{{ tr('manager_account.name-col', 'Name') }}</th>
            <th class="text-center small-col">{{ tr('manager_account.listing-col', 'Listing') }}</th>
            <th class="text-center small-col">{{ tr('manager_account.lang-col', 'Lang') }}</th>
            <th class="text-center small-col">{{ tr('manager_account.visit-col', 'Visits') }}</th>
            <th class="small-col">{{ tr('manager_account.last-login-col', 'Last login') }}</th>
            <th class="icon-col">{{ tr('manager_account.login-col', 'Login') }}</th>
            <th class="icon-col">{{ tr('manager_account.active-col', 'Active') }}</th>
            <th class="check-col"></th>
            <th class="check-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($users as $user) {
            //$last_login = date('d/m/y', strtotime($user->last_login));
            $lang = new \App\Helpers\Language($user->lang);
            $login_stats = $user->getLoginStats();   
            $profile = $user->getProfile();
            ?>
            <tr>
                <td>{{ $user->getPublicId() }}</td>
                <td><a href="/agent/{{ $user->id }}" target="_blank">{{ $profile->getName() }}</a></td>
                <td class="text-center">{{ $user->countProperty() }}</td>
                <td class="text-center"><img src="{{ $lang->getSmallFlag() }}"/></td>
                <td class="text-center">{{ $user->countLogin() }}</td>
                <td>{{ $user->getLastLogin() }}</td>
                <td class="text-center">
                    <?php
                    if (count($login_stats)) {
                        $sources = array_keys($login_stats);
                        ?>
                        <i class="fa fa-{{ $sources[0] }}"></i>
                        <?php
                    } else {
                        ?>
                        <i class="fa fa-envelope-o"></i>
                        <?php
                    }
                    ?>
                </td>
                <td class="text-center">
                    <?php
                    if ($user->active) {
                        ?>
                        <i class="fa fa-check user-active"></i>
                        <?php
                    } else {
                        ?>
                        <i class="fa fa-times user-inactive"></i>
                        <?php
                    }
                    ?>
                </td>
                <td class="text-center"><a href="#" class="edit-account" data-id="{{ $user->id }}"><i class="fa fa-pencil"></i></a></td>
                <td class="text-center">
                    <a href="#" class="delete-account" data-id="{{ $user->id }}" data-name="{{ $user->getProfile()->getName() }}">
                        <i class="fa fa-times text-danger"></i>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $users->links() }}

@include('desktop.manager.account.modal')

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/account.js') }}"></script>
@endsection