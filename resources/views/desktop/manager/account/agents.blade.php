@extends('desktop.layouts.manager')

@section('title', 'Page Title')

@section('content')

<?php
$active = 'agents';
?>

<h3>{{ tr('manager_account.agent.list-title', 'List of freelance agents') }}</h3>

<ul class="nav nav-tabs">
    <li role="presentation" class="{{ __select($type, 'agent', 'active') }}">
        <a href="/manager/agent/list?type=agent">{{ tr('manager_agent.freelance-tab', 'Freelances') }}</a>
    </li>
    <li role="presentation" class="{{ __select($type, 'company', 'active') }}">
        <a href="/manager/agent/list?type=company">{{ tr('manager_agent.realestate-tab', 'Companies') }}</a>
    </li>
</ul>

<div class="toolbar">
    <ul class="nav nav-pills">           
        <li role="presentation" class="input">  
            <form class="form-inline" method="get" action="/manager/agent/list">
                <div class="form-group">
                    <input type="text" name="q" value="{{ $request->input('q') }}" 
                           class="form-control search" placeholder="{{ tr('manager_agent.search-placeholder', 'Search agent by name, email or ID') }}"/>
                </div>
                <input type="hidden" name="type" value="{{ $type }}"/>
                <button type="submit" class="btn">{{ tr('button.search', 'Search') }}</button>
            </form>
        </li>
    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="text-center tiny-col">ID</th>
            <th>{{ tr('manager_account.name-col', 'Name') }}</th>
            <th>{{ tr('manager_account.listing-col', 'Listing') }}</th>
            <th class="text-center small-col">{{ tr('manager_account.visit-col', 'Visits') }}</th>
            <th>{{ tr('manager_account.company-col', 'Company') }}</th>
            <th class="check-col">R</th>
            <th class="check-col">V</th>
            <th class="check-col">M</th>            
            <th class="small-col">{{ tr('manager_account.last-login-col', 'Last login') }}</th>
            <th class="icon-col">{{ tr('manager_account.login-col', 'Login') }}</th>
            <th class="text-center check-col"><i class="fa fa-search"></i></th>
            <th class="text-center tiny-col">{{ tr('manager_account.active-col', 'Active') }}</th>
            <th class="check-col"></th>
            <th class="check-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($users as $user) {
            $lang = new \App\Helpers\Language($user->lang);
            $login_stats = $user->getLoginStats();
            $profile = $user->getProfile();
            ?>
            <tr>
                <td>{{ $user->getPublicId() }}</td>
                <td><a href="/agent/{{ $user->id }}" target="_blank">{{ $profile->getName() }}</a></td>
                <td class="text-center">{{ $user->countProperty() }}</td>
                <td class="text-center">{{ $profile->total_visitors }}</td>
                <td></td>
                <td class="text-center"><i class="fa fa-check text-danger"></i></td>
                <td class="text-center">
                    <?php
                    if ($user->verified) {
                        ?>
                        <i class="fa fa-check text-primary"></i>
                        <?php
                    } else {
                        ?>
                        <i class="fa fa-times user-inactive"></i>
                        <?php
                    }
                    ?>
                </td>
                <td class="text-center"><i class="fa fa-times user-inactive"></i></td>
                <td>{{ $user->getLastLogin() }}</td>
                <td class="text-center">
                    <?php
                    if (count($login_stats)) {
                        $sources = array_keys($login_stats);
                        ?>
                        <i class="fa fa-{{ $sources[0] }}"></i>
                        <?php
                    } else {
                        ?>
                        <i class="fa fa-envelope-o"></i>
                        <?php
                    }
                    ?>
                </td>
                <td class="text-center">{{ $user->listing_level }}</td>
                <td class="text-center">
                    <?php
                    if ($user->active) {
                        ?>
                        <i class="fa fa-check user-active"></i>
                        <?php
                    } else {
                        ?>
                        <i class="fa fa-times user-inactive"></i>
                        <?php
                    }
                    ?>
                </td>
                <td class="text-center"><a href="#" class="edit-account" data-id="{{ $user->id }}"><i class="fa fa-pencil"></i></a></td>
                <td class="text-center">
                    <a href="#" class="delete-account" data-id="{{ $user->id }}" data-name="{{ $user->getProfile()->getName() }}">
                        <i class="fa fa-times text-danger"></i>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $users->appends($request->all())->links() }}

@include('desktop.manager.account.modal')

@endsection

@section('scripts')
<script src="{{ __asset('/js/manager/account.js') }}"></script>
@endsection