<?php
$sidebar = isset($sidebar) ? $sidebar : '';

$currentUser = Auth::user();
$msgCount = $currentUser->countNewMessage();
$lstCount = $currentUser->countProperty();

if ($currentUser->ctype == 'visitor') {
    ?>
    @include('desktop.member.sidebar.visitor')
    <?php
} else {
    ?>
    @include('desktop.member.sidebar.agent')
    <?php
}
?>
