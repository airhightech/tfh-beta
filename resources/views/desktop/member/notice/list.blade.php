<?php

$sidebar = 'notice';

?>

@extends('desktop.layouts.member')

@section('content')

<h3>{{ tr('manager_notice.list-title', 'List of notices') }}</h3>

<table class="table table-list">
    <thead>
        <tr>
            <th class="medium-col">{{ tr('manager_notice.type-col', 'Type') }}</th>
            <th class="tiny-col text-center">{{ tr('manager_notice.date-col', 'Date') }}</th>
            <th>{{ tr('manager_notice.news-col', 'News') }}</th>     
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($notifications as $notice) {
            ?>
            <tr>
                <td>
                    {{ $notice->type }}
                </td>
                <td>
                    <?php
                    echo date('d/m/Y', strtotime($notice->updated_at))
                    ?>
                </td>
                <td>
                    <div class="content hideContent" >{!! $notice->message !!}</div>
                    <div class="show-more">
                        <a href="#" data-id="{{ $notice->id }}">{{ tr('manager_notice.show-more', 'Show more') }}</a>
                    </div>
                </td>                              
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $notifications->links() }}

@endsection


@section('scripts')
<script src="{{ __asset('/js/member/profile.js') }}"></script>
<script src="{{ __asset('/js/member/notice.js') }}"></script>
@endsection