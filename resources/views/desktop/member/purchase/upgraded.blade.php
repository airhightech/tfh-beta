<p class="text-success">
    <strong>{{ $property->getTranslatable('name') }}</strong> {{ tr('member_purchase.upgraded', 'have been successfully upgraded') }}
</p>
