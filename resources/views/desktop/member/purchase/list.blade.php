<?php
$sidebar = 'purchase';
?>

@extends('desktop.layouts.member')

@section('content')

<h3>{{ tr('manager_package.list-title', 'Package list') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/member/purchase/buy-featured" class="text-bold">{{ tr('member_purchase.buy-featured', 'Buy featured') }}</a>
        </li>
        <li role="presentation">
            <a href="/member/purchase/buy-exclusive" class="text-bold">{{ tr('member_purchase.buy-exclusive', 'Buy exclusive') }}</a>
        </li>
    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="small-col">{{ tr('member_purchase.col-id', 'Invoice ID') }}</th>
            <th>{{ tr('member_purchase.col-type', 'Type') }}</th>
            <th>{{ tr('member_purchase.col-package', 'Package') }}</th>
            <th class="small-col text-center">{{ tr('member_purchase.col-status', 'Status') }}</th>
            <th class="tiny-col text-center">{{ tr('member_purchase.col-used', 'Used') }}</th>
            <th class="tiny-col text-center">{{ tr('member_purchase.col-remaining', 'Remaining') }}</th>
            <th class="tiny-col text-center">{{ tr('member_purchase.col-period', 'Period') }}</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($purchases as $key => $purchase) {
            $data = $purchase->getData();

            if ($data) {
                $package = $data->getPackage();
                if ($package) {
                    ?>
                    <tr>
                        <td>{{ $purchase->invoice_id }}</td>
                        <td>{{ ucfirst($package->type) }}</td>
                        <td>{{ $package->getTranslatable('name') }}</td>
                        <td class="text-center">{{ tr('payment_status.'.$purchase->payment_status, ucfirst($purchase->payment_status))}}</td>
                        <td class="text-center">{{ $purchase->used_listing }}</td>
                        <td class="text-center">{{ ($package->listing_count - $purchase->used_listing) }}</td>
                        <td class="text-center">{{ $package->duration }}</td>
                    </tr>
                    <?php
                }
            }
        }
        ?>
    </tbody>
</table>

{{ $purchases->links() }}

@endsection

@section('scripts')
<script src="{{ __asset('/js/member/profile.js') }}"></script>
@endsection