@extends('desktop.layouts.default')

@section('title', 'Page Title')

@section('breadcrumb')
<li><a rhef="/">{{ tr('nav.breadcrumb.home', 'Home') }}</a></li>
<li class="active">{{ tr('nav.breadcrumb.buy-package', 'Buy package') }}</li>

@endsection

<?php
$image = __trimg('featured_image');
$line_qrcode = __dbconf('line_qrcode');
?>

@section('content')
<div class="container">
    <form id="payment-form" method="post" action="/member/purchase/buy-featured">
        <div class="buy-package-box package-box medium-container">
            <h1>{{ tr('member.package.buy-featured-package', 'Buy featured package') }}</h1>

            <div class="small-crumb">
                <span><a href="/member/listing/select-type">{{ tr('member.package.free-listing', 'Free listing') }}</a></span>
                <span class="active">{{ tr('member.package.upgrade', 'Upgrade') }}</span>
            </div>

            <div class="ad-container">
                <div class="package-selector">
                    <span class="active">{{ tr('member.package.featured', 'Featured') }}</span>
                    <span><a href="/member/purchase/buy-exclusive">{{ tr('member.package.exclusive', 'Exclusive') }}</a></span>                
                </div>
                <div class="poster">
                    <?php
                    if ($image) {
                        ?>
                        <img src="/image/{{ $image }}" class="img-responsive"/>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="package-stepper">
                <div class="header">
                    <span class="step">{{ tr('member.package.step', 'Step') }}</span>
                </div>            

                <div class="step-container">
                    <div class="step step-1">
                        <span><img src="/img/online_payment.jpg" class="img-responsive"/></span>
                        <p>{{ trget($featured_step_1) }}</p>
                    </div>
                    <div class="step step-2">
                        <span><img src="/img/screen-with-a-presentation.png" class="img-responsive"/></span>
                        <p>{{ trget($featured_step_2) }}</p>
                    </div>
                </div>
                <div class="footer">
                    <span class="arrow"><i class="fa fa-angle-right"></i></span>
                </div>    

            </div>

            <div class="package-list">
                <?php
                foreach ($packages as $package) {
                    $percent = 100 - (int) (100 * $package->discounted_price / $package->normal_price);
                    $current_price = 0;
                    ?>
                    <div class="package-container exclusive">
                        <div class="listing-container">
                            {{ $package->listing_count }} {{ tr('member.package.featured-listing', 'Featured listing') }}
                            <?php
                            if ($package->free_listing_count > 0) {
                                ?>
                                <span class="free-listing-count">
                                    + Free {{ $package->free_listing_count }} listing
                                </span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="price-container">
                            <?php
                            if ($package->discounted_price > 0) {
                                $current_price = $package->discounted_price;
                                ?>
                                <span class="old-price">{{ number_format($package->normal_price) }}</span>
                                <span class="actual-price">{{ number_format($package->discounted_price) }}</span>
                                <span class="percent-box">{{ $percent }}&percnt;<br/><i class="fa fa-arrow-down" aria-hidden="true"></i></span>
                                <?php
                            } else {
                                $current_price = $package->normal_price;
                                ?>
                                <span class="actual-price">{{ number_format($package->normal_price) }}</span>

                                <?php
                            }
                            ?>
                            <p class="currency">
                                BATH
                            </p>
                            <p class="per-month">
                                {{ $package->getTranslatable('name') }}
                            </p>
                            <p class="select-package">
                                <label for="package-{{ $package->id }}" data-ref="ico-{{ $package->id }}" class="package-checkbox" data-package="{{ $package->id }}">
                                    <i id="ico-{{ $package->id }}" class="ico-choice fa fa-circle" aria-hidden="true"></i>
                                </label>
                                <input type="radio" id="package-{{ $package->id }}" name="package" value="{{ $package->id }}" class="hidden"/>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>

            <div class="package-price-details">
                <table class="table" cellspacing="5">
                    <tr>
                        <td><div class="package-name"></div></td>
                        <td><div class="package-price"></div></td>
                    </tr>
                    <tr>
                        <td><div><?php echo tr('package.vat-label', '+ VAT (7&percnt;)'); ?></div></td>
                        <td><div class="package-vat"></div></td>
                    </tr>
                    <tr>
                        <td><div>{{ tr('package.total-amount', 'Total Amount') }}</div></td>
                        <td><div class="total-amount"></div></td>
                    </tr>
                </table>
            </div>
            <div class="package-price-select">
                <div class="alert alert-warning text-center">
                    {{ tr('member_purchase.select-package', 'Please select a package') }}
                </div>
            </div>

            <div class="package-payment-option">
                <div class="radio">
                    <label>
                        <input type="radio" class="poption" name="poption" value="bank">
                        {{ tr('member.package.payment-bank-trfer', 'Bank transfer') }}
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" class="poption" name="poption" value="card">
                        {{ tr('member.package.payment-bank-card', 'Credit card') }}
                    </label>
                </div>
            </div>

            <div class="package-bank-accounts">
                <table class="table">
                    <?php
                    foreach ($banks as $bank) {
                        ?>
                        <tr>
                            <td>
                                <input type="radio" name="bank" value="{{ $bank->id }}">
                            </td>
                            <td>
                                <div class="bank {{ $bank->code }}">
                                    <strong>{{ \App\Models\System\Bank::getBankName($bank->code) }}</strong><br/>
                                    {{ tr('package.account-name', 'Name') }}: {{ $bank->name }}<br/>
                                    {{ tr('package.bank-account', 'Bank Account No.') }} : {{ $bank->branch }}<br/>
                                    {{ tr('package.branch', 'Branch') }} : {{ $bank->account_no }}
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>

            </div>

            <p class="payment-warning">
                {{ tr('member.package.payment-limit-warning', '* The user must') }}
            </p>

            <p class="step-title">
                <span>
                    {{ tr('member.package.step-2-title', '2 METHODS TO SEND A RECEIPT') }}
                </span>
            </p>

            <div class="package-method-list">

                <?php
                if ($line_qrcode) {
                    ?>
                    <p>
                        1. {{ tr('member.package.receipt-method-line-text', 'You can SCAN our Line QR Code and send your receipt picture with your agent or owner ID') }}
                    </p>
                    <p>
                        <img src="/image/{{ $line_qrcode }}" class="img-responsive">
                    </p>
                    <p>
                        2. <?php echo tr('member.package.receipt-method-email-text', 'You can send the receipt picture with your agent or owner ID to info@thaifullhouse.com'); ?>
                    </p>
                    <?php
                } else {
                    echo tr('member.package.receipt-method-email-text', 'You can send the receipt picture with your agent or owner ID to info@thaifullhouse.com');
                }
                ?>
            </div>
            <div class="package-action text-right">
                {{ csrf_field() }}
                <button id="submit-payment" type="button" class="btn btn-blue btn-big" >{{ tr('member.package.payment-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="payment-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body">
                <h4 class="text-center">
                    {{ tr('member.package.buy.success-message', 'THANK YOU FOR YOU ORDER') }}
                </h4>
                <p class="text-center">{{ tr('member.package.buy.order-id', 'Order ID') }} : <span id="order-id"></span></p>
                <p class="text-center">{{ tr('member.package.buy.confirm-message', 'Please confirm your payment within 24 hours') }}</p>
                <p class="text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="payment-processing" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>            
            <div class="modal-body">
                <h4 class="text-center">
                    {{ tr('member.package.buy.processing-title', 'Getting payment data') }}
                </h4>
                <p class="text-center">{{ tr('member.package.buy.processing-message', 'We are redirecting you to our payment gateway, please do not close this window') }}</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

@endsection

@section('scripts')
<script>
    var packages = <?php echo json_encode($packages); ?>;
</script>
<script src="{{ __asset('/js/member/purchase.js') }}"></script>
@endsection