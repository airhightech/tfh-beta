<?php
$sidebar = 'purchase';
?>

@extends('desktop.layouts.member')

@section('content')

<h3>{{ tr('member_purchase.contact-title', 'Contact Us') }}</h3>


<p class="text-danger">
    {{ tr('member_purchase.contact-message', 'Our exclusive place is full, please contact us') }}
</p>
<div class="">
    <?php echo nl2br(__dbconf('footer_contact', true)); ?>
</div>


@endsection