<?php

$user = Auth::user();
$profile = $user->getProfile();
$personalData = $user->getPersonalData();
$companyData = $user->getCompanyData();

$profile_photo = $profile->getProfilePicture();

?>

<div class="member-profile-header">
    <div class="account-cover">
        <img id="cover-img" src="{{ $profile->getCoverPicture() }}" class="img-responsive"/>
        <label class="upload-trigger" for="cover">
            <input type="file" accept="image/*" name="cover" id="cover" data-id="{{ $user->id }}" class="hide">
            <i class="fa fa-camera"></i>
        </label>
        <span class="cover-loading"><img src="/img/transfer.gif" class="img-responsive"/></span>
    </div>
    <div class="member-header">
        <div class="profile-picture">
            <img id="profile-img" src="{{ $profile_photo }}" class="img-responsive"/>
            <label class="upload-trigger" for="photo">
                <input type="file" accept="image/*" name="photo" id="photo" data-id="{{ $user->id }}" class="hide">
                <i class="fa fa-camera"></i>
            </label>
            <?php
            /*if (!$profile->haveProfilePicture()) {
                ?>
                <label class="photo-required-msg">
                    {!! tr('visitor_registration.freelance.upload-photo-description', 
                                    '* Please upload your profile picture to get verified mark, this mark will give the customer confidence in your identity.') !!}
                </label>
                <?php
            }*/
            ?>             
        </div>
        <h4>
            <i class="fa fa-home"></i> {{ $profile->getName() }} 
            <?php
            if ($user->ctype == 'visitor') {
                ?>
                <a class="pull-right" href="/auth/register/type">
                    <small>{{ tr('member_profile.upgrade-profile-title', 'Are you an Agent ?') }}</small>
                </a>
                <?php
            } else {
                ?>
                <small>(Agent ID: {{ $user->getPublicId() }})</small>
                <?php
            }
            ?>
        </h4>
        <?php
        if ($user->ctype == 'agent') {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ tr('member_profile.contact-name', 'Contact name') }}</strong> : {{ $profile->getName() }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ tr('member_profile.phone', 'Phone') }}</strong> : {{ $profile->phone }}</td> 
                    <td><strong>{{ tr('member_profile.email', 'Email') }}</strong> : {{ $user->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ tr('member_profile.address', 'Address') }}</strong> : {{ $profile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        } else if ($user->ctype == 'company') {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ tr('member_profile.contact-name', 'Contact name') }}</strong> : {{ $profile->getName() }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ tr('member_profile.phone', 'Phone') }}</strong> : {{ $profile->phone }}</td> 
                    <td><strong>{{ tr('member_profile.email', 'Email') }}</strong> : {{ $user->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ tr('member_profile.address', 'Address') }}</strong> : {{ $profile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        } else if ($user->ctype == 'developer') {
            ?>
            <table class="table">
                <tr>
                    <td><strong>{{ tr('member_profile.company-name', 'Company name') }}</strong> : {{ $companyData->company_name }}</td> 
                    <td><strong>{{ tr('member_profile.website', 'Website') }}</strong> : {{ $companyData->website }}</td> 
                </tr>
                <tr>
                    <td><strong>{{ tr('member_profile.phone', 'Phone') }}</strong> : {{ $profile->phone }}</td> 
                    <td><strong>{{ tr('member_profile.email', 'Email') }}</strong> : {{ $user->email }}</td> 
                </tr>
            </table>
            <?php
        } else {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ tr('member_profile.personal-name', 'Contact name') }}</strong> : {{ $profile->getName() }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ tr('member_profile.phone', 'Phone') }}</strong> : {{ $profile->phone }}</td> 
                    <td><strong>{{ tr('member_profile.email', 'Email') }}</strong> : {{ $user->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ tr('member_profile.address', 'Address') }}</strong> : {{ $profile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        }
        ?>

    </div>
</div>