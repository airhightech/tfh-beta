<?php

$sidebar = 'profile';
$profile = $user->getProfile();
$personalData = $user->getPersonalData();
$companyData = $user->getCompanyData();

?>

@extends('desktop.layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ tr('nav.breadcrumb.home', 'Home') }}</li>

@endsection

@section('content')

<div class="box">
    <h4>{{ tr('member_profile.title', 'Setting profile') }}</h4>
    <table class="table setting-table">
        <tr>
            <td>{{ tr('member_profile.company-name', 'Company Name') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="company_name" value="{{ $companyData->company_name }}" data-name="company_name" 
                       class="setting-data-item"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.phone', 'Phone') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="phone" value="{{ $profile->phone }}" 
                       class="small setting-data-item" data-name="phone"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.citizen_id', 'Passport or citizen ID number') }} </td>
            <td class="setting-data">
                <input type="text" name="citizen_id" value="{{ $personalData->citizen_id }}" data-name="citizen_id" class="setting-data-item"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.custom_address', 'Address') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <textarea rows="5" name="custom_address" data-name="custom_address" class="setting-data-item">{{ $profile->custom_address }}</textarea>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.birthdate', 'Birthday') }} </td>
            <td class="setting-data">
                <input type="text" id="birthdate" name="birthdate" value="{{ $personalData->getBirthdate() }}" class="birthdate small" data-name="birthdate"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.languages', 'Language ability') }} </td>
            <td class="setting-data">
                <?php
                
                $supported = explode(',', $profile->languages);
                
                $languages = config('countries.languages');
                
                ?>
                <select id="languages" name="languages" class="setting-data-item languages" data-name="languages" multiple style="width: 100%">
                    <?php
                        foreach($languages as $code => $lang) {
                            ?>
                            <option value="{{ $code }}" <?php echo in_array($code, $supported) ? 'selected' : ''; ?> >
                                {{ $lang }}
                            </option>
                            <?php
                        }
                    ?>
                </select>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default" onclick="updateLanguages('#languages');">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.gender', 'Gender') }} </td>
            <td class="setting-data">
                <select name="gender" class="setting-data-item data-select" data-name="gender">
                    <option value="">Select a gender</option>
                    <option value="female" <?php echo $personalData->gender == 'female' ? 'selected' : ''; ?> > Female</option>
                    <option value="male" <?php echo $personalData->gender == 'male' ? 'selected' : ''; ?> > Male</option>
                </select>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.nationality', 'Nationality') }} </td>
            
            <td class="setting-data">                
                <select name="nationality" class="setting-data-item data-select" data-name="nationality">
                    <option value="">Select a nationality</option>
                    <?php        
                    
                        $countries = config('countries.list');
                    
                        foreach($countries as $country) {
                            ?>
                            <option value="{{ $country['code'] }}" <?php echo $personalData->nationality == $country['code'] ? 'selected' : ''; ?> > {{ $country['name'] }}</option>
                            <?php
                        }                    
                    ?>
                </select>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td>{{ tr('member_profile.introduction', 'Introduce yourself') }}</td>
            <td class="setting-data">
                <textarea rows="5" name="introduction" data-name="introduction" class="setting-data-item">{{ $profile->introduction }}</textarea>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        
        <tr>
            <td>{{ tr('member_profile.pinperty-link', 'Pinperty URL') }}</td>
            <td class="setting-data">
                <input type="text" name="pinperty_link" value="{{ $companyData->pinperty_link }}" data-name="pinperty_link" 
                       class="setting-data-item"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        
        <tr>
            <td>{{ tr('member_profile.twitter-link', 'Twitter URL') }}</td>
            <td class="setting-data">
                <input type="text" name="twitter_link" value="{{ $companyData->twitter_link }}" data-name="twitter_link" 
                       class="setting-data-item"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        
        <tr>
            <td>{{ tr('member_profile.facebook-link', 'Facebook URL') }}</td>
            <td class="setting-data">
                <input type="text" name="facebook_link" value="{{ $companyData->facebook_link }}" data-name="facebook_link" 
                       class="setting-data-item"/>
            </td>
            <td class="action">
<!--                <button type="button" class="btn btn-default">Update</button>-->
            </td>
        </tr>
        <tr>
            <td></td>
            
            <td class="setting-data">                
                <button type="button" class="btn btn-primary">Update</button>               
            </td>
            <td class="action">
<!--                -->
            </td>
        </tr>
    </table>
</div>

@endsection

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet">
<script src="/libs/chosen/chosen.jquery.js"></script>
<script src="{{ __asset('/js/member/profile.js') }}"></script>
@endsection