<?php
$sidebar = 'message';
?>

@extends('desktop.layouts.member')

@section('content')

<table class="table listing-table">
    <thead>
        <tr>
            <th style="width: 25px;"></th>
            <th style="width: 75px;">{{ tr('member.messages.col-date', 'Date') }}</th>
            <th>{{ tr('member.messages.col-topic', 'Message') }}</th>							
            <th style="width: 200px;">{{ tr('member.messages.col-name', 'Name') }}</th>
            <th style="width: 200px;">{{ tr('member_account.email-col', 'Email') }}</th>
            <th style="width: 200px;">{{ tr('member_account.phone-col', 'Phone') }}</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($messages as $message) {
            $time = strtotime($message->created_at);
            $date = date('d/M/Y', $time);
            ?>
            <tr class="even">
                <td style="width: 25px;"><input type="checkbox"/></td>
                <td style="width: 75px;">{{ $date }}</td>
                <td>{!! $message->message !!}</td>
                <td style="width: 200px;">{{ $message->sender_name }}</td>
                <td>{{ $message->sender_email }}</td>
                <td>{{ $message->sender_phone }}</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $messages->links() }}

@endsection

@section('scripts')
<script src="{{ __asset('/js/member/profile.js') }}"></script>
@endsection