<?php
$img = $property->getMainImage();
?>
<form id="listing-upgrade-exclusive" action="/rest/listing/exclusive-upgrade" method="post">
    <?php
    if ($img != null) {
        ?>
        <div class="row">
            <div class="col-md-3">
                <img src="{{ $img->getThumbUrl('64x64', true) }}" class="img-responsive"/>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <p class="form-control-static">{{ $property->getTranslatable('name') }} - {{ $property->getTitle() }}</p>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="form-group">
            <label for="property">{{ tr('member_upgrade.property', 'Property') }}</label>
            <p class="form-control-static">{{ $property->getTranslatable('name') }} - {{ $property->getTitle() }}</p>
        </div>
        <?php
    }
    ?>

    <div class="form-group">
        <label for="purchase">{{ tr('member_upgrade.select-purchase', 'Select a package') }}</label>
        <select class="form-control" name="purchase_id">
            <?php
            if (count($exclusives)) {
                foreach ($exclusives as $featured) {
                    $purchase = \App\Models\Customer\Purchase::find($featured->id);
                    $package = \App\Models\Listings\Package::find($featured->package_id);
                    ?>
                    <option value="{{ $purchase->id }}">{{ $package->getTranslatable('name') }}</option>
                    <?php
                }
            }
            ?>

        </select>
    </div>
    <div class="form-group">
        <label for="upgrade_start">{{ tr('member_upgrade.upgrade-start', 'Upgrade start') }}</label>
        <div class="upgrade_start_calendar"></div>
        <input type="hidden"  id="upgrade_start" name="upgrade_start" value="" placeholder="">
    </div>
    <input type="hidden" name="property_id" value="{{ $property->id }}"/>
</form>