<div class="form-group">
    <label for="listing_price" class="col-sm-2 control-label">{{ tr('manager_property.sale', 'Sale price') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="listing_price" value="{{ $listing->listing_price }}">
    </div>
    <label for="total_size" class="col-sm-2 control-label">{{ tr('manager_property.total-size', 'Size') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="total_size" value="{{ $extended->total_size }}">
    </div>
</div>

<div class="form-group">
    <label for="psf" class="col-sm-2 control-label">{{ tr('manager_property.psf', 'Price/Sqm') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="psf" value="{{ $extended->psf }}">
    </div>
    <label for="bedroom_num" class="col-sm-2 control-label">{{ tr('manager_property.bedrooms', 'Bedrooms') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="bedroom_num" value="{{ $extended->bedroom_num }}">
    </div>
</div>

<div class="form-group">                    
    <label for="floor_num" class="col-sm-2 control-label">{{ tr('manager_property.floor_num', 'Floor') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="floor_num" value="{{ $extended->floor_num }}">
    </div>
    <label for="bathroom_num" class="col-sm-2 control-label">{{ tr('manager_property.bathrooms', 'Bathrooms') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="bathroom_num" value="{{ $extended->bathroom_num }}">
    </div>
</div>

<div class="form-group">                    
    <label for="land_size" class="col-sm-2 control-label">{{ tr('manager_property.land_size', 'Land size') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="land_size" value="{{ $extended->land_size }}">
    </div>
    <label for="developer_id" class="col-sm-2 control-label">{{ tr('manager_property.developer_id', 'Developer') }}</label>
    <div class="col-sm-4">
        <select name="developer_id" class="form-control">
            <option value="0"><?php echo tr('property.select-developer', 'Select a developer'); ?></option>
            <?php
            foreach ($developers as $developer) {
                ?>
                <option value="{{ $developer->user_id }}"<?php echo $building->developer_id == $developer->user_id ? ' selected' : ''; ?>>{{ $developer->getName() }}</option>
                <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">    
    <label for="movein_date" class="col-sm-2 control-label">{{ tr('manager_property.movein_date', 'Move-in') }}</label>
    <div class="col-sm-4">
        <?php
        if ($listing->movein_date) {
            ?>
            <input type="text" id="movein_date" name="movein_date" value="<?php echo __pgdate2hr($listing->movein_date); ?>" 
                   class="form-control single-date">
            <?php
        } else {
            ?>
            <input type="text" id="movein_date" name="movein_date" value="<?php echo date('d/m/Y'); ?>" 
                   class="form-control single-date">
            <?php
        }
        ?>
    </div>
    <label for="year_built" class="col-sm-2 control-label">{{ tr('manager_property.year-built', 'Year built') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="year_built" value="{{ $extended->year_built }}" placeholder="">
    </div>
</div>

