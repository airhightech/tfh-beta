@extends('desktop.layouts.member')

@section('title', 'Page Title')

<?php
$active = 'condo-community';
$title = tr('manager_property.condo-details', 'Condo details');
$tab_link = 'condo';
$tab = 'details';
$extended = $property->getExtendedData();
$building = $property->getBuildingData();
$listing = $property->getListingData();

$building_id = $property->belongsToBuilding();
?>

@section('content')

<h3>{{ $property->getTranslatable('name') }}</h3>

<div class="manager-content">     

    @include('desktop.member.property.tab')

    <div class="form-container row">
        <div class="col-md-12">

            <h4>{{ tr('manager_property.general-info', 'General info') }}</h4>
            <form id="general-info-form" class="form-horizontal" action="/rest/property/details-info" method="post">

                @include($view)

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" onclick="submitGenericForm('general-info-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>
            </form>       

            <?php
            if (!$building_id) {
                ?>
                <h4>Facilities</h4>
                <form id="update-facility-form" class="form-horizontal" action="/rest/property/update-facilities" method="post">
                    <?php
                    if (count($facilities)) {
                        foreach ($facilities as $facility) {
                            ?>
                            <div class="checkbox col-md-4">
                                <label>
                                    <input type="checkbox" name="facilities[]" value="{{ $facility->id }}" <?php echo in_array($facility->id, $property_facilities) ? 'checked' : ''; ?> > {{ $facility->getTranslatable('name') }}
                                </label>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                            <button type="button" class="btn btn-primary pull-right" 
                                    onclick="submitGenericForm('update-facility-form')">{{ tr('button.update', 'UPDATE') }}</button>
                        </div>
                    </div>
                </form>
                <?php
            }
            ?>

            <h4>Features</h4>
            <form id="update-feature-form" class="form-horizontal" action="/rest/property/update-features" method="post">

                <?php
                if (count($features)) {
                    foreach ($features as $feature) {
                        ?>
                        <div class="checkbox col-md-4">
                            <label>
                                <input type="checkbox" name="facilities[]" value="{{ $feature->id }}" <?php echo in_array($feature->id, $property_features) ? 'checked' : ''; ?> > {{ $feature->getTranslatable('name') }}
                            </label>
                        </div>
                        <?php
                    }
                }
                ?>

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" 
                                onclick="submitGenericForm('update-feature-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>

            </form>

            <h4>Nearby Transportation</h4>

            <form id="nearby-info-form" class="form-horizontal" action="/rest/property/nearby-info" method="post">

                @include('desktop.member.posting.generic.nearby-transportation')

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" 
                                onclick="submitGenericForm('nearby-info-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>
            </form>

            <h4>Optional features</h4>
            <form id="update-optional-feature-form" class="form-horizontal" action="/rest/property/update-optional-features" method="post">

                <?php
                if (count($optinal_features)) {
                    foreach ($optinal_features as $feature) {
                        ?>
                        <div class="checkbox col-md-4">
                            <label>
                                <input type="checkbox" name="facilities[]" value="{{ $feature->id }}" <?php echo in_array($feature->id, $property_features) ? 'checked' : ''; ?> > {{ $feature->getTranslatable('name') }}
                            </label>
                        </div>
                        <?php
                    }
                }
                ?>

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" 
                                onclick="submitGenericForm('update-optional-feature-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>
            </form>

            <form id="other-details-form" class="form-horizontal" action="/rest/property/other-details" method="post">
                <div class="row">
                    <div class="col-md-5">
                        <h4>{{ tr('manager_property.extended-custom-address', 'Custom address') }}</h4>
                        {{ __trfields('custom_address', $extended, true) }}
                    </div>
                    <div class="col-md-7">
                        <h4>{{ tr('manager_property.extended-details', 'Details') }}</h4>
                        {{ __trfields('details', $extended, true) }}
                    </div>
                </div>


                <div class="form-group clearfix">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $property->id }}" class="property-id"/>
                        <button type="button" class="btn btn-primary pull-right" onclick="submitGenericForm('other-details-form')">{{ tr('button.update', 'UPDATE') }}</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

@endsection

@section('scripts')

<script src="{{ __asset('/js/manager/rooms.js') }}">
</script>
<link href="/libs/chosen/chosen.min.css" rel="stylesheet">
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script>
                            $(function () {

                                $(".bts").chosen({max_selected_options: 4});
                                $(".mrt").chosen({max_selected_options: 4});
                                $(".apl").chosen({max_selected_options: 4});

                            });

                            var dateFormat = "dd/mm/yy";
                            $("#movein_date").datepicker({
                                minDate: new Date(),
                                defaultDate: "+1w",
                                changeMonth: true,
                                dateFormat: dateFormat
                            });

</script>
@endsection