<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/member/listing"><i class="fa fa-long-arrow-left"></i> {{ tr('manager_property.back', 'Back') }}</a>
        </li>
    </ul>
</div>

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'info' ? 'class="active"' : ''; ?> >
        <a href="/member/property/info/{{ $property->id }}">{{ tr('manager_property.info-tab', "Info") }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'details' ? 'class="active"' : ''; ?>>
        <a href="/member/property/details/{{ $property->id }}">{{ tr('manager_property.details-tab', 'Details') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'media' ? 'class="active"' : ''; ?>>
        <a href="/member/property/media/{{ $property->id }}">{{ tr('manager_property.media-tab', 'Media') }}</a>
    </li>
</ul>