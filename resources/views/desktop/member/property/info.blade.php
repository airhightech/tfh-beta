@extends('desktop.layouts.member')

@section('title', 'Page Title')

<?php
$tab = 'info';

$address = $property->getAddress();
$extended = $property->getExtendedData();
$building = $property->getBuildingData();
$listing = $property->getListingData();
$langs = \App\Helpers\Language::getActives();

$new_project = isset($new_project) ? $new_project : false;
?>

@section('content')

<h3>{{ $property->getTranslatable('name') }}</h3>

<div class="manager-content">     

    @include('desktop.member.property.tab')

    <form id="condo-info-form" action="/rest/property/info" method="post">

        <div class="form-container row">
            <div class="col-md-6">

                <h4><?php echo tr('manager_property.listing-type-label', 'Listing type'); ?> <span class="text-danger">*</span></h4>

                <div class="row">
                    <div class="col-md-6">
                        <div class="radio">
                            <label>
                                <input type="radio" name="ltype" value="rent" required <?php echo $listing->ltype == 'rent' ? 'checked' : ''; ?> >
                                <span>{{ tr('member_posting.listing-type.rent', 'RENT') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio">
                            <label>
                                <input type="radio" name="ltype" value="sale" required <?php echo $listing->ltype == 'sale' ? 'checked' : ''; ?> >
                                <span>{{ tr('member_posting.listing-type.sale', 'SALE') }}</span>
                            </label>
                        </div>
                    </div>
                </div>                

                <h4><?php echo tr('manager_property.name-label', 'Property names'); ?> <span class="text-danger">*</span></h4>

                {{ __trfields('name', $property) }}

                <h4><?php echo tr('manager_property.custom-address-label', 'Custom address'); ?></h4>

                {{ __trfields('custom_address', $extended) }}

                <div class="checkbox">
                    <label>
                        <input type="checkbox" {{ $property->published ? 'checked' : '' }} onclick="toggleStatus({{ $property->id }})"/> 
                        {{ tr('manager_property.active-label', 'Active') }}
                    </label>
                </div>

                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id" value="{{ $property->id }}">
                <button type="button" class="btn btn-primary" onclick="submitGenericForm('condo-info-form')">{{ tr('button.update', 'UPDATE') }}</button>
            </div>

            <div class="col-md-6">

                <h4><?php echo tr('manager_property.address-label', 'Property location'); ?></h4>

                <div class="form-group">
                    <label for="ptype">{{ tr('manager_property.property-type-label', 'Type') }}</label>
                    <select class="form-control">
                        <option value="ap" <?php echo $property->ptype == 'ap' ? 'selected' : ''; ?> >
                            {{ tr('member_posting.property-type.apartment', 'Apartment') }}
                        </option>
                        <option value="cd" <?php echo $property->ptype == 'cd' ? 'selected' : ''; ?> >
                            {{ tr('member_posting.property-type.condominium', 'Condominium') }}
                        </option>
                        <option value="sh" <?php echo $property->ptype == 'sh' ? 'selected' : ''; ?> >
                            {{ tr('member_posting.property-type.detached-house', 'Single House') }}
                        </option>
                        <option value="th" <?php echo $property->ptype == 'th' ? 'selected' : ''; ?> >
                            {{ tr('member_posting.property-type.townhouse', 'Townhouse') }}
                        </option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="ptype">{{ tr('manager_property.property-type-label', 'Type') }}</label>
                    <select name="developer_id" class="form-control">
                        <option value="0"><?php echo tr('property.select-developer', 'Select a developer'); ?></option>
                        <?php
                        foreach ($developers as $developer) {
                            ?>
                            <option value="{{ $developer->user_id }}"<?php echo $building->developer_id == $developer->user_id ? ' selected' : ''; ?>>{{ $developer->getName() }}</option>
                            <?php
                        }
                        ?>
                    </select>
                </div>

                <?php
                $building_id = $property->belongsToBuilding();

                if (!$building_id) {
                    ?>
                    <div class="form-group">
                        <label for="">{{ tr('manager_property.province-label', 'Province') }}</label>
                        <select class="form-control" id="province_id" name="province_id">
                            <?php
                            if ($property->province_id == 0) {
                                ?>
                                <option value="0">{{ tr('manager_property.province-select', 'Select a province') }}</option>
                                <?php
                            }
                            foreach ($provinces as $province) {
                                ?>
                                <option value="{{ $province->id }}" <?php echo (isset($address->province_id) && $province->id == $address->province_id) ? 'selected' : ''; ?> >
                                    <?php echo $province->getTranslatable('name'); ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">{{ tr('manager_property.district-label', 'District') }}</label>
                        <select class="form-control" id="district_id" name="district_id" {{ isset($address->district_id) ? '' : 'disabled' }}>
                        <?php
                        if (count($districts)) {
                            foreach ($districts as $district) {
                                ?>
                                        <option value="<?php echo $district->id; ?>" <?php (isset($address->district_id) && $district->id == $address->district_id) ? 'selected' : ''; ?> >
                                                <?php echo $district->getTranslatable('name') ?>
                                    </option>
                                    <?php
                                }
                            } else {
                                ?>
                                <option value="0">{{ tr('manager_property.province-select', 'Please select a province') }}</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for=""><?php echo tr('manager_property.area-label', 'Area'); ?></label>
                        <select class="form-control" id="area_id" name="area_id" {{ isset($address->area_id) ? '' : 'disabled' }}>

                                <?php
                                if (count($areas)) {
                                    foreach ($areas as $area) {
                                        ?>
                                        <option value="{{ $area->id }}" {{ (isset($address->area_id) && $area->id == $address->area_id) ? 'selected' : '' }}>
                                        {{ $area->getTranslatable('name') }}
                                    </option>
                                    <?php
                                }
                            } else {
                                ?>
                                <option value="0">{{ tr('manager_property.district-select', 'Please select a district') }}</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for=""><?php echo tr('manager_property.postal-code-label', 'Postal code'); ?></label>
                        <input type="text" class="form-control" id="postal_code" name="postal_code" value="{{ old('postal_code', $address->postal_code) }}" placeholder="">
                    </div>               

                    <div class="form-group">
                        <label for=""><?php echo tr('manager_property.latitude-label', 'Latitude'); ?></label>
                        <input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat', $address->lat()) }}" placeholder="">
                    </div>

                    <div class="form-group">
                        <label for=""><?php echo tr('manager_property.longitude-label', 'Longitude'); ?></label>
                        <input type="text" class="form-control" id="lng" name="lng" value="{{ old('lng', $address->lng()) }}" placeholder="">
                    </div>
                    <?php
                } else {
                    $_building = $property->getBuilding();
                    ?>
                    <div class="alert alert-info">
                        {{ tr('manager_property.property_location_sync', 'The address of this property is synched with the address of his condo') }} : 
                        <strong>{{ $_building->getTranslatable('name') }}</strong>
                    </div>
                    <?php
                }
                ?>
            </div>

            <?php
            if (!$building_id) {
                ?>
                <div class="col-md-12">
                    <div id="map-picker" 
                         style="width: 100%; height: 400px; margin-top: 25px; margin-bottom: 15px;"></div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="area" name="area" placeholder="Type the area name ...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="clear-area-btn" type="button">Clear</button>
                            </span>
                        </div>

                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </form>

</div>

@endsection

<?php
$lat = old('lat', $address->lat());
$lng = old('lng', $address->lng());

$lat = $lat ? $lat : 13.746318;
$lng = $lng ? $lng : 100.534875;
?>

@section('scripts')
<link href="/libs/chosen/chosen.min.css" rel="stylesheet"> 
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&key={{ config('google.browser_key') }}'></script>
<script src="/js/lp/locationpicker.jquery.js"></script>
<script src="/libs/moment.min.js"></script>
<script src="{{ __asset('/js/visitor/address.js') }}"></script>
<script src="{{ __asset('/js/manager/property.js') }}"></script>
<script src="{{ __asset('/js/manager/dates.js') }}"></script>
<script>
                    var lat0 = <?php echo $lat; ?>;
                    var lng0 = <?php echo $lng; ?>;
</script>
@endsection
