<div class="form-group">
    <label for="deposite" class="col-sm-2 control-label">{{ tr('manager_property.deposit', 'Deposit') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="deposite" value="{{ $listing->deposite }}">
    </div>
    <label for="total_size" class="col-sm-2 control-label">{{ tr('manager_property.total-size', 'Size') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="total_size" value="{{ $extended->total_size }}">
    </div>
</div>

<div class="form-group">
    <label for="listing_price" class="col-sm-2 control-label">{{ tr('manager_property.rental', 'Rent') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="listing_price" value="{{ $listing->listing_price }}">
    </div>
    <label for="bedroom_num" class="col-sm-2 control-label">{{ tr('manager_property.bedrooms', 'Bedrooms') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="bedroom_num" value="{{ $extended->bedroom_num }}">
    </div>
</div>

<div class="form-group">                    
    <label for="psf" class="col-sm-2 control-label">{{ tr('manager_property.psf', 'Price/Sqm') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="psf" value="{{ $extended->psf }}">
    </div>
    <label for="bathroom_num" class="col-sm-2 control-label">{{ tr('manager_property.bathrooms', 'Bathrooms') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="bathroom_num" value="{{ $extended->bathroom_num }}">
    </div>
</div>

<div class="form-group">                    
    <label for="floor_num" class="col-sm-2 control-label">{{ tr('manager_property.floor_num', 'Floor') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="floor_num" value="{{ $extended->floor_num }}">
    </div>
    <label for="year_built" class="col-sm-2 control-label">{{ tr('manager_property.year-built', 'Year built') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="year_built" value="{{ $extended->year_built }}" placeholder="">
    </div>
</div>

<div class="form-group">                    
    <label for="number_of_tower" class="col-sm-2 control-label">{{ tr('manager_property.tower', 'Tower') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="number_of_tower" value="{{ $building->number_of_tower }}">
    </div>
    <label for="developer_id" class="col-sm-2 control-label">{{ tr('manager_property.developer_id', 'Developer') }}</label>
    <div class="col-sm-4">
        <select name="developer_id" class="form-control">
            <option value="0"><?php echo tr('property.select-developer', 'Select a developer'); ?></option>
            <?php
            foreach ($developers as $developer) {
                ?>
                <option value="{{ $developer->user_id }}"<?php echo $building->developer_id == $developer->user_id ? ' selected' : ''; ?>>{{ $developer->getName() }}</option>
                <?php
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group">                    
    <label for="minimal_rental_period" class="col-sm-2 control-label">{{ tr('manager_property.minimal_rental_period', 'Minimun rental period') }}</label>
    <div class="col-sm-4">
        <select name="minimal_rental_period" class="form-control">
            <option value="0">{{ tr('listing.select-period', 'Select a period') }}</option>
            <?php
            for ($period = 1; $period <= 12; $period++) {
                ?>
                <option value="{{ $period }}" <?php echo $listing->minimal_rental_period == $period ? 'selected' : ''; ?> >{{ $period }} months</option>
                <?php
            }
            ?>
        </select>
    </div>
    <label for="parking_lot" class="col-sm-2 control-label">{{ tr('manager_property.parking-lot', 'Parking lot') }}</label>
    <div class="col-sm-4">
        <input type="number" class="form-control" name="parking_lot" value="{{ $building->parking_lot }}" placeholder="">
    </div>
</div>