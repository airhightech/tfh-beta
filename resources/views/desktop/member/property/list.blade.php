<?php
$sidebar = 'listing';
$available_exclusive = $user->countAvailableExclusive();
$available_featured = $user->countAvailableFeatured();
?>

@extends('desktop.layouts.member')

@section('content')

<div class="box">
    <h4>{{ tr('member_listing.title', 'Property listing') }} ({{ $properties->total() }})</h4>
    <a href="/member/listing/select-type" class="btn btn-blue pull-right right-action">{{ tr('member_listing.create-new', 'Add new property') }}</a>
    <div class="clearfix"></div>

    <table class="table table-filter">
        <tr>
            <td class="filter-label">{{ tr('manager_listing.filter-type', 'Filters') }}</td>
            <td class="filter-all">
                <input type="radio" name="lt" value="all" class="filter" <?php echo array_get($filters, 'lt') == 'all' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td class="filter">
                <input type="radio" name="lt" value="rent" 
                       class="filter" <?php echo array_get($filters, 'lt') == 'rent' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-type-rent', 'Rent') }}
            </td>
            <td class="filter">
                <input type="radio" name="lt" value="sale" 
                       class="filter" <?php echo array_get($filters, 'lt') == 'sale' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-type-sale', 'Sale') }}
            </td>
            <td class="filter"></td>
            <td class="filter"></td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-property', 'Property type') }}
            </td>
            <td>
                <input type="radio" name="pt" value="all" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'all' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="pt" value="cd" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'cd' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-condo', 'Condo') }}
            </td>
            <td>
                <input type="radio" name="pt" value="th" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'th' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-townhouse', 'Townhouse') }}
            </td>
            <td>
                <input type="radio" name="pt" value="sh" 
                       class="filter" <?php echo array_get($filters, 'pt') == 'sh' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-house', 'Single house') }}
            </td>
            <td>
                <input type="radio" name="pt" value="ap" 
                       class="filter" <?php echo array_get($filters, 'property') == 'ap' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-property-apartment', 'Apartment') }}
            </td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-class', 'Class') }}
            </td>
            <td>
                <input type="radio" name="class" value="all" 
                       class="filter" <?php echo array_get($filters, 'class') == 'all' ? 'checked' : ''; ?> >
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="class" value="standard" 
                       class="filter" <?php echo array_get($filters, 'class') == 'standard' ? 'checked' : ''; ?> >
                {{ tr('manager_listing.filter-class-standard', 'Standard') }}
            </td>
            <td>
                <input type="radio" name="class" value="featured" 
                       class="filter" <?php echo array_get($filters, 'class') == 'featured' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-class-featured', 'Featured') }}
            </td>
            <td>
                <input type="radio" name="class" value="exclusive" 
                       class="filter" <?php echo array_get($filters, 'class') == 'exclusive' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-class-exclusive', 'Exclusive') }}
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="filter-label">
                {{ tr('manager_listing.filter-status', 'Status') }}
            </td>
            <td>
                <input type="radio" name="status" value="all" 
                       class="filter" <?php echo array_get($filters, 'status') == 'all' ? 'checked' : ''; ?> > 
                {{ tr('manager_listing.filter-all', 'All') }}
            </td>
            <td>
                <input type="radio" name="status" value="expired" 
                       class="filter" <?php echo array_get($filters, 'status') == 'expired' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-status-expired', 'Expired') }}
            </td>
            <td>
                <input type="radio" name="status" value="present" 
                       class="filter" <?php echo array_get($filters, 'status') == 'present' ? 'checked' : ''; ?> >  
                {{ tr('manager_listing.filter-status-present', 'Present') }}
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>

    <div class="listing-table-container">
        <table class="table table-list">
            <thead>
                <tr>
                    <th style="width: 50px;">ID</th>
                    <th>{{ tr('member_listing.col-image', 'Image') }}</th>
                    <th style="width: 50px;">{{ tr('member_listing.col-class', 'Class') }}</th>
                    <th style="width: 50px;">{{ tr('member_listing.col-type', 'Type') }}</th>
                    <th style="width: 150px;">{{ tr('member_listing.col-area', 'Area') }}</th>
                    <th style="width: 50px;" class="text-right">{{ tr('member_listing.col-size', 'Size') }}</th>
                    <th style="width: 75px;" class="text-right">{{ tr('member_listing.col-price', 'Price') }}</th>
                    <th style="width: 200px;">{{ tr('member_listing.col-start', 'Start') }}</th>
                    <th style="width: 50px;">{{ tr('member_listing.col-status', 'Status') }}</th>
                    <th style="width: 50px;">{{ tr('member_listing.col-active', 'Active') }}</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($properties as $key => $property) {
                    $extended = $property->getExtendedData();
                    $listing = $property->getListingData();

                    $area = '';

                    $address = $property->getAddress();

                    if ($address) {
                        $_area = $address->getArea();
                        if ($_area) {
                            $area = $_area->getTranslatable('name');
                        }
                    }

                    $img = $property->getMainImage();
                    ?>
                    <tr class="odd">
                        <td><a href="/property/{{ $property->id }}" target="_blank">{{ $property->id }}</a></td>
                        <td rowspan="2">
                            <a href="/property/{{ $property->id }}" target="_blank">
                            <?php
                            if ($img != null) {
                                ?>
                                <img src="{{ $img->getThumbUrl('64x64', true) }}" class="img-responsive"/>
                                <?php
                            } else {
                                ?>
                                <img src="" class="img-responsive"/>
                                <?php
                            }
                            ?>
                            </a>
                        </td>
                        <td>{{ ucfirst($listing->listing_class) }}</td>
                        <td>{{ $listing->ltype == 'rent' ? 'RENT' : 'SALE' }}</td>
                        <td>{{ $area }}</td>
                        <td class="text-right">{{ $extended->total_size }}</td>
                        <td class="text-right">{{ number_format($listing->listing_price) }}</td>
                        <td>{{ $property->getPublicationPeriode() }}</td>
                        <td>{{ ucfirst($property->getStatus()) }}</td>
                        <td class="text-center">
                            <input type="checkbox" {{ $property->published ? 'checked' : '' }} onclick="toggleStatus({{ $property->id }})"/></td>
                    </tr>
                    <tr class="even">
                        <td style="width: 50px;"></td>
                        <td colspan="8" class="text-right">
                            <span class="label label-default">{{ $property->ptype }}</span>
                            <span class="label label-default">{{ $property->clicks + 0 }}</span>
                            <?php
                            if ($listing->listing_class == 'standard') {

                                if ($available_exclusive > 0) {
                                    ?>
                                    <span class="label label-warning" onclick="upgradeExclusive({{ $property->id }});">{{ tr('member_listing.upgrade-exclusive', 'Upgrade to Exclusive') }}</span>
                                    <?php
                                } else {
                                    ?>
                                    <a href="/member/purchase/buy-exclusive" class="label label-warning">{{ tr('member_listing.upgrade-exclusive', 'Upgrade to Exclusive') }}</a>
                                    <?php
                                }


                                if ($available_featured > 0) {
                                    ?>
                                    <span class="label label-success" onclick="upgradeFeatured({{ $property->id }});">{{ tr('member_listing.update-featured', 'Upgrade to Featured') }}</span>
                                    <?php
                                } else {
                                    ?>
                                    <a href="/member/purchase/buy-featured" class="label label-success">{{ tr('member_listing.update-featured', 'Upgrade to Featured') }}</a>
                                    <?php
                                }
                                ?>


                                <?php
                            } else if ($listing->listing_class == 'exclusive') {
                                
                            } else if ($listing->listing_class == 'featured') {
                                if ($available_exclusive > 0) {
                                    ?>
                                    <span class="label label-warning" onclick="upgradeExclusive({{ $property->id }});">{{ tr('member_listing.upgrade-exclusive', 'Upgrade to Exclusive') }}</span>
                                    <?php
                                } else {
                                    ?>
                                    <a href="/member/purchase/buy-exclusive" class="label label-warning">{{ tr('member_listing.upgrade-exclusive', 'Upgrade to Exclusive') }}</a>
                                    <?php
                                }
                            }
                            ?>
                            <a href="/member/property/details/{{ $property->id }}" class="label label-primary">{{ tr('member_listing.edit', 'Edit') }}</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            </tbody>
        </table>
        {{ $properties->links() }}
    </div>
</div>

<?php
if ($available_exclusive > 0) {
    ?>
    <div id="exclusive-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ tr('member_listing.upgrade-exclusive', 'Upgrade to Exclusive') }}</h4>
                </div>
                <div class="modal-body exclusive-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary submit-ajax-form" data-target="listing-upgrade-exclusive">Apply upgrade</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <?php
}

if ($available_featured > 0) {
    ?>
    <div id="featured-modal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ tr('member_listing.update-featured', 'Upgrade to Featured') }}</h4>
                </div>
                <div class="modal-body featured-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary submit-ajax-form" data-target="listing-upgrade-featured">Apply upgrade</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <?php
}

?>

<div id="upgrade-modal-success" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ tr('member_listing.update-featured', 'Upgrade to Featured') }}</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/member/upgrade.js') }}"></script>
<script src="{{ __asset('/js/member/profile.js') }}"></script>
<script src="{{ __asset('/js/member/property-list.js') }}"></script>
@endsection
