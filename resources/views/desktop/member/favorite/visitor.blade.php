<?php
$sidebar = 'favorite';
?>

@extends('desktop.layouts.member')

@section('content')

<h3>{{ tr('member_favorite.title', 'Favorite') }}</h3>

<table class="table listing-table">
    <thead>
        <tr>
            <th class="small-col">{{ tr('member_favorite.col-property-id', 'Property ID') }}</th>
            <th >{{ tr('member_favorite.col-date', 'Picture') }}</th>			
            <th class="c">{{ tr('member_favorite.col-name', 'Likes') }}</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($favorites as $favorite) {

            $image = $favorite->getMainImage();
            ?>
            <tr class="even">
                <td><a href="/property/{{ $favorite->id }}" target="_blank">{{ $favorite->id }}</a></td>
                <td>
                    <a href="/property/{{ $favorite->id }}" target="_blank">
                        <?php
                        if ($image != null) {
                            ?>
                            <img src="{{ $image->getThumbUrl('200x100', true) }}" class="img-responsive"/>
                            <?php
                        } else {
                            ?>
                            <img src="" class="img-responsive"/>
                            <?php
                        }
                        ?>
                    </a>
                </td>
                <td>{{ $favorite->countFans() }}</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<?php
if (count($favorites)) {
    echo $favorites->links();
}
?>


@endsection