<?php
$sidebar = isset($sidebar) ? $sidebar : '';

$currentUser = Auth::user();
$msgCount = $currentUser->countNewMessage();
$lstCount = $currentUser->countProperty();
?>

<div class="list-group manager-sidebar">
    <a href="/member/account" class="list-group-item <?php echo $sidebar == 'profile' ? 'active' : ''; ?>">{{ tr('member_sidebar.profile', 'Profile') }} </a>
    <a href="/member/listing" class="list-group-item <?php echo $sidebar == 'listing' ? 'active' : ''; ?>">{{ tr('member_sidebar.listing', 'Listings') }} 
        <?php
        if ($lstCount > 0) {
            echo '(', $lstCount, ')';
        }
        ?>
    </a>
    <a href="/member/purchase" class="list-group-item <?php echo $sidebar == 'purchase' ? 'active' : ''; ?>">{{ tr('member_sidebar.purchase', 'Purchase') }} </a>
    <a href="/member/favorite" class="list-group-item <?php echo $sidebar == 'favorite' ? 'active' : ''; ?>">{{ tr('member_sidebar.favorite', 'Favorites') }} </a>
    <a href="/member/message" class="list-group-item <?php echo $sidebar == 'message' ? 'active' : ''; ?>">{{ tr('member_sidebar.message', 'Messages') }} 
        <?php
        if ($msgCount > 0) {
            echo '(', $msgCount, ')';
        }
        ?>
    </a>
    <a href="/member/notice" class="list-group-item <?php echo $sidebar == 'notice' ? 'active' : ''; ?>">{{ tr('member_sidebar.notice', 'Notifications') }} </a>
    <a href="/auth/logout" class="list-group-item <?php echo $sidebar == 'logout' ? 'active' : ''; ?>">{{ tr('auth.logout', 'Logout') }} </a>
</div>
<div class="summary">
    <h4>{{ tr('member_sidebar.summary-title', 'Summary') }}</h4>
    <h5>{{ tr('member_sidebar.total-listing', 'Total listing') }}</h5>
    <p>
        <span class="count">{{ $currentUser->countProperty() }}</span> {{ tr('member_sidebar.listing', 'listings') }}
    </p>
    <h5>{{ tr('member_sidebar.exclusive-listing', 'Exlusive listing') }}</h5>
    <p>
        <span class="count">{{ $currentUser->countExclusiveProperty() }}</span> {{ tr('member_sidebar.listing', 'listings') }}
    </p>
    <h5>{{ tr('member_sidebar.featured-listing', 'Featured listing') }}</h5>
    <p>
        <span class="count">{{ $currentUser->countFeaturedProperty() }}</span> {{ tr('member_sidebar.listing', 'listings') }}
    </p>
    <h5>{{ tr('member_sidebar.visit', 'Visits') }}</h5>
    <p>
        <span class="count">{{ $currentUser->getTotalVisit() }}</span> {{ tr('member_sidebar.times', 'times') }}
    </p>
</div>