
<div class="list-group manager-sidebar">
    <a href="/member/account" class="list-group-item <?php echo $sidebar == 'profile' ? 'active' : ''; ?>">{{ tr('member_sidebar.profile', 'Profile') }} </a>
    <a href="/member/favorite" class="list-group-item <?php echo $sidebar == 'favorite' ? 'active' : ''; ?>">{{ tr('member_sidebar.favorite', 'Favorites') }} </a>
    <a href="/member/message" class="list-group-item <?php echo $sidebar == 'message' ? 'active' : ''; ?>">{{ tr('member_sidebar.message', 'Messages') }} 
        <?php
        if ($msgCount > 0) {
            echo '(', $msgCount, ')';
        }
        ?>
    </a>
    <a href="/member/notice" class="list-group-item <?php echo $sidebar == 'notice' ? 'active' : ''; ?>">{{ tr('member_sidebar.notice', 'Notifications') }} </a>
    <a href="/auth/logout" class="list-group-item <?php echo $sidebar == 'logout' ? 'active' : ''; ?>">{{ tr('auth.logout', 'Logout') }} </a>
</div>