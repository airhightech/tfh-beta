<div class="property-details">

    <?php
    $images = $property->getImages();
    ?>
    <div class="row property-media">
        <div class="col-md-12">
            <h3>Current media</h3>
        </div>
        <div class="col-md-12 review-media-list">
            <?php
            if (count($images)) {
                foreach ($images as $image) {
                    ?>
                    <div class="media-item">
                        <img src="/thumb/100x100/{{ $image->filepath }}"/>
                        <a href="#" class="rm-media" onclick="showDeleteImage('#delete-{{ $image->id }}');">
                            <i class="fa fa-times"></i>
                        </a>
                        <div id="delete-{{ $image->id }}" class="rm-links">
                            <p>{{ tr('manager_property.delete-image', 'Delete ?') }}</p>
                            <a href="/rest/property/delete-image/{{ $image->id }}" class="text-danger silent-link">
                                {{ tr('manager_property.delete-image-yes', 'YES') }}
                            </a>
                            <a href="#" class="text-primary"
                               onclick="cancelDeleteImage('#delete-{{ $image->id }}');">{{ tr('manager_property.delete-image-no', 'NO') }}</a>
                        </div>

                    </div>
                    <?php
                }
            } else {
                if ($status == 'upload_image') {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                Please some image of your property before continuing
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

    </div>
    <form method="post" id="property-image-upload" 
          action="/rest/property/upload" data-progress="#upload-progress">

        <h3>{{ tr('listing.image-upload', 'Add new images') }}</h3>
        <div class="row img-preview-container">

        </div>
        <div class="row upload-progress-container">
            <div class="col-md-10">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped" role="progressbar" 
                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" id="upload-progress">

                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <span id="progress-label"></span>
            </div>
        </div>
        <div class="upload-actions">
            <div class="btn-group">
                <label title="Upload image file" for="prop_image" class="btn btn-primary">
                    <input type="file" accept="image/*" name="images[]" id="prop_image" class="hide" multiple>
                    {{ tr('listing.upload-images', 'Add images') }}
                </label>
            </div>
        </div>
        <input type="hidden" id="id" name="id" value="{{ $property->id }}"/>
    </form>
</div>