<div class="row facility-list">
    <?php
    if (count($optional_features)) {
        foreach ($optional_features as $feature) {

            if (in_array($feature->id, $selected_features)) {
                ?>
                <div class="col-md-2">
                    <div class=" text-center facility-item options_features active" data-ref="#feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                        <img src="{{ $feature->getThumbUrl('100x100', true, '/img/lift.png') }}"
                             class="img-responsive"
                             title="{{ $feature->getTranslatable('name') }}"/>
                        <span class="text-success">{{ $feature->getTranslatable('name') }}</span>
                        <input type="checkbox" id="feature_{{ $feature->id }}"
                               class="hidden" name="features[]" value="{{ $feature->id }}" checked/>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="col-md-2">
                    <div class=" text-center facility-item options_features" data-ref="#feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                        <img src="{{ $feature->getThumbUrl('100x100', true, '/img/lift.png') }}"
                             class="img-responsive"
                             title="{{ $feature->getTranslatable('name') }}"/>
                        <span class="text-success">{{ $feature->getTranslatable('name') }}</span>
                        <input type="checkbox" id="feature_{{ $feature->id }}"
                               class="hidden" name="features[]" value="{{ $feature->id }}"/>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
        }
    }
    ?>
</div>