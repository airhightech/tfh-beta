<tr>
    <td>{{ tr('listing.sale-price', 'Sale price') }} <span class="text-danger">*</span></td>
    <td>
        <div class="form-group">
            <div class="input-group">
                <input type="number" id="listing_price" name="listing_price" value="{{ old('listing_price', $listing->listing_price) }}" class="form-control" required>
                <span class="input-group-addon" id="basic-addon2">฿</span>
            </div>
            <p class="input-error listing_price hidden-error"></p>
        </div>
    </td>
    <td width="18%"><?php echo tr('property.size', 'Size'); ?> <span class="text-danger">*</span></td>
    <td width="32%">
        <div class="form-group">
            <div class="input-group">
                <input type="number" id="total_size" name="total_size" value="{{ old('total_size', $extended->total_size) }}" class="form-control">
                <span class="input-group-addon" id="basic-addon2">sqm</span>
            </div>
        </div>
    </td>
</tr>
<tr>
     <td><?php echo tr('property.psf', 'Price/Sqm'); ?></td>
    <td>
        <div class="form-group">
            <div class="input-group">
                <input type="number" id="psf" name="psf" value="{{ old('psf', $extended->psf) }}" class="form-control">
                <span class="input-group-addon" id="basic-addon2">฿/sqm</span>
            </div>
        </div>
    </td>
    <td>{{ tr('property.bedrooms', 'Bedrooms') }}</td>
    <td>
        <div class="form-group">
            <input type="number" name="bedroom_num" value="{{ old('bedroom_num', $extended->bedroom_num) }}" class="form-control">
        </div>
    </td>
</tr>
<tr>
    <td>{{ tr('property.total_floors', 'Floors') }}</td>
    <td>
        <?php
        if ($property->ptype == 'sh' || $property->ptype == 'th') {
            ?>
            <div class="form-group">
                <div class="input-group">
                    <input type="number" name="total_floors" value="{{ old('total_floors', $extended->total_floors) }}" class="form-control">
                    <span class="input-group-addon" id="basic-addon2">Floor</span>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="form-group">
                <div class="input-group">
                    <input type="number" name="floor_num" value="{{ old('floor_num', $extended->floor_num) }}" class="form-control">
                    <span class="input-group-addon" id="basic-addon2">Floor</span>
                </div>
            </div>
            <?php
        }
        ?>
    </td>
    <td>{{ tr('property.bathrooms', 'Bathrooms') }}</td>
    <td>
        <div class="form-group">
            <input type="number" name="bathroom_num" value="{{ old('bathroom_num', $extended->bathroom_num) }}" class="form-control">
        </div>
    </td>
</tr>
<tr>
    <?php
    if ($property->ptype == 'sh' || $property->ptype == 'th') {
        ?>
        <td><?php echo tr('property.land-size', 'Land size'); ?></td>
        <td>
            <div class="form-group">
                <input type="text" name="land_size" value="{{ old('land_size', $extended->land_size) }}" class="form-control">
            </div>
        </td>
        <?php
    } else {
        ?>
        <td><?php echo tr('property.towers', 'Tower'); ?></td>
        <td>
            <div class="form-group">
                <input type="text" name="number_of_tower" value="{{ old('number_of_tower', $building->number_of_tower) }}" class="form-control">
            </div>
        </td>
        <?php
    }
    ?>
    <td><?php echo tr('property.developer', 'Developer'); ?></td>
    <td>
        <div class="form-group">
            <select name="developer_id" class="form-control">
                <option value="0"><?php echo tr('property.select-developer', 'Select a developer'); ?></option>
                <?php
                foreach ($developers as $developer) {
                    ?>
                    <option value="{{ $developer->user_id }}"<?php echo $building->developer_id == $developer->user_id ? ' selected' : ''; ?>>{{ $developer->getName() }}</option>
                    <?php
                }
                ?>
            </select>
        </div>
    </td>
</tr>
<tr>

    <td>{{ tr('property.year-built', 'Year built') }}</td>
    <td>
        <div class="form-group">
            <input type="number" name="year_built" value="{{ old('year_built', $extended->year_built) }}" class="form-control">
        </div>
    </td>
    <td></td>
    <td>
    </td>
</tr>
