<div class="bs-wizard-container">
    <div class="row bs-wizard">

        <div class="col-md-6 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum">{{ tr('member_posting.step-1', 'Step 1') }}</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">{{ tr('member_posting.wizard-select-property', 'Select property') }}</div>
        </div>

        <div class="col-md-6 bs-wizard-step active"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">{{ tr('member_posting.step-2', 'Step 2') }}</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">{{ tr('member_posting.wizard-post-details', 'Post details') }}</div>
        </div>

    </div>
</div>