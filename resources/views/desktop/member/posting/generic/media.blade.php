<?php
$images = $property->getImages();

if (count($images)) {
    foreach ($images as $image) {
        ?>
        <div class="media-item">
            <img src="/thumb/100x100/{{ $image->filepath }}"/>
            <a href="#" class="rm-media" onclick="showDeleteImage('#delete-{{ $image->id }}');">
                <i class="fa fa-times"></i>
            </a>
            <div id="delete-{{ $image->id }}" class="rm-links">
                <p>{{ tr('manager_property.delete-image', 'Delete ?') }}</p>
                <a href="/rest/property/delete-image/{{ $image->id }}" class="text-danger silent-link">
                    {{ tr('manager_property.delete-image-yes', 'YES') }}
                </a>
                <a href="#" class="text-primary"
                   onclick="cancelDeleteImage('#delete-{{ $image->id }}');">{{ tr('manager_property.delete-image-no', 'NO') }}</a>
            </div>

        </div>
        <?php
    }
}