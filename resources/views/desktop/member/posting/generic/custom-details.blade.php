<?php

$parentBuiling = $property->getBuilding();

?>

<tr class="row-data">
    <td>{{ tr('property.name', 'Property name') }}</td>
    <td colspan="3">
        <?php echo __trfields('name', $property, false); ?>
    </td>
</tr>
<tr class="row-data">
    <td>{{ tr('property.details', 'Details') }}</td>
    <td colspan="3">
        <?php 
        if ($parentBuiling) {
            $parentExtended = $parentBuiling->getExtendedData();
            echo __trfields('details', $parentExtended, true);
        } else {
            echo __trfields('details', $extended, true);
        } 
        ?>
    </td>
</tr>
<tr class="row-data">
    <td>{{ tr('property.custom-address', 'Custom address') }}</td>
    <td colspan="3">
        <?php 
        if ($parentBuiling) {
            $parentExtended = $parentBuiling->getExtendedData();
            echo __trfields('custom_address', $parentExtended, true);
        } else {
            echo __trfields('custom_address', $extended, true);
        } 
        ?>
    </td>
</tr>
<tr class="row-data">
    <td>{{ tr('property.youtube-link-1', 'Youtube link #1') }}</td>
    <td colspan="3">
        <input type="text" id="ylink1" name="ylink1" value="{{ $listing->ylink1 }}" class="form-control">
    </td>
</tr>
<tr class="row-data">
    <td>{{ tr('property.youtube-link-2', 'Youtube link #2') }}</td>
    <td colspan="3">
        <input type="text" id="ylink2" name="ylink2" value="{{ $listing->ylink2 }}" class="form-control">
    </td>
</tr>