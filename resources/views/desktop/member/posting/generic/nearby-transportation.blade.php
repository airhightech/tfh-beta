<div class="train-list">    
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">BTS</span>
        <select class="form-control bts" name="nearby_bts[]" multiple>
            <?php
                foreach($bts_list as $train) {
                    ?>
                    <option value="{{ $train->id }}" <?php echo in_array($train->id, $selected_bts_list) ? 'selected' : ''; ?> >{{ $train->getTranslatable('name') }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">MRT</span>
        <select class="form-control mrt" name="nearby_mrt[]" multiple>
            <?php
                foreach($mrt_list as $train) {
                    ?>
                    <option value="{{ $train->id }}" <?php echo in_array($train->id, $selected_mrt_list) ? 'selected' : ''; ?> >{{ $train->getTranslatable('name') }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
    <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">APL</span>
        <select class="form-control apl" name="nearby_apl[]" multiple>
            <?php
                foreach($apl_list as $train) {
                    ?>
                    <option value="{{ $train->id }}" <?php echo in_array($train->id, $selected_apl_list) ? 'selected' : ''; ?> >{{ $train->getTranslatable('name') }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
</div>