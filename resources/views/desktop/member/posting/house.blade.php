@extends('desktop.layouts.default')

<?php
$listing = $property->getListingData();
$extended = $property->getExtendedData();
$building = $property->getBuildingData();
$listing->generateTitle();
$address = $property->getAddress();
$lnglat = $address->getLngLat();
?>

@section('content')
<div class="container">
    <div class="row posting-container">

        <div class="col-md-12">
            <h2 class="top-title">
                <?php echo tr('posting.house-title', 'Advertise with <em>Thai full house</em><br/><small>For Single House</small>'); ?>
            </h2>
        </div>

        <div class="col-md-8 col-md-offset-2">
            @include('desktop.member.posting.generic.step2')
        </div>
        <div class="col-md-8 col-md-offset-2">

            @include('desktop.member.posting.generic.property-media')

            <div class="property-details">

                <h3>{{ tr('listing.details', 'Details') }}</h3>

                <form method="post" action="/rest/listing/details" id="property-details-form">

<!--                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>{{ tr('listing.title', 'Title') }}</label>
                                <?php echo __trfields('title', $listing); ?>
                            </div>
                        </div>
                    </div>-->

                    <table class="table table-bordered">
                        <?php
                        if ($listing->ltype == 'rent') {
                            ?>
                            @include('desktop.member.posting.generic.rent-details')
                            <?php
                        } else {
                            ?>
                            @include('desktop.member.posting.generic.sale-details')
                            <?php
                        }
                        ?>   
                        <tr>
                            <td>{{ tr('property.key-features', 'Key features') }}</td>
                            <td colspan="3">
                                @include('desktop.member.posting.generic.features')
                            </td>
                        </tr>
                        <?php
                        $building_id = $property->belongsToBuilding();
                        if (!$building_id) {
                            ?>
                            <tr>
                                <td>{{ tr('property.facilities', 'Facilities') }} <span class="text-danger">*</span></td>
                                <td colspan="3">
                                    @include('desktop.member.posting.generic.facilities')
                                </td>
                            </tr>
                            <?php
                        }
                        ?>

                        <?php
                        if ($listing->ltype == 'rent') {
                            ?>
                            <tr>
                                <td>{{ tr('property.optional-features', 'Optional features') }}</td>
                                <td colspan="3">
                                    @include('desktop.member.posting.generic.optional-features')
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td>
                                {{ tr('property.nearby-transportation', 'Nearby Transportation') }}
                            </td>
                            <td colspan="3">
                                @include('desktop.member.posting.generic.nearby-transportation')
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <?php
                                if ($listing->ltype == 'rent') {
                                    echo tr('listing.availability-date', 'Move-in available from');
                                } else {
                                    echo tr('listing.movein-date', 'Move-in date');
                                }
                                ?>
                            </td>
                            <td colspan="2">
                                <input type="text" id="movein_date" name="movein_date" value="<?php echo __pgdate2hr($listing->movein_date); ?>" class="form-control single-date">
                            </td>
                            <td></td>
                        </tr>

                        <?php
                        if ($listing->ltype == 'rent') {
                            ?>
                            <tr>
                                <td>{{ tr('listing.min-rental-period', 'Min rental period') }}</td>
                                <td colspan="2">
                                    <select name="minimal_rental_period" class="form-control">
                                        <option value="0">Select a period</option>
                                        <?php
                                        for ($period = 1; $period <= 12; $period++) {
                                            ?>
                                            <option value="{{ $period }}" <?php echo $listing->minimal_rental_period == $period ? 'selected' : ''; ?> >{{ $period }} months</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <?php
                        }
                        ?>
                        @include('desktop.member.posting.generic.custom-details')
                    </table>
                    <div class="upload-actions text-center">
                        <p>
                            <input type="checkbox" id="agree_tac" name="agree_tac" value="yes" required/>                             
                            {{ tr('member.posting-agree-term', 'Agree with the terms and conditions of ThaiFullHouse.com') }}
                        <p class="input-error agree_tac hidden-error keep-message">{{ tr('listing.accept-agree-term', 'Please accept the terms and conditions of ThaiFullHouse.com') }}</p>
                        </p>
                        {{ csrf_field() }}
                        <input type="hidden" id="id" name="id" value="{{ $property->id }}"/>
                        <button class="btn btn-success submit-ajax-form" type="button" id="submit_property_details" 
                                data-target="property-details-form">{{ tr('button.submit', 'Submit') }}</button>
<!--                        <button class="btn btn-primary" type="button" onclick="openModalPreview();">{{ tr('button.preview', 'Preview') }}</button>-->
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade property-preview" id="property-preview" tabindex="-1" role="dialog" aria-labelledby="property-label">
    <div class="modal-dialog modal-preview" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-header">
                <button type="button" class="action dismiss" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i> CLOSE</button>
                <button type="button" class="action expand" onclick="showFullProperty();"><i class="fa fa-external-link"></i> EXPAND</button>
                <h4 class="modal-title" id="property-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<link rel="stylesheet" href="/libs/fancybox/jquery.fancybox.css" />
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/libs/fancybox/jquery.fancybox.pack.js"></script>
<link href="/libs/chosen/chosen.min.css" rel="stylesheet">
<script src="/libs/chosen/chosen.jquery.min.js"></script>
<script src="{{ __asset('/js/member/listing.js') }}"></script>
<script>
    var propertyId = <?php echo $property->id; ?>;
    var lnglat = <?php echo json_encode($lnglat); ?>;
</script>
<script src="{{ __asset('/js/visitor/preview.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-gallery.js') }}"></script>
@endsection