@extends('desktop.layouts.default')

<?php
$lang = \App::getLocale();
?>

@section('content')
<div class="container">
    <div class="row default-container posting-container">
        <div class="col-md-12">
            <h2 class="top-title">Advertise with <em>Thai full house</em></h2>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="listing-crumb">
                <span class="active">{{ tr('member_posting.package.free-listing', 'Free listing') }}</span>
                <span><a href="/member/purchase/buy-exclusive">{{ tr('member_posting.package.upgrade', 'Upgrade') }}</a></span>
            </div>

            <div class="bs-wizard-container">
                <div class="row bs-wizard">

                    <div class="col-md-6 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum">{{ tr('member_posting.step-1', 'Step 1') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ tr('member_posting.wizard-select-property', 'Select property') }}</div>
                    </div>

                    <div class="col-md-6 bs-wizard-step"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ tr('member_posting.step-2', 'Step 2') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ tr('member_posting.wizard-post-details', 'Post details') }}</div>
                    </div>

                </div>
            </div>

            <form id="create-property" class="form-horizontal highlighted" action="/rest/listing/create" method="post">
                <div class="form-group">
                    <h5 for="" class="col-md-12 posting-label">{{ tr('member_posting.listing-type.label', 'Want to') }} <span class="required">*</span></h5>
                    <div class="col-md-4">
                        <div class="radio">
                            <label>
                                <input type="radio" name="listing_type" value="rent" required>
                                <span>{{ tr('member_posting.listing-type.rent', 'RENT') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio">
                            <label>
                                <input type="radio" name="listing_type" value="sale" required>
                                <span>{{ tr('member_posting.listing-type.sale', 'SALE') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p class="input-error listing_type hidden-error"></p>
                    </div>
                </div>
                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ tr('member_posting.property-type.label', 'Property type') }} <span class="required">*</span></h5>
                    <div class="clearfix"></div>
                    <div class="property-types">
                        <button class="btn" type="button" data-type="ap">
                            <img src="/img/property-types/apartment.png"/><br/>
                            {{ tr('member_posting.property-type.apartment', 'Apartment') }}
                        </button>
                        <button class="btn" type="button" data-type="cd">
                            <img src="/img/property-types/condo.png"/><br/>
                            {{ tr('member_posting.property-type.condominium', 'Condominium') }}
                        </button>
                        <button class="btn" type="button" data-type="sh">
                            <img src="/img/property-types/detachedhouse.png"/><br/>
                            {{ tr('member_posting.property-type.detached-house', 'Single House') }}
                        </button>
                        <button class="btn" type="button" data-type="th">
                            <img src="/img/property-types/townhouse.png"/><br/>
                            {{ tr('member_posting.property-type.townhouse', 'Townhouse') }}
                        </button>
                        <input type="hidden" id="property_type" name="property_type" value="" required/>
                    </div>
                    <p class="input-error property_type hidden-error keep-message">
                        {{ tr('member_posting.property-type.error', 'Please select a property type') }}
                    </p>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ tr('member_posting.property-name.label', 'Property name') }} <span class="required required-condo-name hidden-error">*</span></h5>
                    <div class="col-sm-9 property-name">
                        <input type="text" class="form-control" id="property_name" name="property_name" value="" placeholder="">
                        <input type="hidden" id="property_id" name="property_id" value=""/>
                        <p class="text-danger required-condo-name"><strong>{{ tr('member_posting.property-name-help', 'Select the condo name from the list here. If not listed, please type the name in manually') }}</strong></p>
                        <p class="input-error property_name hidden-error"></p>
                        
                    </div>
                    <div class="col-sm-3">
                        <i class="fa fa-refresh fa-spin" style="margin-top: 9px; display: none;"></i>
                        <button type="button" style="display: none;" class="btn btn-danger clear-property-name"><i class="fa fa-times"></i></button>
                    </div>

                </div>

                <div class="location-container">
                    <div class="form-group">
                        <h5 class="col-md-12 posting-label">{{ tr('member_posting.province.label', 'Province') }} <span class="required">*</span></h5>
                        <div class="col-sm-9">
                            <select class="form-control" id="province_id" name="province_id" required>
                                <option value="0">{{ tr('member_posting.select-province', 'Select a province') }}</option>
                                <?php
                                foreach ($provinces as $id => $province) {
                                    ?>
                                    <option value="{{ $province->id }}" data-zoom="{{ $province->zoom_level }}"
                                            data-lat="{{ $province->lat() }}" data-lng="{{ $province->lng() }}">
                                        {{ $province->getTranslatable('name') }}
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                            <p class="input-error province_id hidden-error"></p>
                            <div class="text-danger posting_error" id="province_error">
                                {{ tr('member_posting.province.error', 'Please select a province') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5 class="col-md-12 posting-label">{{ tr('member_posting.district.label', 'District') }} <span class="required">*</span></h5>
                        <div class="col-sm-9">
                            <select class="form-control" id="district_id" name="district_id" disabled required>                        
                                <option value="0">{{ tr('member_posting.select-province', 'Select a province') }}</option>
                            </select>
                            <p class="input-error district_id hidden-error"></p>
                            <div class="text-danger posting_error" id="district_error">
                                {{ tr('member_posting.district.error', 'Please select a district') }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <i class="fa fa-spinner fa-spin district_loading" style="margin-top: 9px;"></i>
                        </div>

                    </div>

                    <div class="form-group">
                        <h5 class="col-md-12 posting-label">{{ tr('member_posting.area.label', 'Area') }}</h5>
                        <div class="col-sm-9">
                            <select class="form-control" id="area_id" name="area_id" disabled>
                                <option value="">{{ tr('member_posting.select-district', 'Please select a district') }}</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <i class="fa fa-spinner fa-spin area_loading" style="margin-top: 9px;"></i>
                        </div>

                    </div> 

                    <div class="form-group">
                        <h5 class="col-md-12 posting-label">{{ tr('member_posting.street-number.label', 'Street Number') }}</h5>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="street_number" name="street_number" value=""  placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <h5 class="col-md-12 posting-label">{{ tr('member_posting.street-name.label', 'Street Name') }}</h5>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="street_name" name="street_name" value=""  placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <h5 class="col-md-12 posting-label">{{ tr('member_posting.postale-code.label', 'Postal code') }}</h5>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="postal_code" name="postal_code" value="" placeholder="">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ tr('member_posting.map.label', 'Map') }}</h5>
                    <div class="col-md-12 text-center mark-location-container">
                        <button type="button" class="btn btn-default btn-cyan"><i class="fa fa-map-pin"></i> {{ tr('member_posting.mark-location.label', 'Mark Location On Map') }}</button>
                    </div>
                    <div class="clearfix"></div>
                    <div class="alert alert-danger input-error lat hidden-error keep-message text-center">
                        {{ tr('posting.location-error', 'Property location is required, please select location on the map') }}
                    </div>
                    <input type="hidden" id="lat" name="lat" value="" required/>
                    <input type="hidden" id="lng" name="lng" value="" required/>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="map-picker" style="width: 100%; height: 400px;"></div>
                    </div>
                </div>

                <div class="col-md-12 text-danger posting_error" id="all_error">
                    {{ tr('member_posting.error.text', 'Some error occured, please check your data') }}
                </div>
                {{ csrf_field() }}

                <p class="text-center posting-nav">
                    <button class="btn btn-primary submit-ajax-form" 
                            type="button" data-target="create-property">{{ tr('member_posting.button.next', 'NEXT') }} <i class="fa fa-angle-double-right"></i></button>
                </p>                

            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css"> 

<script src="/libs/autocomp/jquery.auto-complete.min.js"></script> 
<script src="/js/lp/locationpicker.jquery.js"></script>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?key={{ config('google.browser_key') }}'></script>
<script src="{{ __asset('/js/member/posting.js') }}"></script>
<script src="{{ __asset('/js/visitor/address.js') }}"></script>
@endsection