@extends('desktop.layouts.default')

@section('content')
<div class="container default-container">
    <div class="row free-listing-hero">
        <div class="col-md-6 col-md-offset-3">
            <h3>{{ tr('authtype.title', 'Who are you ?') }}</h3>
            <form id="usertype-form" action="/auth/register/type" method="post">
                <div class="radio">
                    <label class="user-type-owner">
                        <input type="radio" name="utp" value="owner" class="user-selector" data-next="/auth/register/owner">
                        {{ tr('authtype.owner-title', 'I am an owner') }}
                    </label>
                </div>
                <div class="radio">
                    <label class="user-type-agent">
                        <input type="radio" name="utp" value="freelance" class="user-selector" data-next="/auth/register/agent">
                        {{ tr('authtype.agent-title', 'I am a Freelance Agent') }}
                    </label>
                </div>
                <div class="radio">
                    <label class="user-type-realestate">
                        <input type="radio" name="utp" value="realestate" class="user-selector" data-next="/auth/register/company">
                        {{ tr('authtype.company-title', 'I am an Real Estate Company Agent') }}
                    </label>
                </div>

                <div class="radio">
                    <label class="user-type-developer">
                        <input type="radio" name="utp" value="developer" class="user-selector" data-next="/auth/register/developer">
                        {{ tr('authtype.developer-title', 'I am a land developer') }}
                    </label>
                </div>
                {{ csrf_field() }}
                <button type="button" id="submit-user-type" class="btn btn-primary pull-right">{{ tr('button.next', 'NEXT') }} <i class="fa fa-angle-double-right"></i></button>
            </form>
        </div>
    </div>


</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/visitor/register.js') }}"></script>
@endsection