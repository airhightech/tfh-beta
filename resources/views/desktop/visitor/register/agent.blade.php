@extends('desktop.layouts.default')

@section('breadcrumb')
<li><a href="/">{{ tr('visitor_breadcrumb.home', 'Home') }}</a></li>
<li>{{ tr('visitor_breadcrumb.register', 'Register') }}</li>
<li class="active">{{ tr('visitor_breadcrumb.register.freelance-agent', 'Agent') }}</li>
@endsection

@section('content')

<div class="container registration-container">
    
    <div class="row">
        <div class="col-md-12">
            <h1 class="registration-title">{{ tr('visitor_registration.register-title', 'Register') }}</h1>
        </div>
    </div>
    
    <div class="registration-banner agent-banner">
        <h1>{{ tr('visitor_registration.register.freelance-title', 'FOR FREELANCE AGENT') }}</h1>
        <p>
            {{ tr('visitor_registration.register.freelance-description', 'By simple registration, you can start listing your property free of charge.') }}            
        </p>
    </div>

    <form id="user-registration-form" action="/rest/register/agent" method="post" enctype="multipart/form-data">

        <div class="row registration-form-container registration-form">

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="firstname">{{ tr('visitor_registration.freelance.firstname', 'First name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="" required>
                    <p class="input-error firstname hidden-error"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">{{ tr('visitor_registration.freelance.lastname', 'Last name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="" required>
                    <p class="input-error lastname hidden-error"></p>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="phone">{{ tr('visitor_registration.freelance.telephone', 'Tel.') }} </label>
                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="mobile">{{ tr('visitor_registration.freelance.mobile', 'Mobile') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="line_id">{{ tr('visitor_registration.freelance.line-id', 'Line ID') }}</label>
                    <input type="text" class="form-control" name="line_id" value="{{ old('line_id') }}" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="photo">{{ tr('visitor_registration.freelance.upload-photo', 'Upload photo') }}</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="citizen_id_preview" placeholder="{{ tr('visitor_registration.freelance.upload-placeholder', 'Select a file ...') }}" disabled>
                        <span class="input-group-btn">
                            <label title="Upload image file" for="photo" class="btn btn-default upload">
                                <input type="file" accept="image/*" name="photo" id="photo" class="hide uploader file-value-updater" data-target="#citizen_id_preview">
                                upload
                            </label>
                        </span>                        
                    </div>
                </div>
            </div>
        </div>

        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="email">{{ tr('visitor_registration.freelance.email', 'ID (E-mail)') }} <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="" required>
                    <p class="input-error email hidden-error"></p>
                </div>
            </div>
        </div>
        <div class="row registration-form">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="password">{{ tr('visitor_registration.freelance.password', 'Password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="" required>
                    <p class="input-error password hidden-error"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password_confirmation">{{ tr('visitor_registration.freelance.password-confirmation', 'Re-password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" value="" placeholder="" required>
                    <p class="input-error password hidden-error"></p>
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-3 col-md-6 text-center">
                
            </div>
            <div class="col-md-offset-3 col-md-6 text-center with-marging-top">
                {{ csrf_field() }}
                <button type="button" data-target="user-registration-form" class="btn btn-gray submit-ajax-form">{{ tr('visitor_registration.freelance.button-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>

</div>

@endsection


@section('scripts')
<script src="{{ __asset('/js/visitor/register.js') }}"></script>
@endsection