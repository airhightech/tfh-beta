@extends('desktop.layouts.default')

@section('breadcrumb')
<li><a href="/">{{ tr('visitor_breadcrumb.home', 'Home') }}</a></li>
<li>{{ tr('visitor_breadcrumb.register', 'Register') }}</li>
<li class="active">{{ tr('visitor_breadcrumb.register.owner', 'Owner') }}</li>
@endsection

@section('content')

<div class="container registration-container">

    <div class="row">
        <div class="col-md-12">
            <h1 class="registration-title">{{ tr('visitor_registration.register-title', 'Register') }}</h1>
        </div>
    </div>

    <div class="registration-banner owner-banner">
        <h1>{{ tr('visitor_registration.register.owner-title', 'FOR OWNER') }}</h1>
        <p>
            {{ tr('visitor_registration.register.owner-description', 'By simple registration, you can start listing your property free of charge.') }}
        </p>
    </div>

    <form id="user-registration-form" action="/rest/register/owner" method="post">

        <div class="row registration-form-container registration-form">

            <div class="clearfix">
                <div class="col-md-offset-2 col-md-4">

                    <div class="form-group">
                        <label for="firstname">{{ tr('visitor_registration.owner.firstname', 'First name') }} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="" required>
                        <p class="input-error firstname hidden-error"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="lastname">{{ tr('visitor_registration.owner.lastname', 'Last name') }} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="" required>
                        <p class="input-error lastname hidden-error"></p>
                    </div>
                </div>
            </div>

            <div class="clearfix">
                <div class="col-md-offset-2 col-md-4">
                    <div class="form-group">
                        <label for="phone">{{ tr('visitor_registration.owner.telephone', 'Tel.') }} </label>
                        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="">
                        <p class="input-error phone hidden-error"></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="mobile">{{ tr('visitor_registration.owner.mobile', 'Mobile') }} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="">
                    </div>
                </div>
            </div>

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="line_id">{{ tr('visitor_registration.owner.line-id', 'Line ID') }}</label>
                    <input type="text" class="form-control" name="line_id" value="{{ old('line_id') }}" placeholder="">
                </div>
            </div>

        </div>
        
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="email">{{ tr('visitor_registration.owner.email', 'ID (E-mail)') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="" required autocomplete="{{ str_random() }}">
                    <p class="input-error email hidden-error"></p>
                </div>
            </div>
        </div>
        <div class="row registration-form">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="password">{{ tr('visitor_registration.owner.password', 'Password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="" required autocomplete="{{ str_random() }}">
                    <p class="input-error password hidden-error"></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password_confirmation">{{ tr('visitor_registration.owner.password-confirmation', 'Re-password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" value=""  placeholder="" required autocomplete="{{ str_random() }}">
                    <p class="input-error password_confirmation hidden-error"></p>
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-3 col-md-6 text-center">
                <div class="checkbox accept-registration-condition">
                    <label>
                        <input type="checkbox" class="accept_condition" name="accept_condition" value="yes"> 
                        {!! tr('visitor_registration.accept-confirmation'
                        , 'I am the owner or an authorized person for the property that is listed in this website<br/>
                        * Otherwise, please acknowledge that your listing shall be removed from the platform.') !!}
                    </label>
                </div>
            </div>
            <div class="col-md-offset-3 col-md-6 text-center with-marging-top hidden-error accept-condition-error">
                <p class="text-danger">
                    {!! tr('visitor_registration.accept-condition-error'
                    , 'Please accept the above condition.') !!}
                </p>
            </div>
            <div class="col-md-offset-3 col-md-6 text-center with-marging-top">
                {{ csrf_field() }}
                <button type="button" data-target="user-registration-form" class="btn btn-gray submit-ajax-form">{{ tr('visitor_registration.owner.button-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>
</div>

@endsection

@section('scripts')
<script src="{{ __asset('/js/visitor/register.js') }}"></script>
@endsection