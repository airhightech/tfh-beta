<div class="reviews-container">
    <?php
    foreach ($reviews as $review) {
        ?>
        <div class="row">
            <div class="col-md-2">
                <?php
                if ($review->hide_user_id == false) {
                    $_user = $review->getUser();
                    $img = $_user->getVisitorPicture();
                    if ($img) {
                        ?>
                        <img src="{{ $img }}" class="img-responsive"/>
                        <?php
                    } else {
                        ?>
                        <img src="/img/user.png" class="img-responsive"/>
                        <?php
                    }
                } else {
                    ?>
                    <img src="/img/user.png" class="img-responsive"/>
                    <?php
                }
                ?>
            </div>
            <div class="col-md-10">
                <h5>{{ $review->title }}</h5>
                <p class="agent-rating">
                    {{ render_simple_rating($review->rating, 5, true)  }} <span class="date">({{ $review->getDateAgo() }})</span>
                </p>
                <p>
                    {{ $review->comment }}
                </p>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="text-center">
        {{ $reviews->appends(['tab' => 'review'])->links() }}
    </div>
</div>

<div class="form-container">
    <h4>{{ tr('agent_reviews.write-comments', 'Write comments') }}</h4>

    <form id="agent-review-form" action="/rest/agent/review" method="post" class="auth-required">
        <span class="title">{{ tr('agent_reviews.give-recommendation', 'Give your recommendation') }}</span>
        <div class="rating-container">
            <input type="radio" name="rating" class="rating" value="1" required/>
            <input type="radio" name="rating" class="rating" value="2" required/>
            <input type="radio" name="rating" class="rating" value="3" required/>
            <input type="radio" name="rating" class="rating" value="4" required/>
            <input type="radio" name="rating" class="rating" value="5" required/>
        </div>

        <div class="form-group">
            <label for="title">{{ tr('agent_review.title-label', 'Title') }}</label>
            <input type="text" class="form-control" id="title" name="title" value="" required>
            <p class="input-error title hidden-error"></p>
        </div>

        <div class="form-group">
            <label for="comment">{{ tr('agent_review.experience-label', 'Describe in details your experience with this agent') }}</label>
            <textarea class="form-control" name="comment" rows="3" required></textarea>
            <p class="input-error review hidden-error"></p>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="hide_user_id" value="yes"> {{ tr('agent_review.hide-identity', 'Hide your identity') }}
            </label>
        </div>
        <input type="hidden" name="agent_id" value="{{ $user->id }}"/>
        <button type="button" class="btn btn-grey submit-ajax-form pull-right" 
                data-target="agent-review-form">{{ tr('button.send-review', 'SEND THE REVIEW') }}</button>
        <div class="clearfix"></div>
    </form>

</div>