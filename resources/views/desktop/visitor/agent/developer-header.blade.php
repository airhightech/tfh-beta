<?php
$profile = $user->getProfile();
$personalData = $user->getPersonalData();
$companyData = $user->getCompanyData();
$countProperty = $user->countProperty(true);
$score = $profile->getRating();
?>


<div class="member-profile-header company-agent">
    <div class="account-cover">
        <img id="cover-img" src="{{ $profile->getCoverPicture() }}" class="img-responsive"/>   
        <div class="profile-picture">
            <img id="profile-img" src="{{ $profile->getProfilePicture() }}" class="img-responsive"/>            
        </div>
    </div>
    <div class="member-header">
        
        <h4>
            <i class="fa fa-certificate"></i> <span class="name">{{ $profile->getName() }} </span>
            {{ render_simple_rating($score) }}
            <span class="post-count">
                <?php
                if ($countProperty > 0) {
                    ?>
                    ({{ tr('member_profile.profile-count', 'Posting') }} {{ $countProperty }})
                    <?php
                }
                ?>                
            </span>
        </h4>
        <p>
            {{ $profile->introduction }}
        </p>
        <hr/>
        <table class="table agent-info">
            <tr>
                <td class="info-label" width="15%">
                    <strong>{{ tr('member_profile.agent-id', 'Agent ID') }}</strong> 
                </td>
                <td width="35%">: {{ $user->getPublicId() }}</td>
                <td class="info-label" width="15%"><strong>{{ tr('member_profile.website', 'Website') }}</strong> </td>
                <td width="35%">: {{ $companyData->website }}</td>
            </tr>
            <tr>
                <td class="info-label">
                    <strong>{{ tr('member_profile.telephone', 'Tel') }}</strong> 
                </td>
                <td>
                    : {{ $personalData->phone }}
                </td>
                <td class="info-label">
                    <strong>{{ tr('member_profile.agent-email', 'Email') }}</strong> 
                </td>
                <td>: {{ $user->email }}</td>                    
            </tr>                
            <tr>
                <td class="info-label">
                    <strong>{{ tr('member_profile.language-ability', 'Language Ability') }}</strong> 
                </td>
                <td>: {{ $personalData->languages }}</td>     
                <td class="info-label">
                    <strong>{{ tr('member_profile.total-lisitng', 'Total listing') }}</strong> 
                </td>
                <td>: {{ $countProperty }}</td> 
            </tr>
            <tr>
                <td class="info-label">
                    <strong>{{ tr('member_profile.address', 'Address') }}</strong> 
                </td>
                <td colspan="3">: {{ $profile->getTranslatable('custom_address') }}</td>                     
            </tr>
        </table>

    </div>
</div>
