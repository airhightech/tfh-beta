@extends('desktop.layouts.default')

@section('title', 'Page Title')

<?php
$profile = $user->getProfile();
$groupedPropertyCount = $user->countGroupedProperty(true);


$count = array_merge(['ap' => 0, 'cd' => 0, 'sh' => 0, 'th' => 0], $groupedPropertyCount);
?>

@section('breadcrumb')
<li><a href="/">{{ tr('nav.breadcrumb.home', 'Home') }}</a></li>
<li><a href="/agent/search">{{ tr('nav.breadcrumb.agent', 'Agent') }}</a></li>
<li class="active">{{ $profile->getName() }}</li>
@endsection

@section('content')

<div class="container agent-container front-container">
    @include('desktop.visitor.agent.developer-header')

    <div class="row">
        <div class="col-md-9">
            <div class="agent-details">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="<?php echo $tab == 'review' ? '' : 'active' ?>">
                        <a href="#properties" aria-controls="home" role="tab" data-toggle="tab">{{ tr('member_profile.agent-properties', 'Total property items') }}</a>
                    </li>
                    <li role="presentation" class="<?php echo $tab == 'review' ? 'active' : '' ?>">
                        <a href="#reviews" aria-controls="profile" role="tab" data-toggle="tab">{{ tr('member_profile.agent-reviews', 'Review') }}</a>
                    </li>
                </ul>

                <!-- Tab panes                 -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?php echo $tab == 'review' ? '' : 'active' ?>" id="properties">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="property-item apartment">
                                    <div class="img-container">
                                        <img src="/img/agent/apartment.png" class="img-responsive"/>
                                    </div>
                                    <p>
                                        <span><i class="fa fa-circle"></i> {{ tr('agent_profile.apartment', 'Apartment') }}</span>
                                        <br/><span>{{ $count['ap'] }}</span>
                                    </p>                                    
                                </div>
                                <div class="property-item condo">
                                    <div class="img-container">
                                        <img src="/img/agent/condo.png" class="img-responsive"/>
                                    </div>
                                    <p>
                                        <span><i class="fa fa-circle"></i> {{ tr('agent_profile.condo', 'Condominium') }}</span>
                                        <br/><span>{{ $count['cd'] }}</span>
                                    </p>
                                </div>
                                <div class="property-item house">
                                    <div class="img-container">
                                        <img src="/img/agent/detachedhouse.png" class="img-responsive"/>
                                    </div>
                                    <p>
                                        <span><i class="fa fa-circle"></i> {{ tr('agent_profile.house', 'Single house') }}</span>
                                        <br/><span>{{ $count['sh'] }}</span>
                                    </p>
                                </div>
                                <div class="property-item townhouse">
                                    <div class="img-container">
                                        <img src="/img/agent/townhouse.png" class="img-responsive"/>
                                    </div>
                                    <p>
                                        <span><i class="fa fa-circle"></i> {{ tr('agent_profile.townhouse', 'Townhouse') }}</span>
                                        <br/><span>{{ $count['th'] }}</span>
                                    </p>
                                </div>

                            </div>
                        </div>

                        @include('desktop.visitor.agent.properties')

                    </div>
                    <div role="tabpanel" class="tab-pane <?php echo $tab == 'review' ? 'active' : '' ?> reviews" id="reviews">
                        @include('desktop.visitor.agent.reviews')
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="contact-box">
                <h4>{{ tr('property.contact', 'Contact') }}</h4>
                <div class="contact-box-header">
                    <div class="row">
                        <div class="col-md-4">
                            <img src="{{ $profile->getContactPicture() }}" class="img-responsive">
                        </div>
                        <div class="col-md-8 name">
                            {{ $profile->getName() }}<br/>
                            <a href="#" class="single-line" target="_blank">{{ $profile->website }}</a>
                        </div>
                    </div>
                    <p class="tel-btn">
                        <button type="button" onclick="showPhone(this);" data-tel="{{ $profile->phone }}">{{ tr('message.tel', 'Tel.') }}</button>
                    </p>
                </div>
                <h5>{{ tr('property.contact', 'Contact') }}</h5>
                <div class="contact-box-body">
                    <form id="property-message" action="/rest/message/send" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="sender_name" value="" placeholder="{{ tr('message.user-name', 'Name') }}" required>
                            <p class="input-error sender_name hidden-error"></p>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="sender_phone" value="" placeholder="{{ tr('message.user-tel', 'Your phone number') }}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="sender_email" value="" placeholder="{{ tr('message.user-email', 'E-mail') }}" required>
                            <p class="input-error sender_email hidden-error"></p>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">{{ tr('property.message', 'Message') }}</label>
                            <textarea class="form-control" rows="5" name="message" required></textarea>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="subscribe" value="yes"> {{ tr('property.send-newsletter', 'You want to receive our newsletter') }}
                            </label>
                        </div>
                        <p class="text-center">
                            <input type="hidden" name="agent_id" value="{{ $user->id }}"/>
                            <button type="button" class="btn btn-send submit-ajax-form" data-target="property-message" ><i class="fa fa-check"></i> {{ tr('button.submit', 'Submit') }}</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

@endsection
<?php
$labels = [
    tr('property.apartment', 'Apartment'),
    tr('property.condo', 'Condominium'),
    tr('property.single-house', 'Single house'),
    tr('property.townhouse', 'Townhouse')
];
?>

@section('scripts')
<link rel="stylesheet" href="/libs/rating/min/rating.css" />
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script>
                        var labels = <?php echo json_encode($labels); ?>;
                        var propertyData = <?php echo json_encode(array_values($count)); ?>;
</script>
<script src="/libs/rating/min/rating.js"></script>
<script src="{{ __asset('/js/visitor/agent.js') }}"></script>
@endsection