@extends('desktop.layouts.default')

@section('content')

<div class="container agent-container agent-search">

    <h1>{{ tr('search_agent.title', 'Find an agent') }}</h1>

    <div class="row search-filter-titles">
        <div class="col-md-2">
            <label>{{ tr('search_agent.name-label', 'Name') }}</label>
        </div>
        <div class="col-md-3">
            <label>{{ tr('search_agent.location-label', 'Location') }}</label>            
        </div>
        <div class="col-md-3">
            <label>{{ tr('search_agent.agent-type-label', 'Agent type') }}</label>
        </div>
    </div>
    <div class="search-filters">
        <form id="search_agent" action="/agent/search" method="get" class="row form-inline">
            <div class="col col-md-2">
                <input type="text" name="nam" value="{{ array_get($search, 'nam')}}" placeholder="Agent name" class="form-control"/>                
            </div>
            <div class="col col-md-2">
                <input type="text" name="loc" value="{{ array_get($search, 'loc')}}" placeholder="Location" class="form-control"/>
            </div>
            <div class="col col-md-1">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
            </div>
            <div class="col col-md-3">
                <div class="agent-types">

                    <label <?php echo array_get($search, 'typ', 'all') == 'all' ? 'class="active"' : ''; ?> data-type="all">Both</label>

                    <label <?php echo array_get($search, 'typ') == 'agent' ? 'class="active"' : ''; ?>
                        data-type="agent">Freelance</label>

                    <label <?php echo array_get($search, 'typ') == 'company' ? 'class="active"' : ''; ?>
                        data-type="company">Agency</label>

                    <input type="hidden" id="typ" name="typ" value="{{ array_get($search, 'typ', 'all')}}"/>

                </div>
            </div>
        </form>
    </div>

    <h3>
        {{ tr('search_agent.result-title', 'Freelance Agent Search Results') }}
    </h3>

    <div class="row agent-list">
        <?php
        if (count($agents)) {

            foreach ($agents as $agent) {

                $profile = $agent->getProfile();
                $score = $profile->getRating();
                ?>
                <div class="col-md-4">
                    <div class="agent-cell">
                        <div class="rating">
                            {{ render_simple_rating($score, 5, true) }}
                        </div>
                        <div class="agent-picture">
                            <a href="/agent/<?php echo $agent->id; ?>">
                                <img src="<?php echo $profile->getProfilePicture(); ?>"/>
                            </a>
                        </div>
                        <div class="agent-name">
                            <a href="/agent/<?php echo $agent->id; ?>"><?php echo $profile->getName(); ?></a>
                        </div>
                        <div class="row agent-stat">
                            <div class="col-sm-4 agent-status">
                                <?php
                                if ($agent->verified && $profile->haveProfilePicture()) {
                                    ?>
                                    <span class="verified">
                                        <i class="fa fa-check"></i> Verified
                                    </span>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-sm-4 agent-posts">
                                <?php
                                $postings = $agent->countProperty(true);
                                if ($postings > 1) {
                                    echo '(', $postings, ' postings)';
                                } else if ($postings == 1) {
                                    echo '(1 posting)';
                                } else {
                                    echo '(No posting)';
                                }
                                ?>
                            </div>
                            <div class="col-sm-4">

                            </div>
                        </div>
                        <div class="agent-info">
                            <p class="tags">
                                <em>Expert :</em> <?php echo $profile->getTags(); ?>
                            </p>
                            <p class="tags">
                                <em>Review :</em> <?php echo $agent->countReview(); ?> reviews
                            </p>
                            <p class="agent-intro">
                                <?php echo $profile->introduction; ?>
                            </p>
                        </div>
                        <a href="/agent/<?php echo $agent->id; ?>" class="btn btn-details">Contact</a>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <div class="agent-list-pagination">
        <?php echo $agents->appends($search)->links(); ?>
    </div>
    
    <div class="row" style="margin-top: 25px; margin-bottom: 25px;">
        <div class="col-md-4 col-md-offset-4">
            <div class="fb-page" data-href="https://www.facebook.com/ThaiFullHouse/" 
                 data-tabs="timeline" data-small-header="false" 
                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/ThaiFullHouse/" class="fb-xfbml-parse-ignore">
                    <a href="https://www.facebook.com/ThaiFullHouse/">Thaifullhouse</a>
                </blockquote>
            </div>
        </div>
    </div>

</div>



@endsection

@section('scripts')
<script src="{{ __asset('/js/visitor/agent.js') }}"></script>
<script>
    (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId={{ config('services.facebook.client_id') }}";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
@endsection