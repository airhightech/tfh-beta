<?php
$profile = $user->getProfile();
$personalData = $user->getPersonalData();
$companyData = $user->getCompanyData();
$countProperty = $user->countProperty(true);
$score = $profile->getRating();
?>


<div class="member-profile-header">
    <div class="account-cover">
        <img id="cover-img" src="{{ $profile->getCoverPicture() }}" class="img-responsive"/>        
    </div>
    <div class="member-header">
        <div class="profile-picture">
            <img id="profile-img" src="{{ $profile->getProfilePicture() }}" class="img-responsive"/>            
        </div>
        <h4>
            <i class="fa fa-home"></i> <span class="name">{{ $profile->getName() }} </span>
            {{ render_simple_rating($score) }}
            <span class="post-count">
                <?php
                if ($countProperty > 0) {
                    ?>
                    ({{ tr('member_profile.profile-count', 'Posting') }} {{ $countProperty }})
                    <?php
                }
                ?>                
            </span>
        </h4>
        <?php
        if ($user->ctype == 'agent') {
            ?>
            <table class="table agent-info">
                <tr>
                    <td class="info-label" width="15%">
                        <strong>{{ tr('member_profile.agent-name', 'Agent name') }}</strong> 
                    </td>
                    <td width="35%">: {{ $profile->getName() }}</td>
                    <td class="info-label" width="15%"><strong>{{ tr('member_profile.website', 'Website') }}</strong> </td>
                    <td width="35%">: {{ $companyData->website }}</td>
                </tr>
                <tr>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.agent-id', 'Agent ID') }}</strong> 
                    </td>
                    <td>: {{ $user->getPublicId() }}</td>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.language-ability', 'Language Ability') }}</strong> 
                    </td>
                    <td>: {{ $personalData->languages }}</td>
                </tr>
                <tr>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.agent-email', 'Email') }}</strong> 
                    </td>
                    <td>: {{ $user->email }}</td>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.telephone', 'Tel') }}</strong> 
                    </td>
                    <td>
                        : {{ $personalData->phone }}
                    </td>
                </tr>
            </table>
            <?php
        } else if ($user->ctype == 'company') {
            ?>
            <table class="table agent-info">
                <tr>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.agent-id', 'Agent ID') }}</strong> 
                    </td>
                    <td>: {{ $user->getPublicId() }}</td>
                    <td class="info-label"><strong>{{ tr('member_profile.website', 'Website') }}</strong> </td>
                    <td>: {{ $companyData->website }}</td>
                    
                </tr>
                <tr>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.telephone', 'Tel') }}</strong> 
                    </td>
                    <td>
                        : {{ $personalData->phone }}
                    </td>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.agent-email', 'Email') }}</strong> 
                    </td>
                    <td>: {{ $user->email }}</td>                    
                </tr>                
                <tr>
                    <td class="info-label">
                        <strong>{{ tr('member_profile.language-ability', 'Language Ability') }}</strong> 
                    </td>
                    <td>: {{ $personalData->languages }}</td>     
                    <td class="info-label">
                        <strong>{{ tr('member_profile.total-lisitng', 'Total listing') }}</strong> 
                    </td>
                    <td>: {{ $countProperty }}</td> 
                </tr>
            </table>
            <?php
        } else if ($user->ctype == 'developer') {
            ?>
            <table class="table">
                <tr>
                    <td><strong>{{ tr('member_profile.company-name', 'Company name') }}</strong> : {{ $companyData->company_name }}</td> 
                    <td><strong>{{ tr('member_profile.website', 'Website') }}</strong> : {{ $companyData->website }}</td> 
                </tr>
                <tr>
                    <td><strong>{{ tr('member_profile.phone', 'Phone') }}</strong> : {{ $profile->phone }}</td> 
                    <td><strong>{{ tr('member_profile.email', 'Email') }}</strong> : {{ $user->email }}</td> 
                </tr>
            </table>
            <?php
        }
        else {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ tr('member_profile.personal-name', 'Contact name') }}</strong> : {{ $profile->getName() }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ tr('member_profile.phone', 'Phone') }}</strong> : {{ $profile->phone }}</td> 
                    <td><strong>{{ tr('member_profile.email', 'Email') }}</strong> : {{ $user->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ tr('member_profile.address', 'Address') }}</strong> : {{ $profile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        }
        ?>

    </div>
</div>
