<?php
$stat = array_merge(['rent' => 0, 'sale' => 0], $user->countListingStats());

$total = $stat['rent'] + $stat['sale'];

$stat_sale = 0;
$stat_rent = 0;

if ($total > 0) {
    $stat_sale = 100 * $stat['sale'] / $total;
    $stat_rent = 100 * $stat['rent'] / $total;
}

if ($user->ctype !== 'developer') {
    ?>
    <div class="rental-stat">
        <div class="progress">
            <div class="progress-bar progress-bar-sale" style="width: {{ $stat_sale }}%">
                <span>{{ round($stat_sale) }}%</span>
            </div>
            <div class="progress-bar progress-bar-rent" style="width: {{ $stat_rent }}%">
                <span>{{ round($stat_rent) }}%</span>
            </div>        
        </div>
        <p>
            <span>{{ tr('property.sale', 'Sale') }}</span><span class="pull-right">{{ tr('property.rent', 'Rent') }}</span>
        </p>
    </div>
    <?php
}
?>

<div class="property-filter">
    <div>
        <select name="ltype" class="ltype">
            <option value="all">{{ tr('property.all', 'All') }}</option>
            <option value="sale">{{ tr('property.sale', 'Sale') }}</option>
            <option value="rent">{{ tr('property.rent', 'Rent') }}</option>
        </select>
    </div>
    <div>
        <select name="ptype" class="ptype">
            <option value="all">{{ tr('property.all', 'All') }}</option>
            <option value="ap">{{ tr('property.apartment', 'Apartment') }}</option>
            <option value="cd">{{ tr('property.condo', 'Condominium') }}</option>
            <option value="sh">{{ tr('property.single-house', 'Single house') }}</option>
            <option value="th">{{ tr('property.townhouse', 'Townhouse') }}</option>
        </select>
    </div>
    <div>
        <select name="order" class="order">
            <option value="none">{{ tr('property_order.none', 'None') }}</option>
            <option value="price">{{ tr('property_order.price', 'Price') }}</option>
            <option value="beds">{{ tr('property_order.bedroom', 'Bedroom') }}</option>
            <option value="baths">{{ tr('property_order.bathroom', 'Bathroom') }}</option>
            <option value="size">{{ tr('property_order.size', 'Size') }}</option>
        </select>
    </div>
</div>
<div class="property-list">
    <?php
    $properties = $user->getProperties();

    foreach ($properties as $property) {
        $img = $property->getMainImage();
        $listing = $property->getListingData();
        $extended = $property->getExtendedData();
        $images = $property->getImages();
        ?>
        <div class="property-item {{ $listing->ltype }} {{ $property->ptype }}" 
             data-price="{{ $listing->listing_price }}" data-size="{{ $extended->total_size }}" 
             data-beds="{{ $extended->bedroom_num }}" data-baths="{{ $extended->bathroom_num }}">
            <div id="pager-{{ $property->id }}" class="img-pager" data-count="{{ count($images) }}">
                <?php
                    if (count($images)) {
                        $i = 1;
                        foreach($images as $image) {
                            if ($i == 1) {
                                ?>
                                <img src="/image/{{ $image->filepath }}" class="img-responsive rank-{{ $i }} current" data-rank="{{ $i }}"/>
                                <?php
                            } else {
                                ?>
                                <img src="/image/{{ $image->filepath }}" class="img-responsive  rank-{{ $i }}" style="display: none;" data-rank="{{ $i }}"/>
                                <?php
                            }
                            
                            $i++;
                        }
                        ?>
                            <div class="pager-num"><span class="current-rank">1</span>/{{ count($images) }}</div>
                            <div class="pager-handler pager-left" onclick="pagerPrevious('#pager-{{ $property->id }}')"><i class="fa fa-angle-left"></i></div>
                            <div class="pager-handler pager-right" onclick="pagerNext('#pager-{{ $property->id }}')"><i class="fa fa-angle-right"></i></div>
                        <?php
                    }
                ?>
                            
            </div>
            <div class="meta-info-bg"></div>
            <div class="meta-info">
                <p><a href="/property/{{ $property->id }}">{{ $property->getTranslatable('name') }}</a></p>
                <span class="price">฿ {{ number_format($listing->listing_price) }}</span>
                <span>
                    | {{ $extended->bedroom_num }} beds | {{ $extended->bathroom_num }} baths | {{ $extended->total_size }} sqm
                </span>
                <p>
                    {{ $extended->getCustomAddress() }}
                </p>
                </div>
            <div class="favorite-container">
                <?php
                    if (in_array($property->id, $favorites)) {
                        ?>
                        <span class="favorite active" onclick="propertyToggleFavorite({{ $property->id }}, this)"><i class="fa fa-heart text-danger"></i></span>
                        <?php
                    } else {
                        ?>
                        <span class="favorite" onclick="propertyToggleFavorite({{ $property->id }}, this)"><i class="fa fa-heart-o"></i></span>
                        <?php
                    }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
