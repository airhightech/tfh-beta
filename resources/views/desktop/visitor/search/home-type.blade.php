<div class="dropdown listing-type">
    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ tr('map_search.home-type', 'Home type') }}
        <span class="caret"></span>
    </button>
    <?php
    $pt = array_filter(explode(',', array_get($all, 'pt')));

    if (count($pt) == 0) {
        $pt = ['ap', 'cd', 'sh', 'th'];
    }
    ?>
    <div class="dropdown-menu option-dropdown" aria-labelledby="dLabel">
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="pt-ap" name="pt" value="ap" class="pt nm"
                    <?php echo (count($pt) == 0 || in_array('ap', $pt)) ? 'checked' : ''; ?> <?php echo in_array('cdb', $pt) ? 'disabled' : ''; ?>>
            </div>
            <div class="colxs-10">
                {{ tr('map_search.options.apartment', 'Apartment') }}
            </div>
        </div>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="pt-co" name="pt" value="cd" class="pt nm" 
                    <?php echo (count($pt) == 0 || in_array('cd', $pt)) ? 'checked' : ''; ?> <?php echo in_array('cdb', $pt) ? 'disabled' : ''; ?>>
            </div>
            <div class="colxs-10">
                {{ tr('map_search.options.condo', 'Condos') }}
            </div>
        </div>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="pt-dh" name="pt" value="sh" class="pt nm" 
                    <?php echo (count($pt) == 0 || in_array('sh', $pt)) ? 'checked' : ''; ?> <?php echo in_array('cdb', $pt) ? 'disabled' : ''; ?>>
            </div>
            <div class="colxs-10">
                {{ tr('map_search.options.detached-house', 'Single house') }}
            </div>
        </div>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="pt-th" name="pt" value="th" class="pt nm" 
                    <?php echo (count($pt) == 0 || in_array('th', $pt)) ? 'checked' : ''; ?> <?php echo in_array('cdb', $pt) ? 'disabled' : ''; ?>>
            </div>
            <div class="colxs-10">
                {{ tr('map_search.options.townhouse', 'Townhouse') }}
            </div>
        </div>
        <hr/>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="pt-cd" name="pt" value="cdb" class="pt co" <?php echo in_array('cdb', $pt) ? 'checked' : ''; ?> >
            </div>
            <div class="colxs-10">
                {{ tr('map_search.options.condo-dir', 'Condo community') }}
            </div>
        </div>
        <hr/>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="np" name="np" value="yes" class="np" <?php echo array_get($all, 'np') == 'yes' ? 'checked' : ''; ?> >
            </div>
            <div class="colxs-10">
                {{ tr('map_search.options.new-project', 'New project') }}
                <p>
                    
                </p>
            </div>
        </div>
        
    </div>
</div>