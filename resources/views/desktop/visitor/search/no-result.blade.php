<?php
$all = $request->all();

$pt = explode(',', array_get($all, 'pt'));

$q = $request->input('q');
?>
<h2>
    {{ tr('search_result.not-result-title', 'Sorry, we couldn\'t find any results for : ') }}
<?php
if ($q) {
    ?>
        <span class="text-info"><?php echo $q; ?></span>
        <?php
    }
    ?>
</h2>



<?php
if (in_array('cdb', $pt)) {
    ?>
    <span class="label label-default">{{ tr('map_search.options.condo-dir', 'Condo community') }}</span>
    <?php
}
if (in_array('ap', $pt)) {
    ?>
    <span class="label label-default">{{ tr('map_search.options.apartment', 'Apartment') }}</span>
    <?php
}
if (in_array('cd', $pt)) {
    ?>
    <span class="label label-default">{{ tr('map_search.options.condo', 'Condos') }}</span>
    <?php
}
if (in_array('sh', $pt)) {
    ?>
    <span class="label label-default">{{ tr('map_search.options.detached-house', 'Single house') }}</span>
    <?php
}
if (in_array('th', $pt)) {
    ?>
    <span class="label label-default">{{ tr('map_search.options.townhouse', 'Townhouse') }}</span>
    <?php
}

$lt = explode(',', array_get($all, 'lt'));

if (in_array('rent', $lt)) {
    ?>
    <span class="label label-success">{{ tr('map_search.listing.for-rent', 'Rent') }}</span>
    <?php
}
if (in_array('sale', $lt)) {
    ?>
    <span class="label label-success">{{ tr('map_search.listing.for-sale', 'Sale') }}</span>
    <?php
}