@extends('desktop.layouts.default')

@section('title', 'Page Title')

@section('breadcrumb')
<li><a href="/">{{ tr('nav.breadcrumb.home', 'Home') }}</a></li>
<li class="active">{{ tr('nav.breadcrumb.Search', 'Search') }}</li>
@endsection

<?php
$all = $request->all();
?>

@section('content')

<div class="map-search-container">
    <div class="filter-bar list-view">

        <input type="text" class="search form-control" id="q" name="q" value="{{ array_get($all, 'q') }}" placeholder="{{ tr('map_search.placeholder', 'Enter area, BTS station') }}">
        <i class="fa fa-search"></i>

        @include('desktop.visitor.search.listing-type')

        @include('desktop.visitor.search.home-type')

        @include('desktop.visitor.search.price-range')

        @include('desktop.visitor.search.bedrooms')    

        @include('desktop.visitor.search.bathrooms')

        @include('desktop.visitor.search.more')           

        <button type="button" class="btn btn-map-search">
            {{ tr('map_search.search-button', 'SEARCH') }}
        </button>
        <div class="dropdown save-dropdown">    
            <button type="button" class="search-action saved" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ tr('map_search.saved', 'SAVED') }} <span class="saved-count"></span>
            </button>
            <div class="dropdown-menu seach-list" aria-labelledby="dLabel">

            </div>
        </div>
        <button type="button" class="search-action save">{{ tr('map_search.save', 'SAVE') }}</button>
    </div>
    <div class="container search-results-container">
        <div class="header">
            <h3>{{ tr('search_result.list-title', 'Search Result') }}</h3>
            <p>
                <span class="count">{{ $properties->total() }}</span> {{ tr('search_result.found-result', 'properties found') }}
            </p>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?php
                        $attr = isset($all['sort']) ? $all['sort'] : false;
                        ?>
                        <select class="form-control sorter">
                            <option value="">{{ tr('map_search.sorter.select', 'Select a filter') }}</option>
                            <option value="price" <?php echo $attr == 'price' ? 'selected' : ''; ?>>{{ tr('map_search.sorter.cheapest', 'Cheapest') }}</option>
                            <option value="year" <?php echo $attr == 'year' ? 'selected' : ''; ?>>{{ tr('map_search.sorter.newest', 'Newest') }}</option>
                            <option value="beds" <?php echo $attr == 'beds' ? 'selected' : ''; ?>>{{ tr('map_search.sorter.bedrooms', 'Bedrooms') }}</option>
                            <option value="baths" <?php echo $attr == 'baths' ? 'selected' : ''; ?>>{{ tr('map_search.sorter.bathrooms', 'Bathrooms') }}</option>
                            <option value="size" <?php echo $attr == 'size' ? 'selected' : ''; ?>>{{ tr('map_search.sorter.size', 'Size') }}</option>
                            <option value="rental" <?php echo $attr == 'rental' ? 'selected' : ''; ?>>{{ tr('map_search.sorter.short-rental', 'Short rental') }}</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-offset-3 col-md-3">
                    <?php
                    
                    $all = $request->all();
                    unset($all['match']);
                    
                    ?>
                    <a href="/search?<?php echo http_build_query($all); ?>" class="btn">{{ tr('map_search.goto-map-view', 'Map view') }}</a>
                </div>
            </div>

        </div>
        <div class="property-list">
            <?php
            if (count($properties)) {

                foreach ($properties as $_property) {

                    $property = \App\Models\Property::find($_property->id);

                    $img = $property->getMainImage();
                    $listing = $property->getListingData();
                    $extended = $property->getExtendedData();
                    $images = $property->getImages();
                    $address = $property->getAddress();

                    $title = [];

                    if ($address) {
                        $district = $address->getDistrict();
                        if ($district) {
                            $title[] = $district->getTranslatable('name');
                        }

                        $province = $address->getProvince();
                        if ($province) {
                            $title[] = $province->getTranslatable('name');
                        }
                    }
                    ?>
                    <h4><a href="/property/{{ $property->id }}">{{ $property->getTitle() }}</a></h4>
                    <p>
                        {{ $extended->getCustomAddress() }}
                    </p>
                    <div class="property-item {{ $listing->ltype }} {{ $property->ptype }}" 
                         data-price="{{ $listing->listing_price }}" data-size="{{ $extended->total_size }}" 
                         data-beds="{{ $extended->bedroom_num }}" data-baths="{{ $extended->bathroom_num }}">
                        <div id="pager-{{ $property->id }}" class="img-pager" data-count="{{ count($images) }}">
                            <?php
                            if (count($images)) {
                                $i = 1;
                                foreach ($images as $image) {
                                    if ($i == 1) {
                                        ?>
                                        <img src="/image/{{ $image->filepath }}" class="img-responsive rank-{{ $i }} current" data-rank="{{ $i }}"/>
                                        <?php
                                    } else {
                                        ?>
                                        <img src="/image/{{ $image->filepath }}" class="img-responsive  rank-{{ $i }}" style="display: none;" data-rank="{{ $i }}"/>
                                        <?php
                                    }

                                    $i++;
                                }
                                ?>
                                <div class="pager-num"><span class="current-rank">1</span>/{{ count($images) }}</div>
                                <div class="pager-handler pager-left" onclick="pagerPrevious('#pager-{{ $property->id }}')"><i class="fa fa-angle-left"></i></div>
                                <div class="pager-handler pager-right" onclick="pagerNext('#pager-{{ $property->id }}')"><i class="fa fa-angle-right"></i></div>
                                <?php
                            }
                            ?>

                        </div>
                        <div class="meta-info-bg"></div>
                        <div class="meta-info">
                            <p><a href="/property/{{ $property->id }}">{{ $property->getTranslatable('name') }}</a></p>

                            <?php
                            if ($property->ptype == 'cdb' || $property->new_project) {
                                ?>
                                <span class="price">฿ {{ number_format($property->getStartingPrice()) }}</span>
                                <?php
                                $room_types = $property->getRoomTypes();
                                if (count($room_types)) {
                                    foreach ($room_types as $room_type) {
                                        ?>
                                        <p>
                                            <?php echo $room_type->name; ?> : <?php echo $room_type->size; ?> sqm
                                        </p>
                                        <?php
                                    }
                                }
                            } else {
                                ?>
                                <span class="price">฿ {{ number_format($listing->listing_price) }}</span>
                                <span>
                                    | {{ $extended->bedroom_num }} <i class="fa fa-bed" aria-hidden="true"></i> 
                                    | {{ $extended->bathroom_num }} <i class="fa fa-bath" aria-hidden="true"></i> 
                                    | {{ $extended->total_size }} sqm
                                </span>
                                <?php
                            }
                            ?>
                            <p>
                                <?php echo implode(' / ', $title); ?>
                            </p>
                        </div>
                        <div                class="favorite-container">
                            <?php
                            if (in_array($property->id, $favorites)) {
                                ?>
                                <span class="favorite active" onclick="propertyToggleFavorite({{ $property->id }}, this)"><i class="fa fa-heart text-danger"></i></span>
                                <?php
                            } else {
                                ?>
                                <span class="favorite" onclick="propertyToggleFavorite({{ $property->id }}, this)"><i class="fa fa-heart-o"></i></span>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>                        
                    @include('desktop.visitor.search.no-result')         
                <?php               
                
            }
            ?>
            <div class="">
                <?php echo $properties->appends($request->all())->links(); ?>
            </div>
        </div>
        <div class="ads">
            <?php
            $np = array_get($all, 'np');
            $pt = explode(',', array_get($all, 'pt'));

            if ($np == 'yes') {
                ?>
                <div class="banner banner-list-project" data-location="list-project">                             
                </div> 
                <?php
            } else if (in_array('cdb', $pt)) {
                ?>
                <div class="banner banner-list-condo" data-location="list-condo">            
                </div> 
                <?php
            } else {
                $lt = explode(',', array_get($all, 'lt'));

                if (in_array('sale', $lt)) {
                    ?>
                    <div class="banner banner-list-sale" data-location="list-sale">            
                    </div> 
                    <?php
                } else {
                    ?>
                    <div class="banner banner-list-rent" data-location="list-rent">            
                    </div> 
                    <?php
                }
            }
            ?>
            
            <div class="fb-page" data-href="https://www.facebook.com/ThaiFullHouse/" 
                 data-tabs="timeline" data-small-header="false" 
                 data-adapt-container-width="true" data-hide-cover="false" 
                 data-show-facepile="true"  style="margin-top: 25px; margin-bottom: 25px;">
                <blockquote cite="https://www.facebook.com/ThaiFullHouse/" class="fb-xfbml-parse-ignore">
                    <a href="https://www.facebook.com/ThaiFullHouse/">Thaifullhouse</a>
                </blockquote>
            </div>
        </div>
    </div>


</div>

@endsection


@section('scripts')
<link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css"> 
<script>
    var currentPage = <?php echo isset($all['page']) ? $all['page'] : 1; ?>;
    var searchCriteria = <?php echo json_encode($all); ?>;
    var f_scope = '<?php echo ($np == 'yes') ? 'project' : (in_array('cdb', $pt) ? 'cdb' : 'listing'); ?>';
</script>
<script src="/libs/autocomp/jquery.auto-complete.min.js"></script> 
<script src="{{ __asset('/js/visitor/search-list.js') }}"></script>
<script src="{{ __asset('/js/visitor/agent.js') }}"></script>
<script src="{{ __asset('/js/visitor/banner.js') }}"></script>
<script>
    (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId={{ config('services.facebook.client_id') }}";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
@endsection