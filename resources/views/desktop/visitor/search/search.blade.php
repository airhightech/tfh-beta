@extends('desktop.layouts.search')

@section('title', 'Page Title')

@section('content')

<div class="map-search-container">
    <div class="filter-bar">

        <input type="text" class="search form-control" id="q" name="q" value="{{ array_get($all, 'q') }}" placeholder="{{ tr('map_search.placeholder', 'Enter area, BTS station') }}">
        <i class="fa fa-search"></i>

        @include('desktop.visitor.search.listing-type')

        @include('desktop.visitor.search.home-type')

        @include('desktop.visitor.search.price-range')

        @include('desktop.visitor.search.bedrooms')    

        @include('desktop.visitor.search.bathrooms')

        @include('desktop.visitor.search.more')           

        <button type="button" class="btn btn-map-search">
            {{ tr('map_search.search-button', 'SEARCH') }}
        </button>
        <div class="dropdown save-dropdown">
            <button type="button" class="search-action saved" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ tr('map_search.saved', 'SAVED') }} <span class="saved-count"></span>
            </button>
            <div class="dropdown-menu seach-list" aria-labelledby="dLabel">

            </div>
        </div>
        <button type="button" class="search-action save">{{ tr('map_search.save', 'SAVE') }}</button>
    </div>
    <div class="search-result-container">
        <table>
            <tr>
                <td id="col-map" class="col-map">
                    <div class="map map-container" style="height: auto;">
                        <div id="map" class="map">
                        </div>
                        <div class="map-tools">
                            <button class="more-maps" type="button">
                                <span class="more">{{ tr('map_search.more-map', 'More map') }} &nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
                                <span class="less"><i class="fa fa-angle-left"></i>&nbsp;&nbsp; {{ tr('map_search.less-map', 'Less map') }}</span>
                            </button>
                            <button class="list-view" type="button"><span>{{ tr('map_search.list-view', 'List view') }}</span></button>
                        </div>
                        <div class="cluster-info" style="display: none;">

                        </div>
                    </div>
                </td>
                <td id="col-list" class="col-list">
                    <div class="property-listing">
                        
                        <?php
                        
                        $np = array_get($all, 'np');
                        $pt = explode(',', array_get($all, 'pt'));

                        if ($np == 'yes') {
                            ?>
                            <div class="col-md-12 banner banner-map-project" data-location="map-project">            
                            </div> 
                            <?php
                        } else if (in_array('cdb', $pt)) {
                            ?>
                            <div class="col-md-12 banner banner-map-condo" data-location="map-condo">            
                            </div> 
                            <?php
                        } else {
                            $lt = explode(',', array_get($all, 'lt'));

                            if (in_array('sale', $lt)) {
                                ?>
                                <div class="col-md-12 banner banner-map-sale" data-location="map-sale">            
                                </div> 
                                <?php
                            } else {
                                ?>
                                <div class="col-md-12 banner banner-map-rent" data-location="map-rent">            
                                </div> 
                                <?php
                            }
                        }
                        ?>

                        <h3>
                            <span></span> <small class="property-count"></small>
                        </h3>
                        <ul class="nav nav-pills nav-filter">
                            <li role="presentation" id="sort-rank" class="active" onclick="sortby('year', 'rank');"><a href="#">{{ tr('map_search.sorter.home-for-you', 'Home') }}</a></li>
                            <li role="presentation" id="sort-new" onclick="sortby('year', 'new');"><a href="#">{{ tr('map_search.sorter.newest', 'Newest') }}</a></li>
                            <li role="presentation" id="sort-price" onclick="sortby('price', 'price');" class="cheap-filter"><a href="#">{{ tr('map_search.sorter.cheapest', 'Cheapest') }}</a></li>
                            <li role="presentation" id="sort-more"  class="dropdown more-filter">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ tr('map_search.sorter.more', 'More') }}</a>
                                <ul class="dropdown-menu">
                                    <li onclick="sortby('beds', 'more');"><a href="#">{{ tr('map_search.sorter.bedrooms', 'Bedrooms') }}</a></li>
                                    <li onclick="sortby('baths', 'more');"><a href="#">{{ tr('map_search.sorter.bathrooms', 'Bathrooms') }}</a></li>
                                    <li onclick="sortby('size', 'more');"><a href="#">{{ tr('map_search.sorter.size', 'Size') }}</a></li>
                                    <li onclick="sortby('rental', 'more');"><a href="#">{{ tr('map_search.sorter.short-rental', 'Short rental') }}</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="search-results">
                        </div>
                        <div class="no-search-results" style="display: none;">
                            <div class="alert alert-warning">
                                {{ tr('map_search.no-result', 'No result was found, please try to change some search options') }}
                            </div>
                        </div>
                        <div class="search-results-pagination">
                        </div>
                        <div class="loading-results">
                            <p>
                                <span><i class="fa fa-spinner fa-pulse fa-fw"></i> {{ tr('map_search.results.updating', 'Updating results') }}</span>
                            </p>
                        </div>
                    </div>
                </td>
            </tr>

        </table>


    </div>

</div>
<div id="property-template" class="hidden">
    <div class="property-item" data-year="" data-price="" data-beds="" data-baths="" data-size="" data-rental="">
        <img src="" class="prop-image img-responsive"/>
        <span class="img-count">2 images</span>
        <button class="favorite-btn"><i class="favorite fa"></i></button>
        <div class="details">
            <span class="listing-type"><i class="fa fa-circle"></i></span>  <span class="property"></span><br/>
            ‎<span class="listing-price"></span> |
            <span class="prop-only">
                    <span class="beds"></span> <i class="fa fa-bed" aria-hidden="true"></i> 
                    <span class="bath"></span> <i class="fa fa-bath" aria-hidden="true"></i> |
                    <span class="size"></span>sqm
                </span>
            <div class="summary">                
                <span class="address"></span>
            </div>
        </div>
    </div>
</div>

<div id="property-preview" class="modal fade property-preview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="action dismiss" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i> CLOSE</button>
                <button type="button" class="action expand" onclick="showFullProperty();"><i class="fa fa-external-link"></i> EXPAND</button>
                <h4 class="modal-title" id="property-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="saved-message" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-check"></i> {{ tr('map_search.saved', 'Saved') }}</h4>
            </div>
            <div class="modal-body">
                <?php echo addslashes(tr('map_search.saved-message', 'Your search was saved on the right button')); ?>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<link rel="stylesheet" href="/libs/scrollbar/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css"> 

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/libs/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/libs/autocomp/jquery.auto-complete.min.js"></script> 
<script src="/libs/pagination/jquery.simplePagination.js"></script>
<script src="/libs/markerclusterer.js"></script>
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script>
    var currentId = <?php echo isset($all['id']) ? $all['id'] : 'null'; ?>;
    var searchCriteria = <?php echo json_encode($all); ?>;
    var currentPoiData = <?php echo json_encode($place, true); ?>;
    var f_scope = '<?php echo ($np == 'yes') ? 'project' : (in_array('cdb', $pt) ? 'cdb' : 'listing'); ?>';
</script>
<script src="{{ __asset('/js/property.js') }}"></script>    
<script src="{{ __asset('/js/visitor/search.js') }}"></script>
<script src="{{ __asset('/js/visitor/bigmap.js') }}"></script>
<script src="{{ __asset('/libs/infobox_packed.js') }}"></script>
<script src="{{ __asset('/libs/lodash.min.js') }}"></script>
<script src="{{ __asset('/js/visitor/mortgage.js') }}"></script>
<script src="{{ __asset('/js/visitor/banner.js') }}"></script>
@endsection
