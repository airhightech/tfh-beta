<?php
$bed = array_get($all, 'bed');
?>
<div class="dropdown">
    
    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ tr('map_search.beds', 'Beds') }} <span class="bed-count"><?php echo empty($bed) ? '' : '(' . $bed . ')'; ?></span>
        <span class="caret"></span>
    </button>

    <ul class="dropdown-menu list-group beds">
        <li class="list-group-item bed-item<?php echo $bed == '' ? ' active' : ''; ?>" data-filter="">{{ tr('map_search.all', 'All') }}</li>
        <li class="list-group-item bed-item<?php echo $bed == 'studio' ? ' active' : ''; ?>" data-filter="studio">{{ tr('map_search.studio', 'Studio') }}</li>
        <li class="list-group-item bed-item<?php echo $bed == '1' ? ' active' : ''; ?>" data-filter="1">1</li>
        <li class="list-group-item bed-item<?php echo $bed == '2' ? ' active' : ''; ?>" data-filter="2">2</li>
        <li class="list-group-item bed-item<?php echo $bed == '3' ? ' active' : ''; ?>" data-filter="3">3</li>
        <li class="list-group-item bed-item<?php echo $bed == '4+' ? ' active' : ''; ?>" data-filter="4+">4+</li>
    </ul>
</div>