@extends('desktop.layouts.default')

<?php
$lang = \App::getLocale();
?>

@section('content')

<div class="container">
    <div class="row default-container page-container">
        <div class="col-md-offset-2 col-md-8 title">
            <h2 class="top-title">{{ $page->getTranslatable('title') }}</h2>            
        </div>
        
        <div class="col-md-offset-2 col-md-8 content">
            {!! $page->getTranslatable('html') !!} 
            <p class="text-muted">
                {{ tr('page.updated-on', 'Updated on') }} : <?php echo date('d/m/Y', strtotime($page->updated_at)); ?>
            </p>
        </div>
    </div>
</div>

@endsection