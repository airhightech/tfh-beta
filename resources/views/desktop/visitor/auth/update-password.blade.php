@extends('desktop.layouts.default')

@section('breadcrumb')
<li class="active">{{ tr('auth.update_password_title', 'Update password') }}</li>
@endsection

@section('content')
<div class="container">
    <div class="auth-container">
        <div class="row">
            <div class="col-md-6">
                <h1>{{ tr('title.update_password_title', 'Update password') }}</h1>
                <p>
                    {{ tr('text.update_password_text', 'Please input your new password below') }}
                </p>
                <form action="/rest/auth/update-password" id="update-password-form" method="post">                    
                    <div class="form-group">
                        <label for="password">{{ tr('label.password', 'Password') }}</label>
                        <input type="password" class="form-control" name="password" 
                               value="{{ old('password') }}" placeholder="New password" required>
                        <p class="input-error password hidden-error"></p>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">{{ tr('label.password_confirmation', 'Confirm passqword') }}</label>
                        <input type="password" class="form-control" name="password_confirmation" 
                               value="{{ old('password_confirmation') }}" placeholder="Confirm your password" required>
                        <p class="input-error password_confirmation hidden-error"></p>
                    </div>
                    <p class="form-error hidden-error text-danger">                        
                    </p>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="token" value="{{ $password_reset_token }}">
                    <input type="hidden" name="id" value="{{ $id }}">
                    <button type="button" data-target="update-password-form"
                            class="btn btn-primary submit-ajax-form">{{ tr('button.submit', 'Submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection