@extends('desktop.layouts.default')

@section('breadcrumb')
<li class="active">{{ tr('auth.forget_password_title', 'Forgot password') }}</li>
@endsection

@section('content')

<div class="container">
    <div class="auth-container">
        <div class="row">
            <div class="col-md-6">
                <h1>{{ tr('title.forget-password-title', 'Forget my password') }}</h1>
                <p>
                    {{ tr('text.forget-password-text', 'Please input your email below to receive a link to reset your password') }}
                </p>
                <form id="forgot-password-form" action="/rest/auth/forgot-password" method="post">                    
                    <div class="form-group">
                        <label for="email">{{ tr('label.email-address', 'Email') }}</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
                        <p class="input-error email hidden-error"></p>
                    </div>
                    <p class="form-error hidden-error text-danger">                        
                    </p>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <button type="button" data-target="forgot-password-form" class="btn btn-primary submit-ajax-form">{{ tr('button.submit', 'Submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
