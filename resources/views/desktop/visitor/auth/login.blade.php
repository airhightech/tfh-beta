
@extends('desktop.layouts.default')

@section('content')

<?php
$message = $request->input('message');
?>

<div class="container ">

    <div class="auth-container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="auth-title">{{ tr('auth_login.sign-in-title', 'Sign-in') }}</h1>
            </div>

            <div class="col-md-6">
                <?php
                if (!empty($message) && $message == 'password-updated') {
                    ?>
                    <p class="text-success">
                        {{ tr('auth.password-updated', 'Your password have been updated successfully') }}
                    </p>
                    <?php
                }
                ?>
                <form action="/rest/auth/login" method="post" id="main-login-form">
                    <div class="form-group">
                        <label for="email">{{ tr('auth_login.email', 'E-mail') }}</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="" required>
                        <p class="input-error email hidden-error"></p>
                    </div>
                    <div class="form-group">
                        <label for="password">{{ tr('auth_login.password', 'Password') }}</label>
                        <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="" required>
                        <p class="input-error password hidden-error"></p>
                    </div>
                    {{ csrf_field() }}
                    <p class="form-error hidden-error text-danger">
                    </p>
                    <button type="button" data-target="main-login-form" class="btn btn-primary submit-ajax-form">{{ tr('button.sign-in', 'SIGN-IN') }}</button>
                    <p class="text-right">
                        <a href="/auth/forgot-password">{{ tr('auth.forgot-password', 'Forgot password ?') }}</a>
                    </p>
                </form>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <div class="social-btn-container">
                    <p>
                        <a href="/auth/facebook" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> <?php echo tr('nav_menu.sign-facebook', 'Login with Facebook') ?></a>
                    </p>
                    <p>
                        <a href="/auth/twitter" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> <?php echo tr('nav_menu.sign-twitter', 'Login with Twitter') ?></a>
                    </p>
                    <p>
                        <a href="/auth/google" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus"></i> <?php echo tr('nav_menu.sign-google', 'Login with Google') ?></a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection