@extends('desktop.layouts.default')

@section('content')
<div class="container">
    <div class="auth-container">
        <div class="row">
            <div class="col-md-12">
                <h1>{{ tr('auth.check-email', 'Check your email') }}</h1>
                <?php
                    if ($reason == 'password-link') {
                        ?>
                        <p>               
                            {{ tr('auth.password-reset-message', 'A password reset link has been sent your email address.') }}
                        </p>
                        <?php
                    } else if(isset($user)) {
                        ?>
                        <p>               
                            {{ tr('auth.check-email-message', 'Please check your e-mail and click the link in the e-mail to verify your e-mail address.') }}
                        </p>
                        <p>
                            <a href="/auth/request-link?email={{ $user->email }}">{{ tr('auth.request-validation-link', 'Request a new validation link') }}</a>
                        </p>
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
</div>
@endsection
