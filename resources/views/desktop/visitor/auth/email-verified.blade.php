@extends('desktop.layouts.default')

@section('content')
<div class="container">
    <div class="auth-container">
        <div class="row">
            <div class="col-md-12">
                <h3>Congratulations</h3>
                <p class="text-success">
                    {{ tr('auth.email-verified-message', 'Your email have been successfully verified.') }}
                </p>
                <p>
                    {{ tr('auth.login-message', 'You can now sign in with your email and password.') }}
                    <a href="/auth/login">{{ tr('auth.login', 'Go to Login plage') }}</a>
                </p>
            </div>
        </div>
    </div>


</div>
@endsection
