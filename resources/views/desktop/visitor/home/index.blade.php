
@extends('desktop.layouts.default')

@section('content')

<div class="big-banner-container">

    <img class="main-bg rent bg-rent" src="/image/{{ __trimg('home_image', 'rent', 'rent') }}" style="display: none;"/>
    <img class="main-bg sale bg-sale" src="/image/{{ __trimg('home_image', 'sale', 'rent') }}" style="display: none;"/>
    <img class="main-bg cd bg-condo" src="/image/{{ __trimg('home_image', 'condo', 'rent') }}"/>
    <img class="main-bg np bg-project" src="/image/{{ __trimg('home_image', 'new_project', 'rent') }}" style="display: none;"/>

    <div class="container options-container">
        <p class="home-search-options text-center">
            <span class="option" data-value="sale" data-name="lt" data-bg="sale">{{ tr('home.options.sale', 'SALE') }}</span>
            <span class="option" data-value="rent" data-name="lt" data-bg="rent">{{ tr('home.options.rent', 'RENT') }}</span>
            <span class="option" data-value="yes" data-name="np" data-bg="project">{{ tr('home.options.new-project', 'NEW PROJECT') }}</span>
            <span class="option active" data-value="cdb" data-name="pt" data-bg="condo">{{ tr('home.options.condo', 'CONDO') }}</span>
        </p>
        <form class="home-search-options" metho="get" action="/search">
            <div class="input-group">
                <input type="text" class="form-control input-lg" id="q" name="q" value="" 
                       placeholder="{{ tr('home.input.search-placeholder', 'Enter a neighborhood or Property Name') }}">
                <span class="input-group-btn">
                    <button class="btn btn-blue btn-lg" type="submit">{{ tr('home.button.search', 'SEARCH') }}</button>
                </span>
                <input type="hidden" id="lt" name="lt" value=""/>
                <input type="hidden" id="pt" name="pt" value="cdb"/>
                <input type="hidden" id="np" name="np" value=""/>
                <input type="hidden" id="scope" name="scope" value="cdb"/>
            </div>
        </form>
    </div>
</div>

<div class="home-container">
    <div class="front-hero">
        <h1>{{ tr('home.hero.title', 'HOW IS THAIFULLHOUSE DIFFERENT ?') }}</h1>
        <div class="hero-cell">
            <img src="{{ __icon('hero_search', '/img/home/searching.png') }}">
            <h3>{{ tr('home.hero.search.title', 'SEARCH FASTER') }}</h3>
            <p>
                {{ tr('home.hero.search.text', 'we provide a searching map services to reach your dream house faster') }}
            </p>
        </div>
        <div class="hero-cell">
            <img src="{{ __icon('hero_free', '/img/home/free.png') }}">
            <h3>{{ tr('home.hero.free.title', 'FREE POSTING') }}</h3>
            <p>
                {{ tr('home.hero.free.text', 'You can post a listing for free on Thaifullhouse website') }}
            </p>
        </div>
        <div class="hero-cell">
            <img src="{{ __icon('hero_info', '/img/home/thumbs-up.png') }}">
            <h3>{{ tr('home.hero.info.title', 'FULL INFORMATION') }}</h3>
            <p>
                {{ tr('home.hero.info.text', 'Our website provide all needed information to you for the better decision') }} 
            </p>
        </div>
        <div class="hero-cell">
            <img src="{{ __icon('hero_agents', '/img/home/trophy.png') }}">
            <h3>{{ tr('home.hero.agents.title', 'BEST AGENTS') }}</h3>
            <p>
                {{ tr('home.hero.agents.text', 'We provide you the information of proffesional agents, trustable and ready to be your best partner') }}
            </p>
        </div>
    </div>
    <div class="row premium">
        <div class="col-md-12">
            <h3>{!! tr('home.home.premium-listing', 'Premium listing') !!}</h3>            
        </div>

        <div class="col-md-12 premium-listings">
            <?php
            if (count($exclusive)) {

                foreach ($exclusive as $property) {

                    $listing = $property->getListingData();
                    $extended = $property->getExtendedData();
                    $address = $property->getAddress();

                    $title = trget(json_decode($listing->title, true));
                    $district = null;

                    $lng = 100.536738;
                    $lat = 13.740791;

                    if ($address) {
                        $district = $address->getDistrict();
                        $lng = $address->lng();
                        $lat = $address->lat();
                    }

                    $images = $property->getImages();
                    $image = false;

                    if (count($images)) {
                        $image = array_shift($images);
                    }
                    ?>
                    <div class="listing-item premium-item" 
                         data-lng="{{ $lng }}"
                         data-lat="{{ $lat }}"
                         data-id="{{ $property->id }}">
                        <div class="ribbon"><span>{{ tr('home-rubon-exclusive-text', 'Exclusive') }}</span></div>
                        <div class="listing-img">
                            <?php
                            if ($image) {
                                ?>
                                <img src="{{ $image->getThumbUrl('310x250', true) }}" class="img-responsive"/>
                                <?php
                            } else {
                                ?>
                                <img src="/img/default-property.png" class="img-responsive"/>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="listing-info">
                            <div class="row">
                                <?php
                                if ($district) {
                                    ?>
                                    <div class="col-xs-7 property-type">    
                                        <i class="fa fa-home"></i> {{ $property->getTitle() }}
                                    </div>
                                    <div class="col-xs-5 location text-right">
                                        <i class="fa fa-map-marker"></i> {{ $district->getTranslatable('name') }}
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="col-xs-12 property-type">    
                                        <i class="fa fa-home"></i> {{ $property->getTitle() }}
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                            <div class="row">
                                <div class="col-xs-12 price">
                                    ฿ {{ number_format($property->getSalePrice()) }}
                                </div>
                                <div class="col-xs-12 location">
                                    {{ $extended->getCustomAddress() }}
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>


    <div class="row home-banner">
        <div class="col-md-12 banner banner-home" data-location="home">            
        </div>        
    </div>

    <div class="row home-sale">
        <div class="col-md-12">
            <h3>{{ tr('home.for-sale', 'For sale') }}</h3>
        </div>        
        <div class="col-md-12">
            <div class="exclusive-listings">
                <div class="listing current-listing main-sale">
                    <ul id="sale-slider" class="list-unstyled" data-target=".featured-sale">
                        <?php
                        if (count($featured_sale)) {
                            foreach ($featured_sale as $property) {

                                $image = $property->getMainImage();
                                $listing = $property->getListingData();
                                $title = $property->getTitle();
                                ?>
                                <li data-target=".featured-{{ $property->id }}">
                                    <a href="/property/{{ $property->id }}">
                                        <?php
                                        if ($image) {
                                            ?>
                                            <img src="{{ $image->getThumbUrl('618x370', true) }}">
                                            <?php
                                        } else {
                                            ?>
                                            <img src="http://placehold.it/618x370?text=No+image">
                                            <?php
                                        }
                                        ?>
                                    </a>
                                    <div class="info">
                                        <p class="name">{{ $title }}</p>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p class="address">
                                                    <span class="location"><i class="fa fa-map-marker"></i> {{ $extended->getCustomAddress() }}</span> 
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="address">
                                                    <span class="price pull-right">฿ {{ number_format($listing->listing_price) }}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>

                <?php
                if (count($featured_sale)) {
                    foreach ($featured_sale as $property) {
                        $image = $property->getMainImage();
                        ?>
                        <div class="listing pending-listing featured-sale featured-{{ $property->id }}">
                            <a href="/property/{{ $property->id }}">
                                <?php
                                if ($image) {
                                    ?>
                                    <img src="{{ $image->getThumbUrl('120x120', true) }}">
                                    <?php
                                } else {
                                    ?>
                                    <img src="http://placehold.it/120x120?text=No+image">
                                    <?php
                                }
                                ?>

                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="row home-rent">
        <div class="col-md-12">
            <h3>{{ tr('home.for-rent', 'For rent') }}</h3>
        </div>
        <div class="col-md-12">
            <div class="exclusive-listings">
                <div class="listing current-listing main-rent">
                    <ul id="rent-slider" class="list-unstyled" data-target=".featured-rent">
                        <?php
                        if (count($featured_rent)) {
                            foreach ($featured_rent as $property) {

                                $image = $property->getMainImage();
                                $listing = $property->getListingData();
                                $title = $property->getTitle();
                                ?>
                                <li data-target=".featured-{{ $property->id }}">
                                    <a href="/property/{{ $property->id }}">
                                        <?php
                                        if ($image) {
                                            ?>
                                            <img src="{{ $image->getThumbUrl('618x370', true) }}">
                                            <?php
                                        } else {
                                            ?>
                                            <img src="http://placehold.it/618x370?text=No+image">
                                            <?php
                                        }
                                        ?>

                                    </a>
                                    <div class="info">
                                        <p class="name">{{ $title }}</p>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <p class="address">
                                                    <span class="location"><i class="fa fa-map-marker"></i> {{ $extended->getCustomAddress() }}</span> 
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <p class="address">
                                                    <span class="price pull-right">฿ {{ number_format($listing->listing_price) }}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>

                </div>

                <?php
                if (count($featured_rent)) {
                    foreach ($featured_rent as $property) {
                        $image = $property->getMainImage();
                        ?>
                        <div class="listing pending-listing featured-rent featured-{{ $property->id }}">
                            <a href="/property/{{ $property->id }}">
                                <?php
                                if ($image) {
                                    ?>
                                    <img src="{{ $image->getThumbUrl('120x120', true) }}">
                                    <?php
                                } else {
                                    ?>
                                    <img src="http://placehold.it/120x120?text=No+image">
                                    <?php
                                }
                                ?>

                            </a>
                        </div>
                        <?php
                    }
                }
                ?>

            </div>
        </div>
    </div>

    <div class="row condo-community">
        <div class="col-md-12">
            <h3>{!! tr('home.condo-community', 'Condo <em>community</em>') !!}</h3>            
        </div>

        <div class="col-md-12">
            <?php
            $image = __trimg('condo_image');

            if ($image) {
                ?>
                <div class="condo-image">
                    <a href="/search?pt=cdb">
                        <img src="/image/{{ $image }}" class="img-responsive"/>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

    <div class="newsletter-container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="text-right">{{ tr('home.signup-newsletter', 'Sign up to receive Newsletters') }}</h3>
            </div>
            <div class="col-md-6">
                <form id="subscription-form">
                    <input type="text" name="email" value="" class="form-control" autocomplete="off"/>
                    <button type="button" class="btn btn-grey" id="subscribe-me">{{ tr('button.ok', 'OK') }}</button>
                </form>
            </div>
        </div>
    </div>
    
    <div class="row" style="margin-top: 25px; margin-bottom: 25px;">
        <div class="col-md-6 col-md-offset-3">
            <div class="fb-page" data-href="https://www.facebook.com/ThaiFullHouse/" 
                 data-tabs="timeline" data-width="500" data-height="500" data-small-header="false" 
                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/ThaiFullHouse/" class="fb-xfbml-parse-ignore">
                    <a href="https://www.facebook.com/ThaiFullHouse/">Thaifullhouse</a>
                </blockquote>
            </div>
        </div>
    </div>

    <div id="property-preview" class="modal fade property-preview" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="action dismiss" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i> CLOSE</button>
                    <button type="button" class="action expand" onclick="showFullProperty();"><i class="fa fa-external-link"></i> EXPAND</button>
                    <h4 class="modal-title" id="property-title">&nbsp;</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
    
    

</div>

<?php
if ($popup) {
    ?>
    <div id="promo-popup" class="modal fade promo-popup" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="property-title">&nbsp;</h4>
                </div>
                <div class="modal-body">    
                    <div class="promo-popup-content" data-link="{{ $popup->related_link }}">
                        <?php echo $popup->getTranslatable('content'); ?>
                    </div>
                    <div class="hide-popup-message text-right">
                        <input type="checkbox" id="hide_popup" name="hide_popup" value="yes"/> {{ tr('popup.hide-message', 'Do not show up again within 24 hours') }}
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <?php
}
?>

<div id="fb-root"></div>


@endsection

@section('scripts')

<link type="text/css" rel="stylesheet" href="/libs/lightslider/css/lightslider.min.css" />   
<link rel="stylesheet" href="/libs/autocomp/jquery.auto-complete.css">  
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/libs/lightslider/js/lightslider.min.js"></script>
<script src="/libs/autocomp/jquery.auto-complete.min.js"></script> 
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script src="{{ __asset('/js/visitor/home.js') }}"></script>
<script src="{{ __asset('/js/visitor/preview.js') }}"></script>
<script src="{{ __asset('/js/visitor/mortgage.js') }}"></script>
<script src="{{ __asset('/js/visitor/banner.js') }}"></script>
<script>
    (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId={{ config('services.facebook.client_id') }}";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
@endsection