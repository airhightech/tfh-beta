<div class="nearby-trans">

    <?php
    if (count($bts_list)) {
        ?>
        <div class="trans-item">
            <label>{{ tr('property.nearby-bts', 'Near BTS') }}</label> 
            <?php
            foreach ($bts_list as $bts) {
                ?>
                <span>{{ $bts->getTranslatable('name') }}</span>
                <?php
            }
            ?>
        </div>
        <?php
    }
    
    if (count($mrt_list)) {
        ?>
        <div class="trans-item">
            <label>{{ tr('property.nearby-mrt', 'Near MRT') }}</label> 
            <?php
            foreach ($mrt_list as $mrt) {
                ?>
                <span>{{ $mrt->getTranslatable('name') }}</span>
                <?php
            }
            ?>
        </div>
        <?php
    }
    
    if (count($apl_list)) {
        ?>
        <div class="trans-item">
            <label>{{ tr('property.nearby-apl', 'Near APL') }}</label> 
            <?php
            foreach ($apl_list as $apl) {
                ?>
                <span>{{ $apl->getTranslatable('name') }}</span>
                <?php
            }
            ?>
        </div>
        <?php
    }
    ?>

</div>