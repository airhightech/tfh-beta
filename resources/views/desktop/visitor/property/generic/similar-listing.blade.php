<?php
if (count($related)) {
    ?>
    <ul id="similar-slider">
        <?php
        foreach ($related as $_property) {
            if (!$_property) {
                continue;
            }
            $title = $_property->getGeneratedTitle();
            $listing = $_property->getListingData();
            $extended = $_property->getExtendedData();

            $address = $extended->getCustomAddress();
            $name = $_property->getTranslatable('name');

            $image = $_property->getMainImage();
            ?>
            <li>
                <div class="listing-item">
                    <a href="/property/{{ $_property->id }}">
                        <div class="listing-img">
                            <?php
                            if ($image) {
                                ?>
                                <img src="/image/{{ $image->filepath }}" class="img-responsive"/>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="listing-info">
                            <p class="title">
                                {{ $title  }}
                            </p>
                            <p class="lname">
                                {{ $address }}
                            </p>
                            <p class="price">
                                ฿ {{ number_format($listing->listing_price) }}
                                <?php
                                if ($listing->ltype == 'rent') {
                                    echo tr('listing_type.per-month', '/ month');
                                }
                                ?>
                            </p>
                        </div>
                    </a>
                </div>
            </li>
            <?php
        }
        ?>
    </ul>
    <?php
}