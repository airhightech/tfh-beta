<h4>{{ tr('property_review.location', 'How to get there') }}</h4>

<?php
$review = $property->getManagerReview();

$location_images = $review->getLocationImages();
$floorplan_images = $review->getFloorplanImages();
$facility_images = $review->getFacilityImages();

$ylink = $review->getYoutubeVideo();

?>
<ul id="location-slider" class="gallery list-unstyled property-review-gallery">
    <?php
    foreach ($location_images as $image) {
        ?>
        <li data-thumb="/image/{{ $image->filepath }}">
            <img src="/image/{{ $image->filepath }}" />
        </li>
        <?php
    }
    ?>
</ul>
<p>
    {{ tr('property_review.details', 'Details') }} : {{ $review->getTranslatable('comment_loc') }}
</p>
<h4>{{ tr('property_review.floorplan', 'Room type') }}</h4>
<ul id="floorplan-slider" class="gallery list-unstyled property-review-gallery">
    <?php
    foreach ($floorplan_images as $image) {
        ?>
        <li data-thumb="/image/{{ $image->filepath }}">
            <img src="/image/{{ $image->filepath }}" />
        </li>
        <?php
    }
    ?>
</ul>

<p>
    {{ tr('property_review.details', 'Details') }} : {{ $review->getTranslatable('comment_pla') }}
</p>

<h4>{{ tr('property_review.facilities', 'Facilities') }}</h4>
<ul id="facility-slider" class="gallery list-unstyled property-review-gallery">
    <?php
    foreach ($facility_images as $image) {
        ?>
        <li data-thumb="/image/{{ $image->filepath }}">
            <img src="/image/{{ $image->filepath }}" />
        </li>
        <?php
    }
    ?>
</ul>
<p>
    {{ tr('property_review.details', 'Details') }} : {{ $review->getTranslatable('comment_fac') }}
</p>

<h4>{{ tr('property_review.video', 'Project video') }}</h4>

<div class="embed-responsive embed-responsive-16by9">
   <?php
    if($ylink) {
        ?>
        <iframe src="https://www.youtube.com/embed/{{ $ylink }}?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
        <?php
    }
   ?>
    
</div>

<h4>{{ tr('property_review.overview', 'Overview') }}</h4>
<p>
    {{ $review->getTranslatable('overview') }}
</p> 