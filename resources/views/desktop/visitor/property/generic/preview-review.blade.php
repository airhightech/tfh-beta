<div class="review-container">
    <div class="text-center">
        {{ $reviews->links() }}
    </div>
    <?php
    if (count($reviews)) {
        foreach ($reviews as $review) {
            $user = $review->getCustomer();
            $profile = $user->getProfile();
            $images = $review->getImages();
            $ratings = $review->getRatings();
            $likes = $review->getLikes();

            //var_dump($ratings);
            ?>
            <div class="row review-item">
                <div class="col-md-4">
                    <div class="like-container">
                        <div class="user-like likes text-center" data-review="{{ $review->id }}" data-liked="true">
                            <i class="fa fa-heart"></i><br/>
                            <span class="likes-{{ $review->id }}">{{ $likes['likes'] }}</span>
                        </div>
                        <div class="user-like dislikes text-center" data-review="{{ $review->id }}" data-liked="false">
                            <i class="fa fa-thumbs-o-down"></i>
                            <br/>
                            <span class="dislikes-{{ $review->id }}">{{ $likes['dislikes'] }}</span>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-8">
                    <p class="text-right">
                        <span class="customer">{{ $profile->getName() }}</span><br/>
                        {{ tr('property_review.psoted-on', 'Posted on') }} <?php echo date('d.m.Y', strtotime($review->created_at)); ?>
                    </p>
                    <p>
                        {{ $review->review }}
                    </p>
                    <div class="row">
                        <?php
                        foreach ($images as $image) {
                            ?>
                            <div class="col-md-4">
                                <img src="/thumb/200x200/{{ $image }}" class="img-responsive review-fancy"/>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row customer-rating">
                        <div class="col-md-4">
                            <p>{{ tr('property_review.facilities', 'Condo facilities') }}</p>
                            <div>
                                <?php render_rating('facilities', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.room', 'Room design') }}</p>
                            <div>
                                <?php render_rating('room', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.transport', 'Transport Accessibility') }}</p>
                            <div>
                                <?php render_rating('transport', $ratings); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row customer-rating">
                        <div class="col-md-4">
                            <p>{{ tr('property_review.management', 'Condo management') }}</p>
                            <div>
                                <?php render_rating('management', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.security', 'Security') }}</p>
                            <div>
                                <?php render_rating('security', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.amenities', 'Nearby Amenities') }}</p>
                            <div>
                                <?php render_rating('amenities', $ratings); ?>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>