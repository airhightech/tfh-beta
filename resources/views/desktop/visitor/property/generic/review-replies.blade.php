<?php
foreach ($replies as $reply) {
    $user = $reply->getCustomer();
    $profile = $user->getProfile();
    ?>
    <div class="row reply-item">
        <p>
            <span class="customer">{{ $profile->getName() }}</span>
            <span class="date pull-right">{{ tr('property_review.psoted-on', 'Posted on') }} <?php echo date('d.m.Y', strtotime($reply->created_at)); ?></span>
        </p>
        <p class="message">
            {{ $reply->message }}
        </p>
    </div>
    <?php
}

