<div class="review-container">

    <?php
    if (!isset($preview_mode)) {
        ?>
        <div class="text-center">
            {{ $reviews->links() }}
        </div>
        <?php
    }
    if (count($reviews)) {
        foreach ($reviews as $review) {
            $user = $review->getCustomer();
            $profile = $user->getProfile();
            $images = $review->getImages();
            $ratings = $review->getRatings();
            $likes = $review->getLikes();

            //var_dump($ratings);
            ?>
            <div class="row review-item">
                <div class="col-md-4">
                    <div class="like-container">
                        <div class="user-like likes text-center" data-review="{{ $review->id }}" data-liked="true">
                            <i class="fa fa-heart"></i><br/>
                            <span class="likes-{{ $review->id }}">{{ $likes['likes'] }}</span>
                        </div>
                        <div class="user-like dislikes text-center" data-review="{{ $review->id }}" data-liked="false">
                            <i class="fa fa-thumbs-o-down"></i>
                            <br/>
                            <span class="dislikes-{{ $review->id }}">{{ $likes['dislikes'] }}</span>
                        </div>
                    </div>

                </div>
                <div class="col-md-8">
                    <p class="text-right">
                        <span class="customer">{{ $profile->getName() }}</span><br/>
                        {{ tr('property_review.psoted-on', 'Posted on') }} <?php echo date('d.m.Y', strtotime($review->created_at)); ?>
                    </p>
                    <p>
                        {{ $review->review }}
                    </p>
                    <div class="row">
                        <?php
                        foreach ($images as $image) {
                            ?>
                            <div class="col-md-4">
                                <img src="/thumb/200x200/{{ $image }}" class="img-responsive review-fancy"/>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row customer-rating">
                        <div class="col-md-4">
                            <p>{{ tr('property_review.facilities', 'Condo facilities') }}</p>
                            <div>
                                <?php render_rating('facilities', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.room', 'Room design') }}</p>
                            <div>
                                <?php render_rating('room', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.transport', 'Transport Accessibility') }}</p>
                            <div>
                                <?php render_rating('transport', $ratings); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row customer-rating">
                        <div class="col-md-4">
                            <p>{{ tr('property_review.management', 'Condo management') }}</p>
                            <div>
                                <?php render_rating('management', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.security', 'Security') }}</p>
                            <div>
                                <?php render_rating('security', $ratings); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p>{{ tr('property_review.amenities', 'Nearby Amenities') }}</p>
                            <div>
                                <?php render_rating('amenities', $ratings); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="reply-box clearfix">
                <p class="text-right">
                    <span onclick="toggleReviewReply({{ $review->id }});" style="cursor: pointer;"><i class="fa fa-reply"></i> {{ tr('property_review.reply', 'Reply') }}</span>
                </p>
                <form id="reply_review_form_{{ $review->id }}" action="/rest/message/reply" method="post" class="auth-required clear-form" style="display: none;">
                    <div class="form-group">
                    <textarea class="form-control clear" rows="5" name="message" required></textarea>
                    </div>

                    <input type="hidden" name="review_id" value="{{ $review->id }}"/>
                    <button type="button" class="btn btn-primary pull-right submit-ajax-form"
                            data-target="reply_review_form_{{ $review->id }}">{{ tr('button.post', 'POST') }}</button>
                </form>
            </div>
            <div id="review_replies_{{ $review->id }}" class="replies-container clearfix">
                <?php
                $replies = $review->getLastReplies();
                ?>
                @include('desktop.visitor.property.generic.review-replies')
            </div>
            <?php
        }
    }
    ?>
</div>

<?php
if (!isset($preview_mode)) {
    ?>
    <div class="post-comment">
        <h4>{{ tr('property_details.leave-your-comment', 'Give us your comment') }}</h4>
        <form id="property-review-form" action="/rest/review/post" method="post">
            <div class="form-group">
                <label for="review">{{ tr('property_details.review-label', 'Your comment about this property') }}</label>

                <textarea class="form-control" rows="5" name="review" required></textarea>
                <p class="input-error review hidden-error"></p>
            </div>
            <div class="form-group">
                <label for="score">{{ tr('property_details.score-label', 'Give your score review') }}</label>
                <div class="row rating-wrapper">
                    <div class="col-md-4">
                        <p>{{ tr('property_review.facilities', 'Condo facilities') }}</p>
                        <div class="rating-container">
                            <input type="radio" name="rating[facilities]" class="rating" value="1" />
                            <input type="radio" name="rating[facilities]" class="rating" value="2" />
                            <input type="radio" name="rating[facilities]" class="rating" value="3" />
                            <input type="radio" name="rating[facilities]" class="rating" value="4" />
                            <input type="radio" name="rating[facilities]" class="rating" value="5" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p>{{ tr('property_review.room', 'Room design') }}</p>
                        <div class="rating-container">
                            <input type="radio" name="rating[room]" class="rating" value="1" />
                            <input type="radio" name="rating[room]" class="rating" value="2" />
                            <input type="radio" name="rating[room]" class="rating" value="3" />
                            <input type="radio" name="rating[room]" class="rating" value="4" />
                            <input type="radio" name="rating[room]" class="rating" value="5" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p>{{ tr('property_review.transport', 'Transport Accessibility') }}</p>
                        <div class="rating-container">
                            <input type="radio" name="rating[transport]" class="rating" value="1" />
                            <input type="radio" name="rating[transport]" class="rating" value="2" />
                            <input type="radio" name="rating[transport]" class="rating" value="3" />
                            <input type="radio" name="rating[transport]" class="rating" value="4" />
                            <input type="radio" name="rating[transport]" class="rating" value="5" />
                        </div>
                    </div>
                </div>
                <div class="row rating-wrapper">
                    <div class="col-md-4">
                        <p>{{ tr('property_review.management', 'Condo management') }}</p>
                        <div class="rating-container">
                            <input type="radio" name="rating[management]" class="rating" value="1" />
                            <input type="radio" name="rating[management]" class="rating" value="2" />
                            <input type="radio" name="rating[management]" class="rating" value="3" />
                            <input type="radio" name="rating[management]" class="rating" value="4" />
                            <input type="radio" name="rating[management]" class="rating" value="5" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p>{{ tr('property_review.security', 'Security') }}</p>
                        <div class="rating-container">
                            <input type="radio" name="rating[security]" class="rating" value="1" />
                            <input type="radio" name="rating[security]" class="rating" value="2" />
                            <input type="radio" name="rating[security]" class="rating" value="3" />
                            <input type="radio" name="rating[security]" class="rating" value="4" />
                            <input type="radio" name="rating[security]" class="rating" value="5" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <p>{{ tr('property_review.amenities', 'Nearby Amenities') }}</p>
                        <div class="rating-container">
                            <input type="radio" name="rating[amenities]" class="rating" value="1" />
                            <input type="radio" name="rating[amenities]" class="rating" value="2" />
                            <input type="radio" name="rating[amenities]" class="rating" value="3" />
                            <input type="radio" name="rating[amenities]" class="rating" value="4" />
                            <input type="radio" name="rating[amenities]" class="rating" value="5" />
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-group rating-upload">
                <label for="score">{{ tr('property_details.picture-label', 'Upload picture') }}</label>
                <div class="row">
                    <div class="uploader col-xs-3">
                        <label for="review_photo_1">
                            <img id="review_uploaded_1_placeholder" class="cover img-responsive" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                            <img src="" class="uploaded img-responsive" id="review_uploaded_1" />
                        </label>
                        <input type="file" name="review_photo_1" id="review_photo_1" class="hidden image-input"
                               data-target="#review_uploaded_1"/>
                    </div>
                    <div class="uploader col-xs-3">
                        <label for="review_photo_2">
                            <img id="review_uploaded_2_placeholder" class="cover img-responsive" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                            <img src="" class="uploaded img-responsive" id="review_uploaded_2" />
                        </label>
                        <input type="file" name="review_photo_2" id="review_photo_2" class="hidden image-input"
                               data-target="#review_uploaded_2"/>
                    </div>
                    <div class="uploader col-xs-3">
                        <label for="review_photo_3">
                            <img id="review_uploaded_3_placeholder" class="cover img-responsive" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                            <img src="" class="uploaded img-responsive" id="review_uploaded_3" />
                        </label>

                        <input type="file" name="review_photo_3" id="review_photo_3" class="hidden image-input"
                               data-target="#review_uploaded_3"/>
                    </div>
                </div>
            </div>
            <div class="form-group review-action text-center">
                <p class="text-center">
                    <input type="checkbox" id="accept-condition" name="accept" value="yes" required/>
                    {{ tr('property_review.accept-tfh-terms', 'I agree with Thaifullhouse Terms and Conditions') }}
                </p>
                <p class="text-danger text-center with-marging-top hidden-error accept-condition-error">
                    {!! tr('visitor_registration.accept-condition-error'
                    , 'Please accept the above condition.') !!}
                </p>
                <input type="hidden" name="id" value="{{ $property->id }}"/>
                <button type="button" class="btn btn-primary" onclick="postUserReview();">{{ tr('button.post', 'POST') }}</button>
            </div>
        </form>
    </div>
    <?php
}
