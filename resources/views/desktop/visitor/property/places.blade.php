
<?php
$list = $property->getNearbyPlaces();

$places = [];

foreach ($list as $place) {
    $places[$place->type][] = $place;
}

if (count($places)) {
    ?>
    <div class="nearby-places">
        <?php
        foreach ($places as $type => $_list) {
            ?>
            <div class="input-group">
                <span class="input-group-addon"> 
                    <?php
                    echo \App\Models\Geo\Place::getIcon($type);
                    ?>
                    ({{ count($list) }})
                </span>
                <select type="text" class="form-control poi-list" placeholder="Username" aria-describedby="basic-addon1" >
                    <option value="0">{{ tr('property.select-poi', 'Select a POI') }}</option>
                    <?php
                    foreach ($_list as $place) {
                        $names = json_decode($place->name, true)
                        ?>
                        <option value="{{ $place->id }}" 
                                data-lat="{{ $place->lat }}"
                                data-lng="{{ $place->lng }}"
                                data-type="{{ $place->type }}">{{ trget($names) }} ({{ __distance(round($place->distance)) }}m)</option>
                                <?php
                            }
                            ?>
                </select>
            </div>
            <?php
        }
        ?>
    </div>
    <?php
}