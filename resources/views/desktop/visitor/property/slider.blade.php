<div class="handler-container">
    <span class="handler left" onclick="mediaScrollLeft();">
        <i class="fa fa-angle-left"></i>
    </span>
    <span class="handler right" onclick="mediaScrollRight();">
        <i class="fa fa-angle-right"></i>
    </span>
</div>
<div class="media-slider-container">
    <?php
    $images = $property->getImages();
    $images_copy = $images;
    $image = array_shift($images_copy);
    ?>
    <div class="media-slider">
        <?php
        if ($image) {
            ?>
            <div class="media-group big-media">
                <a class="fancybox" rel="group" href="{{ $image->getRelativeUrl() }}">
                    <img src="{{ $image->getThumbUrl('400x400', true) }}">
                </a>
            </div>
            <?php
        }
        ?>

        <div class="media-group">
            <div class="small-media">
                <img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $latlng->lat, ',', $latlng->lng; ?>&zoom=13&size=300x200&maptype=roadmap
                     &markers=color:blue%7Clabel:P%7C<?php echo $latlng->lat, ',', $latlng->lng; ?>&key={{ config('google.browser_key') }}" 
                     class="img-responsive map-preview" data-lat="<?php echo $latlng->lat; ?>" data-lng="<?php echo $latlng->lng; ?>">
            </div>
            <div class="small-media">
                <?php
                $ylink1 = $listing->getYoutubeVideo1();
                $ylink2 = $listing->getYoutubeVideo2();
                if ($ylink1) {
                    ?>
                    <iframe width="300" height="200" src="https://www.youtube.com/embed/{{ $ylink1 }}?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <?php
                } else if ($ylink2) {
                    ?>
                    <iframe width="300" height="200" src="https://www.youtube.com/embed/{{ $ylink2 }}?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <?php
                } else {
                    $image = array_shift($images_copy);

                    if ($image) {
                        ?>
                        <a class="fancybox" rel="group" href="{{ $image->getRelativeUrl() }}">
                            <img src="{{ $image->getThumbUrl('300x200', true) }}">
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

        <?php
        $chunks = array_chunk($images_copy, 2);

        if (count($chunks)) {
            foreach ($chunks as $chunk) {
                $img0 = $chunk[0];
                $img1 = isset($chunk[1]) ? $chunk[1] : false;
                ?>
                <div class="media-group">
                    <?php
                    if ($img0) {
                        ?>
                        <div class="small-media">
                            <a class="fancybox" rel="group" href="{{ $img0->getRelativeUrl() }}">
                                <img src="{{ $img0->getThumbUrl('300x200', true) }}">
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if ($img1) {
                        ?>
                        <div class="small-media">
                            <a class="fancybox" rel="group" href="{{ $img1->getRelativeUrl() }}">
                                <img src="{{ $img1->getThumbUrl('300x200', true) }}">
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>

<div id="big-map" class="modal fade big-map" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ tr('popup.default-title', 'Thaifullhouse') }}</h4>
            </div>
            <div class="modal-body">
                <div id="big-map-container" class="big-map-container">
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>