<?php
$address = $property->getAddress();
$latlng = $address->getLngLat();

$extended = $property->getExtendedData();
$listing = $property->getListingData();
$building = $property->getBuildingData();
$developer = $building->getDeveloper();

$poster = $property->getPublisherProfile();

$images = $property->getImages();
$currentUser = Auth::user();

if ($address) {
    $district = $address->getDistrict();
}

$preview_mode =true;

?>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/cool-share.css" />
<div class="container property-details house">
    <div class="row preview-header">
        <div class="col-md-8 title">
            <h3 id="title"><span class="fa fa-home"></span> {{ $property->getTitle() }}</h3>
            <p>
                <span class="listing-type">
                    <?php
                    if ($listing->ltype == 'rent') {
                        echo tr('property.rent', 'Rent');
                    } else {
                        echo tr('property.sale', 'Sale');
                    }
                    ?>
                </span>
                <?php
                if ($currentUser && $property->isInUserFavorites($currentUser)) {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart"></i></button>
                    <?php
                } else {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart-o"></i></button>
                    <?php
                }
                ?>
                <span id="shareDiv" style="width:100px;height:0;display:inline-block;margin:-4px 0 0;vertical-align:top;"><button class="share">Share</button></span>
            </p>
        </div>
        <div class="col-md-4">
            <span class="listing-price">

                <?php
                if ($listing->ltype == 'rent') {
                    ?>
                    {{ tr('property.rental', 'Rental') }} : ฿ {{ number_format($listing->listing_price) }}
                    <?php
                } else {
                    ?>
                    {{ tr('property.sale-price', 'Sale price') }} : ฿ {{ number_format($property->getSalePrice()) }}
                    <?php
                }
                ?>
            </span>
        </div>
    </div>
    <div class="media-container">
        @include('desktop.visitor.property.slider')
    </div>
    <div class="details-container">
        <div class="details-box">
            <table class="table">
                <?php
                if ($listing->ltype == 'rent') {
                    ?>
                    <tr>                
                        <td class="plabel">{{ tr('property.deposit', 'Deposit') }}</td>
                        <td class="value">฿ {{ number_format($listing->deposite) }}</td>
                        <td>{{ tr('property.size', 'Size') }}</td>
                        <td class="value">{{ $extended->total_size }} sqm</td>
                        
                    </tr>
                    <tr>
                        <td>{{ tr('property.price-rent', 'Rental') }}</td>
                        <td class="value">฿ {{ number_format($listing->listing_price) }}</td>
                        <td class="plabel">{{ tr('property.bedrooms', 'Bedrooms') }}</td>
                        <td class="value">{{ $extended->bedroom_num }}</td>
                    </tr>
                    <tr>
                        <td>{{ tr('property.psf', 'Price/Sqm') }}</td>
                        <td class="value">{{ number_format($extended->psf) }} ฿/sqm</td>
                        <td>{{ tr('property.bathrooms', 'Bathrooms') }}</td>
                        <td class="value">{{ $extended->bathroom_num }}</td>
                    </tr>
                    <tr>
                        <td>{{ tr('property.floors', 'Floors') }}</td>
                        <td class="value">{{ $extended->floor_num }}</td>
                        <td>{{ tr('property.year-built', 'Year built') }}</td>
                        <td class="value">{{ $extended->year_built }}</td>
                    </tr>
                    <tr>                
                        <td>{{ tr('property.land-size', 'Land size') }}</td>
                        <td class="value">{{ $extended->land_size }} sqm</td>
                        <td>{{ tr('property.developer', 'Developer') }}</td>
                        <td class="value">{{ $developer ? $developer->getName() : '' }}</td>
                    </tr>
                    <tr>
                        <td>{{ tr('property.listed', 'Listed on') }}</td>
                        <td class="value">{{ $property->getListedOn() }}</td>
                        <td>{{ tr('property.property-id', 'Property ID') }}</td>
                        <td class="value">{{ $property->getRefCode() }}</td>  
                    </tr>
                    <?php
                } else {
                    ?>
                    <tr>                
                        <td>{{ tr('property.price-sale', 'Sale') }}</td>
                        <td class="value">฿ {{ number_format($listing->listing_price) }}</td>
                        <td>{{ tr('property.size', 'Size') }}</td>
                        <td class="value">{{ $extended->total_size }} sqm</td>
                    </tr>
                    <tr>
                        <td>{{ tr('property.psf', 'Price/Sqm') }}</td>
                        <td class="value">{{ number_format($extended->psf) }} ฿/sqm</td>
                        <td class="plabel">{{ tr('property.bedrooms', 'Bedrooms') }}</td>
                        <td class="value">{{ $extended->bedroom_num }}</td>
                    </tr>
                    <tr>
                        <td>{{ tr('property.floors', 'Floors') }}</td>
                        <td class="value">{{ $extended->floor_num }}</td>
                        <td>{{ tr('property.bathrooms', 'Bathrooms') }}</td>
                        <td class="value">{{ $extended->bathroom_num }}</td>
                    </tr>
                    <tr>                        
                        <td>{{ tr('property.year-built', 'Year built') }}</td>
                        <td class="value">{{ $extended->year_built }}</td>
                        <td>{{ tr('property.land-size', 'Land size') }}</td>
                        <td class="value">{{ $extended->land_size }} sqm</td>
                    </tr>
                    <tr>   
                        <td>{{ tr('property.developer', 'Developer') }}</td>
                        <td class="value">{{ $developer ? $developer->getName() : '' }}</td>
                        <td>{{ tr('property.property-id', 'Property ID') }}</td>
                        <td class="value">{{ $property->getRefCode() }}</td> 
                    </tr>
                    <tr>
                        <td>{{ tr('property.listed', 'Listed on') }}</td>
                        <td class="value">{{ $property->getListedOn() }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php
                }
                ?>

                @include('desktop.visitor.property.facilities')

                <tr>
                    <td>{{ tr('property.nearby-trans', 'Nearby transportation') }}</td>
                    <td colspan="3">
                        @include('desktop.visitor.property.generic.nearby-transportation')
                    </td>
                </tr>

                <tr>
                    <?php
                    if ($listing->ltype == 'rent') {
                        ?>
                        <td>{{ tr('property.availability-date', 'Move-in available from') }}</td>
                        <td class="value">{{ $listing->getAvailabilityDate() }}</td>
                        <td>{{ tr('property.minimum-rental-period', 'Min. rental period') }}</td>
                        <td class="value">{{ $listing->minimal_rental_period }} {{ tr('property.months', 'months') }}</td>
                        <?php
                    } else {
                        ?>
                        <td>{{ tr('property.availability-date', 'Move-in available from') }}</td>
                        <td class="value">{{ $listing->getAvailabilityDate() }}</td>
                        <td></td>
                        <td class="value"></td>
                        <?php
                    }
                    ?>

                </tr>
                <tr>
                    <td>{{ tr('property.details', 'Details') }}</td>
                    <td colspan="3">{{ $extended->getDetails() }}</td>
                </tr>
                <tr>
                    <td>{{ tr('property.address', 'Address') }}</td>
                    <td colspan="3">{{ $extended->getCustomAddress() }}</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="row">
                            <div class="col-md-12 property-map-container">
                                <div id="property-map" class="property-map"></div>
                                @include('desktop.visitor.property.places')
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <?php
            if ($listing->ltype == 'sale') {
                ?>
                @include('desktop.visitor.property.generic.mortgage')
                <?php
            }
            ?>
        </div>



        <div class="contact-box">
            <h4>{{ tr('property.information', 'Information') }}</h4>
            <div class="contact-box-header">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ $poster->getContactPicture() }}" class="img-responsive">
                    </div>
                    <div class="col-md-8 name">
                        {{ $poster->getName() }}<br/>
                        <a href="#" class="single-line" target="_blank">{{ $poster->website }}</a>
                    </div>
                </div>
                <p class="tel-btn">
                    <button type="button" onclick="showPhone(this);" data-tel="{{ $poster->phone }}">{{ tr('message.tel', 'Tel.') }}</button>
                </p>
            </div>
            <h5>{{ tr('property.contact', 'Contact') }}</h5>
            <div class="contact-box-body">
                <form id="property-message" action="/rest/message/send" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="sender_name" value="" placeholder="{{ tr('message.user-name', 'Name') }}" required>
                        <p class="input-error sender_name hidden-error"></p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sender_phone" value="" placeholder="{{ tr('message.user-tel', 'Your phone number') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sender_email" value="" placeholder="{{ tr('message.user-email', 'E-mail') }}" required>
                        <p class="input-error sender_email hidden-error"></p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{ tr('property.message', 'Message') }}</label>
                        <textarea class="form-control" rows="5" name="message" required><?php echo __dbconf('default_message', true); ?></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="subscribe" value="yes"> {{ tr('property.send-newsletter', 'You want to receive our newsletter') }}
                        </label>
                    </div>
                    <p class="text-center">
                        <input type="hidden" name="agent_id" value="{{ $property->publisher_id }}"/>
                        <input type="hidden" name="property_id" value="{{ $property->id }}"/>
                        <button type="button" class="btn btn-send" onclick="submitGenericForm('property-message');" ><i class="fa fa-check"></i> {{ tr('button.submit', 'Submit') }}</button>
                    </p>
                </form>
            </div>
        </div>

    </div>
</div>
