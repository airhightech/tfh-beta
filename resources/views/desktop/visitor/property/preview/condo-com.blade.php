<?php
$address = $property->getAddress();
$latlng = $address->getLngLat();

$extended = $property->getExtendedData();
$listing = $property->getListingData();
$building = $property->getBuildingData();
$developer = $building->getDeveloper();

$poster = $property->getPublisherProfile();

$images = $property->getImages();
$currentUser = Auth::user();

$district = null;

if ($address) {
    $district = $address->getDistrict();
}

$preview_mode = true;
?>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/cool-share.css" />
<div class="container property-details condo-com">
    <div class="row preview-header">
        <div class="col-md-8 title">
            <h3 id="title"><span class="fa fa-building"></span> {{ $property->getTitle() }}</h3>
            <p>
                <?php
                if ($currentUser && $property->isInUserFavorites($currentUser)) {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart"></i></button>
                    <?php
                } else {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart-o"></i></button>
                    <?php
                }
                ?>
                <span id="shareDiv" style="width:100px;height:0;display:inline-block;margin:-4px 0 0;vertical-align:top;"><button class="share">Share</button></span>
            </p>
        </div>
        <div class="col-md-4">
            <span class="listing-price">
                {{ tr('property.starting-price', 'Starting price') }} : ฿ {{ number_format($property->getSalePrice()) }}
            </span>
        </div>
    </div>
    <div class="media-container">
        @include('desktop.visitor.property.preview-slider')
    </div>

    <div>

        <div class="listing-highlights">
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/money.png"/>
                </div>
                <p><?php echo tr('property.starting-price', 'Sales Price Start From'); ?></p>
                <h4> ฿ <?php echo number_format($property->getStartingPrice()); ?></h4>        
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/circular-clock.png"/>
                </div>
                <p><?php echo tr('property.office-opening-hours', 'Office Operating Hours'); ?></p>
                <h4><?php echo $extended->office_hours; ?></h4>  
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/crane.png"/>
                </div>
                <p><?php echo tr('property.year-built', 'Year Built'); ?></p>
                <h4><?php echo $extended->year_built; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/building-3.png"/>
                </div>
                <p><?php echo tr('property.tower', 'Tower'); ?></p>
                <h4><?php echo $building->number_of_tower; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/lift.png"/>
                </div>
                <p><?php echo tr('property.floors', 'Floors'); ?></p>
                <h4><?php echo $extended->total_floors; ?></h4>
            </div>
        </div>


    </div>

    <div class="details-container">    
        <div class="details-box">

            <ul class="nav nav-tabs nav-property" role="tablist">
                <li role="presentation" class="active">
                    <a href="#property-details" aria-controls="home" role="tab" data-toggle="tab">
                        {{ tr('property.information', 'Information') }}
                    </a>
                </li>
                <li role="presentation">
                    <a href="#property-review" aria-controls="profile" role="tab" data-toggle="tab">
                        {{ tr('property.review', 'Review') }}
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="property-details">
                    <table class="table">
                        <tr>
                            <td>{{ tr('property.total-units', 'Total units') }}</td>
                            <td class="value">{{ $building->total_units }}</td>
                            <td class="plabel"></td>
                            <td class="value"></td>
                        </tr>

                        <?php
                        $room_types = $property->getRoomTypes();
                        ?>

                        <tr>
                            <td>{{ tr('property.room-type', 'Room type') }}</td>
                            <td class="value">
                                <?php
                                if (count($room_types)) {
                                    foreach ($room_types as $room_type) {
                                        ?>
                                        <p>
                                            <?php echo $room_type->name; ?> : <?php echo $room_type->size; ?> sqm
                                        </p>
                                        <?php
                                    }
                                }
                                ?>
                            </td>
                            <td></td>
                            <td class="value"></td>
                        </tr>


                        <tr>
                            <td>{{ tr('property.district', 'District') }}</td>
                            <td class="value">

                                <?php
                                if ($district) {
                                    echo $district->getTranslatable('name');
                                }
                                ?>
                            </td>

                            <td>{{ tr('property.contact', 'Contact') }}</td>
                            <td class="value">{{ $extended->contact_person }}</td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.website', 'Website') }}</td>
                            <td class="value">{{ $extended->website }}</td>
                            <td></td>
                            <td class="value"></td>
                        </tr>

                        @include('desktop.visitor.property.facilities')

                        <tr>
                            <td>{{ tr('property.nearby-trans', 'Nearby transportation') }}</td>
                            <td colspan="3">
                                @include('desktop.visitor.property.generic.nearby-transportation')
                            </td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.details', 'Details') }}</td>
                            <td colspan="3">{{ $extended->getDetails() }}</td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.address', 'Address') }}</td>
                            <td colspan="3">{{ $extended->getCustomAddress() }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="row">
                                    <div class="col-md-12 property-map-container">
                                        <div id="property-map" class="property-map"></div>
                                        @include('desktop.visitor.property.places')
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="property-review">
                    @include('desktop.visitor.property.generic.manager-review')                    
                </div>
            </div>

            @include('desktop.visitor.property.generic.mortgage')

            <div class="member-review-container">
                @include('desktop.visitor.property.generic.preview-review')
            </div>

        </div>
        <div class="property-listing" style="margin-top: 45px;">
            <h4>{{ tr('property.listed-property', 'LISTING IN THIS CONDO') }}</h4>

            <?php
            $count = $property->countListedProperties();
            ?>
            
            <p class="filters">
                <button class="btn current" data-target=".listing-rent">{{ tr('property.for-rent-property', 'For rent') }} (<?php echo array_get($count, 'rent', 0); ?>)</button>
                <button class="btn pull-right" data-target=".listing-sale">{{ tr('property.for-sale-property', 'For sale') }} (<?php echo array_get($count, 'sale', 0); ?>)</button>
            </p>
            <div class="property-list">
                <?php
                $properties = $property->getListedProperties();

                foreach ($properties as $_property) {
                    $_listing = $_property->getListingData();
                    $_extended = $_property->getExtendedData();
                    $img = $_property->getMainImage();
                    ?>
                    <div class="listing-room listing-<?php echo $_listing->ltype; ?>">
                        <div class="listing-room-image">
                            <?php
                            if ($img) {
                                ?>
                                <img src="{{ $img->getThumbUrl('300x300', true) }}" class="img-responsive"/>
                                <?php
                            } else {
                                ?>
                                <img src="http://placehold.it/300x300?text=No+image" class="img-responsive"/>
                                <?php
                            }
                            ?>
                            
                        </div>
                        <div class="listing-room-details">
                            <a href="/property/{{ $_property->id }}">
                                <span class="name"><?php echo $_property->getTranslatable('name'); ?></span>
                                <span class="address"><?php echo $_property->getCustomAddress() ?></span>
                                <span class="price">
                                    &#3647; <?php echo number_format($_listing->listing_price); ?>  
                                    <?php echo $_listing->ltype == 'rent' ? ' / month' : ''; ?>
                                </span>
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>