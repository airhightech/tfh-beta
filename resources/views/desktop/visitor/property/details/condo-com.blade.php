@extends('desktop.layouts.default')

@section('title', 'Page Title')

<?php
$address = $property->getAddress();
$latlng = $address->getLngLat();

$extended = $property->getExtendedData();
$listing = $property->getListingData();
$building = $property->getBuildingData();

$poster = $property->getPublisherProfile();
$developer = $building->getDeveloper();

$images = $property->getImages();

$currentUser = Auth::user();

$twitter_description = $extended->getDetails();

if (empty($twitter_description)) {
    $twitter_description = $property->getTitle();
}

$score = $property->getScore();

?>


@section('custom-header')

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@thaifullhouse">
<meta name="twitter:title" content="{{ $property->getTitle() }}">
<meta name="twitter:description" content="{{ $twitter_description }}">
<?php
if (count($images)) {
    ?>
    <meta name="twitter:image" content="{{ $images[0]->getThumbUrl('400x400', true) }}" />
    <?php
}
?>
@endsection

@section('content')
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/cool-share.css" />
<div class="container property-details">
    <div class="row preview-header">
        <div class="col-md-9 title">
            <h3 id="title"><span class="fa fa-building"></span> {{ $property->getTitle() }}</h3>
            <p>
                <?php
                if ($currentUser && $property->isInUserFavorites($currentUser)) {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart"></i></button>
                    <?php
                } else {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart-o"></i></button>
                    <?php
                }
                ?>
                <span id="shareDiv" style="width:100px;height:0;display:inline-block;margin:-4px 0 0;vertical-align:top;"><button class="share">Share</button></span>
            </p>
            <p class="ranking">
                {{ tr('property.ranking', 'Ranking') }} : {{ render_simple_rating($score, 5, true) }}
            </p>
        </div>
        <div class="col-md-3">
            <span class="btn btn-back pull-right" onclick="goBack();">&laquo; BACK</span><br/>
            <span class="listing-price">
                {{ tr('property.starting-price', 'Starting price') }} : ฿ {{ number_format($property->getStartingPrice()) }}
            </span>
        </div>
    </div>
    <div class="media-container">
        @include('desktop.visitor.property.slider')
    </div>

    <div>

        <div class="listing-highlights">
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/money.png"/>
                </div>
                <p><?php echo tr('property.starting-price', 'Sales Price Start From'); ?></p>
                <h4> ฿ <?php echo number_format($property->getStartingPrice()); ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/circular-clock.png"/>
                </div>
                <p><?php echo tr('property.office-opening-hours', 'Office Operating Hours'); ?></p>
                <h4><?php echo $extended->office_hours; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/crane.png"/>
                </div>
                <p><?php echo tr('property.year-built', 'Year Built'); ?></p>
                <h4><?php echo $extended->year_built; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/building-3.png"/>
                </div>
                <p><?php echo tr('property.tower', 'Tower'); ?></p>
                <h4><?php echo $building->number_of_tower; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/lift.png"/>
                </div>
                <p><?php echo tr('property.floors', 'Floors'); ?></p>
                <h4><?php echo $extended->total_floors; ?></h4>
            </div>
        </div>


    </div>

    <div class="details-container">
        <div class="details-box">

            <ul class="nav nav-tabs nav-property" role="tablist">
                <li role="presentation" class="active">
                    <a href="#property-details" aria-controls="home" role="tab" data-toggle="tab" data-ref="property-details">
                        {{ tr('property.information', 'Information') }}
                    </a>
                </li>
                <li role="presentation">
                    <a href="#property-review" aria-controls="profile" role="tab" data-toggle="tab" data-ref="property-review">
                        {{ tr('property.review', 'Review') }}
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="property-details">
                    <table class="table">

                        <tr>
                            <td>{{ tr('property.total-units', 'Total units') }}</td>
                            <td class="value">{{ $building->total_units }}</td>
                            <td class="plabel">{{ tr('property.total-floors', 'Total floors') }}</td>
                            <td class="value">{{ $extended->total_floors }}</td>
                        </tr>

                        <tr>
                            <td>{{ tr('property.room-type', 'Room type') }}</td>
                            <td class="value">
                                <?php
                                $room_types = $property->getRoomTypes();
                                if (count($room_types)) {
                                    foreach ($room_types as $room_type) {
                                        ?>
                                        <p>
                                            <?php echo $room_type->name; ?> : <?php echo $room_type->size; ?> sqm
                                        </p>
                                        <?php
                                    }
                                }
                                ?>
                            </td>
                            <td>{{ tr('property.district', 'District') }}</td>
                            <td class="value">
                                <?php
                                if ($address) {
                                    echo $address->getDistrictName();
                                }
                                ?>
                            </td>
                        </tr>


                        <tr>
                            <td>{{ tr('property.parking-space', 'Parking space') }}</td>
                            <td class="value">{{ $extended->parking_lot }}</td>
                            <td>{{ tr('property.contact', 'Contact') }}</td>
                            <td class="value">{{ $extended->contact_person }}</td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.website', 'Website') }}</td>
                            <td class="value">{{ $extended->website }}</td>
                            <td></td>
                            <td class="value"></td>
                        </tr>

                        @include('desktop.visitor.property.facilities')

                        <tr>
                            <td>{{ tr('property.nearby-trans', 'Nearby transportation') }}</td>
                            <td colspan="3">
                                @include('desktop.visitor.property.generic.nearby-transportation')
                            </td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.details', 'Details') }}</td>
                            <td colspan="3">{{ $extended->getDetails() }}</td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.address', 'Address') }}</td>
                            <td colspan="3">{{ $extended->getCustomAddress() }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="row">
                                    <div class="col-md-12 property-map-container">
                                        <div id="property-map" class="property-map"></div>
                                        @include('desktop.visitor.property.places')
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    @include('desktop.visitor.property.generic.mortgage')
                    <div class="member-review-container">
                        @include('desktop.visitor.property.generic.member-review')
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="property-review">
                    @include('desktop.visitor.property.generic.manager-review')
                </div>
            </div>



        </div>
        <div class="property-listing" style="margin-top: 45px;">
            <h4>{{ tr('property.listed-property', 'LISTING IN THIS CONDO') }}</h4>

            <?php
            $count = $property->countListedProperties();
            ?>
            <p class="filters">
                <button class="btn current" data-target=".listing-rent">{{ tr('property.for-rent-property', 'For rent') }} (<?php echo array_get($count, 'rent', 0); ?>)</button>
                <button class="btn pull-right" data-target=".listing-sale">{{ tr('property.for-sale-property', 'For sale') }} (<?php echo array_get($count, 'sale', 0); ?>)</button>
            </p>
            <div class="property-list">
                <?php
                $properties = $property->getListedProperties();

                foreach ($properties as $_property) {
                    $_listing = $_property->getListingData();
                    $_extended = $_property->getExtendedData();
                    $img = $_property->getMainImage();
                    ?>
                    <div class="listing-room listing-<?php echo $_listing->ltype; ?>">
                        <div class="listing-room-image">
                            <?php
                            if ($img) {
                                ?>
                                <img src="{{ $img->getThumbUrl('300x300', true) }}" class="img-responsive"/>
                                <?php
                            } else {
                                ?>
                                <img src="http://placehold.it/300x300?text=No+image" class="img-responsive"/>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="listing-room-details">
                            <a href="/property/{{ $_property->id }}">
                                <span class="name"><?php echo $_property->getTranslatable('name'); ?></span>
                                <span class="address"><?php echo $_property->getCustomAddress(); ?></span>
                                <span class="price">
                                    &#3647; <?php echo number_format($_listing->listing_price); ?>
                                    <?php echo $_listing->ltype == 'rent' ? ' / month' : ''; ?>
                                </span>
                            </a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<link rel="stylesheet" href="/libs/fancybox/jquery.fancybox.css" />
<link rel="stylesheet" href="/libs/rating/min/rating.css" />
<link rel="stylesheet" href="/libs/lightslider/css/lightslider.min.css" />
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/libs/fancybox/jquery.fancybox.pack.js"></script>
<script src="/libs/rating/min/rating.js"></script>
<script src="/libs/lightslider/js/lightslider.min.js"></script>
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script src="/js/cool-share.js"></script>
<script>
                        var latlng = <?php echo json_encode($latlng); ?>;
                        $(document).ready(function () {
                        $('.fancybox').fancybox();
                        var url = window.location.href;
                        var options = {

                        twitter: {
                        text: ''/*$('#title').text(),
                         via: 'thaifullhouse'*/
                        },
                                facebook : true,
                                googlePlus : true
                        };
                        $('#shareDiv').shareButtons(url, options);
                        $('.socials').css('position', 'fixed');
                        $('.socials').css('margin-left', '-105px');
                        $('.socials').css('margin-top', '-65px');
                        $('.rating-container').rating();
                        });</script>
<script src="{{ __asset('/js/visitor/property-details.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-gallery.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-review.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-condo.js') }}"></script>
<script src="{{ __asset('/js/visitor/mortgage.js') }}"></script>

<script src="{{ __asset('/js/manager/uploader.js') }}"></script>
@endsection
