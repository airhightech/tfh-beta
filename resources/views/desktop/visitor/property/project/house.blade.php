<?php

$address = $property->getAddress();
$latlng = $address->getLngLat();

$extended = $property->getExtendedData();
$listing = $property->getListingData();
$building = $property->getBuildingData();
$developer = $building->getDeveloper();

$poster = $property->getPublisherProfile();

$images = $property->getImages();

$currentUser = Auth::user();

$twitter_description = $extended->getDetails();

$district = null;

if ($address) {
    $district = $address->getDistrict();
}

if (empty($twitter_description)) {
    $twitter_description = $property->getTitle();
}

?>

@extends('desktop.layouts.default')

@section('title', 'Page Title')

@section('custom-header')

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@thaifullhouse">
<meta name="twitter:title" content="{{ $property->getTitle() }}">
<meta name="twitter:description" content="{{ $twitter_description }}">
<?php
if (count($images)) {
    ?>
    <meta name="twitter:image" content="{{ $images[0]->getThumbUrl('400x400', true) }}" />
    <?php
}
?>
@endsection

@section('content')
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/cool-share.css" />
<div class="container property-details">
    <div class="row preview-header">
        <div class="col-md-9 title">
            <h3 id="title">{{ $property->getTitle() }}</h3>
            <p>
                <?php
                if ($currentUser && $property->isInUserFavorites($currentUser)) {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart"></i></button>
                    <?php
                } else {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart-o"></i></button>
                    <?php
                }
                ?>
                <span id="shareDiv" style="width:100px;height:0;display:inline-block;margin:-4px 0 0;vertical-align:top;"><button class="share">Share</button></span>
            </p>
        </div>
        <div class="col-md-3">
            <span class="listing-price">
                {{ tr('property.starting-price', 'Starting price') }} : ฿ {{ number_format($property->getSalePrice()) }}
            </span>
        </div>
    </div>
    <div class="media-container">
        @include('desktop.visitor.property.slider')
    </div>

    <div>

        <div class="listing-highlights">
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/money.png"/>
                </div>
                <p><?php echo tr('property.starting-price', 'Sales Price Start From'); ?></p>
                <h4> ฿ <?php echo number_format($property->getStartingPrice()); ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/circular-clock.png"/>
                </div>
                <p><?php echo tr('property.office-opening-hours', 'Office Operating Hours'); ?></p>
                <h4><?php echo $extended->office_hours; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/crane.png"/>
                </div>
                <p><?php echo tr('property.year-built', 'Year Built'); ?></p>
                <h4><?php echo $extended->year_built; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/building-3.png"/>
                </div>
                <p><?php echo tr('property.tower', 'Tower'); ?></p>
                <h4><?php echo $building->number_of_tower; ?></h4>
            </div>
            <div class="listing-highlight">
                <div class="icon">
                    <img src="/img/lift.png"/>
                </div>
                <p><?php echo tr('property.floors', 'Floors'); ?></p>
                <h4><?php echo $extended->total_floors; ?></h4>
            </div>
        </div>


    </div>

    <div class="details-container">
        <div class="details-box">

            <ul class="nav nav-tabs nav-property" role="tablist">
                <li role="presentation" class="active">
                    <a href="#property-details" aria-controls="home" role="tab" data-toggle="tab" data-ref="property-details">
                        {{ tr('property.information', 'Information') }}
                    </a>
                </li>
                <li role="presentation">
                    <a href="#property-review" aria-controls="profile" role="tab" data-toggle="tab" data-ref="property-review">
                        {{ tr('property.review', 'Review') }}
                    </a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="property-details">
                    <table class="table">

                        <tr>
                            <td>{{ tr('property.total-units', 'Total units') }}</td>
                            <td class="value">{{ $building->total_units }}</td>
                            <td class="plabel"></td>
                            <td class="value"></td>
                        </tr>

                        <?php
                        $room_types = $property->getRoomTypes();
                        ?>

                        <tr>
                            <td>{{ tr('property.house-type', 'House type') }}</td>
                            <td class="value">
                                {{ $extended->house_type }}
                            </td>
                            <td>{{ tr('property.size', 'Size') }}</td>
                            <td class="value">{{ $extended->total_size }} sqm</td>
                        </tr>


                        <tr>
                            <td>{{ tr('property.land-size', 'Land size') }}</td>
                            <td class="value">{{ $extended->total_size }} sqm</td>
                            <td>{{ tr('property.district', 'District') }}</td>
                            <td class="value">

                                <?php
                                if ($district) {
                                    echo $district->getTranslatable('name');
                                }
                                ?>
                            </td>
                                                       
                        </tr>
                        <tr>
                            <td>{{ tr('property.website', 'Website') }}</td>
                            <td class="value">{{ $extended->website }}</td> 
                            
                            <td>{{ tr('property.contact', 'Contact') }}</td>
                            <td class="value">{{ $extended->contact_person }}</td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.developer', 'Developer') }}</td>
                            <td class="value">
                                <?php 
                                if ($developer != null) {
                                    echo $developer->getName();
                                }
                                ?>
                            </td>
                            <?php
                            if ($property->new_project) {
                                ?>
                                <td>
                                    {{ tr('property.property-type', 'Property type') }}
                                </td>
                                <td class="value">
                                    {{ tr('member_posting.property-type.detached-house', 'Single House') }}
                                </td>
                                <?php
                            } else {
                                ?>
                                <td></td>
                                <td class="value"></td>
                                <?php
                            }
                            ?>
                            
                        </tr>

                        @include('desktop.visitor.property.facilities')

                        <tr>
                            <td>{{ tr('property.nearby-trans', 'Nearby transportation') }}</td>
                            <td colspan="3">
                                @include('desktop.visitor.property.generic.nearby-transportation')
                            </td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.details', 'Details') }}</td>
                            <td colspan="3">{{ $extended->getDetails() }}</td>
                        </tr>
                        <tr>
                            <td>{{ tr('property.address', 'Address') }}</td>
                            <td colspan="3">{{ $extended->getCustomAddress() }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="row">
                                    <div class="col-md-12 property-map-container">
                                        <div id="property-map" class="property-map"></div>
                                        @include('desktop.visitor.property.places')
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    @include('desktop.visitor.property.generic.mortgage')
                    <div class="member-review-container">
                        @include('desktop.visitor.property.generic.member-review')
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="property-review">
                    @include('desktop.visitor.property.generic.manager-review')
                </div>
            </div>



        </div>
        <div class="contact-box">
            <h4>{{ tr('property.information', 'Information') }}</h4>
            <div class="contact-box-header">
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ $poster->getContactPicture() }}" class="img-responsive">
                    </div>
                    <div class="col-md-8">
                        {{ $poster->getName() }}<br/>
                        <a href="#" class="single-line" target="_blank">{{ $poster->website }}</a>
                    </div>
                </div>
                <p class="tel-btn">
                    <button type="button" onclick="showPhone(this);" data-tel="{{ $poster->phone }}">{{ tr('message.tel', 'Tel.') }}</button>
                </p>
            </div>
            <h5>{{ tr('property.information', 'Information') }}</h5>
            <div class="contact-box-body">
                <form id="property-message" action="/rest/message/send" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" name="sender_name" value="" placeholder="{{ tr('message.user-name', 'Name') }}" required>
                        <p class="input-error sender_name hidden-error"></p>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sender_phone" value="" placeholder="{{ tr('message.user-tel', 'Your phone number') }}">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="sender_email" value="" placeholder="{{ tr('message.user-email', 'E-mail') }}" required>
                        <p class="input-error sender_email hidden-error"></p>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{ tr('property.message', 'Message') }}</label>
                        <textarea class="form-control" rows="5" name="message" required><?php echo __dbconf('default_message', true); ?></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="subscribe" value="yes"> {{ tr('property.send-newsletter', 'You want to receive our newsletter') }}
                        </label>
                    </div>
                    <p class="text-center">
                        <input type="hidden" name="agent_id" value="{{ $property->publisher_id }}"/>
                        <input type="hidden" name="property_id" value="{{ $property->id }}"/>
                        <button type="button" class="btn btn-send submit-ajax-form" data-target="property-message" ><i class="fa fa-check"></i> {{ tr('button.submit', 'Submit') }}</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<link rel="stylesheet" href="/libs/fancybox/jquery.fancybox.css" />
<link rel="stylesheet" href="/libs/rating/min/rating.css" />
<link rel="stylesheet" href="/libs/lightslider/css/lightslider.min.css" />
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/libs/fancybox/jquery.fancybox.pack.js"></script>
<script src="/libs/rating/min/rating.js"></script>
<script src="/libs/lightslider/js/lightslider.min.js"></script>
<script src="/libs/chartjs/Chart.bundle.min.js"></script>
<script src="/js/cool-share.js"></script>
<script>
                        var latlng = <?php echo json_encode($latlng); ?>;
                        $(document).ready(function () {
                        $('.fancybox').fancybox();
                        var url = window.location.href;
                        var options = {

                        twitter: {
                        text: ''/*$('#title').text(),
                         via: 'thaifullhouse'*/
                        },
                                facebook : true,
                                googlePlus : true
                        };
                        $('#shareDiv').shareButtons(url, options);
                        $('.socials').css('position', 'fixed');
                        $('.socials').css('margin-left', '-105px');
                        $('.socials').css('margin-top', '-65px');
                        $('.rating-container').rating();
                        });</script>
<script src="{{ __asset('/js/visitor/property-details.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-gallery.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-review.js') }}"></script>
<script src="{{ __asset('/js/visitor/property-condo.js') }}"></script>
<script src="{{ __asset('/js/visitor/mortgage.js') }}"></script>

<script src="{{ __asset('/js/manager/uploader.js') }}"></script>
@endsection