@extends('desktop.layouts.error')

@section('title', 'Page Title')

@section('content')

<h4>404. {{ tr('errors.not-found-title', 'Page not found') }}</h4>
<p>
    {{ tr('errors.not-found-text', 'The requested URL was not found on this server.') }} 
</p>

<form action="/search" method="get">
    <div class="form-group">
        <input type="text" name="q" class="form-control" value=""
               placeholder="{{ tr('errors.suggestion-placeholder', 'Do you want to find a property ?') }}">
    </div>
    <button type="submit" class="btn btn-default">{{ tr('button.search', 'Search') }}</button>
</form>

@endsection