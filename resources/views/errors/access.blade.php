@extends('desktop.layouts.error')

@section('title', 'Page Title')

@section('content')

<h4>{{ tr('errors.forbidden-access-title', 'Access forbidden') }}</h4>
<p>
    {{ tr('errors.forbidden-access-text', 'You are not allowed to access this') }}. 
</p>
<p>
    If you think this is an error please contact our administrator: admin@thaifullhouse.com    
</p>

@endsection