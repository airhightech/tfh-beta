
## Mobile

Mobile source code have been merged with desktop to allow user to switch easily  from mobile to desktop.

### Controllers

Controllers for mobile can be found at

```
/app/Http/Controller/Mobile
```

*AgentController* : display search agent page, and agent details page

*HomeController* : display homepage and premium listing page

*PropertyController* : display search page, and property details

*UserController* : display user favorites pages


### Views

Views for mobile can be found at

```
/resources/views/mobile
```


### Routes

Route for mobile can be found at

```
/routes/mobile.php
```

as mobile and desktop use the same code, the route need to be configured correctly, so they will use a different domain. for example on live website.

desktop : beta.thaifullhouse.com
mobile : m.thaifullhouse.com

For the configuration, please check

```
/routes/web.php
```


### Problem to be fixed on mobile

#### 1. Homepage search field

The text field on homepage don't have a submit button, so some users cannot submit the search. You need to replace the icon on the right, by a real submit button.

#### 2. Many CSS problem on every page

For example, when some data like address are too long, the layout of the website look very strange. The solution is to cut the long text and replace them with an ellipsis (...)

#### 3. The filters on search map if not yet active.

http://m.thaifullhouse.com/search

Search function accept many parameters:

- q : property or place name
- lt : listing types, values are *rent* or *sale*. can have multiple values separated by [,]
- pt : property type, values are *ap*, *cd*, *sh*, *th*, *cdb*. can have multiple values separated by [,]
- np : show new project ? values are *yes* or *no*, can be empty also

REMARK:
- if user select *pt=cdb*, then *np* MUST be = NO
- if user select *pt=cdb*, then the other values (*ap*, *cd*, *dh*, *th*) MUST be removed from the *pt* param
- if user select *np=yes*, then *cdb* MUST be removed from *pt* param

#### 4. Need to activate all Javascript functions:

1. Add to favorite (when the user touch on heart button)
2. Send message to agent (when user fill contact box on agent page) [table : messages]
3. Send reviews (when user fill review form on property and agent pages)

To add/remove a property to user favorites, please call

```
http://m.thaifullhouse.com/user/favorite-property/{id}

-- {id} : property ID

```

##### 5. Add listing view

Add list view for search result
