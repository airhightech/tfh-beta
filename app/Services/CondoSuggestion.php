<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Helpers\Language;
use App\Models\Property;
use DB;
use Illuminate\Support\Facades\Log;

class CondoSuggestion {

    protected $request;
    protected $langs;
    protected $terms;

    public function __construct(Request $request) {
        $this->request = $request;
        $this->langs = Language::getActiveCodes();
    }

    public function find() {
        $this->sanitize();

        if (count($this->terms)) {
            return$this->getSuggestion();
        } else {
            return [];
        }
    }

    protected function sanitize() {
        $term = trim($this->request->input('term'));
        $terms = preg_split('/(\s+)/', $term);
        $this->terms = array_filter($terms);
    }

    protected function getSuggestion() {

        $query = DB::table('properties')->select('id', 'name');

        $terms = $this->terms;

        $that = $this;

        $query->where(function($query) use ($that, $terms) {
            foreach ($that->langs as $lang) {
                $query->orWhere(function($query) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $query->where('name->' . $lang, '~*', $_term);
                    }
                });
            }
        });

        $query->where('ptype', Property::TYPE_CONDO_BUILDING);
        //$query->where('user_submited', false);
        $query->where(function($query) {
            $query->orWhere('user_submited', false);
            $query->orWhereRaw('user_submited IS NULL');
        });
            
        $query->where('new_project', false);
        $query->where('published', true);

        return $query->limit(50)->get()->all();
    }

    public function getPropertySuggestion() {
        return [];
    }

}
