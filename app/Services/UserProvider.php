<?php

namespace App\Services;

use Auth;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserProvider {

    public static function findOrCreateFromFB($fbuser, $validated_email = false) {
        $user = null;

        try {

            $user = User::where('email', $fbuser->email)->firstOrFail();
            $user->name = $fbuser->name;
            $user->save();

        } catch (ModelNotFoundException $ex) {

            $ex = null;

            $user = new User;
            $user->email = $fbuser->email;
            $user->name = $fbuser->name;
            $user->email_validated = $validated_email;
            $user->ctype = 'visitor';
            $user->password = '';

            if (!$validated_email) {
                $user->email_validation_token = str_random(20);
            }

            $user->save();
        }

        return $user;
    }

}
