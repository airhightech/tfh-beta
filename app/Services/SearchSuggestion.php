<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Helpers\Language;
use DB;
use App\Models\Property\Address;
use App\Models\Geo\Province;
use App\Models\Geo\Dictrict;


//use Illuminate\Support\Facades\Log;

class SearchSuggestion {

    protected $request;
    protected $langs;
    protected $terms;

    public function __construct(Request $request) {
        $this->request = $request;
        $this->langs = Language::getActiveCodes();
    }

    public function find() {
        $this->sanitize();

        if (count($this->terms)) {
            return array_merge(
                    $this->getPropertySuggestion()
                    , $this->getPlaceSuggestion()
            );
        } else {
            return [];
        }
    }

    protected function sanitize() {
        $term = trim($this->request->input('term'));
        $terms = preg_split('/(\s+)/', $term);
        $this->terms = array_filter($terms);
    }

    protected function getPlaceSuggestion() {

        $query = DB::table('places')
                ->select(
                'id'
                , 'name'
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                , 'type');

        $terms = $this->terms;

        foreach ($this->langs as $lang) {
            $query->orWhere(function($query) use ($terms, $lang) {
                foreach ($terms as $_term) {
                    $query->where('name->' . $lang, '~*', $_term);
                }
            });
        }

        $query->whereNull('deleted_at');

        return $query->limit(50)->get()->all();
    }

    public function getPropertySuggestion() {

        $lt = $this->request->input('lt');
        $np = $this->request->input('np');
        $pt = $this->request->input('pt');

        $scope = $this->request->input('scope', 'listing');

        $query = DB::table('properties')->select(
                DB::raw('DISTINCT ON (properties.id) properties.id')
                , 'properties.ptype AS type'
                , 'properties.name'
                , 'property_listing_data.ltype As ltype'
                , DB::raw('ST_X(ST_TRANSFORM(property_address.location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(property_address.location::geometry, 4326)) AS lat'));

        $query->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id');
        $query->leftJoin('property_address', 'property_address.id', 'properties.address_id');

        //$query->where('properties.publisher_id', '>', 0);
        $query->where('properties.published', true);
        $query->whereNull('properties.deleted_at');

        if ($pt == 'cdb') {
            $query->where(function($query) {
                $query->orWhere('properties.user_submited', false);
                $query->orWhereRaw('properties.user_submited IS NULL');
            });
        }

        $pt = explode(',', $pt);

        if ($scope == 'project') {
            $query->where('properties.new_project', true);
        } else {
            $query->where('properties.new_project', false);
            if ($scope == 'listing') {
                $lt = explode(',', $lt);
                $query->whereIn('property_listing_data.ltype', $lt);
                $query->whereIn('properties.ptype', ['cd', 'ap', 'th', 'sh']);

            } else if ($scope == 'cdb') {
                $query->where('properties.ptype', 'cdb');
            }
        }

        $terms = $this->terms;

        /*$query->where(function($query) use ($terms) {
            foreach ($this->langs as $lang) {
                $query->orWhere(function($query) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $query->where('properties.name->' . $lang, '~*', $_term);
                    }
                });
            }
        });*/

        $address_ids = $this->getAddressIds();
        $building_ids = $this->getBuildingIds($address_ids);

        $query->where(function($query) use ($terms, $building_ids, $address_ids) {
            $langs = Language::getActiveCodes();
            foreach ($langs as $lang) {
                $query->orWhere(function($query) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $query->where('properties.name->' . $lang, '~*', $_term);
                    }
                });
            }

            if (count($building_ids)) {
                $query->orWhereIn('properties.id', $building_ids);
            }

            if (count($address_ids)) {
                $query->orWhereIn('properties.address_id', $address_ids);
            }
        });

        //Log::info('SUGGEST NO RESULT : ' . $query->toSql());

        $results = $query->limit(50)->get()->all();

        return $results;
    }

    protected function getBuildingIds($address_ids) {
        $query = DB::table('properties')->select(
                'id');

        $ids = [];

        if (count($this->terms)) {
            $terms = $this->terms;

            $query->where(function($query) use ($terms, $address_ids) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('name->' . $lang, '~*', $_term);
                        }
                    });
                }

                if (count($address_ids)) {
                    $query->orWhereIn('address_id', $address_ids);
                }
            });

            $query->where('ptype', 'cdb');
            $query->where('new_project', false);
            $query->whereNull('deleted_at');

            $ids = $query->pluck('id');

            $ids = DB::table('property_belongs_to_building')->select('property_id')
                            ->whereIn('building_id', $ids)->pluck('property_id');
        }

        return $ids;
    }

    public function getAddressIds() {

        // District

        $query = DB::table('geo_districts')->select(
                'id');

        $district_ids = [];

        if (count($this->terms)) {
            $terms = $this->terms;

            $query->where(function($query) use ($terms) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('name->' . $lang, '~*', $_term);
                        }
                    });
                }
            });

            $query->whereNull('deleted_at');

            $district_ids = $query->pluck('id');
        }

        // Province

        $query = DB::table('geo_provinces')->select(
                'id');

        $province_ids = [];

        if (count($this->terms)) {
            $terms = $this->terms;

            $query->where(function($query) use ($terms) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('name->' . $lang, '~*', $_term);
                        }
                    });
                }
            });

            $query->whereNull('deleted_at');

            $province_ids = $query->pluck('id');
        }

        $ids = DB::table('property_address')->select('id')
                ->whereIn('province_id', $province_ids)
                ->orWhereIn('district_id', $district_ids)
                ->pluck('id');

        return $ids;
    }

}
