<?php

namespace App\Services;

use Illuminate\Http\Request;
use DB;
use App\Models\Property\Address;
use App\Models\Geo\Province;
use App\Models\Geo\Dictrict;
use App\Helpers\Language;

class SearchProperty {

    protected $request;
    protected $query;
    protected $map_center;
    protected $includes;
    protected $apply_listing_type;

    public function __construct(Request $request) {
        $this->apply_listing_type = true;
        $this->request = $request;
        $this->includes = [];
        $this->langs = Language::getActiveCodes();
        if ($request->input('np') == 'yes') {
            $this->apply_listing_type = false;
        }
    }

    /**
     * 
     * @param type $exclude
     * @return type
     */
    public function findForMap($exclude = []) {

        // remove details data from map result to minimize data transfer
        $this->prepareSearchQuery($exclude, false);

        return $this->query->get();
    }

    public function includePropertiesById($ids) {
        if (is_scalar($ids)) {
            $this->includes[] = $ids;
        } else if (is_array($ids)) {
            $this->includes = array_merge($this->includes, $ids);
        }
    }

    /**
     * 
     * @param type $exclude
     * @return type
     */
    public function findForListing($exclude = []) {

        $this->prepareSearchQuery($exclude, true);

        $this->sortResult();

        $list = $this->query->paginate(20)->appends($this->request->all());


        foreach ($list as $index => $row) {
            $list[$index]->images = DB::table('property_have_images')
                    ->where('property_id', $row->id)
                    ->count();

            $addrr = [];

            if ($row->address_id) {
                $address = Address::find($row->address_id);

                $district = $address->getDistrict();
                $province = $address->getProvince();

                if ($district) {
                    $addrr[] = $district->getTranslatable('name');
                }

                if ($province) {
                    $addrr[] = $province->getTranslatable('name');
                }
            }

            $list[$index]->address = implode(', ', $addrr);
        }

        return $list;
    }

    protected function sanitize() {
        $term = trim($this->request->input('q'));
        $terms = preg_split('/(\s+)/', $term);
        $this->terms = array_filter($terms);
    }

    protected function prepareSearchQuery($exclude, $getDetails = false) {

        $this->query = DB::table('properties')->select(
                'properties.id'
                , 'properties.ptype'
                , 'property_listing_data.ltype'
                , 'property_listing_data.listing_class AS lclass'
                , 'properties.new_project AS np'
                , DB::raw('ST_X(ST_TRANSFORM(property_address.location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(property_address.location::geometry, 4326)) AS lat'));

        $this->query->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id');
        $this->query->leftJoin('property_extended_data', 'property_extended_data.property_id', 'properties.id');
        $this->query->leftJoin('property_address', 'property_address.id', 'properties.address_id');

        if ($getDetails) {
            $this->addDetailedData();
        }

        if (is_array($exclude) && count($exclude) > 0) {
            $this->query->whereNotIn('properties.id', $exclude);
        }

        $this->query->where('properties.publisher_id', '>', 0);
        $this->query->where('properties.published', true);
        $this->query->whereNull('properties.deleted_at');
        /* $this->query->where('properties.publication_date', '<=', date('Y-m-d'));
          $this->query->where(function($query) {
          $query->orWhere('property_listing_data.expiration_date', '>=', date('Y-m-d'));
          $query->orWhereRaw('property_listing_data.expiration_date IS NULL');
          }); */



        // Include buildings

        $this->sanitize();

        $address_ids = $this->getAddressIds();
        $building_ids = $this->getBuildingIds($address_ids);

        $this->applyAllFitlers();

        if ($this->request->input('match') && count($this->terms)) {

            $terms = $this->terms;

            $this->query->where(function($query) use ($terms, $building_ids, $address_ids) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('properties.name->' . $lang, '~*', $_term);
                        }
                    });
                }

                if (count($building_ids)) {
                    $query->orWhereIn('properties.id', $building_ids);
                }

                if (count($address_ids)) {
                    $query->orWhereIn('properties.address_id', $address_ids);
                }
            });
        }

        if ($this->map_center) {
            $this->query->whereRaw("ST_Distance(property_address.location, ST_GeogFromText('SRID=4326;POINT({$this->map_center->lng} {$this->map_center->lat})')) < " . 20000);
        } else {
            $this->query->limit(1000);
        }
    }

    protected function getBuildingIds($address_ids) {
        $query = DB::table('properties')->select(
                'id');

        $ids = [];

        if (count($this->terms)) {
            $terms = $this->terms;

            $query->where(function($query) use ($terms, $address_ids) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('name->' . $lang, '~*', $_term);
                        }
                    });
                }

                if (count($address_ids)) {
                    $query->orWhereIn('address_id', $address_ids);
                }
            });

            $query->where('ptype', 'cdb');
            $query->where('new_project', false);
            $query->whereNull('deleted_at');

            $ids = $query->pluck('id');

            $ids = DB::table('property_belongs_to_building')->select('property_id')
                            ->whereIn('building_id', $ids)->pluck('property_id');
        }

        return $ids;
    }

    public function getAddressIds() {

        // District

        $query = DB::table('geo_districts')->select(
                'id');

        $district_ids = [];

        if (count($this->terms)) {
            $terms = $this->terms;

            $query->where(function($query) use ($terms) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('name->' . $lang, '~*', $_term);
                        }
                    });
                }
            });

            $query->whereNull('deleted_at');

            $district_ids = $query->pluck('id');
        }

        // Province

        $query = DB::table('geo_provinces')->select(
                'id');

        $province_ids = [];

        if (count($this->terms)) {
            $terms = $this->terms;

            $query->where(function($query) use ($terms) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere(function($query) use ($terms, $lang) {
                        foreach ($terms as $_term) {
                            $query->where('name->' . $lang, '~*', $_term);
                        }
                    });
                }
            });

            $query->whereNull('deleted_at');

            $province_ids = $query->pluck('id');
        }

        $ids = DB::table('property_address')->select('id')
                ->whereIn('province_id', $province_ids)
                ->orWhereIn('district_id', $district_ids)
                ->pluck('id');

        return $ids;
    }

    protected function sortResult() {
        // Add sorting

        $sort = $this->request->input('sort');
        $dir = $this->request->input('dir');

        $column = '';

        if ($sort) {

            if ($sort == 'price') {
                $ptypes = array_filter(explode(',', $this->request->input('pt')));
                if ($this->request->input('np') == 'yes' || in_array('cdb', $ptypes)) {
                    $column = 'property_extended_data.starting_sales_price';
                } else {
                    $column = 'property_listing_data.listing_price';
                }
            } else if ($sort == 'year') {
                $column = 'property_extended_data.year_built';
            } else if ($sort == 'beds') {
                $column = 'property_extended_data.bedroom_num';
            } else if ($sort == 'baths') {
                $column = 'property_extended_data.bathroom_num';
            } else if ($sort == 'size') {
                $column = 'property_extended_data.total_size';
            } else if ($sort == 'rental') {
                $column = 'property_listing_data.minimal_rental_period';
            }
        }

        if ($column) {
            if ($dir == 'desc') {
                $this->query->orderBy($column, 'DESC');
            } else {
                $this->query->orderBy($column, 'ASC');
            }
        }
    }

    protected function addDetailedData() {

        //$lang = \App::getLocale();

        $this->query->addSelect(DB::raw("properties.name"));
        $this->query->addSelect(DB::raw('round(property_listing_data.listing_price) AS listing_price'));
        $this->query->addSelect(DB::raw('round(property_extended_data.starting_sales_price) AS starting_sales_price'));
        $this->query->addSelect('property_extended_data.bedroom_num');
        $this->query->addSelect('property_extended_data.bathroom_num');
        $this->query->addSelect('property_extended_data.total_size');
        $this->query->addSelect('property_extended_data.year_built');
        $this->query->addSelect('property_listing_data.minimal_rental_period');
        $this->query->addSelect('properties.address_id');
    }

    protected function applyAllFitlers() {

        if ($this->request->input('np') != 'yes') {
            $this->addPropertyTypeFilter();
            $this->addListingTypeFilters();
        }


        $this->addPriceRangeFilter();

        if ($this->request->input('np') != 'yes') {
            $this->addBedroomFitler();
            $this->addBathroomFilter();
            $this->addSizeRangeFitler();
        }

        $this->addYearBuiltFilter();
        $this->addLocationFitler();
        $this->addNewProjectFitler();
    }

    //

    protected function addListingTypeFilters() {

        $ltypes = $this->translateListingType(
                explode(',', $this->request->input('lt'))
        );

        if ($this->apply_listing_type && is_array($ltypes) && count($ltypes) > 0) {
            $this->query->whereIn('property_listing_data.ltype', $ltypes);
        }
    }

    protected function addPropertyTypeFilter() {

        $ptypes = array_filter(explode(',', $this->request->input('pt')));

        if (in_array('cdb', $ptypes)) {
            $this->apply_listing_type = false;
            $this->query->where('properties.ptype', 'cdb');
        } else if (is_array($ptypes) && count($ptypes) > 0) {
            $this->query->whereIn('properties.ptype', $ptypes);
        }
    }

    public function addPriceRangeFilter() {

        $price_min = (int) $this->request->input('pmin');
        $price_max = (int) $this->request->input('pmax');

        if (is_int($price_min) && $price_min > 0) {
            $this->query->where('property_listing_data.listing_price', '>=', $price_min);
        }
        if (is_int($price_max) && $price_max > 0) {
            $this->query->where('property_listing_data.listing_price', '<=', $price_max);
        }
    }

    public function addBedroomFitler() {

        $bedroom = $this->request->input('bed');

        if ($bedroom == 'studio') {
            $this->query->where('property_extended_data.bedroom_num', '=', 0);
        } else if (preg_match('/\+/', $bedroom)) {
            $bedroom = str_replace('+', '', $bedroom) + 0;

            if (is_int($bedroom)) {
                $this->query->where('property_extended_data.bedroom_num', '>=', $bedroom);
            }
        } else if (!empty($bedroom)) {
            $bedroom = ((int) $bedroom) + 0;

            if (is_int($bedroom)) {
                $this->query->where('property_extended_data.bedroom_num', '=', $bedroom);
            }
        }
    }

    public function addBathroomFilter() {

        $bathroom = $this->request->input('bath');
        $comp = '=';

        if (preg_match('/\+/', $bathroom)) {
            $bathroom = str_replace('+', '', $bathroom) + 0;
            $comp = '>=';
        } else {
            $bathroom = ((int) $bathroom) + 0;
            $comp = '=';
        }

        if (is_int($bathroom) && $bathroom > 0) {
            $this->query->where('property_extended_data.bathroom_num', $comp, $bathroom);
        }
    }

    public function addSizeRangeFitler() {
        $size_min = (int) $this->request->input('smin');
        $size_max = (int) $this->request->input('smax');

        if (is_int($size_min) && $size_min > 0) {
            $this->query->where('property_extended_data.total_size', '>=', $size_min);
        }
        if (is_int($size_max) && $size_max > 0) {
            $this->query->where('property_extended_data.total_size', '<=', $size_max);
        }
    }

    public function addYearBuiltFilter() {
        $year_min = (int) $this->request->input('ymin');
        $year_max = (int) $this->request->input('ymax');

        if (is_int($year_min) && $year_min > 0) {
            $this->query->where('property_extended_data.year_built', '>=', $year_min);
        }
        if (is_int($year_max) && $year_max > 0) {
            $this->query->where('property_extended_data.year_built', '<=', $year_max);
        }
    }

    public function addLocationFitler() {

        $nl = $this->request->input('nl');
        $fr = $this->request->input('fr');

        if ($nl && $fr) {
            list($lat_min, $lng_min) = explode(',', $nl);
            list($lat_max, $lng_max) = explode(',', $fr);

            if (is_numeric($lng_min) && is_numeric($lat_min) && is_numeric($lng_max) && is_numeric($lat_max)) {

                $this->map_center = new \stdClass;
                $this->map_center->lng = ($lng_min + $lng_max) / 2.0;
                $this->map_center->lat = ($lat_min + $lat_max) / 2.0;

                $this->query->whereRaw(DB::raw("property_address.location::geometry && ST_MakeEnvelope($lng_min, $lat_min, $lng_max, $lat_max, 4326)"));
            }
        }
    }

    public function addNewProjectFitler() {

        $np = $this->request->input('np');

        if ($np == 'yes') {
            $this->query->where('properties.new_project', true);
        } else {
            $this->query->where('properties.new_project', false);
        }
    }

    protected function translateListingType($values) {
        $types = [];
        foreach ($values as $val) {
            switch ($val) {
                case 'sale':
                    $types[] = 'sale';
                    break;
                case 'rent':
                    $types[] = 'rent';
                    break;
                default:
                    break;
            }
        }

        return $types;
    }

}
