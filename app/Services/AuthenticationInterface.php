<?php

namespace App\Services;

interface AuthenticationInterface {
    
    const LOGIN_SUCCESS = 0;    
    const EMAIL_NOT_VALIDATED = 1;
    const CREDENTIAL_NOT_FOUND = 2;
    
}