<?php

namespace App\Services;

use DB;
use App\Models\Property;
use Illuminate\Database\QueryException;

class SimilarProperty {

    public function findSimilar(Property $property, $count = 4, $distance = 10000) {

        $address = $property->getAddress();
        $listing = $property->getListingData();

        $related = [];

        if ($address && $listing && $listing->listing_price > 0) {

            if ($address->lng() && $address->lat()) {
                $point = 'POINT(' . $address->lng() . ' ' . $address->lat() . ')';

                try {
                    $query = DB::table('properties')
                            ->select('properties.id'
                                    , DB::raw("ST_Distance(property_address.location, ST_GeomFromText('$point',4326)) AS distance")
                                    , DB::raw("abs(property_listing_data.listing_price - {$listing->listing_price}) AS price_diff"))
                            ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                            ->leftJoin('property_address', 'property_address.id', 'properties.address_id')
                            ->where('properties.ptype', $property->ptype)
                            ->where('properties.published', true)
                            ->whereRaw("ST_Distance(property_address.location, ST_GeomFromText('$point',4326)) < $distance")
                            ->where('property_listing_data.ltype', $listing->ltype)
                            ->orderBy('distance', 'ASC')
                            ->orderBy('price_diff', 'ASC')
                            ->take($count);



                    foreach ($query->get() as $row) {
                        $related[] = Property::find($row->id);
                    }
                } catch (QueryException $ex) {
                    
                }
            }
        }

        return $related;
    }

}
