<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;
use URL;
use Auth;
use Carbon\Carbon;

class AppLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $lang = $request->input('hl', false);

        if ($lang) {
            $lang = strtolower($lang);
            Session::set('lang', $lang);
            $url = str_replace('&hl=', '&chl=', URL::previous()); // Prevent infinite redirect
            
            Carbon::setLocale($lang);
            
            // Update user prefered lang
            
            if (Auth::check()) {
                $user = Auth::user();
                $user->lang = $lang;
                $user->save();
            }
            
            return redirect($url);
        }

        App::setLocale(Session::has('lang') ? Session::get('lang') : Config::get('app.locale'));
        return $next($request);
    }
}
