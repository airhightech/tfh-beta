<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            abort(403, 'Unauthorized action.');
        }
        
        $user = Auth::user();         
        
        if (!in_array($user->ctype, ['manager', 'editor', 'translator', 'accountant'])) {
            abort(403, 'Unauthorized action.');
        }

        return $next($request);
    }
}
