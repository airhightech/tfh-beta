<?php

namespace App\Http\Requests\Customer\Staff;

use App\Http\Requests\Base\ManagerRequest;
use App\Models\User\Staff;

class Update extends ManagerRequest { 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        $roles = array_keys(Staff::getRoles());
        
        return [
            'email' => 'required|email|exists:users',
            'role' => 'required|in:' . implode(',', $roles)        
        ];
    }

}
