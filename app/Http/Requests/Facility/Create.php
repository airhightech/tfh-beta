<?php

namespace App\Http\Requests\Facility;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'name.th' => 'required',
            'name.en' => 'required'
        ];
    }

}
