<?php

namespace App\Http\Requests\Package;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest {    
    
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name.th' => 'required',
            'listing_count' => 'required',
            'normal_price' => 'required',
            'duration' => 'required'
        ];
    }

}
