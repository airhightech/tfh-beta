<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePassword extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'password' => 'required|confirmed'
        ];
    }
}
