<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

class CreateStaff extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'role' => 'required',
            'password' => 'required|confirmed',
        ];
    }
}
