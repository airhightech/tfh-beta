<?php

namespace App\Http\Requests\Register;

use Illuminate\Foundation\Http\FormRequest;

class Agent extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'firstname' => 'required',
            'lastname' => 'required'
        ];
    }
}
