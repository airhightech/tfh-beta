<?php

namespace App\Http\Requests\Subscription;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'email' => 'required|email|unique:newsletter_subscriptions'
        ];
    }

    public function messages() {
        return [
            'email.required' => tr('javascript.email-empty', 'Please input your email'),
            'email.email' => tr('javascript.email-not-valid', 'Your email is not valid'),
            'email.unique' => tr('javascript.subscription-exist', 'Your email is already registered'),
        ];
    }

}
