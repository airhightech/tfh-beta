<?php

namespace App\Http\Requests\Place;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'name.th' => 'required',
            'name.en' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'type' => 'required'
        ];
    }

}
