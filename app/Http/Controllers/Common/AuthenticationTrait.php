<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Requests\Registration as RegistrationRequest;
use Auth;
use App\Models\User;
use App\Models\User\Profile;
use App\Models\User\PersonalData;
use App\Models\User\CompanyData;
use Hash;
use App\Emails\EmailValidation;
use Mail;
use DB;
use Illuminate\Database\QueryException;

trait AuthenticationTrait {

    protected $lastUser;

    protected function doLogin(Request $request) {

        $data = array_only($request->all(), ['email', 'password']);

        $data['email_validated'] = true;
        $data['active'] = true;

        if (Auth::attempt($data)) {
            $user = Auth::user();

            if ($user->email_validated == true) {

                $lang = \App::getLocale();
                $user->lang = $lang;
                $user->last_login = date('Y-m-d H:i:s');
                $user->save();

                return self::LOGIN_SUCCESS;
            } else {
                $this->lastUser = $user;
                Auth::logout();
                return self::EMAIL_NOT_VALIDATED;
            }
        } else {
            return self::CREDENTIAL_NOT_FOUND;
        }
    }

    protected function doRegister(RegistrationRequest $request, $type = 'visitor') {

        return $this->_register($request, $type);
    }

    protected function _register(Request $request, $type, $activate = false, $create_user = false) {

        $user = new User();

        if ($create_user) {
            
            $id = DB::table('users')->orderBy('id', 'desc')->value('id');
            $user->email = 'dev-' . $id . '@example.com';
            $user->password = Hash::make('emptypass');
            
        } else {
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            
        }
        
        $user->ctype = $type;


        if ($activate) {
            $user->active = true;
        } else {
            $user->email_validated = false;
            $user->email_validation_token = str_random(20);
        }

        $user->name = '';
        $user->save();
        
        $this->subscribe($user->email);

        if (!$activate) {
            Mail::to($user)->queue(new EmailValidation($user));
        }

        $this->createProfile($user, $request);
        $this->createPersonalData($user, $request);
        $this->createCompanyData($user, $request);

        return $user;
    }
    
    protected function subscribe($email) {
        $lang = \App::getLocale();
        
        try {
            DB::table('newsletter_subscriptions')->insert([
                'email' => $email,
                'prefered_lang' => $lang,
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()')
            ]);
        } catch (QueryException $ex) {

        }

        
    }

    protected function createProfile($user, $request) {
        $profile = new Profile;
        $profile->user_id = $user->id;
        $profile->updateFromRequest($request);
        $profile->save();
    }

    protected function createPersonalData($user, $request) {
        $personal = new PersonalData;
        $personal->user_id = $user->id;
        $personal->updateFromRequest($request);
        $personal->save();
    }

    protected function createCompanyData($user, $request) {
        $company = new CompanyData;
        $company->user_id = $user->id;
        $company->updateFromRequest($request);
        $company->save();
    }

}
