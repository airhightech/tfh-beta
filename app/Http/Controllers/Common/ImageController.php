<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Helpers\ImageTrait;
//use Illuminate\Support\Facades\Response;
use Image;
use DB;

class ImageController extends Controller {
    
    use ImageTrait;

    public function index($file) {
        $path = storage_path('app/public/' . $file);
        return $this->send_file($path);
    }

    public function getThumb($size, $file, $default = false) {

        $path = storage_path('app/public/' . $file);

        if (file_exists($path)) {
            $thumb = storage_path('app/public/thumb-' . $size . '-' . $file);
            if (!file_exists($thumb)) {
                $img = $this->createThumbFromFile($size, $file);                
                return $img ? $img->response() : $this->send_default($default);
            } else {
                return $this->send_file($thumb);
            }
        } else {
           return $default ? $this->send_file($default) : response('Image Not found', 404);
        }
    }

    protected function send_file($file) {
        /*if (is_readable($file)) {
            return response()->file($file);
        } else {
            return response('Image Not found', 404);
        }*/
        
        if (is_readable($file)) {
            $img = Image::make($file);
            if (substr($file, -4) == '.gif') {
                //return $img->response('gif', 100);
                header('Content-Type: image/gif');
                readfile($file);
                exit();
            } else {                
                return $img->response();
            }
            
        } else {
            return response('Image Not found', 404);
        }
    }
    
    protected function send_default($default) {
        return $default ? $this->send_file($default) : response('Image Not found', 404);
    }

    public function propertyThumb($id) {
        
        $file = DB::table('property_have_images')->where('property_id', $id)
                ->value('filepath');

        if ($file) {
            return $this->getThumb('320x300', $file, public_path('/img/default-property.png'));
        } else {
            return $this->send_file(public_path('/img/default-property.png'));
        }
    }
}
