<?php

namespace App\Http\Controllers\Common;

use DB;
use Socialite;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Socialite\Two\InvalidStateException;
use Auth;
use Illuminate\Support\Facades\Log;

trait SocialLoginTrait {

    public function facebook() {
        
        if (Auth::check()) {
            Auth::logout();
        }        
        return Socialite::driver('facebook')->redirect();
        
    }

    public function facebookCallback() {
        
        try {
            $user = Socialite::driver('facebook')->user();
            $this->getUserFromProvider($user, 'facebook');
        } catch (InvalidStateException $ex) {
            Log::info('FB CALLBACK : ' . $ex->getMessage());
        }

        return redirect('/');
    }

    public function twitter() {
        
        if (Auth::check()) {
            Auth::logout();
        }        
        return Socialite::driver('twitter')->redirect();
    }

    public function twitterCallback() {
        
        try {
            $user = Socialite::driver('twitter')->user();
            $this->getUserFromProvider($user, 'twitter');
        } catch (InvalidStateException $ex) {
            Log::info('TT CALLBACK : ' . $ex->getMessage());
        }
        

        return redirect('/');
    }

    public function google() {
        if (Auth::check()) {
            Auth::logout();
        }
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback() {
        
        try {
            $user = Socialite::driver('google')->user();
            $this->getUserFromProvider($user, 'google');
        } catch (InvalidStateException $ex) {
            Log::info('G+ CALLBACK : ' . $ex->getMessage());
        }
        return redirect('/');
    }

    protected function forceUserLogin(User $user, $source) {
        Auth::login($user, false);

        $user->last_login = date('Y-m-d H:i:s');
        $lang = \App::getLocale();
        $user->lang = $lang;
        $user->save();

        DB::table('login_history')->insert([
            'user_id' => $user->id,
            'source' => $source,
            'created_at' => DB::raw('NOW()')
        ]);
    }

    protected function getUserFromProvider($puser, $provider) {

        try {
            $user = User::where('email', $puser->getEmail())->firstOrFail();
            $user->name = $puser->getName();
            $user->active = true;
            $user->avatar = $puser->getAvatar();
            $user->save();

            $this->forceUserLogin($user, $provider);
        } catch (ModelNotFoundException $ex) {

            $ex = null;

            $user = new User;
            $user->email = $puser->getEmail();
            $user->ctype = 'visitor';
            $user->name = $puser->getName();
            $user->email_validated = true;
            $user->active = true;
            $user->password = '';
            $user->avatar = $puser->getAvatar();
            $user->save();

            $this->forceUserLogin($user, $provider);
        }
    }

}
