<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use App\Http\Controllers\Common\AuthenticationTrait;
use App\Http\Requests\Register\UpdatePassword;
use App\Http\Requests\Registration as RegistrationRequest;
use App\Services\AuthenticationInterface;
use App\Emails\ResetPasswordLink;
use App\Models\User;
use Hash;
use Mail;

class AuthController extends BaseController implements AuthenticationInterface {

    use AuthenticationTrait;

    public function login(Request $request) {
        $result = $this->doLogin($request);

        $response = [];

        switch ($result) {
            case self::LOGIN_SUCCESS:
                $response = ['status' => 'OK', 'next' => '/'];
                break;
            case self::EMAIL_NOT_VALIDATED:
                $response = [
                    'status' => 'ERROR',
                    'next' => '/auth/check-inbox?user=' . $this->lastUser->id
                ];
                break;
            case self::CREDENTIAL_NOT_FOUND:
                $response = [
                    'status' => 'ERROR',
                    'error' => tr('auth_error.credential-not-found', 'Email or password incorrect')
                ];
                break;
        }

        return response()->json($response);
    }

    public function register(RegistrationRequest $request) {
        $user = $this->doRegister($request);

        return response()->json([
                    'status' => 'OK',
                    'next' => '/auth/check-inbox?user=' . $user->id
        ]);
    }

    public function forgotPassword(Request $request) {

        $token = str_random(20);

        $user = User::where('email', $request->input('email'))->first();
        $user->password_reset_token = $token;
        $user->save();

        // Queue email

        Mail::to($user)->queue(new ResetPasswordLink($user));

        return response()->json(['status' => 'OK', 'next' => '/auth/check-inbox?reason=password-link']);
    }

    public function updatePassword(UpdatePassword $request) {
        $user = User::find($request->input('id'));

        if ($user->password_reset_token == $request->input('token')) {
            $user->password = Hash::make($request->input('password'));
            $user->password_reset_token = null;
            $user->save();
            return response()->json(['status' => 'OK', 'next' => '/auth/login?message=password-updated']);
        } else {
            return response()->json([
                        'status' => 'ERROR'
                        , 'error' => tr('auth_error.password-token-invalid', 'The password token is not valid')]);
        }
    }

}
