<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\Common\AuthenticationTrait;
use App\Services\AuthenticationInterface;

use App\Http\Requests\Register\Owner as OwnerRegistrationRequest;
use App\Http\Requests\Register\Agent as AgentRegistrationRequest;
use App\Http\Requests\Register\Company as CompanyRegistrationRequest;
use App\Http\Requests\Register\Developer as DeveloperRegistrationRequest;

class RegistrationController extends BaseController implements AuthenticationInterface {
    
    use AuthenticationTrait;
    
    public function owner(OwnerRegistrationRequest $request) {        
        $this->_register($request, 'owner');        
        return response()->json(['status' => 'OK', 'next' => '/auth/check-inbox']);        
    }
    
    public function agent(AgentRegistrationRequest $request) {        
        $this->_register($request, 'agent');        
        return response()->json(['status' => 'OK', 'next' => '/auth/check-inbox']);        
    }
    
    public function company(CompanyRegistrationRequest $request) {        
        $this->_register($request, 'company');        
        return response()->json(['status' => 'OK', 'next' => '/auth/check-inbox']);        
    }
    
    public function developer(DeveloperRegistrationRequest $request) {        
        $this->_register($request, 'developer', true, true);        
        return response()->json(['status' => 'OK', 'next' => '/manager/developer/list']);        
    }
    
}