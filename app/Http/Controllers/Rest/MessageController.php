<?php

namespace App\Http\Controllers\Rest;

use App\Http\Requests\Message\Create as CreateMessageRequest;
use App\Models\Messages\Message;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\Reviews\ReviewReply;
use App\Models\Property\Notification;
use App\Http\Requests\Message\CreateNotification;

class MessageController extends BaseController {

    public function send(CreateMessageRequest $request) {

        Message::createFromRequest($request);

        return response()->json(['status' => 'OK'
                    , 'message' => tr('message.sent', 'Message sent')]);
    }

    public function reply(Request $request) {

        $user = Auth::user();

        $message = $request->input('message');
        $review_id = $request->input('review_id');
        $now = date('Y-m-d H:i:s');

        if ($message && $review_id) {
            DB::table('customer_review_replies')->insert([
                'review_id' => $review_id,
                'user_id' => $user->id,
                'message' => $message,
                'created_at' => $now,
                'updated_at' => $now
            ]);
        }

        return response()->json(['status' => 'OK', 'loadajax' => true, 'target' => '#review_replies_' . $review_id, 'link' => '/rest/replies/' . $review_id]);
    }

    public function getReplies($id) {
        $replies = ReviewReply::where('review_id', $id)
                ->orderBy('created_at', 'desc')
                ->paginate(5);

        return view('desktop.visitor.property.generic.review-replies', ['replies' => $replies]);
    }

    public function createNotice(CreateNotification $request) {

        $notice = new Notification;
        $notice->message = $request->input('message');
        $notice->type = $request->input('type');
        $notice->sent = false;
        $notice->save();
        
        if ($request->input('send') == 'yes') {
            $notice->send();
        }

        return response()->json(['status' => 'OK', 'next' => '/manager/notice/list']);
    }
    
    public function updateNotice(CreateNotification $request) {

        $notice = Notification::find($request->input('id'));
        $notice->message = $request->input('message');
        $notice->type = $request->input('type');
        $notice->sent = false;
        $notice->save();
        
        if ($request->input('send') == 'yes') {
            $notice->send();
        }

        return response()->json(['status' => 'OK', 'next' => '/manager/notice/list']);
    }

}
