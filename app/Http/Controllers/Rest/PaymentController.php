<?php

namespace App\Http\Controllers\Rest;

use App\Models\Listings\Package;
use App\Models\Customer\Purchase;
use App\Models\Payment\Paysbuy;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentController extends BaseController {
    
    public function getPaysbuyToken(Request $request, $pid) {

        // Create new order  

        $package = Package::find($pid);
        $purchase = $package->createPurchase($request);

        $data = [
            'psbID' => '3045723277',
            'username' => 'pay@wonderpons.com',
            'secureCode' => 'ea6f702f645f3b374310d98be6fb7e2f',
            'inv' => $purchase->invoice_id,
            'itm' => $package->getTranslatable('name'),
            'amt' => $purchase->getAmount(),
            'paypal_amt' => '',
            'curr_type' => 'TH',
            'com' => '',
            'method' => '2',
            'language' => 'T',
            'resp_front_url' => url('/member/purchase'),
            'resp_back_url' => url('/rest/paysbuy/callback'),
            'opt_fix_redirect' => '',
            'opt_fix_method' => '',
            'opt_name' => '',
            'opt_email' => '',
            'opt_mobile' => '',
            'opt_address' => '',
            'opt_detail' => '',
            'opt_param' => '' 
            
        ];

        $client = new HttpClient();

        try {
            //$response = $client->request('POST', 'https://demo.paysbuy.com/api_paynow/api_paynow.asmx/api_paynow_authentication_v3', ['form_params' => $data]);
            $response = $client->request('POST', 'https://www.paysbuy.com/api_paynow/api_paynow.asmx/api_paynow_authentication_v3', ['form_params' => $data]);
            return $response->getBody();
        } catch (ServerException $ex) {
            return Psr7\str($ex->getResponse());
        }
    }
    
    public function handlePaysbuyCallback(Request $request) {

        $result = $request->input('result');

        $invoice_id = substr($result, 2);
        
        $response = [];

        try {

            $purchase = Purchase::where('invoice_id', $invoice_id)->firstOrFail();

            $payment = new Paysbuy;
            $payment->purchase_id = $purchase->id;
            $payment->result = $request->input('result');
            $payment->apcode = $request->input('apCode');
            $payment->amount = round($request->input('amt') * 100);
            $payment->fee = round($request->input('fee') * 100);
            $payment->method = $request->input('method');
            $payment->save();

            $code = substr($result, 0, 2);

            if ($code === '00') {

                $purchase->payment_status = 'paid';
                $purchase->payment_date = date('Y-m-d H:i:s');
                $purchase->payment_method = 'paysbuy';
                $purchase->save();
            }

            //$response = ['status' => 'OK', 'payment' => $payment->id];
            
        } catch (ModelNotFoundException $ex) {
            //$response = ['status' => 'OK', 'error' => $ex->getMessage()];
        }
        
        return response()->json($response);
    }
    
    public function buyFromBank(Request $request) {
        
        $package = Package::find($request->input('package'));

        $purchase = $package->createPurchase($request);

        if ($purchase) {
            return response()->json([
                'status' => 'OK',
                'id' => $purchase->id,
                'orderId' => $purchase->invoice_id
            ]);
        }
        else {
             return response()->json(['status' => 'ERROR']);
        }
    }
}