<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use App\Models\Customer\Banner;

class BannerController extends BaseController {

    public function get(Request $request) {

        $lang = \App::getLocale();

        $location = $request->input('location');

        $banner = Banner::where('location', $location)
                ->where('date_start', '<=', date('Y-m-d'))
                ->where('date_end', '>=', date('Y-m-d'))
                ->where('payment_status', 'paid')
                ->orderBy('print', 'asc')
                ->first();

        if ($banner) {
            $banner->print = $banner->print + 1;
            $banner->save();

            $images = json_decode($banner->images, true);

            if (count($images)) {
                $defaults = array_filter(array_values($images));
                $image = isset($images[$lang]) && $images[$lang] ? $images[$lang] : array_shift($defaults);
                return ['status' => 'OK', 'banner' => '/image/' . $image, 'location' => $banner->location, 'all' => $defaults, 'link' => $banner->related_link];
            } else {
                return ['status' => 'NOT_IMAGES'];
            }
        } else {
            return ['status' => 'NOT_FOUND'];
        }
    }

}
