<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use App\Models\Geo\Place;

class PlaceController extends BaseController {
    
    public function update(Request $request) {
        
        $id = $request->input('id');
        
        $place = Place::find($id);
        
        $place->setTranslatable('name', $request->input('name'));
        $place->type = $request->input('type');
        
        $place->save();

        return response()->json(['status' => 'OK', 'reload' => true]);
        
    }
    
    public function delete(Request $request) {
        
        $id = $request->input('id');
        
        $place = Place::find($id);
        $place->delete();
        
        return response()->json(['status' => 'OK', 'reload' => true]);
    }
    
}