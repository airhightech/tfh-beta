<?php

namespace App\Http\Controllers\Rest;

use App\Http\Requests\Property\Create as CreatePropertyRequest;
use App\Models\Property;
use App\Models\Property\Address;
use Illuminate\Http\Request;
use App\Models\Reviews\ManagerReview;
use App\Models\Property\Image as PropertyImage;
use App\Models\Reviews\Image as ReviewImage;
use App\Services\CondoSuggestion;
use DB;
use Auth;

class PropertyController extends BaseController {

    public function postCreate(CreatePropertyRequest $request) {

        $user = Auth::user();

        $property = new Property;
        $property->publisher_id = $user->id;
        $property->createFromRequest($request);
        $property->published = true;
        $property->publication_date = date('Y-m-d H:i:s');
        $property->save();

        $listing = $property->getListingData();
        $listing->movein_date = date('Y-m-d');
        $time = time() + (config('tfh.default_listing_period') * 24 * 3600);
        $listing->expiration_date = date('Y-m-d', $time);
        $listing->save();

        $extended = $property->getExtendedData();
        $extended->updateFromRequest($request);

        $address = $property->getAddress();

        if ($address) {
            $address->updateFromRequest($request);
        } else {

            $address = new Address;
            $address->updateFromRequest($request);
            $address->save();

            $property->address_id = $address->id;
            $property->save();
        }

        $next = false;

        if ($property->new_project) {
            if ($property->ptype == Property::TYPE_CONDO_BUILDING) {
                $next = '/manager/project/condo/info/' . $property->id;
            } else if ($property->ptype == Property::TYPE_APARTMENT_ROOM) {
                $next = '/manager/project/apartment/info/' . $property->id;
            } else if ($property->ptype == Property::TYPE_SINGLE_HOUSE) {
                $next = '/manager/project/house/info/' . $property->id;
            } else if ($property->ptype == Property::TYPE_TOWNHOUSE) {
                $next = '/manager/project/townhouse/info/' . $property->id;
            }
        } else {
            if ($property->ptype == Property::TYPE_CONDO_BUILDING) {
                $next = '/manager/condo/info/' . $property->id;
            }
        }

        return response()->json(['status' => 'OK', 'next' => $next]);
    }

    public function postInfo(CreatePropertyRequest $request) {

        $property = Property::findOrFail($request->input('id'));
        $property->updateFromRequest($request);
        $property->save();

        $listing = $property->getListingData();
        $listing->updateFromRequest($request);

        $extended = $property->getExtendedData();
        $extended->updateFromRequest($request);

        $address = $property->getAddress();

        $building_id = $property->belongsToBuilding();

        if (!$building_id) {
            if ($address) {
                $address->updateFromRequest($request);
            } else {
                $address = new Address;
                $address->updateFromRequest($request);
                $address->save();

                $property->address_id = $address->id;
                $property->save();
            }
        }

        $building = $property->getBuildingData();
        $building->updateFromRequest($request);

        return response()->json(['status' => 'OK'/* , 'reload' => true */, 'address' => $address]);
    }

    public function postDetails(Request $request) {

        $property = Property::findOrFail($request->input('id'));
        $property->updateFromRequest($request);
        $property->save();

        $extended = $property->getExtendedData();
        $extended->updateFromRequest($request);

        $listing = $property->getListingData();
        $listing->updateFromRequest($request);

        $building = $property->getBuildingData();
        $building->updateFromRequest($request);

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function postOtherDetails(Request $request) {

        $property = Property::findOrFail($request->input('id'));

        $extended = $property->getExtendedData();
        $extended->updateFromRequest($request);

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function postUpload(Request $request) {
        $property = Property::findOrFail($request->input('id'));
        $property->addImageFromRequest($request, 'images', true);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function postRoom(Request $request, $id) {
        $property = Property::findOrFail($id);
        $property->addRoomType($request);

        return view('desktop.manager.property.rooms', ['property' => $property]);
    }

    public function deleteRoom($id, $room) {
        $property = Property::findOrFail($id);
        $property->deleteRoomType($room);

        return view('desktop.manager.property.rooms', ['property' => $property]);
    }

    public function updateRoom(Request $request, $id, $room) {
        $property = Property::findOrFail($id);
        $property->updateRoomType($request);

        return view('desktop.manager.property.rooms', ['property' => $property]);
    }

    public function updateFacilities(Request $request) {

        // Delete facilities first

        $property_id = $request->input('id');

        DB::delete('DELETE FROM property_have_facilities WHERE property_id = ?', [$property_id]);

        $facitities = $request->get('facilities');

        if (count($facitities)) {
            $data = [];
            foreach ($facitities as $facility_id) {
                $data[] = [
                    'facility_id' => $facility_id,
                    'property_id' => $property_id
                ];
            }
            DB::table('property_have_facilities')->insert($data);
        }

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateFeatures(Request $request) {
        // Delete facilities first

        $property_id = $request->input('id');

        DB::delete('DELETE FROM property_have_features WHERE property_id = ? AND feature_id IN (SELECT id FROM property_features WHERE optional = false)', [$property_id]);

        $features = $request->get('features');

        if (count($features)) {
            $data = [];
            foreach ($features as $feature_id) {
                $data[] = [
                    'feature_id' => $feature_id,
                    'property_id' => $property_id
                ];
            }
            DB::table('property_have_features')->insert($data);
        }

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateOptionalFeatures(Request $request) {
        // Delete facilities first

        $property_id = $request->input('id');

        DB::delete('DELETE FROM property_have_features WHERE property_id = ? AND feature_id IN (SELECT id FROM property_features WHERE optional = true)', [$property_id]);

        $features = $request->get('features');

        if (count($features)) {
            $data = [];
            foreach ($features as $feature_id) {
                $data[] = [
                    'feature_id' => $feature_id,
                    'property_id' => $property_id
                ];
            }
            DB::table('property_have_features')->insert($data);
        }

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateNearbyTransportation(Request $request) {

        $id = $request->input('id');

        // Save BTS, MRT, APL

        DB::delete('DELETE FROM property_nearby_transportations WHERE property_id = ?', [$id]);

        $data = [];

        $nearby_bts = $request->input('nearby_bts', []);

        foreach ($nearby_bts as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'bts'
            ];
        }

        $nearby_mrt = $request->input('nearby_mrt', []);

        foreach ($nearby_mrt as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'mrt'
            ];
        }

        $nearby_apl = $request->input('nearby_apl', []);

        foreach ($nearby_apl as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'apl'
            ];
        }

        DB::table('property_nearby_transportations')->insert($data);

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function uploadReviewImage(Request $request) {
        $review = ManagerReview::findOrFail($request->input('id'));
        $review->addImageFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateReviewDetails(Request $request) {
        $review = ManagerReview::findOrFail($request->input('id'));
        $review->updateFromRequest($request);

        return response()->json(['status' => 'OK']);
    }

    public function deleteImage($id) {
        $image = PropertyImage::findOrFail($id);
        $image->deleteFiles();
        $image->delete();

        return response()->json(['status' => 'OK']);
    }

    public function deleteImageReview($id) {
        $image = ReviewImage::findOrFail($id);
        $image->deleteFiles();
        $image->delete();

        return response()->json(['status' => 'OK']);
    }

    public function listingData($id) {

        $property = Property::findOrFail($id);

        $listing = $property->getListingData();

        $lang = \App::getLocale();

        $condos = [];

        if ($property->ptype == 'cd') {
            $collate = Property::getLangCollation();
            $condos = Property::where('ptype', 'cdb')->where('new_project', false)->orderByRaw("name->>'$lang' ASC")
                            ->get()->all();
        }

        return view('desktop.manager.listing.form'
                , ['listing' => $listing, 'property' => $property, 'condos' => $condos]);
    }

    public function updateListingData(Request $request) {
        $options = $request->all();

        $property = Property::findOrFail($options['id']);
        $property->updateDate('publication_date', $options);
        $property->save();

        DB::table('property_belongs_to_building')->where('property_id', $options['id'])->delete();
        DB::table('property_belongs_to_building')->insert([
            'property_id' => $options['id'],
            'building_id' => $request->input('building_id')
        ]);

        $listing = $property->getListingData();
        $listing->updateDate('expiration_date', $options);
        $listing->listing_class = array_get($options, 'listing_class');
        $listing->save();

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function suggestCondo(Request $request) {

        $service = new CondoSuggestion($request);
        $result = $service->find();

        return response()->json($result);
    }

    public function favoriteProperty(Request $request, $id) {

        $user = Auth::user();

        if (is_object($user)) {
            $result = $user->toggleFavoriteProperty($id);
            return response()->json([
                        'status' => 'OK'
                        //, 'favorites' => $user->getFavoriteProperties()
                        , 'result' => $result
            ]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }

    public function getMapInfo($id) {

        $data = DB::table('properties')
                ->select('properties.id'
                        , 'property_listing_data.listing_price'
                        , 'properties.name'
                        , 'property_extended_data.bedroom_num'
                        , 'property_extended_data.bathroom_num'
                        , 'property_extended_data.total_size')
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->leftJoin('property_extended_data', 'property_extended_data.property_id', 'properties.id')
                ->where('properties.id', $id)
                ->first();

        $image = DB::table('property_have_images')->where('property_id', $id)
                ->value('filepath');

        if ($image) {
            $image = url('/thumb/100x100/' . $image);
        } else {
            $image = '/img/default-property.png';
        }

        $names = json_decode($data->name, true);

        $name = trget($names);

        $data->name = $name;

        return view('desktop.visitor.property.map-info', ['data' => $data, 'image' => $image]);
    }

    public function toggle($id) {

        $property = Property::find($id);
        $listing = $property->getListingData();

        $property->published = !$property->published;
        $property->publication_date = date('Y-m-d H:i:s');
        $property->save();

        if ($listing->listing_class == 'standard') {
            $expiration = time() + (3600 * 30 * 24);
            $listing->expiration_date = date('Y-m-d', $expiration);
            $listing->save();
        } else {
            $listing->expiration_date = $listing->upgrade_end;
            $listing->save();
        }
    }

    public function deleteForm($id) {

        $property = Property::findOrFail($id);

        return view('desktop.manager.listing.delete-form', ['property' => $property]);
    }

    public function deleteProperty(Request $request) {
        $id = $request->input('id');

        DB::table('property_belongs_to_building')->where('property_id', $id)->delete();
        DB::table('property_extended_data')->where('property_id', $id)->delete();
        DB::table('property_have_facilities')->where('property_id', $id)->delete();
        DB::table('property_have_features')->where('property_id', $id)->delete();
        DB::table('property_have_images')->where('property_id', $id)->delete();
        DB::table('property_listing_data')->where('property_id', $id)->delete();
        DB::table('property_nearby_transportations')->where('property_id', $id)->delete();
        DB::table('favorite_properties')->where('property_id', $id)->delete();
        DB::table('customer_reviews')->where('property_id', $id)->delete();
        DB::table('manager_property_reviews')->where('property_id', $id)->delete();

        $property = Property::findOrFail($id);
        $property->forceDelete();

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

}
