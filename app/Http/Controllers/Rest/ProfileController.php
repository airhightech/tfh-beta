<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use App\Helpers\ImageUploader;
use Auth;

class ProfileController extends BaseController {
    
    public function update(Request $request) {
        
        $options[$request->input('name')] = $request->input('after');
        
        $user = Auth::user();
        $profile = $user->getProfile();
        $profile->updateFromOptions($options);
        $profile->save();
        
        $personal = $user->getPersonalData();
        $personal->updateFromOptions($options);
        $personal->save();
        
        $company = $user->getCompanyData();
        $company->updateFromOptions($options);
        $company->save();        
    } 
    
    public function uploadCover(Request $request) {
        
        $user = Auth::user();
        $profile = $user->getProfile();        
        
        if ($request->hasFile('image')) {
            $uploader = new ImageUploader($request);
            $filepath = $uploader->saveSingle('image');
            $profile->cover_picture_path = $filepath;            
        }
        
        $profile->save();
        
        return response()->json(['status' => 'OK'
            , 'image' => $profile->getCoverPicture()]);
    }
    
    public function uploadPhoto(Request $request) {
        
        $user = Auth::user();
        $profile = $user->getProfile();
        
        if ($request->hasFile('image')) {
            $uploader = new ImageUploader($request);
            $filepath = $uploader->saveSingle('image');
            $profile->profile_picture_path = $filepath;            
        }
        
        $profile->save();
        
        return response()->json(['status' => 'OK'
            , 'image' => $profile->getProfilePicture()]);
    }
}