<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use App\Models\Reviews\UserReview;
use App\Models\Reviews\AgentReview;
use Auth;
use DB;
use Illuminate\Database\QueryException;

class ReviewController extends BaseController {
    
    public function postUserReview(Request $request) {
        
        if (Auth::check()) {
            $review = new UserReview;
            $review->updateFromRequest($request);
            $review->save();
            
            return response()->json(['status' => 'OK', 'reload' => true]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }  
    }
    
    public function postUserLike(Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            
            $now = date('Y-m-d H:i:s');
            
            $review_id = $request->input('review');
            
            $liked = $request->input('liked') == 'true' ? true : false;
            
            try {
                DB::table('customer_review_likes')->insert([
                    'review_id' => $review_id,
                    'user_id' => $user->id,
                    'liked' => $liked,
                    'created_at' => $now,
                    'updated_at' => $now,
                ]);
            } catch (QueryException $ex) {
                DB::table('customer_review_likes')->where('review_id', $review_id)->where('user_id', $user->id)->update([
                     'liked' => $liked,
                    'updated_at' => $now,
                ]);
            }
            
            return response()->json(['status' => 'OK', 'likes' => DB::table('customer_review_likes')->where('review_id', $review_id)->where('liked', true)->count(),
                'dislikes' => DB::table('customer_review_likes')->where('review_id', $review_id)->where('liked', false)->count()]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }
    
    public function postAgentReview(Request $request) {
        
        if (Auth::check()) {
            
            $review = new AgentReview;
            $review->updateFromRequest($request);
            $review->save();
            
            $agent_id = $request->input('agent_id');
            
            return response()->json(['status' => 'OK', 'next' => '/agent/' . $agent_id . '?tab=review']);
        } else {
            return response()->json(['status' => 'ERROR']);
        }  
        
    }
    
}