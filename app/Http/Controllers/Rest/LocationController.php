<?php

namespace App\Http\Controllers\Rest;

use App\Models\Property;
use App\Models\Geo\Province;
use App\Models\Geo\District;
use App\Models\Geo\Area;
use Illuminate\Http\Request;
use DB;

class LocationController extends BaseController {

    public function getDistrict($id) {
        
        $lang = \App::getLocale();
        $collate = District::getLangCollation();
        $list = District::where('province_id', $id)->orderByRaw("name->>'$lang' ASC")->get();

        $data = [];
        foreach ($list as $district) {
            $data[] = [
                'id' => $district->id,
                'name' => $district->getTranslatable('name'),
                'zoom' => $district->zoom_level,
                'lat' => $district->lat(),
                'lng' => $district->lng()
            ];
        }

        return response()->json($data);
    }

    public function getArea($id) {
        
        $lang = \App::getLocale();
        $collate = Area::getLangCollation();
        $list = Area::where('district_id', $id)->orderByRaw("name->>'$lang' ASC")->get();

        $data = [];
        foreach ($list as $area) {
            $data[] = [
                'id' => $area->id,
                'name' => $area->getTranslatable('name'),
                'zoom' => $area->zoom_level,
                'lat' => $area->lat(),
                'lng' => $area->lng()
            ];
        }

        return response()->json($data);
    }

    public function createProvince(Request $request) {
        $province = new Province;
        $province->createFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateProvince(Request $request) {
        $id = $request->input('id');
        $province = Province::find($id);
        $province->updateFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function createDistrict(Request $request) {
        $district = new District;
        $district->createFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateDistrict(Request $request) {
        $id = $request->input('id');
        $district = District::find($id);
        $district->updateFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function createArea(Request $request) {
        $area = new Area;
        $area->createFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function updateArea(Request $request) {
        $id = $request->input('id');
        $area = Area::find($id);
        $area->updateFromRequest($request);
        return response()->json(['status' => 'OK', 'reload' => true]);
    }
    
    public function sync($id) {
        $property = Property::find($id);
        $address = $property->getAddress();
        return response()->json(['status' => 'OK', 'address' => $address]);
    }
    
    public function updateMap(Request $request, $id) {
        
        $domain = $request->input('domain');
        $center = $request->input('center');
        
        list($lat, $lng) = explode(',', $center);
        
        switch ($domain) {
            case 'province':
                $province = Province::find($id);
                $province->zoom_level = $request->input('zoom');
                $province->map_center = DB::raw("ST_GeomFromText('POINT($lng $lat 0.0)', 4326)");
                $province->save();
                break;  
            
            case 'district':
                $district = District::find($id);
                $district->zoom_level = $request->input('zoom');
                $district->map_center = DB::raw("ST_GeomFromText('POINT($lng $lat 0.0)', 4326)");
                $district->save();
                break;  
            
            case 'area':
                $area = Area::find($id);
                $area->zoom_level = $request->input('zoom');
                $area->map_center = DB::raw("ST_GeomFromText('POINT($lng $lat 0.0)', 4326)");
                $area->save();
                break;
            
            default:
                break;
        }
        
        return response()->json(['status' => 'OK']);
    }

}
