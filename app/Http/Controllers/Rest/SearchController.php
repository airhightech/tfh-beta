<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use App\Services\SearchSuggestion;
use App\Services\SearchProperty;
use App\Models\Property;
use App\Models\Geo\Place;
use Auth;
use DB;

class SearchController extends BaseController {

    public function getSuggestion(Request $request) {

        $service = new SearchSuggestion($request);
        $suggestions = $service->find();
        return response()->json($suggestions);
    }

    public function searchMap(Request $request) {

        $id = $request->input('id');

        $finder = new SearchProperty($request);

        if ($id > 0) {
            $finder->includePropertiesById($id);
        }

        $results = $finder->findForMap();

        $place = null;
        $type = null;

        if ($id > 0) {

            $lntlat = null;

            $rel = $request->input('rel');

            $supported = Place::getSupported();

            if (in_array($rel, $supported)) {
                $place = Place::find($id);
                if ($place) {
                    $type = $place->type;
                    $lntlat = $place->getLngLat();
                }
            } else {
                $property = Property::find($id);
                if ($property){
                  $address = $property->getAddress();
                  $lntlat = $address->getLngLat();
                }
            }

            if ($lntlat) {
                $place = [
                    'id' => $id,
                    'lng' => $lntlat->lng,
                    'lat' => $lntlat->lat,
                    'type' => $type
                ];
            }
        }

        return response()->json([
                    'status' => 'OK'
                    , 'properties' => $results, 'data' => json_encode($place, true)]
                        , 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public function searchList(Request $request) {

        $finder = new SearchProperty($request);
        $results = $finder->findForListing();

        $favorites = [];

        if (Auth::check()) {
            $user = Auth::user();
            $favorites = DB::table('favorite_properties')
                    ->where('user_id', $user->id)
                    ->pluck('property_id');
        }

        return response()->json([
                    'status' => 'OK'
                    , 'properties' => $results, 'favorites' => $favorites]
                        , 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public function getSaved() {
        $saved = [];

        if (Auth::check()) {

            $user = Auth::user();

            $saved = DB::table('saved_searches')
                    ->where('user_id', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->limit(20);
        }

        return $saved;
    }

    public function addSaved(Request $request) {

        if (Auth::check()) {
            $user = Auth::user();

            $url = parse_url($request->input('url'), PHP_URL_QUERY);

            $data = [];

            parse_str($url, $data);

            DB::table('saved_searches')->insert([
                'user_id' => $user->id,
                'search_query' => json_encode($data),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

}
