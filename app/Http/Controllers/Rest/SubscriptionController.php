<?php

namespace App\Http\Controllers\Rest;

use DB;
use App\Http\Requests\Subscription\Create as CreateSubscription;
use Illuminate\Http\Request;
use App\Models\Messages\NewsletterSubscription;

class SubscriptionController extends BaseController {

    public function register(CreateSubscription $request) {

        DB::table('newsletter_subscriptions')->insert([
            'email' => $request->input('email'),
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()')
        ]);

        return response()->json([
                    'status' => 'OK'
                    , 'message' => tr('javascript.subscription-ok', 'Your subscription was created successfully')
        ]);
    }

    public function unsubscribe(Request $request) {
            $newsletter = NewsletterSubscription::where('email', $request->input('email'))->firstOrFail();
            $newsletter->delete();
            return response('Your email address has been unsubscribed', 200)
                ->header('Content-Type', 'text/plain');
    }

}
