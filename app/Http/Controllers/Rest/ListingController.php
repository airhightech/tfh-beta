<?php

namespace App\Http\Controllers\Rest;

//use App\Http\Requests\Property\Create as CreatePropertyRequest;
use App\Models\Property;
use App\Models\Property\ListingData;
use App\Models\Property\ExtendedData;
use App\Models\Property\Address;
use Illuminate\Http\Request;
use App\Models\Customer\Purchase;
//use App\Models\Reviews\ManagerReview;
//use App\Models\Property\Image as PropertyImage;
//use App\Models\Reviews\Image as ReviewImage;
//use App\Services\CondoSuggestion;
use DB;
use Auth;

class ListingController extends BaseController {

    public function postCreate(Request $request) {

        $property_id = $request->input('property_id');

        $building = null;

        $user = Auth::user();

        if ($property_id > 0) {
            $building = Property::find($property_id);
        }

        $lang = \App::getLocale();
        $names[$lang] = $request->input('property_name');

        if (!isset($names['th'])) {
            $names['th'] = $request->input('property_name');
        }

        $ptype = $request->input('property_type');

        $property = new Property;
        $property->publisher_id = $user->id;
        $property->new_project = false;
        $property->user_submited = true;
        $property->published = true;
        $property->publication_date = date('Y-m-d H:i:s');
        $property->ptype = $ptype;
        $property->setTranslatable('name', $names);


        if ($building) {
            $property->address_id = $building->address_id;
        } else {
            $address = new Address;
            $address->updateFromRequest($request);

            $property->address_id = $address->id;

            // Save new condo

            if ($ptype == Property::TYPE_CONDO_ROOM) {
                $building = new Property;
                $building->setTranslatable('name', $names);
                $building->address_id = $address->id;
                $building->publisher_id = $user->id;
                $building->new_project = false;
                $building->published = false;
                $building->user_submited = true;
                $building->ptype = Property::TYPE_CONDO_BUILDING;
                $building->save();
            }
        }

        $property->save();

        if ($building) {
            DB::table('property_belongs_to_building')->insert([
                'building_id' => $building->id,
                'property_id' => $property->id
            ]);
        }

        // Create listing
        $expiration = time() + (3600 * 30 * 24);

        $listing = new ListingData;
        $listing->property_id = $property->id;
        $listing->listing_class = 'standard';
        $listing->ltype = $request->input('listing_type');
        $listing->expiration_date = date('Y-m-d', $expiration);
        $listing->generateTitle();
        $listing->save();

        // Create extended data

        $custom_address = array_filter([
            $request->input('street_number'),
            $request->input('street_name')
        ]);

        $extended = new ExtendedData;
        $extended->property_id = $property->id;
        $extended->setTranslatable('custom_address', [$lang => implode(', ', $custom_address)]);
        $extended->save();

        return response()->json(['status' => 'OK', 'next' => '/member/listing/details/' . $property->id]);
    }

    public function postDetails(Request $request) {

        $id = $request->input('id');

        $property = Property::find($id);
        $listing = $property->getListingData();
        $extended = $property->getExtendedData();
        $building = $property->getBuildingData();

        $property->updateFromRequest($request);
        $extended->updateFromRequest($request);
        $listing->updateFromRequest($request);
        $building->updateFromRequest($request);

        $property->save();

        $features = $request->input('features');

        DB::delete('DELETE FROM property_have_features WHERE property_id = ?', [$id]);

        if (count($features)) {

            $data = [];
            foreach ($features as $value) {
                $data[] = [
                    'property_id' => $id,
                    'feature_id' => $value
                ];
            }
            DB::table('property_have_features')->insert($data);
        }

        $facilities = $request->input('facilities');

        DB::delete('DELETE FROM property_have_facilities WHERE property_id = ?', [$id]);

        if (count($facilities)) {
            $data = [];
            foreach ($facilities as $value) {
                $data[] = [
                    'property_id' => $id,
                    'facility_id' => $value
                ];
            }
            DB::table('property_have_facilities')->insert($data);
        }

        // Save BTS, MRT, APL

        DB::delete('DELETE FROM property_nearby_transportations WHERE property_id = ?', [$id]);

        $data = [];

        $nearby_bts = $request->input('nearby_bts', []);

        foreach ($nearby_bts as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'bts'
            ];
        }

        $nearby_mrt = $request->input('nearby_mrt', []);

        foreach ($nearby_mrt as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'mrt'
            ];
        }

        $nearby_apl = $request->input('nearby_apl', []);

        foreach ($nearby_apl as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'apl'
            ];
        }

        DB::table('property_nearby_transportations')->insert($data);

        return response()->json(['status' => 'OK', 'next' => '/member/listing']);
    }

    public function upgradeExclusive(Request $request) {

        $purchase_id = $request->input('purchase_id');

        $purchase = Purchase::findOrFail($purchase_id);
        $property = Property::findOrFail($request->input('property_id'));

        $data = $purchase->getData();
        $package = $data->getPackage();

        if ($package->type == 'exclusive') {
            if ($data->listing_count > $purchase->used_listing) {
                $purchase->used_listing = $purchase->used_listing + 1;
                $purchase->save();

                $listing = $property->getListingData();
                $listing->updateDate('upgrade_start', $request->all());

                $start = strtotime($listing->upgrade_start) + ($data->duration * 3600 * 24);

                $listing->upgrade_end = date('Y-m-d', $start);
                $listing->listing_class = 'exclusive';
                $listing->save();
            }
        }

        return response()->json(['status' => 'OK', 'reload' => true]);
    }

    public function upgradeFeatured(Request $request) {

        $purchase_id = $request->input('purchase_id');

        $purchase = Purchase::findOrFail($purchase_id);
        $property = Property::findOrFail($request->input('property_id'));
        $listing = $property->getListingData();

        $data = $purchase->getData();
        $package = $data->getPackage();

        if ($package->type == 'featured') {
            if ($data->listing_count > $purchase->used_listing) {
                $purchase->used_listing = $purchase->used_listing + 1;
                $purchase->save();

                $listing->updateDate('upgrade_start', $request->all());

                $start = strtotime($listing->upgrade_start) + ($data->duration * 3600 * 24);

                $listing->upgrade_end = date('Y-m-d', $start);
                $listing->listing_class = 'featured';
                $listing->save();
            }
        } else if ($package->type == 'exclusive') {
            if ($data->free_listing_count > $purchase->used_free_listing) {
                $purchase->used_free_listing = $purchase->used_free_listing + 1;
                $purchase->save();

                $listing->updateDate('upgrade_start', $request->all());

                $start = strtotime($listing->upgrade_start) + ($data->duration * 3600 * 24);

                $listing->upgrade_end = date('Y-m-d', $start);
                $listing->listing_class = 'featured';
                $listing->save();
            }
        }

        $property->syncPublishingDate();

        return response()->json(['status' => 'OK'
                    , 'modal' => '#upgrade-modal-success'
                    , 'content' => view('desktop.member.purchase.upgraded', ['property' => $property])->render()
        ]);
    }

}
