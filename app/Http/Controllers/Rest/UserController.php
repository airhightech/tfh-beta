<?php

namespace App\Http\Controllers\Rest;

use DB;
use Auth;
use App\Models\User;
use App\Models\Property\FavoriteProperty;

class UserController extends BaseController {

    public function getSavedSearch() {

        $searches = [];

        if (Auth::check()) {

            $user = Auth::user();

            $list = DB::table('saved_searches')
                    ->select('search_query', 'created_at')
                    ->where('user_id', $user->id)
                    ->orderby('created_at', 'desc')
                    ->limit(100)
                    ->get();

            foreach ($list as $search) {

                $label = [];

                $data = json_decode($search->search_query, true);
                $date = '<span class="date">' . date('d M', strtotime($search->created_at)) . '</span>';

                $label[] = trim(array_get($data, 'q'));


                $lt = array_filter(explode(',', array_get($data, 'lt')));

                $label = array_filter($label);

                if (count($lt)) {
                    $lt = array_map('ucfirst', $lt);
                    $label[] = implode(', ', $lt);
                }

                $searches[] = [
                    'label' => implode(', ', $label) . ' ' . $date,
                    'url' => http_build_query($data)
                ];
            }
        }

        return response()->json([
                    'status' => 'OK',
                    'searches' => $searches
        ]);
    }

    public function deleteUser($id) {

        $user = User::find($id);
        $user->deleteAllData();
        $user->deleteProperties();
        $user->forceDelete();

        return response()->json(['status' => 'OK']);
    }
}
