<?php

namespace App\Http\Controllers\Desktop\Visitor;

use App\Models\Property;
use App\Models\Property\Facility;
use App\Models\Property\Feature;
use Illuminate\Http\Request;
use App\Models\Geo\Place;
use App\Services\SimilarProperty;
use App\Models\Reviews\UserReview;
use App\Services\SearchProperty;
use Auth;
use DB;

class PropertyController extends BaseController {

    public function search(Request $request) {

        $id = $request->input('id');

        $place = null;

        $all = $request->all();

        $pts = explode(',', $request->input('pt', ''));
        $np = $request->input('np');
        $type = null;

        if ($id > 0) {

            $lntlat = null;

            $rel = $request->input('rel');

            $supported = Place::getSupported();

            if (in_array($rel, $supported)) {
                $place = Place::find($id);
                if ($place) {
                    $lntlat = $place->getLngLat();
                }
                $all['domain'] = 'place';
                $type = $place->type;
            } else {
                $property = Property::find($id);

                $all['domain'] = 'property';

                if ($property) {
                    $address = $property->getAddress();

                    if ($address) {
                        $lntlat = $address->getLngLat();
                    }

                    // Update search filters

                    /*$listing = $property->getListingData();

                    $all['lt'] = $listing->ltype;

                    if ($property->new_project) {
                        $all['np'] = 'yes';
                    } else {
                        $all['np'] = '';
                    }*/
                }
            }

            if ($lntlat) {
                $place = [
                    'id' => $id,
                    'lng' => $lntlat->lng,
                    'lat' => $lntlat->lat,
                    'type' => $type
                ];
            }
        }

        /*if (in_array('cdb', $pts) || $np == 'yes') {
            $all['lt'] = '';
        }*/

        return view('desktop.visitor.search.search'
                , ['all' => $all, 'place' => $place]);
    }
    
    public function searchList(Request $request) {
        $finder = new SearchProperty($request);
        $results = $finder->findForListing();

        $favorites = [];

        if (Auth::check()) {
            $user = Auth::user();
            $favorites = DB::table('favorite_properties')
                    ->where('user_id', $user->id)
                    ->pluck('property_id')->all();
        }
        
        $others = [];
        
        if (count($results) == 0) {
            $q = $request->input('q');
        }
        
        return view('desktop.visitor.search.search-list'
                , ['request' => $request, 'properties' => $results, 'favorites' => $favorites]);
    }

    public function details($id) {

        $property = Property::findOrFail($id);
        $property->clicks = $property->clicks + 1;
        $property->save();

        $view = '';

        if ($property->new_project) {

            switch ($property->ptype) {
                case Property::TYPE_CONDO_BUILDING:
                case Property::TYPE_CONDO_ROOM:
                    $view = 'project.condo';
                    break;
                case Property::TYPE_APARTMENT_ROOM:
                case Property::TYPE_APARTMENT_BUILDING:
                    $view = 'project.apartment';
                    break;
                case Property::TYPE_SINGLE_HOUSE:
                    $view = 'project.house';
                    break;
                case Property::TYPE_TOWNHOUSE:
                    $view = 'project.townhouse';
                    break;
                default:
                    $view = 'default';
                    break;
            }
        } else {
            switch ($property->ptype) {
                case Property::TYPE_CONDO_BUILDING:
                    $view = 'details.condo-com';
                    break;
                case Property::TYPE_CONDO_ROOM:
                    $view = 'details.condo';
                    break;
                case Property::TYPE_APARTMENT_ROOM:
                case Property::TYPE_APARTMENT_BUILDING:
                    $view = 'details.apartment';
                    break;
                case Property::TYPE_SINGLE_HOUSE:
                    $view = 'details.house';
                    break;
                case Property::TYPE_TOWNHOUSE:
                    $view = 'details.townhouse';
                    break;
                default:
                    $view = 'default';
                    break;
            }
        }

        $selected_facilities = $property->getFacilitiesID();
        $selected_features = $property->getFeaturesID();

        $facilities = Facility::all();
        $features = Feature::where('optional', false)->get();
        $optional_features = Feature::where('optional', true)->get();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');

        $bts_list = Place::where('type', 'bts')->whereIn('id', $selected_bts_list)->get()->all();
        $mrt_list = Place::where('type', 'mrt')->whereIn('id', $selected_mrt_list)->get()->all();
        $apl_list = Place::where('type', 'apl')->whereIn('id', $selected_apl_list)->get()->all();

        $service = new SimilarProperty;

        $related = $service->findSimilar($property, 8);

        //var_dump($related);

        $reviews = UserReview::where('property_id', $id)->orderBy('created_at', 'desc')->paginate(3);

        return view('desktop.visitor.property.' . $view, [
            'property' => $property
            , 'selected_facilities' => $selected_facilities
            , 'selected_features' => $selected_features
            , 'facilities' => $facilities
            , 'features' => $features
            , 'optional_features' => $optional_features
            , 'related' => $related
            , 'bts_list' => $bts_list
            , 'mrt_list' => $mrt_list
            , 'apl_list' => $apl_list
            , 'reviews' => $reviews
        ]);
    }

    public function preview($id) {

        $property = Property::findOrFail($id);

        $view = '';

        if ($property->new_project) {

            switch ($property->ptype) {
                case Property::TYPE_CONDO_BUILDING:
                    $view = 'project.condo-com';
                    break;
                case Property::TYPE_CONDO_ROOM:
                    $view = 'project.condo';
                    break;
                case Property::TYPE_APARTMENT_ROOM:
                case Property::TYPE_APARTMENT_BUILDING:
                    $view = 'project.apartment';
                    break;
                case Property::TYPE_SINGLE_HOUSE:
                    $view = 'project.house';
                    break;
                case Property::TYPE_TOWNHOUSE:
                    $view = 'project.townhouse';
                    break;
                default:
                    $view = 'default';
                    break;
            }
        } else {
            switch ($property->ptype) {
                case Property::TYPE_CONDO_BUILDING:
                    $view = 'condo-com';
                    break;
                case Property::TYPE_CONDO_ROOM:
                    $view = 'condo';
                    break;
                case Property::TYPE_APARTMENT_ROOM:
                case Property::TYPE_APARTMENT_BUILDING:
                    $view = 'apartment';
                    break;
                case Property::TYPE_SINGLE_HOUSE:
                    $view = 'house';
                    break;
                case Property::TYPE_TOWNHOUSE:
                    $view = 'townhouse';
                    break;
                default:
                    $view = 'old.default';
                    break;
            }
        }

        /* if ($property->new_project) {
          $view = 'project';
          } else {
          switch ($property->ptype) {
          case Property::TYPE_CONDO_BUILDING:
          $view = 'condo-com';
          break;
          default:
          $view = 'default';
          break;
          }
          } */

        $selected_facilities = $property->getFacilitiesID();
        $selected_features = $property->getFeaturesID();

        $facilities = Facility::all();
        $features = Feature::where('optional', false)->get();
        $optional_features = Feature::where('optional', true)->get();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');

        $bts_list = Place::where('type', 'bts')->whereIn('id', $selected_bts_list)->get()->all();
        $mrt_list = Place::where('type', 'mrt')->whereIn('id', $selected_mrt_list)->get()->all();
        $apl_list = Place::where('type', 'apl')->whereIn('id', $selected_apl_list)->get()->all();

        $related = [];

        $reviews = UserReview::where('property_id', $id)->orderBy('created_at', 'desc')->paginate(3);

        return view('desktop.visitor.property.preview.' . $view, [
            'property' => $property
            , 'selected_facilities' => $selected_facilities
            , 'selected_features' => $selected_features
            , 'facilities' => $facilities
            , 'features' => $features
            , 'optional_features' => $optional_features
            , 'related' => $related
            , 'bts_list' => $bts_list
            , 'mrt_list' => $mrt_list
            , 'apl_list' => $apl_list
            , 'reviews' => $reviews
        ]);
    }

}
