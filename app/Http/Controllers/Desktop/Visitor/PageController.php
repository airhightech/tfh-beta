<?php

namespace App\Http\Controllers\Desktop\Visitor;

use App\Models\System\StaticPage;

class PageController extends BaseController {
    
    public function index($sku) {
        
        $page = StaticPage::where('sku', $sku)->firstOrFail();
        
        return view('desktop.visitor.page.index', [
            'page' => $page
        ]);
        
    }
    
}