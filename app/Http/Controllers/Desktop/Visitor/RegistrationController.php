<?php

namespace App\Http\Controllers\Desktop\Visitor;

use Auth;

class RegistrationController extends BaseController {
    
    public function type() {
        return view('desktop.visitor.register.type');
    }
    
    public function owner() {
        
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->ctype == 'visitor') {
                $user->ctype = 'owner';
                $user->save();
            }
            return redirect('/member/listing/select-type');
        }
        
        return view('desktop.visitor.register.owner');
    }
    
    public function agent() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->ctype == 'visitor') {
                $user->ctype = 'agent';
                $user->save();
            }
            return redirect('/member/listing/select-type');
        }
        return view('desktop.visitor.register.agent');
    }
    
    public function company() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->ctype == 'visitor') {
                $user->ctype = 'company';
                $user->save();
            }
            return redirect('/member/listing/select-type');
        }
        return view('desktop.visitor.register.company');
    }
    
    public function developer() {
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->ctype == 'visitor') {
                $user->ctype = 'developer';
                $user->save();
            }
            return redirect('/member/listing/select-type');
        }
        return view('desktop.visitor.register.developer');
    }    
}