<?php

namespace App\Http\Controllers\Desktop\Visitor;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Reviews\AgentReview;
use Auth;
use DB;

class AgentController extends BaseController {

    public function search(Request $request) {

        $search = $request->all();

        $query = User::leftJoin('publisher_company_profiles', 'publisher_company_profiles.user_id', 'users.id');
        $query->leftJoin('personal_profiles', 'personal_profiles.user_id', 'users.id');

        $typ = strtolower($request->input('typ', 'all'));

        // Type

        if ($typ == 'agent') {
            $query->where('users.ctype', 'agent');
        } else if ($typ == 'company') {
            $query->where('users.ctype', 'company');
        } else {
            $query->whereIn('users.ctype', ['agent', 'company']);
        }
        // Name

        $name = trim(strtolower($request->input('nam', '')));

        if (!empty($name)) {
            $query->where(function ($query) use ($name) {
                $query->orWhere('publisher_company_profiles.company_name', '~*', $name);
                $query->orWhere('personal_profiles.firstname', '~*', $name);
                $query->orWhere('personal_profiles.lastname', '~*', $name);
            });
        }



        // Location

        /* $location = strtolower($request->input('loc', ''));
          $location = trim($location);

          if (!empty($location)) {
          $query->whereRaw('user_profiles.address ~* ?');
          $bindings[] = $location;
          } */

        // Verified agent only

        $query->where('users.verified', true);


        $query->where('users.active', true);

        // Order

        $query->orderBy('users.listing_level', 'ASC');

        $agents = $query->paginate(9);

        return view('desktop.visitor.agent.search', ['search' => $search, 'agents' => $agents]);
    }

    public function details(Request $request, $id) {

        $user = User::where('id', $id)->where('verified', true)->firstOrfail();

        $tab = $request->input('tab');

        $reviews = AgentReview::where('agent_id', $id)
                ->orderBy('created_at', 'desc')
                ->paginate(5);

        $favorites = [];

        if (Auth::check()) {
            $current_user = Auth::user();
            $favorites = DB::table('favorite_properties')
                    ->where('user_id', $current_user->id)
                    ->pluck('property_id')->all();
        }
        
        $profile = $user->getProfile();
        $profile->total_visitors = $profile->total_visitors + 1;
        $profile->save();

        if ($user->ctype == 'agent' || $user->ctype == 'owner') {
            return view('desktop.visitor.agent.details.agent', ['user' => $user, 'tab' => $tab, 'reviews' => $reviews, 'favorites' => $favorites]);
        } else if ($user->ctype == 'company') {
            return view('desktop.visitor.agent.details.company', ['user' => $user, 'tab' => $tab, 'reviews' => $reviews, 'favorites' => $favorites]);
        } else if ($user->ctype == 'developer') {
            return view('desktop.visitor.agent.details.developer', ['user' => $user, 'tab' => $tab, 'reviews' => $reviews, 'favorites' => $favorites]);
        }
    }

}
