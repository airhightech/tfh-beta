<?php

namespace App\Http\Controllers\Desktop\Visitor;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Common\SocialLoginTrait;
use Auth;
use App\Models\User;
use App\Emails\EmailValidation;
use Mail;
use Cookie;

class AuthController extends BaseController {

    use SocialLoginTrait;

    public function login(Request $request) {
        return view('desktop.visitor.auth.login', ['request' => $request]);
    }

    public function checkInbox(Request $request) {

        $user_id = $request->input('user');
        $reason = $request->input('reason');

        $data = ['reason' => $reason];

        if ($user_id > 0) {
            $data['user'] = User::findOrFail($user_id);
        }

        return view('desktop.visitor.auth.check-inbox', $data);
    }

    public function validateEmail(Request $request) {

        $id = $request->input('id');
        $token = $request->input('token');
        $user = User::where('email_validation_token', $token)
                ->where('id', $id)
                ->firstOrFail();
        $user->email_validated = true;
        $user->email_validation_token = '';
        $user->active = true;
        $user->save();

        return view('desktop.visitor.auth.email-verified', ['user' => $user]);
    }

    public function requestEmail(Request $request) {

        try {
            $user = User::where('email', $request->input('email'))->firstOrFail();
            Mail::to($user)->queue(new EmailValidation($user));
        } catch (ModelNotFoundException $ex) {
            
        }

        return redirect('/auth/check-inbox?user=' . $user->id);
    }

    public function forgotPassword() {
        return view('desktop.visitor.auth.forgot-password');
    }

    public function updatePassword(Request $request) {
        return view('desktop.visitor.auth.update-password', [
            'password_reset_token' => $request->input('token'),
            'id' => $request->input('id')
        ]);
    }
    
    public function createRequestPassword(Request $request)
    {
        $token = str_random(20);

        $user = User::where('email', $request->input('email'))->first();
        $user->password_reset_token = $token;
        $user->save();

        $email = new ResetPasswordLink;
        $email->send($user, $token);

        return redirect('/auth/check-inbox?reason=password-link');
    }
    
    public function postUpdatePassword(Request $request)
    {
        $user = User::find($request->input('id'));

        if ($user->password_reset_token == $request->input('token')) {
            $user->password = Hash::make($request->input('password'));
            $user->password_reset_token = null;
            $user->save();
        }
        return redirect('/auth/updated');
    }
    
    public function getUpdated() {
        return view('desktop.visitor.auth.updated');
    }

    public function logout() {
        
        Auth::logout();
        return redirect('/')->withCookie(Cookie::forget('laravel_session'));
    }

    public function register() {

        return view('desktop.visitor.auth.register');
    }

}
