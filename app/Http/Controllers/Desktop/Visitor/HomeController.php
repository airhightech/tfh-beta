<?php

namespace App\Http\Controllers\Desktop\Visitor;

use App\Models\Listings\ExclusiveProject;
use App\Models\Listings\ExclusiveListing;
use App\Models\Messages\Popup;

class HomeController extends BaseController {

    public function index() {

        $exclusive = ExclusiveProject::get();
        $featured_rent = ExclusiveListing::get('rent');
        $featured_sale = ExclusiveListing::get('sale');

        $start = date('Y-m-d 00:00:00');
        $end = date('Y-m-d 23:59:59');

        $popup = Popup::where('start_time', '<=', $start)
                    ->where('end_time', '>=', $end)
                    ->inRandomOrder()
                    ->first();

        return view('desktop.visitor.home.index', [
            'exclusive' => $exclusive,
            'featured_rent' => $featured_rent,
            'featured_sale' => $featured_sale,
            'popup' => $popup
        ]);
    }

}
