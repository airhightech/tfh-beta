<?php

namespace App\Http\Controllers\Desktop\Manager;

use Illuminate\Http\Request;
use App\Models\Property;

class ProjectController extends BaseController {
    
    use PropertyTrait;
    use Projects\CondoTrait;
    use Projects\ApartmentTrait;   
    use Projects\HouseTrait;
    use Projects\TownhouseTrait;  
    
    public function all(Request $request) {
        
        $filters = $this->getListFilters($request);
        
        $type = $request->input('pt', 'all');
        $status = $request->input('status', 'all');
        $class = $request->input('class', 'all');

        $query = Property::where('properties.new_project', true)
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->whereIn('properties.ptype', ['ap', 'cdb', 'sh', 'th']);

        if ($type && $type != 'all') {
            $query->where('properties.ptype', $type);
        }
        
        if ($status == 'present') {
            $query->where('properties.published', true);
        } else if ($status == 'expired') {
            $query->where('properties.published', false);
        }
        
        if ($class && $class != 'all') {
            $query->where('property_listing_data.listing_class', $class);
        }

        $query->orderBy('properties.created_at', 'desc');

        $properties = $query->paginate(10);
        
        return view('desktop.manager.project.all', [
            'request' => $request,
            'properties' => $properties,
            'filters' => $filters
        ]);
        
    }
    
    protected function getListFilters(Request $request) {
        return [
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all'),
            'pt' => $request->input('pt', 'all')
        ];
    }
    
}