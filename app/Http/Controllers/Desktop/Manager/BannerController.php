<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Customer\Banner;
use App\Http\Requests\Banner\Create as BannerCreate;
use App\Helpers\ImageUploader;
use Illuminate\Http\Request;
use DB;

class BannerController extends BaseController {

    public function index() {

        $banners = Banner::orderBy('created_at', 'desc')->paginate(20);

        return view('desktop.manager.banner.index', ['active' => 'banner', 'banners' => $banners]);
    }

    public function getCreate() {

        $locations = Banner::getLocations();

        return view('desktop.manager.banner.create'
                , ['locations' => $locations]);
    }

    public function postCreate(BannerCreate $request) {

        $options = $request->all();

        $banner = new Banner;
        $banner->name = $request->input('name');
        $banner->customer = $request->input('customer');
        $banner->related_link = $request->input('related_link');
        $banner->location = $request->input('location');
        $banner->print = 0;
        $banner->updateDate('date_start', $options);
        $banner->updateDate('date_end', $options);

        $banner->payment_status = $request->input('status');
        $banner->save();

        return redirect('/manager/banner/image/' . $banner->id);
    }

    public function getInfo($id) {

        $banner = Banner::find($id);

        $locations = Banner::getLocations();

        return view('desktop.manager.banner.info', ['banner' => $banner, 'locations' => $locations]);
    }

    public function getImage($id) {
        $banner = Banner::find($id);

        $images = DB::table('banners')->where('id', $id)->value('images');

        if (empty($images)) {
            $langs = \App\Helpers\Language::getActiveCodes();
            $values = array_fill_keys($langs, '');
            DB::table('banners')->where('id', $id)->update(['images' => json_encode($values)]);
        }

        return view('desktop.manager.banner.image', ['banner' => $banner, 'images' => $images]);
    }

    public function postUpdate(Request $request) {

        $id = $request->input('id');

        $banner = Banner::find($id);
        $banner->name = $request->input('name');
        $banner->customer = $request->input('customer');
        $banner->related_link = $request->input('related_link');
        $banner->location = $request->input('location');
        $banner->print = $banner->print + 0;
        
        $options = $request->all();

        $banner->updateDate('date_start', $options);
        $banner->updateDate('date_end', $options);

        $banner->payment_status = $request->input('status');
        $banner->save();

        return redirect('/manager/banner/info/' . $banner->id);
    }

    public function postImage(Request $request) {

        $uploader = new ImageUploader($request);

        $id = $request->input('id');

        $path = $uploader->saveSingle('image', false);

        $lang = $request->input('lang');

        if ($path) {
            if ($lang) {
                $images = DB::table('banners')->where('id', $id)->value('images');
                $values = json_decode($images, true);
                $values[$lang] = $path;
                DB::table('banners')->where('id', $id)->update(['images' => json_encode($values)]);
            } else {
                
            }
            return response()->json(['status' => 'OK', 'path' => $path, 'reload' => true]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }

}
