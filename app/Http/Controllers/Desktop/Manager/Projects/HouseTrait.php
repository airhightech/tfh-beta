<?php

namespace App\Http\Controllers\Desktop\Manager\Projects;

use App\Models\Property;
use Illuminate\Http\Request;

trait HouseTrait {
    
    public function house(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_SINGLE_HOUSE, ['new_project' => true]);
        
        return view('desktop.manager.project.house.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function houseCreate() {
        return $this->_create('desktop.manager.project.house.create', [
            'ptype' => Property::TYPE_SINGLE_HOUSE, 'new_project' => true
        ]);
    }
    
    public function houseInfo($id) {
        return $this->_info('desktop.manager.project.house.info', $id);
    }
    
    public function houseMedia($id) {
        return $this->_media('desktop.manager.project.house.media', $id);
    }
    
    public function houseDetails($id) {
        return $this->_details('desktop.manager.project.house.details', $id);
    }
    
    public function housePlaces($id) {
        return $this->_places('desktop.manager.project.house.places', $id);
    }
    
    public function houseReview($id) {
        return $this->_review('desktop.manager.project.house.review', $id);
    }
}