<?php

namespace App\Http\Controllers\Desktop\Manager\Projects;

use App\Models\Property;
use Illuminate\Http\Request;

trait TownhouseTrait {
    
    public function townhouse(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_TOWNHOUSE, ['new_project' => true]);
        
        return view('desktop.manager.project.townhouse.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function townhouseCreate() {
        return $this->_create('desktop.manager.project.townhouse.create', [
            'ptype' => Property::TYPE_TOWNHOUSE, 'new_project' => true
        ]);
    }
    
    public function townhouseInfo($id) {
        return $this->_info('desktop.manager.project.townhouse.info', $id);
    }
    
    public function townhouseMedia($id) {
        return $this->_media('desktop.manager.project.townhouse.media', $id);
    }
    
    public function townhouseDetails($id) {
        return $this->_details('desktop.manager.project.townhouse.details', $id);
    }
    
    public function townhousePlaces($id) {
        return $this->_places('desktop.manager.project.townhouse.places', $id);
    }
    
    public function townhouseReview($id) {
        return $this->_review('desktop.manager.project.townhouse.review', $id);
    }
}