<?php

namespace App\Http\Controllers\Desktop\Manager\Projects;

use App\Models\Property;
use Illuminate\Http\Request;

trait ApartmentTrait {
    
    public function apartment(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_APARTMENT_ROOM
                , ['new_project' => true]);
        
        return view('desktop.manager.project.apartment.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function apartmentCreate() {
        return $this->_create('desktop.manager.project.apartment.create', [
            'ptype' => Property::TYPE_APARTMENT_ROOM, 'new_project' => true
        ]);
    }
    
    public function apartmentInfo($id) {
        return $this->_info('desktop.manager.project.apartment.info', $id);
    }
    
    public function apartmentMedia($id) {
        return $this->_media('desktop.manager.project.apartment.media', $id);
    }
    
    public function apartmentDetails($id) {
        return $this->_details('desktop.manager.project.apartment.details', $id);
    }
    
    public function apartmentPlaces($id) {
        return $this->_places('desktop.manager.project.apartment.places', $id);
    }
    
    public function apartmentReview($id) {
        return $this->_review('desktop.manager.project.apartment.review', $id);
    }
}