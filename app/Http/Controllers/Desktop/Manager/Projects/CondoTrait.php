<?php

namespace App\Http\Controllers\Desktop\Manager\Projects;

use App\Models\Property;
use Illuminate\Http\Request;

trait CondoTrait {
    
    public function condo(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_CONDO_BUILDING, ['new_project' => true]);
        
        return view('desktop.manager.project.condo.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function condoCreate() {
        return $this->_create('desktop.manager.project.condo.create', [
            'ptype' => Property::TYPE_CONDO_BUILDING, 'new_project' => true
        ]);
    }
    
    public function condoInfo($id) {
        return $this->_info('desktop.manager.project.condo.info', $id);
    }
    
    public function condoMedia($id) {
        return $this->_media('desktop.manager.project.condo.media', $id);
    }
    
    public function condoDetails($id) {
        return $this->_details('desktop.manager.project.condo.details', $id);
    }
    
    public function condoPlaces($id) {
        return $this->_places('desktop.manager.project.condo.places', $id);
    }
    
    public function condoReview($id) {
        return $this->_review('desktop.manager.project.condo.review', $id);
    }
}