<?php

namespace App\Http\Controllers\Desktop\Manager;

use Illuminate\Http\Request;
use App\Models\Reviews\UserReview;
use App\Models\Reviews\AgentReview;

class CommentController extends BaseController {

    public function getComments(Request $request) {

        $type = $request->input('type', 'property');
        $status = $request->input('status', 'PENDING');

        $comments = [];

        if ($type == 'agent') {
            $comments = AgentReview::where('status', $status)
                            ->orderBy('created_at', 'asc')->paginate(10);
        } else {
            $comments = UserReview::where('status', $status)
                            ->orderBy('created_at', 'asc')->paginate(10);
        }

        return view('desktop.manager.comment.index', 
                ['comments' => $comments
                , 'type' => $type
                , 'request' => $request
                , 'status' => $status]);
    }

    public function getReviews() {
        return view('desktop.manager.comment.index');
    }

    public function getDetails(Request $request) {

        $id = $request->input('id');
        $type = $request->input('type');

        $comment = null;

        if ($type == 'agent') {
            $comment = AgentReview::find($id);
        } else {
            $comment = UserReview::find($id);
        }

        return view('desktop.manager.comment.details', ['comment' => $comment, 'type' => $type]);
    }

    public function postUpdate(Request $request) {
        $op = $request->input('op');
        $id = $request->input('id');
        $type = $request->input('type');

        $comment = null;

        if ($type == 'agent') {
            $comment = AgentReview::find($id);
        } else {
            $comment = UserReview::find($id);
        }

        if ($op == 'update') {
            $comment->updateComment($request);
        } else if($op == 'approve') {
            $comment->approve();
        } else if ($op == 'delete') {
            $comment->delete();
        }
        return response()->json(['status' => 'OK', 'reload' => true]);
    }

}
