<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Property;
use App\Models\Property\Facility;
use App\Models\Geo\Province;
use App\Models\Geo\District;
use App\Models\Geo\Area;
use App\Models\Geo\Place;
use Illuminate\Http\Request;
use App\Helpers\Language;
use App\Models\User\Profile;

trait PropertyTrait {

    protected function searchProperty(Request $request, $type, $options = []) {

        $q = trim($request->input('q'));

        $query = Property::where('ptype', $type);

        if (count($options)) {
            foreach ($options as $field => $value) {
                if (is_numeric($field)) {
                    $query->whereRaw($value);
                } else {
                    $query->where($field, $value);
                }                
            }
        }

        if ($q) {

            $terms = preg_split('/(\s+)/', $q);

            $query->where(function($query) use ($q) {
                $langs = Language::getActiveCodes();
                foreach ($langs as $lang) {
                    $query->orWhere('name->' . $lang, '~*', $q);
                }
            });
        }
        
        $query->whereNull('deleted_at');

        return $query->orderBy('updated_at', 'desc')
                        ->paginate(20);
    }

    protected function _create($view, $options = []) {
        $lang = \App::getLocale();
        $collate = Province::getLangCollation();
        $provinces = Province::orderByRaw("name->>'$lang' ASC")->get();
        return view($view, array_merge(['provinces' => $provinces], $options));
    }

    protected function _info($view, $id) {

        $lang = \App::getLocale();
        $collate = Province::getLangCollation();

        $property = Property::find($id);
        $provinces = Province::orderByRaw("name->>'$lang' ASC")->get();
        $address = $property->getAddress();
        $districts = [];
        $areas = [];

        if ($address->province_id > 0) {
            $districts = District::where('province_id', $address->province_id)->orderByRaw("name->>'$lang' ASC")->get();
        }

        if ($address->district_id > 0) {
            $areas = Area::where('district_id', $address->district_id)->orderByRaw("name->>'$lang' ASC")->get();
        }

        return view($view, ['property' => $property,
            'provinces' => $provinces,
            'districts' => $districts,
            'areas' => $areas]);
    }

    protected function _details($view, $id) {
        $property = Property::find($id);
        $facilities = Facility::all();
        $property_facilities = $property->getFacilitiesID();

        $bts_list = Place::where('type', 'bts')->get()->all();
        $mrt_list = Place::where('type', 'mrt')->get()->all();
        $apl_list = Place::where('type', 'apl')->get()->all();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');
        
        $query = Profile::leftJoin('users', 'users.id', 'profiles.user_id')
                ->where('users.ctype', 'developer')
                ->whereNull('users.deleted_at');
                
        $developers = $query->get();

        return view($view
                , [
            'property' => $property
            , 'facilities' => $facilities
            , 'bts_list' => $bts_list
            , 'mrt_list' => $mrt_list
            , 'apl_list' => $apl_list
            , 'selected_bts_list' => $selected_bts_list
            , 'selected_mrt_list' => $selected_mrt_list
            , 'selected_apl_list' => $selected_apl_list
            , 'property_facilities' => $property_facilities
            , 'developers' => $developers
        ]);
    }

    protected function _media($view, $id) {
        $property = Property::find($id);
        return view($view, ['property' => $property]);
    }

    protected function _places($view, $id) {
        $property = Property::find($id);
        return view($view, ['property' => $property]);
    }

    protected function _review($view, $id) {
        $property = Property::find($id);
        return view($view, ['property' => $property]);
    }

}
