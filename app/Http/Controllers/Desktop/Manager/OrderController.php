<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Customer\Purchase;
use App\Models\Customer\Account;
use App\Models\System\Bank;
use Illuminate\Http\Request;

class OrderController extends BaseController {
    
    public function index() {        
        
        $purchases = Purchase::orderBy('updated_at', 'desc')->paginate(25);
        
        return view('desktop.manager.order.index', ['purchases' => $purchases]);        
    }
    
    public function getDetails($id) {
        
        $status = session('status');
        
        $purchase = Purchase::find($id);
        
        $purchases = Purchase::where('account_id', $purchase->account_id)
                ->orderBy('created_at', 'desc')
                ->paginate(15); 
        $account = Account::find($purchase->account_id);
        $banks = Bank::orderBy('created_at', 'asc')->get();
        
        return view('desktop.manager.order.details', [
            'purchase' => $purchase,
            'purchases' => $purchases,
            'account' => $account,
            'banks' => $banks,
            'status' => $status
        ]);
        
    }
    
    public function updateOrder(Request $request) {
        
        $id = $request->input('id');
        $purchase = Purchase::find($id);
        $purchase->bank_id = $request->input('bank_id');
        $purchase->updated_amount = $request->input('last_amount');
        $purchase->payment_status = $request->input('payment_status');
        $purchase->save();
        
        $request->session()->flash('status', 'order_updated');
        
        return response()->json(['status' => 'OK', 'reload' => true]);
        
    }
    
    public function getStat() {
        
        $last30days = DB::select('SELECT d.date, SUM(d.amount) AS total '
                . 'FROM (SELECT (total_amount::float/100) AS amount, created_at::date AS date FROM customer_purchases WHERE created_at::date > current_date - interval ? day) '
                . 'AS d GROUP BY d.date ORDER BY d.date ASC', [30]);
        
        
        $last6months = DB::select('SELECT d.month, SUM(d.amount) AS total '
                . 'FROM (SELECT (total_amount::float/100) AS amount, '
                . 'to_char(date(created_at::date,\'YYYY-MM\') AS month '
                . 'FROM customer_purchases WHERE created_at::date > current_date - interval ? month) '
                . 'AS d GROUP BY d.date ORDER BY d.date ASC', [6]);
        
        return view('manager.order.stat', 
                [
                    'active' => 'order', 
                    'last30days' => $last30days,
                    'last6months' => $last6months,
                ]);
    }
    
}
