<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Http\Requests\Facility\Create as CreateFacilityRequest;
use App\Models\Property\Facility;
use App\Helpers\ImageUploader;
use Illuminate\Http\Request;
use DB;

class FacilityController extends BaseController {
    
    public function index() {        
        $facilities = Facility::orderBy('listing_rank', 'asc')->get();
        return view('desktop.manager.facility.index', ['facilities' => $facilities]);        
    }
    
    public function create() {
        return view('desktop.manager.facility.create');
    }
    
    public function postCreate(CreateFacilityRequest $request) {
        
        $facility = new Facility;
        $facility->setTranslatable('name', $request->input('name'));
        $facility->save();
        
        //
        
        $uploader = new ImageUploader($request);
        
        $file = $uploader->saveSingle('image');
        
        if ($file) {
            $facility->icon_path = $file;
            $facility->save();
        }
        
        return response()->json(['status' => 'OK', 'next' => '/manager/facility/list']);        
    }
    
    public function details($id) {
        $facility = Facility::findOrFail($id);
        return view('desktop.manager.facility.details', ['facility' => $facility]);
    }
    
    public function postUpdate(CreateFacilityRequest $request) {
        
        $facility = Facility::find($request->input('id'));
        $facility->setTranslatable('name', $request->input('name'));
        $facility->save();
        
        //
        
        $uploader = new ImageUploader($request);
        
        $file = $uploader->saveSingle('image');
        
        if ($file) {    
            $facility->removeOldIcon();
            $facility->icon_path = $file;
            $facility->save();
        }
        
        return response()->json(['status' => 'OK', 'next' => '/manager/facility/list']);        
    }
    
    public function order(Request $request)
    {
        $ranks = $request->input('ranks');

        if (count($ranks)) {
            $index = 1;
            foreach($ranks as $rank) {
                DB::update('UPDATE property_facilities SET listing_rank = ? WHERE id = ?', [$index, $rank]);
                $index++;
            }
        }
    }
    
}