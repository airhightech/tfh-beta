<?php

namespace App\Http\Controllers\Desktop\Manager;

class CondoController extends BaseController {
    
    use PropertyTrait;
    use Buildings\CondoTrait;
    use Buildings\NewCondoTrait;
    
}
