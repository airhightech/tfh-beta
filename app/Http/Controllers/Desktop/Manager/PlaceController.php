<?php

namespace App\Http\Controllers\Desktop\Manager;

use Illuminate\Http\Request;
use App\Models\Geo\Place;
use App\Http\Requests\Place\Create as CreatePlaceRequest;
use DB;

class PlaceController extends BaseController {

    public function index() {
        $supported = Place::getSupported();
        return view('desktop.manager.place.index', ['supported' => $supported]);
    }
    
    public function getlist(Request $request) {
        $places = Place::orderBy('type', 'asc')->paginate(15);
        return view('desktop.manager.place.list', ['places' => $places, 'request' => $request]);
    }

    public function create(CreatePlaceRequest $request) {

        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $place = new Place;
        $place->setTranslatable('name', $request->input('name'));
        $place->type = $request->input('type');
        $place->location = DB::raw("ST_GeomFromText('POINT($lng $lat 0.0)', 4326)");
        $place->save();

        return response()->json(['status' => 'OK', 'id' => $place->id]);
    }

    public function getEntries(Request $request) {

        $nl = $request->input('nl');
        $fr = $request->input('fr');

        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);

        $filters = explode(',', $request->input('filter'));

        $query = DB::table('places')->select(
                'id'
                , 'name'
                , 'type'
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
        );
        
        $supported = Place::getSupported();

        if (count($filters) < count($supported)) {
            $query->whereIn('type', $filters);
        }
        
        $query->whereRaw("location::geometry && ST_MakeEnvelope($lng_min, $lat_min, $lng_max, $lat_max, 4326)");
        $query->whereRaw('deleted_at IS NULL');
        
        $entries = $query->get();
        
         return response()->json(['status' => 'OK', 'entries' => $entries]);
    }

    public function update(CreatePlaceRequest $request) {

        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $place = Place::find($request->input('id'));
        $place->setTranslatable('name', $request->input('name'));
        $place->type = $request->input('type');
        $place->location = DB::raw("ST_GeomFromText('POINT($lng $lat 0.0)', 4326)");
        $place->save();

        return response()->json(['status' => 'OK']);
    }

    public function delete(Request $request) {

        $place = Place::find($request->input('id'));
        $place->delete();

        return response()->json(['status' => 'OK']);
    }
    
    public function getForm($id) {
        $place = Place::find($id);
        $supported = Place::getSupported();
        
        return view('desktop.manager.place.form', ['place' => $place, 'supported' => $supported]);
    }
    
    public function getDeleteForm($id) {
        $place = Place::find($id);
        
        return view('desktop.manager.place.delete-form', ['place' => $place]);
    }

}
