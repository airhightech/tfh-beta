<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\User;
use Illuminate\Http\Request;

class AccountController extends BaseController {
    
    const USER_PER_PAGE = 15;
    
    public function getVisitors(Request $request) {
        
        $users = $this->searchUsers($request, 'visitor');
        
        return view('desktop.manager.account.visitors', ['users' => $users, 'request' => $request]);
    }
    
    public function getOwners(Request $request) {
        
        $users = $this->searchUsers($request, 'owner');
        
        return view('desktop.manager.account.owners', ['users' => $users, 'request' => $request]);
    }
    
    public function getAgents(Request $request) {
        
        $type = $request->input('type', 'agent');
        
        $users = $this->searchUsers($request, $type, ['users.listing_level' => 'asc']);
        
        return view('desktop.manager.account.agents', ['users' => $users, 'type' => $type, 'request' => $request]);
    }
    
    public function getDevelopers(Request $request) {
        
        $users = $this->searchUsers($request, 'developer');
        
        return view('desktop.manager.account.developers', ['users' => $users, 'request' => $request]);
    }
    
    public function getProfile($id) {
        
        $user = User::find($id);
        
        return view('desktop.manager.account.profile', ['user' => $user]);
    }
    
    public function updateProfile(Request $request) {
        
        $id = $request->input('id');
        
        $user = User::find($id);
        
        $user->active = $request->input('active', 'false');
        
        if ($user->isAgent()) {
            $user->verified = $request->input('verified', 'false');
            $user->listing_level = $request->input('listing_level');
            
            $user->listing_level = $user->listing_level ? $user->listing_level : 10;
        }
        
        $options = $request->all();
        
        $profile = $user->getProfile();
        $profile->updateFromOptions($options);
        $profile->save();
        
        $personal = $user->getPersonalData();
        $personal->updateFromOptions($options);
        $personal->save();
        
        $company = $user->getCompanyData();
        $company->updateFromOptions($options);
        $company->save();     
        
        $user->save();
        
        return response()->json(['status' => 'OK']);
        
    }
    
    protected function searchUsers(Request $request, $type = null, $orders = []) {       
        
        $query = User::where('ctype', $type);
        
        $q = trim($request->input('q'));
        
        if (!empty($q)) {
            
            $query->where(function($query) use ($q){
                
                $id = 0;
                
                if(preg_match('/([0]*)([0-9]+)/', $q, $out)) {
                    $id = $out[2];
                }
                
                $query->orWhere('users.email', '~*', $q);
                $query->orWhere('users.name', '~*', $q);
                
                if ($id > 0) {
                    $query->orWhere('users.id', $id);
                }
            });
        }
        
        if (count($orders)) {
            foreach($orders as $field => $orientation) {
                $query->orderby($field, $orientation);
            }
        }
        
        $query->orderBy('users.created_at', 'desc');
        
        return $query->orderby('users.last_login', 'desc')->paginate(self::USER_PER_PAGE);
    }
    
    public function createDeveloper() {
        return view('desktop.manager.account.create-developer');
    }    
}