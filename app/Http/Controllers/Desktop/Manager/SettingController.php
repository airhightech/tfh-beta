<?php

namespace App\Http\Controllers\Desktop\Manager;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Requests\Bank\Create as CreateBankRequest;
use App\Models\System\Bank;
use DB;
use App\Helpers\ImageUploader;
use App\Models\System\Icon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SettingController extends BaseController {

    public function index() {

        $settings = DB::table('settings')->pluck('value', 'token')->all();

        return view('desktop.manager.setting.index', ['active' => 'setting', 'settings' => $settings]);
    }

    public function update(Request $request) {

        $this->_update($request, 'facebook_link');
        $this->_update($request, 'instagram_link');
        $this->_update($request, 'twitter_link');
        $this->_update($request, 'google_link');

        return redirect('/manager/setting');
    }

    protected function _update($request, $token, $json = false) {
        $date = date('Y-m-d H:i:s');

        $value = '';

        if (!$json) {
            $value = $request->input($token);
        } else {
            $value = json_encode($request->input($token));
        }

        try {
            DB::insert('INSERT INTO settings (token, value, created_at, updated_at) VALUES (?, ?, ?, ?)'
                    , [$token, $value, $date, $date]);
        } catch (QueryException $ex) {
            DB::update('UPDATE settings SET value = ?, updated_at = ? WHERE token = ?', [$value, $date, $token]);
        }
    }

    public function getContact() {

        $setting = DB::table('settings')->where('token', 'default_message')->value('value');

        $footer = DB::table('settings')->where('token', 'footer_contact')->value('value');

        $line_qrcode = DB::table('settings')->where('token', 'line_qrcode')->value('value');

        if (!$line_qrcode) {
            try {
                DB::table('settings')->insert([
                    'token' => 'line_qrcode',
                    'value' => null
                ]);
            } catch (QueryException $ex) {
                
            }
        }

        return view('desktop.manager.setting.contact', [
            'active' => 'setting'
            , 'default_message' => $setting
            , 'footer' => $footer
            , 'line_qrcode' => $line_qrcode]);
    }

    public function updateContact(Request $request) {

        $value = json_encode($request->input('details'));

        $date = date('Y-m-d H:i:s');
        try {
            DB::insert('INSERT INTO settings (token, value, created_at, updated_at) VALUES (?, ?, ?, ?)'
                    , ['default_message', $value, $date, $date]);
        } catch (QueryException $ex) {
            DB::update('UPDATE settings SET value = ?, updated_at = ? WHERE token = ?', [$value, $date, 'default_message']);
        }

        return redirect('/manager/setting/contact');
    }

    public function updateFooter(Request $request) {

        $value = json_encode($request->input('footer'));

        $date = date('Y-m-d H:i:s');
        try {
            DB::insert('INSERT INTO settings (token, value, created_at, updated_at) VALUES (?, ?, ?, ?)'
                    , ['footer_contact', $value, $date, $date]);
        } catch (QueryException $ex) {
            DB::update('UPDATE settings SET value = ?, updated_at = ? WHERE token = ?', [$value, $date, 'footer_contact']);
        }

        return redirect('/manager/setting/contact');
    }

    /**
     * Get bank list
     * @return type
     */
    public function getBank() {
        $banks = Bank::orderBy('created_at', 'asc')->get();
        return view('desktop.manager.setting.bank'
                , ['active' => 'setting', 'banks' => $banks]);
    }

    /**
     * Get new bank form
     * @return type
     */
    public function getAddBank() {
        return view('desktop.manager.setting.add-bank'
                , ['active' => 'setting']);
    }

    /**
     * Save new bank
     * @param CreateBankRequest $request
     * @return type
     */
    public function addBank(CreateBankRequest $request) {

        $bank = new Bank;
        $bank->code = $request->input('code');
        $bank->name = $request->input('name');
        $bank->account_no = $request->input('account_no');
        $bank->branch = $request->input('branch');
        $bank->save();

        $request->session()->flash('status', 'bank_created');

        return redirect('/manager/setting/bank');
    }

    public function editBank($id) {
        $bank = Bank::find($id);
        return view('desktop.manager.setting.edit-bank'
                , ['active' => 'setting', 'bank' => $bank]);
    }

    public function updateBank(CreateBankRequest $request, $id) {

        $bank = Bank::find($id);
        $bank->code = $request->input('code');
        $bank->name = $request->input('name');
        $bank->account_no = $request->input('account_no');
        $bank->branch = $request->input('branch');
        $bank->save();

        $request->session()->flash('status', 'bank_created');

        return redirect('/manager/setting/bank');
    }

    public function deleteBank($id) {
        $bank = Bank::find($id);
        $bank->delete();
        return redirect('/manager/setting/bank');
    }

    public function getImages() {
        $condo_image = DB::table('settings')->where('token', 'condo_image')->value('value');
        $featured_image = DB::table('settings')->where('token', 'featured_image')->value('value');
        $exclusive_image = DB::table('settings')->where('token', 'exclusive_image')->value('value');

        $home_image = DB::table('settings')->where('token', 'home_image')->value('value');

        if (!$condo_image) {
            $this->initTranslatedImages('condo_image');
        }

        if (!$featured_image) {
            $this->initTranslatedImages('featured_image');
        }

        if (!$exclusive_image) {
            $this->initTranslatedImages('exclusive_image');
        }

        if (!$home_image) {
            $this->initHomeImages();
        }

        return view('desktop.manager.setting.images', [
            'active' => 'setting'
            , 'condo_image' => $condo_image
            , 'featured_image' => $featured_image
            , 'exclusive_image' => $exclusive_image
            , 'home_image' => $home_image
        ]);
    }

    public function uploadImage(Request $request) {

        $helper = new ImageUploader($request);

        $path = $helper->saveSingle('image', false);

        $lang = $request->input('lang');
        $token = $request->input('token');

        if ($path) {
            if ($lang) {
                $setting = DB::table('settings')->where('token', $token)->value('value');
                $values = json_decode($setting, true);
                $values[$lang] = $path;
                DB::table('settings')->where('token', $token)->update(['value' => json_encode($values)]);
            } else if ($token == 'line_qrcode') {
                DB::table('settings')->where('token', $token)->update(['value' => $path]);
            }
            return response()->json(['status' => 'OK', 'path' => $path]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }

    public function removeTranslatedImage(Request $request) {
        $lang = $request->input('lang');
        $token = $request->input('token');

        $setting = DB::table('settings')->where('token', $token)->value('value');
        $values = json_decode($setting, true);

        $path = $values[$lang];

        ImageHelper::remove($path);

        $values[$lang] = '';
        DB::table('settings')->where('token', $token)->update(['value' => json_encode($values)]);

        return back();
    }

    protected function initTranslatedImages($token) {
        $langs = \App\Helpers\Language::getActiveCodes();
        $values = array_fill_keys($langs, '');

        DB::table('settings')->insert([
            'token' => $token,
            'value' => json_encode($values)
        ]);
    }

    protected function initHomeImages() {
        DB::table('settings')->insert([
            'token' => 'home_image',
            'value' => json_encode(array_fill_keys(['rent', 'sale', 'new_project', 'condo'], ''))
        ]);
    }

    public function getPurchase() {

        $featured_step_1 = json_decode(DB::table('settings')->where('token', 'featured_step_1')->value('value'), true);
        $featured_step_2 = json_decode(DB::table('settings')->where('token', 'featured_step_2')->value('value'), true);

        $exclusive_step_1 = json_decode(DB::table('settings')->where('token', 'exclusive_step_1')->value('value'), true);
        $exclusive_step_2 = json_decode(DB::table('settings')->where('token', 'exclusive_step_2')->value('value'), true);

        return view('desktop.manager.setting.purchase', [
            'active' => 'setting'
            , 'featured_step_1' => $featured_step_1
            , 'featured_step_2' => $featured_step_2
            , 'exclusive_step_1' => $exclusive_step_1
            , 'exclusive_step_2' => $exclusive_step_2
        ]);
    }

    public function postPurchase(Request $request) {

        $this->_update($request, 'featured_step_1', true);
        $this->_update($request, 'featured_step_2', true);
        $this->_update($request, 'exclusive_step_1', true);
        $this->_update($request, 'exclusive_step_2', true);

        return redirect('/manager/setting/purchase');
    }

    public function getPopup() {
        $homepage_popup = json_decode(DB::table('settings')->where('token', 'popup')->value('value'), true);

        return view('desktop.manager.setting.popup', [
            'active' => 'setting'
            , 'homepage_popup' => $homepage_popup
        ]);
    }

    public function postPopup(Request $request) {
        
        $this->_update($request, 'popup', true);
        return redirect('/manager/setting/popup');
        
    }
    
    public function getIcon() {
        return view('desktop.manager.setting.icon', [
            'active' => 'icon'
        ]);
    }
    
    public function postIcon(Request $request) {
        $code = $request->input('code');
        
        $icon = null;
        
        try {
            $icon = Icon::where('code', $code)->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            $icon = new Icon;
        }
        
        if ($icon) {
            $icon->updateFromRequest($request);
        }
        
        return redirect('/manager/setting/icon');
    }
    
    public function removeIcon(Request $request) {
        $code = $request->input('code');
        try {
            $icon = Icon::where('code', $code)->firstOrFail();
            $icon->removeOld();
            $icon->delete();
        } catch (ModelNotFoundException $ex) {
           
        }
        return redirect('/manager/setting/icon');
    }

}
