<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Geo\Province;
use App\Models\Geo\District;
use App\Models\Geo\Area;

class LocationController extends BaseController {

    public function index() {
        $lang = \App::getLocale();
        $collate = Province::getLangCollation();
        $provinces = Province::orderByRaw("name->>'$lang' ASC")->paginate(15);
        return view('desktop.manager.location.index', ['provinces' => $provinces]);
    }
    
    public function editProvince($id) {
        $province = Province::find($id);
        return view('desktop.manager.location.province-edit', ['province' => $province]);
    }

    public function getDistrictList($id) {
        $lang = \App::getLocale();
        $collate = Province::getLangCollation();
        $districts = District::where('province_id', $id)->orderByRaw("name->>'$lang' ASC")->paginate(15);
        $province = Province::find($id);

        return view('desktop.manager.location.district-list'
                , ['districts' => $districts, 'province' => $province]);
    }
    
    public function editDistrict($id) {
        $district = District::find($id);
        return view('desktop.manager.location.district-edit', ['district' => $district]);
    }

    public function getAreaList($id) {
        $lang = \App::getLocale();
        $collate = Province::getLangCollation();
        $areas = Area::where('district_id', $id)->orderByRaw("name->>'$lang' ASC")->paginate(15);
        $district = District::find($id);
        $province = Province::find($district->province_id);

        return view('desktop.manager.location.area-list'
                , ['areas' => $areas, 'province' => $province, 'district' => $district]);
    }
    
    public function editArea($id) {
        $area = Area::find($id);
        return view('desktop.manager.location.area-edit', ['area' => $area]);
    }

}
