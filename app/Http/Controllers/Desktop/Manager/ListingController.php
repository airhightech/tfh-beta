<?php

namespace App\Http\Controllers\Desktop\Manager;

use Illuminate\Http\Request;
use App\Models\Property;

class ListingController extends BaseController {

    use PropertyTrait;
    use Listings\CondoTrait;
    use Listings\ApartmentTrait;
    use Listings\HouseTrait;
    use Listings\TownhouseTrait;

    public function all(Request $request) {

        $filters = $this->getListFilters($request);

        $query = Property::leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id');
        $query->where('properties.user_submited', true);
        $query->where('properties.new_project', false);

        if ($filters['lt'] !== 'all') {
            $query->where('property_listing_data.ltype', $filters['lt']);
        }
        
        if ($filters['pt'] !== 'all') {
            $query->where('properties.ptype', $filters['pt']);
        }
        
        if ($filters['class'] !== 'all') {
            $query->where('property_listing_data.listing_class', $filters['class']);
        }
        
        if ($filters['status'] !== 'all') {
            if ($filters['status'] == 'present') {
                $query->where('properties.published', true);
            } else {
                $query->where('properties.published', false);
            }
        }

        $properties = $query->orderBy('properties.updated_at', 'desc')
                ->paginate(20);

        return view('desktop.manager.listing.all', ['properties' => $properties
            , 'filters' => $filters
            , 'request' => $request]);
    }

    protected function getListFilters(Request $request) {
        return [
            'lt' => $request->input('lt', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all'),
            'pt' => $request->input('pt', 'all')
        ];
    }

}
