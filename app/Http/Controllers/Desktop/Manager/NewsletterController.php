<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Messages\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends BaseController {

    public function index() {
        $newsletters = Newsletter::orderby('created_at', 'desc')->paginate(10);
        return view('desktop.manager.newsletter.index', ['newsletters' => $newsletters]);
    }

    public function getCreate() {
        $template = file_get_contents('newsletter.html');
        return view('desktop.manager.newsletter.create', ['template' => $template]);
    }

    public function postCreate(Request $request) {
        $options = $request->all();
        $newsletter = new Newsletter;
        $newsletter->status = 'PENDING';
        $newsletter->setTranslatable('subject', $request);
        $newsletter->updateDate('send_time', $options);
        $newsletter->setTranslatable('mail_body', $request);
        $newsletter->save();
        return redirect('/manager/newsletter/list');
    }

    public function getEdit(Request $request) {
      $newsletter = Newsletter::where('id', $request->input('id'))->firstOrFail();
      return view('desktop.manager.newsletter.edit', ['newsletter' => $newsletter]);
    }

    public function postUpdate(Request $request) {
      $id = $request->input('id');
      $options = $request->all();
      $newsletter = Newsletter::find($id);
      $newsletter->setTranslatable('subject', $request);
      $newsletter->updateDate('send_time', $options);
      $newsletter->setTranslatable('mail_body', $request);
      $newsletter->save();
      return response()->json(['status' => 'OK', 'reload' => true]);
    }

}
