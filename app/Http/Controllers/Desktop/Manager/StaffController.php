<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\Register\CreateStaff as CreateStaffRequest;
use App\Http\Requests\Register\UpdateStaff as UpdateStaffRequest;
use App\Http\Controllers\Common\AuthenticationTrait;
use Hash;
use App\Models\User\Staff;

class StaffController extends BaseController {
    
    use AuthenticationTrait;

    /**
     * Get list of staff
     * @return type
     */
    public function getList(Request $request) {

        $sortby = $this->getCurrentSorting($request, ['updated_at', 'desc']);

        $staffs = [];
        
        $count_per_page = 15;
        
        $roles = array_keys(Staff::getRoles());

        if ($sortby[0] == 'name') {
            $staffs = User::whereIn('ctype', $roles)
                    ->orderBy('realname', $sortby[1])
                    ->paginate($count_per_page);
        } else {
            $staffs = User::whereIn('ctype', $roles)
                    ->orderBy($sortby[0], $sortby[1])
                    ->paginate($count_per_page);
        }

        return view('desktop.manager.staff.list', [
            'active' => 'staff',
            'staffs' => $staffs,
            'sortby' => $sortby
        ]);
    }

    public function getCreate() {
        $roles = Staff::getRoles();
        return view('desktop.manager.staff.create', ['roles' => $roles]);
    }

    public function postCreate(CreateStaffRequest $request) {
        
        $role = $request->input('role');
        
        $user = $this->_register($request, $role);
        
        /*$user->active = true;
        $user->email_validated = true;
        $user->email_validation_token = null;
        $user->password = Hash::make($request->input('password'));*/
        $user->name = $request->input('name');
        $user->save();

        // Send email verification

        return response()->json(['status' => 'OK', 'user' => $user->id, 'reload' => true]);
    }

    public function getUpdate($id) {

        $user = User::find($id);
        $roles = Staff::getRoles();
        return view('desktop.manager.staff.update', ['user' => $user, 'roles' => $roles]);
    }

    public function postUpdate(UpdateStaffRequest $request, $id) {

        $role = $request->input('role');
        $user = User::find($id);
        $user->name = $request->input('name');
        $user->ctype = $role;
        $user->save();

        return response()->json(['status' => 'OK', 'user' => $user->id, 'reload' => true]);
    }

    public function getDelete($id) {        
        $user = User::find($id);        
        return view('desktop.manager.staff.delete', ['user' => $user]);
    }
    
    public function delete($id) {
        
        $user = User::find($id);
        $user->delete();
        
        return redirect('/manager/staff/list');
    }

}
