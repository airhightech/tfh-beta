<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Messages\NewsletterSubscription;
use Illuminate\Http\Request;

class NewsletterSubController extends BaseController {

    public function index() {
        $subscribers = NewsletterSubscription::all();
        return view('desktop.manager.newsletter-subscriber.index', ['subscribers' => $subscribers]);
    }

}
