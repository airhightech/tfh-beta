<?php

namespace App\Http\Controllers\Desktop\Manager\Listings;

use App\Models\Property;
use Illuminate\Http\Request;

trait TownhouseTrait {
    
    public function townhouse(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_TOWNHOUSE);
        
        return view('desktop.manager.listing.townhouse.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function townhouseInfo($id) {
        return $this->_info('desktop.manager.listing.townhouse.info', $id);
    }
    
    public function townhouseMedia($id) {
        return $this->_media('desktop.manager.listing.townhouse.media', $id);
    }
    
    public function townhouseDetails($id) {
        return $this->_details('desktop.manager.listing.townhouse.details', $id);
    }
    
    public function townhousePlaces($id) {
        return $this->_places('desktop.manager.listing.townhouse.places', $id);
    }
}