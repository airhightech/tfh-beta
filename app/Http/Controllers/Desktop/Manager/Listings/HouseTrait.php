<?php

namespace App\Http\Controllers\Desktop\Manager\Listings;

use App\Models\Property;
use Illuminate\Http\Request;

trait HouseTrait {
    
    public function house(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_SINGLE_HOUSE);
        
        return view('desktop.manager.listing.house.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function houseInfo($id) {
        return $this->_info('desktop.manager.listing.house.info', $id);
    }
    
    public function houseMedia($id) {
        return $this->_media('desktop.manager.listing.house.media', $id);
    }
    
    public function houseDetails($id) {
        return $this->_details('desktop.manager.listing.house.details', $id);
    }
    
    public function housePlaces($id) {
        return $this->_places('desktop.manager.listing.house.places', $id);
    }
}