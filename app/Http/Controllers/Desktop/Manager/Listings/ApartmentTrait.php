<?php

namespace App\Http\Controllers\Desktop\Manager\Listings;

use App\Models\Property;
use Illuminate\Http\Request;

trait ApartmentTrait {
    
    public function apartment(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_APARTMENT_ROOM);
        
        return view('desktop.manager.listing.apartment.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function apartmentInfo($id) {
        return $this->_info('desktop.manager.listing.apartment.info', $id);
    }
    
    public function apartmentMedia($id) {
        return $this->_media('desktop.manager.listing.apartment.media', $id);
    }
    
    public function apartmentDetails($id) {
        return $this->_details('desktop.manager.listing.apartment.details', $id);
    }
    
    public function apartmentPlaces($id) {
        return $this->_places('desktop.manager.listing.apartment.places', $id);
    }
}