<?php

namespace App\Http\Controllers\Desktop\Manager\Listings;

use App\Models\Property;
use Illuminate\Http\Request;

trait CondoTrait {
    
    public function condo(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_CONDO_ROOM);
        
        return view('desktop.manager.listing.condo.list'
                , ['properties' => $properties, 'request' => $request]);
        
    }
    
    public function condoInfo($id) {
        return $this->_info('desktop.manager.listing.condo.info', $id);
    }
    
    public function condoMedia($id) {
        return $this->_media('desktop.manager.listing.condo.media', $id);
    }
    
    public function condoDetails($id) {
        return $this->_details('desktop.manager.listing.condo.details', $id);
    }
    
    public function condoPlaces($id) {
        return $this->_places('desktop.manager.listing.condo.places', $id);
    }
}