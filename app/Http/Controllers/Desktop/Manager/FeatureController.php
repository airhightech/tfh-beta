<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Http\Requests\Feature\Create as CreateFeatureRequest;
use App\Models\Property\Feature;
use App\Helpers\ImageUploader;
use Illuminate\Http\Request;
use DB;

class FeatureController extends BaseController {
    
    public function index() {        
        $features = Feature::where('optional', false)->orderBy('listing_rank', 'asc')->get();
        return view('desktop.manager.feature.index', ['features' => $features]);        
    }
    
    public function create() {
        return view('desktop.manager.feature.create');
    }
    
    public function postCreate(CreateFeatureRequest $request) {
        
        $feature = new Feature;
        $feature->setTranslatable('name', $request->input('name'));
        $feature->optional = false;
        $feature->save();
        
        //
        
        $uploader = new ImageUploader($request);
        
        $file = $uploader->saveSingle('image');
        
        if ($file) {
            $feature->icon_path = $file;
            $feature->save();
        }
        
        return response()->json(['status' => 'OK', 'next' => '/manager/feature/list']);        
    }
    
    public function details($id) {
        $feature = Feature::findOrFail($id);
        return view('desktop.manager.feature.details', ['feature' => $feature]);
    }
    
    public function postUpdate(CreateFeatureRequest $request) {
        
        $feature = Feature::find($request->input('id'));
        $feature->setTranslatable('name', $request->input('name'));
        $feature->save();
        
        //
        
        $uploader = new ImageUploader($request);
        
        $file = $uploader->saveSingle('image');
        
        if ($file) {            
            $feature->removeOldIcon();
            $feature->icon_path = $file;
            $feature->save();
        }
        
        return response()->json(['status' => 'OK', 'next' => '/manager/optional-feature/list']);        
    }
    
    public function order(Request $request)
    {
        $ranks = $request->input('ranks');

        if (count($ranks)) {
            $index = 1;
            foreach($ranks as $rank) {
                DB::update('UPDATE property_features SET listing_rank = ? WHERE id = ?', [$index, $rank]);
                $index++;
            }
        }
    }
    
    public function optionals() {
        $features = Feature::where('optional', true)->orderBy('listing_rank', 'asc')->get();
        return view('desktop.manager.feature.optionals', ['features' => $features]);
    }
    
    public function createOptional() {
        return view('desktop.manager.feature.create-optional');
    }
    
    public function postCreateOptional(CreateFeatureRequest $request) {
        
        $feature = new Feature;
        $feature->setTranslatable('name', $request->input('name'));
        $feature->optional = true;
        $feature->save();
        
        //
        
        $uploader = new ImageUploader($request);
        
        $file = $uploader->saveSingle('image');
        
        if ($file) {
            $feature->icon_path = $file;
            $feature->save();
        }
        
        return response()->json(['status' => 'OK', 'next' => '/manager/optional-feature/list']);        
    }
    
    public function detailsOptional($id) {
        $feature = Feature::findOrFail($id);
        return view('desktop.manager.feature.details-optional', ['feature' => $feature]);
    }
    
    public function deleteOptional($id) {
        $feature = Feature::findOrFail($id);
        $feature->delete();
        return response()->json(['status' => 'OK']);
    }
    
    public function postUpdateOptional(CreateFeatureRequest $request) {
        
        $feature = Feature::find($request->input('id'));
        $feature->setTranslatable('name', $request->input('name'));
        $feature->save();
        
        //
        
        $uploader = new ImageUploader($request);
        
        $file = $uploader->saveSingle('image');
        
        if ($file) {            
            $feature->removeOldIcon();
            $feature->icon_path = $file;
            $feature->save();
        }
        
        return response()->json(['status' => 'OK', 'next' => '/manager/optional-feature/list']);        
    }
    
}