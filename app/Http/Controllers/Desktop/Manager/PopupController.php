<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Messages\Popup;
use Illuminate\Http\Request;

class PopupController extends BaseController {
    
    public function index() {
        $popups = Popup::orderBy('created_at', 'desc')->paginate(10);
        return view('desktop.manager.popup.list', ['popups' => $popups]);
    }
    
    public function create() {
        return view('desktop.manager.popup.create');
    }
    
    public function edit($id) {
        $popup = Popup::find($id);
        return view('desktop.manager.popup.edit', ['popup' => $popup]);
    }
    
    public function postCreate(Request $request) {
        
        $options = $request->all();
        
        $popup = new Popup;
        $popup->related_link = $request->input('related_link');
        $popup->title = $request->input('title');
        $popup->setTranslatable('content', $request);
        $popup->updateDate('start_time', $options);
        $popup->updateDate('end_time', $options);
        
        $popup->save();
        
        return response()->json([
            'status' => 'OK',
            'next' => '/manager/popup/edit/' . $popup->id
        ]);
    }
    
    public function postUpdate(Request $request) {        
        
        $options = $request->all();
        $id = $request->input('id');
        
        $popup = Popup::find($id);
        $popup->related_link = $request->input('related_link');
        $popup->title = $request->input('title');
        $popup->setTranslatable('content', $request);
        $popup->updateDate('start_time', $options);
        $popup->updateDate('end_time', $options);
        
        $popup->save();
        
        return response()->json([
            'status' => 'OK',
            'next' => '/manager/popup/edit/' . $popup->id
        ]);
    }
    
}