<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\Messages\Notification;

class NoticeController extends BaseController {
    
    public function index() {        
        $notices = Notification::orderBy('created_at', 'desc')->paginate(20);        
        return view('desktop.manager.notices.index', ['notices' => $notices]);
    }
    
    public function edit($id) {
        $notice = Notification::find($id);
        return view('desktop.manager.notices.edit', ['notice' => $notice]);
    }
    
    public function create() {
        return view('desktop.manager.notices.create');
    }
    
    public function send($id) {
        $notice = Notification::find($id);
        $notice->send();
        return back();
    }
    
}