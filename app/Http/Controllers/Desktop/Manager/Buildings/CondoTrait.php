<?php

namespace App\Http\Controllers\Desktop\Manager\Buildings;

use App\Models\Property;
use Illuminate\Http\Request;

trait CondoTrait {
    
    public function index(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_CONDO_BUILDING
                , ['user_submited IS NOT TRUE', 'new_project' => false]);

        return view('desktop.manager.building.condo.list'
                , ['properties' => $properties, 'request' => $request]);
    }
    
    public function create() {
        return $this->_create('desktop.manager.building.condo.create');
    }
    
    public function info($id) {
        return $this->_info('desktop.manager.building.condo.info', $id);
    }
    
    public function media($id) {
        return $this->_media('desktop.manager.building.condo.media', $id);
    }
    
    public function details($id) {
        return $this->_details('desktop.manager.building.condo.details', $id);
    }
    
    public function places($id) {
        return $this->_places('desktop.manager.building.condo.places', $id);
    }
    
    public function review($id) {
        return $this->_review('desktop.manager.building.condo.review', $id);
    }
}