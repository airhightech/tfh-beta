<?php

namespace App\Http\Controllers\Desktop\Manager\Buildings;

use App\Models\Property;
use Illuminate\Http\Request;

trait NewCondoTrait {
    
    public function newindex(Request $request) {
        
        $properties = $this->searchProperty($request, Property::TYPE_CONDO_BUILDING
                , ['user_submited IS TRUE', 'new_project' => false]);

        return view('desktop.manager.building.new-condo.list'
                , ['properties' => $properties, 'request' => $request]);
    }
    
    public function newInfo($id) {
        return $this->_info('desktop.manager.building.new-condo.info', $id);
    }
    
    public function newMedia($id) {
        return $this->_media('desktop.manager.building.new-condo.media', $id);
    }
    
    public function newDetails($id) {
        return $this->_details('desktop.manager.building.new-condo.details', $id);
    }
    
    public function newPlaces($id) {
        return $this->_places('desktop.manager.building.new-condo.places', $id);
    }
    
    public function newReview($id) {
        return $this->_review('desktop.manager.building.new-condo.review', $id);
    }
}