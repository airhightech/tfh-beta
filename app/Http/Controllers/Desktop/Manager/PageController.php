<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Models\System\StaticPage;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Helpers\ImageUploader;

class PageController extends BaseController {
    
    public function index() {        
        return view('desktop.manager.page.index');        
    }
    
    
    public function edit(Request $request) {      
        
        $page = null;
        
        try {
            $page = StaticPage::where('sku', $request->input('page'))->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            $page = new StaticPage;
            $page->sku = $request->input('page');
            $page->save();
        }
        
        return view('desktop.manager.page.edit', ['page' => $page]);        
    }
    
    public function update(Request $request) {
        $id = $request->input('id');
        
        $page = StaticPage::find($id);
        $page->setTranslatable('title', $request);
        $page->setTranslatable('html', $request);
        $page->save();
        
        return response()->json(['status' => 'OK', 'reload' => true]);
        
    }
    
    public function uploadImage(Request $request) {
        
        $uploader = new ImageUploader($request);
        $image = $uploader->saveSingle('image');
        
        echo url('/image/' . $image);
        
    }
    
}