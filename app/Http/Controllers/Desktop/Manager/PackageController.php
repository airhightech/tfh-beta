<?php

namespace App\Http\Controllers\Desktop\Manager;

use App\Http\Requests\Package\Create as CreatePackageRequest;
use App\Models\Listings\Package;


class PackageController extends BaseController {
    
    /**
     * Get the list of all packages
     * @return type
     */
    public function getList() {
        $packages = Package::orderBy('type', 'asc')->orderBy('normal_price', 'asc')->get();
        return view('desktop.manager.package.index', ['active' => 'package', 'packages' => $packages]);
    }
    
    /**
     * Create a new Featured package
     */
    public function createFeatured() {
        return view('desktop.manager.package.create-featured', ['active' => 'package']);
    }
    
    /**
     * Save featured packages
     */
    public function saveFeatured(CreatePackageRequest $request) {
        
        $package = new Package;
        $package->setTranslatable('name', $request->input('name'));
        $package->type = 'featured';
        $package->listing_count = $request->input('listing_count');
        $package->normal_price = $request->input('normal_price') + 0 ;
        $package->discounted_price = $request->input('discounted_price') + 0;
        $package->duration = $request->input('duration') + 0;
        $package->save();
        
        $request->session()->flash('status', 'saved');
        
        return redirect('/manager/package/edit-featured/' . $package->id);
        
    }
    
    /**
     * Edit featured package
     */
    public function editFeatured($id) {        
        $package = Package::find($id);     
        $status = session('status');
        
        return view('desktop.manager.package.edit-featured', 
                ['active' => 'package', 'package' => $package, 'status' => $status]);
    }
    
    public function updateFeatured(CreatePackageRequest $request, $id) {
        
        $package = Package::find($id);
        
        $package->setTranslatable('name', $request->input('name'));
        $package->listing_count = $request->input('listing_count');
        $package->normal_price = $request->input('normal_price') + 0;
        $package->discounted_price = $request->input('discounted_price') + 0;
        $package->duration = $request->input('duration') + 0;
        $package->save();
        
        
        $request->session()->flash('status', 'saved');
        
        return redirect('/manager/package/edit-featured/' . $package->id);
    }
    
    /**
     * Create a new Featured package
     */
    public function createExclusive() {
        return view('desktop.manager.package.create-exclusive', ['active' => 'package']);
    }
    
    /**
     * Save featured packages
     */
    public function saveExclusive(CreatePackageRequest $request) {
        
        $package = new Package;
        $package->setTranslatable('name', $request->input('name'));
        $package->type = 'exclusive';
        $package->listing_count = $request->input('listing_count') + 0;
        $package->free_listing_count = $request->input('free_listing_count') + 0;
        $package->normal_price = $request->input('normal_price') + 0;
        $package->discounted_price = $request->input('discounted_price') + 0;
        $package->duration = $request->input('duration') + 0;
        $package->save();
        
        $request->session()->flash('status', 'saved');
        
        return redirect('/manager/package/edit-exclusive/' . $package->id);
        
    }
    
    /**
     * Edit featured package
     */
    public function editExclusive($id) {
        $package = Package::find($id);     
        $status = session('status');
        
        return view('desktop.manager.package.edit-exclusive', 
                ['active' => 'package', 'package' => $package, 'status' => $status]);
    }
    
    public function updateExclusive(CreatePackageRequest $request, $id) {
        
        $package = Package::find($id);
        
        $package->setTranslatable('name', $request->input('name'));
        $package->listing_count = $request->input('listing_count') + 0;
        $package->free_listing_count = $request->input('free_listing_count') + 0;
        $package->normal_price = $request->input('normal_price') + 0;
        $package->discounted_price = $request->input('discounted_price') + 0;
        $package->duration = $request->input('duration') + 0;
        $package->save();
        
        
        $request->session()->flash('status', 'saved');
        
        return redirect('/manager/package/edit-exclusive/' . $package->id);
    }
    
    public function confirmDeletePackage($id) {
        $package = Package::find($id);
        
        return view('desktop.manager.package.delete', 
                ['active' => 'package', 'package' => $package]);
    }
    
    public function delete($id) {
        $package = Package::find($id);
        $package->delete();
        return redirect('/manager/package/list');
    }
    
}