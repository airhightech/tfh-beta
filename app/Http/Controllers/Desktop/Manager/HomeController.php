<?php

namespace App\Http\Controllers\Desktop\Manager;

use DB;
use Illuminate\Http\Request;

class HomeController extends BaseController {

    public function index() {

        $total_listing = DB::table('properties')->where('ptype', '!=' , 'cdb')->count();
        $total_comments = DB::table('customer_reviews')->count();

        $total_members = DB::table('users')->count();

        /* $total_members = DB::table('users')
          ->whereIn('ctype', ['visitor', 'agent', 'company', 'developer'])->count(); */

        $totalListingData = DB::table('properties')
                        ->select(DB::raw('COUNT(id) AS nb'), 'ptype')
                        ->groupBy('ptype')
                        ->whereNull('properties.deleted_at')
                        ->where('ptype', '!=' , 'cdb')
                        ->get()->pluck('nb', 'ptype')->all();
                        //->toSql();
        //var_dump($totalListingData);

        $totalMemberData = DB::table('users')
                        ->select(DB::raw('COUNT(id) AS nb'), 'ctype')
                        ->groupBy('ctype')
                        ->get()->pluck('nb', 'ctype')->all();

        $totalCommentData = DB::table('customer_reviews')
                        ->join('properties', 'properties.id', '=', 'customer_reviews.property_id')
                        ->select(DB::raw('COUNT(customer_reviews) AS nb'), 'properties.ptype')
                        ->groupBy('properties.ptype')
                        ->get()->pluck('nb', 'ptype')->all();

        $totalPreferedLanguage = DB::table('users')
                        ->select(DB::raw('COUNT(id) AS nb'), 'lang')
                        ->groupBy('lang')
                        ->get()->pluck('nb', 'lang')->all();

        //$totalMember = array_sum($totalMember);
        //var_dump($totalCommentData);

        return view('desktop.manager.home.index', [
            'total_listing' => $total_listing
            , 'total_comments' => $total_comments
            , 'total_members' => $total_members
            , 'totalListingData' => $totalListingData
            , 'totalMemberData' => $totalMemberData
            , 'totalCommentData' => $totalCommentData
            , 'totalPreferedLanguage' => $totalPreferedLanguage
        ]);
    }

    public function getDateFormat($showBy){
      $dateFormat = [];
      switch ($showBy) {
        case 'weekly':
          $dateFormat['dateTruncName'] = 'week';
          $dateFormat['dateTruncFormat'] = 'YYYY-MM-DD';
          $dateFormat['LabelFormat'] = 'd/m/Y';
        break;
        case 'monthly':
          $dateFormat['dateTruncName'] = 'month';
          $dateFormat['dateTruncFormat'] = 'YYYY-MM';
          $dateFormat['LabelFormat'] = 'm/Y';
        break;
        case 'yearly':
          $dateFormat['dateTruncName'] = 'year';
          $dateFormat['dateTruncFormat'] = 'YYYY';
          $dateFormat['LabelFormat'] = 'Y';
        break;
        default:
          $dateFormat['dateTruncName'] = 'week';
          $dateFormat['dateTruncFormat'] = 'YYYY-MM-DD';
          $dateFormat['LabelFormat'] = 'd/m/Y';
        break;
      }
      return $dateFormat;
    }

    public function getTotalMemberData(Request $request){

      $options = $request->all();
      $from = updateDate('from', $options);
      $to = updateDate('to', $options);
      $showBy = $request->input('showBy');
      $dateFormat = $this->getDateFormat($showBy);

      $list = DB::table('users')
                      ->select(DB::raw('COUNT(id) AS listings')
                              , DB::raw("to_char(date_trunc('".$dateFormat['dateTruncName']."', created_at), '".$dateFormat['dateTruncFormat']."' ) AS period")
                              , 'ctype')
                      ->where('created_at', '>=', $from)
                      ->where('created_at', '<=', $to)
                      ->groupBy(DB::raw('2'), DB::raw('3'))
                      ->get()->all();

      ksort($list);

      $data = [];

      $labels = [];

      $grouped_listings = [];

      foreach ($list as $stat) {
        $grouped_listings[$stat->ctype][$stat->period] = $stat->listings;
        $labels[] = $stat->period;
      }

      $labels = array_unique($labels);
      sort($labels);

      $datasets = [
          [
              'label' => tr('property.condo', 'User'),
              'backgroundColor' => '#537e0b',
              'data' => $this->getPropertyListingData('visitor', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.apartment', 'Owner'),
              'backgroundColor' => '#ff77a5',
              'data' => $this->getPropertyListingData('owner', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.house', 'Freelance Agent'),
              'backgroundColor' => '#63d8e9',
              'data' => $this->getPropertyListingData('agent', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.house', 'Real Estate Agent'),
              'backgroundColor' => '#898989',
              'data' => $this->getPropertyListingData('company', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.house', 'Land Developer'),
              'backgroundColor' => '#edff53',
              'data' => $this->getPropertyListingData('developer', $labels, $grouped_listings)
          ]
      ];

      $data['datasets'] = $datasets;

      $data['labels'] = [];

      foreach($labels as $label) {
          $data['labels'][] = date($dateFormat['LabelFormat'], strtotime($label));
      }

      return response()->json($data);
    }

    public function getTotalListingData(Request $request) {

        $options = $request->all();
        $from = updateDate('from', $options);
        $to = updateDate('to', $options);
        $showBy = $request->input('showBy');
        $listing = $request->input('listing');
        $dateFormat = $this->getDateFormat($showBy);

        $baseQuery=DB::table('properties')
            ->join('property_listing_data', 'properties.id', '=', 'property_listing_data.property_id')
            ->select(DB::raw('COUNT(id) AS listings')
                   , DB::raw("to_char(date_trunc('".$dateFormat['dateTruncName']."', created_at), '".$dateFormat['dateTruncFormat']."' ) AS period")
                   , 'ptype')
            ->where('created_at', '>=', $from)
            ->where('created_at', '<=', $to);

        if(isset($listing)){
          if($listing == 'np'){
            $baseQuery->where('properties.new_project', true);
          }
          else{
            $baseQuery->where('property_listing_data.ltype', '=', $listing);
          }
        }

        $baseQuery->groupBy(DB::raw('2'), DB::raw('3'));
        $list = $baseQuery->get()->all();

        $new_project = DB::table('properties')
                        ->select(DB::raw('COUNT(id) AS listings')
                                ,  DB::raw("to_char(date_trunc('".$dateFormat['dateTruncName']."', created_at), '".$dateFormat['dateTruncFormat']."' ) AS period"))
                        ->where('created_at', '>=', $from)
                        ->where('created_at', '<=', $to)
                        ->where('new_project', true)
                        ->groupBy(DB::raw('2'))
                        ->get()->pluck('listings', 'period')->all();

        //var_dump($new_project);

        ksort($list);

        $data = [];

        $labels = [];

        $grouped_listings = [];

        foreach ($list as $stat) {
            $grouped_listings[$stat->ptype][$stat->period] = $stat->listings;
            $labels[] = $stat->period;
        }

        $labels = array_unique($labels);
        sort($labels);

        $nps = [];

        foreach ($labels as $date) {
            $nps[] = array_key_exists($date, $new_project) ? $new_project[$date] : 0;
        }

        $datasets = [
            [
                'label' => tr('property.condo', 'Condo'),
                'backgroundColor' => '#1ea6d0',
                'data' => $this->getPropertyListingData('cd', $labels, $grouped_listings)
            ],
            [
                'label' => tr('property.apartment', 'Apartment'),
                'backgroundColor' => '#7eb4c0',
                'data' => $this->getPropertyListingData('ap', $labels, $grouped_listings)
            ],
            [
                'label' => tr('property.house', 'House'),
                'backgroundColor' => '#b963ea',
                'data' => $this->getPropertyListingData('sh', $labels, $grouped_listings)
            ],
            [
                'label' => tr('property.townhouse', 'Towhnouse'),
                'backgroundColor' => '#e963a8',
                'data' => $this->getPropertyListingData('th', $labels, $grouped_listings)
            ],
            [
                'label' => tr('property.new-project', 'New project'),
                'backgroundColor' => '#7fe963',
                'data' => $nps
            ],

        ];

        $data['datasets'] = $datasets;

        $data['labels'] = [];

        foreach($labels as $label) {
            $data['labels'][] = date($dateFormat['LabelFormat'], strtotime($label));
        }

        return response()->json($data);
    }

    public function getTotalCommentData(Request $request){

      $options = $request->all();
      $from = updateDate('from', $options);
      $to = updateDate('to', $options);
      $showBy = $request->input('showBy');
      $dateFormat = $this->getDateFormat($showBy);

      $list = DB::table('customer_reviews')
      ->join('properties', 'properties.id', '=', 'customer_reviews.property_id')
                      ->select(DB::raw('COUNT(customer_reviews.id) AS listings')
                              , DB::raw("to_char(date_trunc('".$dateFormat['dateTruncName']."', customer_reviews.created_at), '".$dateFormat['dateTruncFormat']."' ) AS period")
                              , 'properties.ptype')
                      ->where('customer_reviews.created_at', '>=', $from)
                      ->where('customer_reviews.created_at', '<=', $to)
                      ->groupBy(DB::raw('2'), DB::raw('3'))
                      ->get()->all();

      ksort($list);

      $data = [];

      $labels = [];

      $grouped_listings = [];

      foreach ($list as $stat) {
        $grouped_listings[$stat->ptype][$stat->period] = $stat->listings;
        $labels[] = $stat->period;
      }

      $labels = array_unique($labels);
      sort($labels);

      $datasets = [
          [
              'label' => tr('property.condo', 'Condo'),
              'backgroundColor' => '#1ea6d0',
              'data' => $this->getPropertyListingData('cdb', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.apartment', 'Apartment'),
              'backgroundColor' => '#7eb4c0',
              'data' => $this->getPropertyListingData('ap', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.house', 'House'),
              'backgroundColor' => '#b963ea',
              'data' => $this->getPropertyListingData('sh', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.townhouse', 'Towhnouse'),
              'backgroundColor' => '#e963a8',
              'data' => $this->getPropertyListingData('th', $labels, $grouped_listings)
          ]
      ];

      $data['datasets'] = $datasets;

      $data['labels'] = [];

      foreach($labels as $label) {
          $data['labels'][] = date($dateFormat['LabelFormat'], strtotime($label));
      }

      return response()->json($data);

    }

    public function getPreferedLanguage(Request $request){

      $options = $request->all();
      $from = updateDate('from', $options);
      $to = updateDate('to', $options);
      $showBy = $request->input('showBy');
      $dateFormat = $this->getDateFormat($showBy);

      $list = DB::table('users')
                      ->select(DB::raw('COUNT(id) AS listings')
                              ,  DB::raw("to_char(date_trunc('".$dateFormat['dateTruncName']."', created_at), '".$dateFormat['dateTruncFormat']."' ) AS period")
                              , 'lang')
                      ->where('created_at', '>=', $from)
                      ->where('created_at', '<=', $to)
                      ->groupBy(DB::raw('2'), DB::raw('3'))
                      ->get()->all();
      //var_dump($list);
      ksort($list);

      $data = [];

      $labels = [];

      $grouped_listings = [];

      foreach ($list as $stat) {
        $grouped_listings[$stat->lang][$stat->period] = $stat->listings;
        $labels[] = $stat->period;
      }

      $labels = array_unique($labels);
      sort($labels);

      $datasets = [
          [
              'label' => tr('property.condo', 'Thai'),
              'backgroundColor' => '#6383e8',
              'data' => $this->getPropertyListingData('th', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.apartment', 'English'),
              'backgroundColor' => '#fd3b5e',
              'data' => $this->getPropertyListingData('en', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.house', 'Japanese'),
              'backgroundColor' => '#edff53',
              'data' => $this->getPropertyListingData('ja', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.townhouse', 'Chinese'),
              'backgroundColor' => '#63d8e9',
              'data' => $this->getPropertyListingData('zh', $labels, $grouped_listings)
          ],
          [
              'label' => tr('property.townhouse', 'Korean'),
              'backgroundColor' => '#898989',
              'data' => $this->getPropertyListingData('ko', $labels, $grouped_listings)
          ]
      ];

      $data['datasets'] = $datasets;

      $data['labels'] = [];

      foreach($labels as $label) {
          $data['labels'][] = date($dateFormat['LabelFormat'], strtotime($label));
      }

      return response()->json($data);
    }

    protected function getPropertyListingData($type, $labels, $grouped_listings) {
        $_data = array_key_exists($type, $grouped_listings) ? $grouped_listings[$type] : [];
        $values = [];

        foreach ($labels as $date) {
            $values[] = array_key_exists($date, $_data) ? $_data[$date] : 0;
        }

        return $values;
    }

}
