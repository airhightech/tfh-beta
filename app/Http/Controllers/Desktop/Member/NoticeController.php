<?php

namespace App\Http\Controllers\Desktop\Member;

use Auth;
use DB;

class NoticeController extends BaseController {
    
    public function index() {
        
        $user = Auth::user();
        
        $notifications = DB::table('notifications')->select('notifications.*', 'notification_receivers.read')
                ->leftJoin('notification_receivers', 'notification_receivers.message_id', 'notifications.id')
                            ->where('notification_receivers.agent_id', $user->id)
                ->orderBy('notification_receivers.created_at', 'desc')
                ->paginate(20);
        
        return view('desktop.member.notice.list', ['notifications' => $notifications]);
    }
    
}