<?php

namespace App\Http\Controllers\Desktop\Member;

use Auth;

class AccountController extends BaseController {
    
    public function index() {
        
        $user = Auth::user();
        
        return view('desktop.member.account.visitor', ['user' => $user]);
        
    }
    
}