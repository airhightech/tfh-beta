<?php

namespace App\Http\Controllers\Desktop\Member\Listing;

use App\Models\Geo\Province;
use App\Models\Property;
use App\Models\User\Profile;
use App\Models\Geo\Place;
use App\Models\Property\Facility;
use App\Models\Property\Feature;
use Illuminate\Support\Facades\Log;
use Auth;

trait PostingTrait {

    public function type() {

        $user = Auth::user();

        if ($user->ctype != 'visitor') {
            $lang = \App::getLocale();
            $collate = Province::getLangCollation();
            $provinces = Province::orderByRaw("name->>'$lang' ASC")->get();

            return view('desktop.member.posting.type', ['provinces' => $provinces]);
        } else {
            redirect('/auth/register/type');
        }
    }

    public function details($id) {

        $property = Property::findOrFail($id);
        $view = $this->getViewFile($property);

        $query = Profile::leftJoin('users', 'users.id', 'profiles.user_id')
                ->where('users.ctype', 'developer')
                ->whereNull('users.deleted_at');

        $developers = $query->get();

        $bts_list = Place::where('type', 'bts')->get()->all();
        $mrt_list = Place::where('type', 'mrt')->get()->all();
        $apl_list = Place::where('type', 'apl')->get()->all();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');

        $facilities = Facility::all();
        $features = Feature::where('optional', false)->get();
        $optional_features = Feature::where('optional', true)->get();

        $selected_facilities = $property->getFacilitiesID();
        $selected_features = $property->getFeaturesID();

        if ($view) {
            return view($view, [
                'images' => [],
                'status' => null,
                'property' => $property,
                'developers' => $developers,
                'bts_list' => $bts_list,
                'mrt_list' => $mrt_list,
                'apl_list' => $apl_list,
                'selected_bts_list' => $selected_bts_list,
                'selected_mrt_list' => $selected_mrt_list,
                'selected_apl_list' => $selected_apl_list,
                'facilities' => $facilities,
                'features' => $features,
                'optional_features' => $optional_features,
                'selected_facilities' => $selected_facilities,
                'selected_features' => $selected_features
            ]);
        } else {
            Log::info("View not found for property [{$property->id}] with type [{$property->ptype}]");
            abort(404);
        }
    }

    protected function getViewFile($property, $page = 'posting') {
        if ($property->ptype == Property::TYPE_CONDO_ROOM) {
            return 'desktop.member.' . $page . '.condo';
        } else if ($property->ptype == Property::TYPE_APARTMENT_ROOM) {
            return 'desktop.member.' . $page . '.apartment';
        } else if ($property->ptype == Property::TYPE_SINGLE_HOUSE) {
            return 'desktop.member.' . $page . '.house';
        } else if ($property->ptype == Property::TYPE_TOWNHOUSE) {
            return 'desktop.member.' . $page . '.townhouse';
        }
    }

    public function createCondo() {
        return view('desktop.member.posting.condo');
    }

    public function createHouse() {
        return view('desktop.member.posting.house');
    }

    public function createHownhouse() {
        return view('desktop.member.posting.townhouse');
    }

}
