<?php

namespace App\Http\Controllers\Desktop\Member;

use App\Models\Listings\Package;
use App\Models\System\Bank;
use App\Models\Customer\Purchase;
use Auth;
use DB;

class PurchaseController extends BaseController {

    public function index() {

        $user = Auth::user();
        $account = $user->getPurchaseAccount();
        $purchases = Purchase::where('account_id', $account->id)->orderBy('updated_at', 'desc')->paginate(10);

        return view('desktop.member.purchase.list'
                , ['purchases' => $purchases]);
    }

    public function getExclusive() {

        /**/
        
        $exclusive_step_1 = json_decode(DB::table('settings')->where('token', 'exclusive_step_1')->value('value'), true);
        $exclusive_step_2 = json_decode(DB::table('settings')->where('token', 'exclusive_step_2')->value('value'), true);

        $packages = Package::where('type', Package::TYPE_EXCLUSIVE)->orderBy('normal_price', 'asc')->get();
        $banks = Bank::all();
        return view('desktop.member.purchase.exclusive', [
            'packages' => $packages,
            'banks' => $banks,
            'exclusive_step_1' => $exclusive_step_1,
            'exclusive_step_2' => $exclusive_step_2
        ]);
    }

    public function getFeatured() {

        $featured_step_1 = json_decode(DB::table('settings')->where('token', 'featured_step_1')->value('value'), true);
        $featured_step_2 = json_decode(DB::table('settings')->where('token', 'featured_step_2')->value('value'), true);

        $packages = Package::where('type', Package::TYPE_FEATURED)->orderBy('normal_price', 'asc')->get();
        $banks = Bank::all();
        return view('desktop.member.purchase.featured', [
            'packages' => $packages,
            'banks' => $banks,
            'featured_step_1' => $featured_step_1,
            'featured_step_2' => $featured_step_2
        ]);
    }

    public function contactAdmin() {
        return view('desktop.member.purchase.contact-admin');
    }

}
