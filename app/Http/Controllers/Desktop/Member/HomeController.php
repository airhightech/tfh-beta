<?php

namespace App\Http\Controllers\Desktop\Member;

class HomeController extends BaseController {
    
    public function index() {
        return redirect('member/account');
    }
    
}