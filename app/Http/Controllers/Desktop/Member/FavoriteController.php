<?php

namespace App\Http\Controllers\Desktop\Member;

use App\Models\Property;
use Auth;
use DB;

class FavoriteController extends BaseController {

    public function index() {

        $user = Auth::user();

        if ($user->ctype == 'visitor') {
            $favorites = Property::leftJoin('favorite_properties', 'favorite_properties.property_id', 'properties.id')
                    ->where('favorite_properties.user_id', $user->id)
                    ->paginate(20);
            


            return view('desktop.member.favorite.visitor', ['favorites' => $favorites]);
            
        } else {
            $favorites = [];
            $list = [];

            $property_ids = DB::table('properties')->where('publisher_id', $user->id)->pluck('id')->all();

            if (count($property_ids)) {
                $list = DB::table('favorite_properties')->select('property_id', DB::raw('COUNT(user_id) AS fans'))
                        ->whereIn('property_id', $property_ids)
                        ->groupBy('property_id')
                        ->paginate(20);

                foreach ($list as $favorite) {
                    $property = Property::find($favorite->property_id);
                    $property->fans = $favorite->fans;
                    $favorites[] = $property;
                }
            }


            return view('desktop.member.favorite.list', ['favorites' => $favorites, 'list' => $list]);
        }
    }

}
