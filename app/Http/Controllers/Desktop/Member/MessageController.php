<?php

namespace App\Http\Controllers\Desktop\Member;

use App\Models\Messages\Message;
use Auth;

class MessageController extends BaseController {
    
    public function index() {
        
        $user = Auth::user();
        
        $messages = Message::where(function($query) use ($user){
            $query->orWhere('agent_id', $user->id);
            $query->orWhere('sender_id', $user->id);
        })->orderBy('created_at', 'desc')->paginate(20);
        
        return view('desktop.member.message.list', ['messages' => $messages]);
    }
    
}