<?php

namespace App\Http\Controllers\Desktop\Member;

use App\Models\Property;
use Auth;
use App\Models\Property\Facility;
use App\Models\Property\Feature;
use App\Models\Geo\Province;
use App\Models\Geo\District;
use App\Models\Geo\Area;
use App\Models\User\Profile;
use App\Models\Geo\Place;

class PropertyController extends BaseController {

    public function info($id) {

        $user = Auth::user();
        $property = Property::find($id);
        if ($user->id != $property->publisher_id) {
            abort(403);
        }

        $lang = \App::getLocale();
        $collate = Province::getLangCollation();
        $provinces = Province::orderByRaw("name->>'$lang' ASC")->get();
        $address = $property->getAddress();
        $districts = [];
        $areas = [];

        if ($address->province_id > 0) {
            $districts = District::where('province_id', $address->province_id)->orderByRaw("name->>'$lang' ASC")->get();
        }

        if ($address->district_id > 0) {
            $areas = Area::where('district_id', $address->district_id)->orderByRaw("name->>'$lang' ASC")->get();
        }

        $condos = Property::where('ptype', 'cdb')
                        ->where('published', true)
                        ->where('new_project', true)
                        ->orderByRaw("name->>'$lang' ASC")->get();
        
        $query = Profile::leftJoin('users', 'users.id', 'profiles.user_id')
                ->where('users.ctype', 'developer')
                ->whereNull('users.deleted_at');
                
        $developers = $query->get();

        return view('desktop.member.property.info'
                , [
            'property' => $property
            , 'provinces' => $provinces
            , 'address' => $address
            , 'address' => $address
            , 'districts' => $districts
            , 'areas' => $areas
            , 'condos' => $condos
            , 'developers' => $developers
        ]);
    }

    public function details($id) {
        $user = Auth::user();
        $property = Property::find($id);
        if ($user->id != $property->publisher_id) {
            abort(403);
        }

        $facilities = Facility::all();
        $property_facilities = $property->getFacilitiesID();

        $features = Feature::where('optional', false)->get();
        $property_features = $property->getFeaturesID();

        $optional_features = Feature::where('optional', true)->get();

        $query = Profile::leftJoin('users', 'users.id', 'profiles.user_id')
                ->where('users.ctype', 'developer')
                ->whereNull('users.deleted_at');
                
        $developers = $query->get();
        
        $bts_list = Place::where('type', 'bts')->get()->all();
        $mrt_list = Place::where('type', 'mrt')->get()->all();
        $apl_list = Place::where('type', 'apl')->get()->all();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');

        return view('desktop.member.property.details', [
            'property' => $property
            , 'view' => $this->getDetailsViewFile($property)
            , 'facilities' => $facilities
            , 'property_facilities' => $property_facilities
            , 'features' => $features
            , 'bts_list' => $bts_list
            , 'mrt_list' => $mrt_list
            , 'apl_list' => $apl_list
            , 'selected_bts_list' => $selected_bts_list
            , 'selected_mrt_list' => $selected_mrt_list
            , 'selected_apl_list' => $selected_apl_list
            , 'optinal_features' => $optional_features
            , 'property_features' => $property_features
            , 'developers' => $developers]);
    }

    public function media($id) {
        $user = Auth::user();
        $property = Property::find($id);
        if ($user->id != $property->publisher_id) {
            abort(403);
        }
        return view('desktop.member.property.media', ['property' => $property]);
    }
    
    protected function getDetailsViewFile($property) {
        $listing = $property->getListingData();
        
        $ltype = $listing->ltype ? $listing->ltype : 'rent';
        
        if ($property->ptype == Property::TYPE_CONDO_ROOM) {
            return 'desktop.member.property.details-'.$ltype.'.condo';
        } else if ($property->ptype == Property::TYPE_APARTMENT_ROOM) {
            return 'desktop.member.property.details-'.$ltype.'.apartment';
        } else if ($property->ptype == Property::TYPE_SINGLE_HOUSE) {
            return 'desktop.member.property.details-'.$ltype.'.house';
        } else if ($property->ptype == Property::TYPE_TOWNHOUSE) {
            return 'desktop.member.property.details-'.$ltype.'.townhouse';
        }
    }
    
    public function getPostingMedia($id) {
        $property = Property::find($id);
        return view('desktop.member.posting.generic.media', ['property' => $property]);
    }

}
