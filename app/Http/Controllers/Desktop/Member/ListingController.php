<?php

namespace App\Http\Controllers\Desktop\Member;

use App\Http\Controllers\Desktop\Member\Listing\PostingTrait;
use App\Models\Property;
use Auth;
use App\Models\Property\Facility;
use App\Models\Property\Feature;
use App\Models\Geo\Place;
use DB;
use Illuminate\Http\Request;

class ListingController extends BaseController {

    use PostingTrait;

    public function index(Request $request) {

        $user = Auth::user();

        $filters = $this->getListFilters($request);
        $lt = $request->input('lt', 'all');
        $pt = $request->input('pt', 'all');
        $class = $request->input('class', 'all');

        $query = Property::where('properties.publisher_id', $user->id)
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->whereIn('properties.ptype', ['ap', 'cd', 'sh', 'th'])
                ->whereIn('property_listing_data.ltype', ['rent', 'sale']);

        if ($lt && $lt != 'all') {
            $query->where('property_listing_data.ltype', $lt);
        }
        
        if ($pt && $pt != 'all') {
            $query->where('properties.ptype', $pt);
        }
        
        if ($class && $class != 'all') {
            $query->where('property_listing_data.listing_class', $class);
        }
        
        if ($filters['status'] !== 'all') {
            if ($filters['status'] == 'present') {
                $query->where('properties.published', true);
            } else {
                $query->where('properties.published', false);
            }
        }

        $query->orderBy('properties.created_at', 'desc');

        $properties = $query->paginate(10);

        return view('desktop.member.property.list', [
            'properties' => $properties
            , 'user' => $user
            , 'filters' => $filters
        ]);
    }
    
    protected function getListFilters(Request $request) {
        return [
            'lt' => $request->input('lt', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all'),
            'pt' => $request->input('pt', 'all')
        ];
    }

    public function preview($id) {

        $property = Property::findOrFail($id);
        //$user = Auth::user();

        /* if ($property->publisher_id != $user->id) {
          abort(404);
          } */

        $facilities = Facility::all();
        $features = Feature::where('optional', false)->get();
        $optional_features = Feature::where('optional', true)->get();

        $selected_facilities = $property->getFacilitiesID();
        $selected_features = $property->getFeaturesID();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');

        $bts_list = Place::where('type', 'bts')->whereIn('id', $selected_bts_list)->get()->all();
        $mrt_list = Place::where('type', 'mrt')->whereIn('id', $selected_mrt_list)->get()->all();
        $apl_list = Place::where('type', 'apl')->whereIn('id', $selected_apl_list)->get()->all();

        $view = $this->getViewFile($property, 'preview');

        return view($view, [
            'property' => $property,
            'facilities' => $facilities,
            'features' => $features,
            'optional_features' => $optional_features,
            'selected_facilities' => $selected_facilities,
            'selected_features' => $selected_features,
            'bts_list' => $bts_list,
            'mrt_list' => $mrt_list,
            'apl_list' => $apl_list
        ]);
    }

    public function getExclusiveUpgrade($id) {

        /* $exclusive = $this->countCurrentExclusive();

          if ($exclusive >= 9) {
          return view('desktop.member.upgrade.full');
          } else {
          $property = Property::findOrFail($id);
          $user = Auth::user();
          $exclusives = $user->getAvailableExclusive();
          return view('desktop.member.upgrade.exclusive', ['property' => $property, 'exclusives' => $exclusives]);
          } */

        $property = Property::findOrFail($id);
        $user = Auth::user();
        $exclusives = $user->getAvailableExclusive();
        return view('desktop.member.upgrade.exclusive', ['property' => $property, 'exclusives' => $exclusives]);
    }

    public function getFeaturedUpgrade($id) {
        $property = Property::findOrFail($id);
        $user = Auth::user();
        $featureds = $user->getAvailableFeatured();
        $free_featureds = $user->getAvailableFreeFeatured();

        return view('desktop.member.upgrade.featured', [
            'property' => $property
            , 'featureds' => $featureds
            , 'free_featureds' => $free_featureds]);
    }

    protected function countCurrentExclusive() {
        $count_exclusive = DB::table('property_listing_data')
                        ->where('listing_class', 'exclusive')
                        ->where(function($query) {
                            $query->orWhere('expiration_date', '>', date('Y-m-d'));
                            $query->orWhereNull('expiration_date');
                        })->count();

        return $count_exclusive;
    }

}
