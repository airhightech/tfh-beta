<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Listings\ExclusiveProject;
use App\Models\Listings\ExclusiveListing;

class HomeController extends BaseController {
    
    public function index() {
        
        $exclusive = ExclusiveProject::get(3);
        $featured_rent = ExclusiveListing::get('rent');
        $featured_sale = ExclusiveListing::get('sale');
        
        return view('mobile.home.index', [
            'exclusive' => $exclusive,
            'featured_rent' => $featured_rent,
            'featured_sale' => $featured_sale
        ]);
    }
    
}