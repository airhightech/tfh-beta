<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Property;
use App\Models\Property\Facility;
use App\Models\Property\Feature;
use Illuminate\Http\Request;
use App\Helpers\Language;
use App\Models\Reviews\UserReview;
use Auth;
use App\Models\Geo\Place;
use App\Services\SearchProperty;
use DB;

class PropertyController extends BaseController {

    public function search(Request $request) {

              $query = $request->input('q');
              $id = $request->input('id');

              $place = null;

              $all = $request->all();

              $pts = explode(',', $request->input('pt', ''));
              $np = $request->input('np');
              $type = null;

              if ($id > 0) {

                  $lntlat = null;

                  $rel = $request->input('rel');

                  $supported = Place::getSupported();

                  if (in_array($rel, $supported)) {
                      $place = Place::find($id);
                      if ($place) {
                          $lntlat = $place->getLngLat();
                      }
                      $all['domain'] = 'place';
                      $type = $place->type;
                  } else {
                      $property = Property::find($id);

                      $all['domain'] = 'property';

                      if ($property) {
                          $address = $property->getAddress();

                          if ($address) {
                              $lntlat = $address->getLngLat();
                          }

                          // Update search filters

                          $listing = $property->getListingData();

                          $all['lt'] = $listing->ltype;

                          if ($property->new_project) {
                              $all['np'] = 'yes';
                          } else {
                              $all['np'] = '';
                          }
                      }
                  }

                  if ($lntlat) {
                      $place = [
                          'id' => $id,
                          'lng' => $lntlat->lng,
                          'lat' => $lntlat->lat,
                          'type' => $type
                      ];
                  }
              }

              if (in_array('cdb', $pts) || $np == 'yes') {
                  $all['lt'] = '';
              }

              return view('mobile.property.search'
                      , ['all' => $all, 'place' => $place, 'query' => $query]);
    }

    public function getDetails($id) {

        $property = Property::findOrFail($id);

        $view = $this->getView($property);

        $selected_facilities = $property->getFacilitiesID();
        $selected_features = $property->getFeaturesID();

        $facilities = Facility::all();
        $features = Feature::where('optional', false)->get();
        $optional_features = Feature::where('optional', true)->get();

        $reviews = UserReview::leftJoin('users', 'users.id', '=', 'customer_reviews.customer_id')
                    ->where('property_id', $id)->orderBy('customer_reviews.created_at', 'desc')->get();

        return view('mobile.property.details.' . $view, [
            'property' => $property,
            'selected_facilities' => $selected_facilities,
            'selected_features' => $selected_features,
            'facilities' => $facilities,
            'features' => $features,
            'optional_features' => $optional_features,
            'reviews' => $reviews
        ]);
    }

    protected function getView($property) {
        $view = $property->ptype;
        switch ($property->ptype) {
            case Property::TYPE_APARTMENT_ROOM:
                $view = 'apartment';
                break;
            case Property::TYPE_CONDO_ROOM:
                $view = 'condo';
                break;
            case Property::TYPE_CONDO_BUILDING:
                $view = 'condo-com';
                break;
            case Property::TYPE_SINGLE_HOUSE:
                $view = 'house';
                break;
            case Property::TYPE_TOWNHOUSE:
                $view = 'townhouse';
                break;
        }

        return $view;
    }

    public function updateReviewDetails(Request $request) {
        $review = ManagerReview::findOrFail($request->input('id'));
        $review->updateFromRequest($request);

        return response()->json(['status' => 'OK']);
    }

    public function postUserReview(Request $request) {
        if (Auth::check()) {
          $review = new UserReview;
          $review->updateFromRequest($request);
          $review->save();
          return redirect('/property/'.$request->input('id'));
        }
        else{
          return redirect('/property/'.$request->input('id').'?login=false');
        }
    }

    public function searchList(Request $request) {
        $finder = new SearchProperty($request);
        $results = $finder->findForListing();
        $all = $request->all();
        $q = $request->input('q');
        $favorites = [];

        if (Auth::check()) {
                $user = Auth::user();
                $favorites = DB::table('favorite_properties')
                        ->where('user_id', $user->id)
                        ->pluck('property_id');
            }

                    /*return response()->json([
                                'status' => 'OK'
                                , 'properties' => $results, 'favorites' => $favorites]
                                    , 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);*/
        return view('mobile.property.list', ['properties' => $results, 'all' => $all, 'q' => $q]);
    }

}
