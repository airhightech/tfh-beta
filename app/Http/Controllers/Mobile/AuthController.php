<?php

namespace App\Http\Controllers\Mobile;

use Socialite;
use App\Services\UserProvider;
use App\Http\Controllers\Common\SocialLoginTrait;
use Auth;
use Cookie;

class AuthController extends BaseController {

  use SocialLoginTrait;

    public function getLogin() {

        return view('mobile.auth.login');

    }

    public function register() {

        return view('mobile.auth.register');

    }

    public function logout() {

        Auth::logout();
        return redirect('/')->withCookie(Cookie::forget('laravel_session'));
  }

    public function redirectToFacebook() {

        return Auth::check() ? redirect('/') : Socialite::driver('facebook')
            ->redirectUrl('http://m.thaifullhouse.com/callback-facebook')->redirect();
    }

    public function handleFacebookCallback() {

        $fbuser = Socialite::driver('facebook')->redirectUrl('http://m.thaifullhouse.com/callback-facebook')->user();


        $user = UserProvider::findOrCreateFromFB($fbuser);

        Auth::login($user, false);

        return redirect('/');
    }

}
