<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Property;
use Auth;
use DB;

class UserController extends BaseController {

    public function getProfile() {
      $user = Auth::user();
      return view('mobile.user.account.visitor', ['user' => $user]);
    }

    public function getPage() {
        return view('user.page');
    }

    public function getFavorites() {

        $favorites = [];

        if (Auth::check()) {
            $user = Auth::user();
            $ids = DB::table('favorite_properties')
                    ->where('user_id', $user->id)
                    ->orderby('created_at', 'desc')
                    ->limit(10)
                    ->pluck('property_id');

            $favorites = Property::whereIn('id', $ids)->get();
        }

        return view('mobile.user.favorites', ['favorites' => $favorites]);
    }

    public function getNotif() {

        $user = Auth::user();

        $notifications = DB::table('notifications')->select('notifications.*', 'notification_receivers.read')
                ->leftJoin('notification_receivers', 'notification_receivers.message_id', 'notifications.id')
                            ->where('notification_receivers.agent_id', $user->id)
                ->orderBy('notification_receivers.created_at', 'desc')
                ->paginate(20);

        return view('mobile.user.notice', ['notifications' => $notifications]);
    }
}
