<?php

namespace App\Http\Controllers\Mobile;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Reviews\AgentReview;

class AgentController extends BaseController {

    public function search(Request $request) {

        $search = $request->all();

        $query = User::leftJoin('publisher_company_profiles', 'publisher_company_profiles.user_id', 'users.id');
        $query->leftJoin('personal_profiles', 'personal_profiles.user_id', 'users.id');

        $typ = strtolower($request->input('typ', 'all'));

        // Type

        if ($typ == 'agent') {
            $query->where('users.ctype', 'agent');
        } else if ($typ == 'company') {
            $query->where('users.ctype', 'company');
        } else {
            $query->whereIn('users.ctype', ['agent', 'company']);
        }
        // Name

        $name = trim(strtolower($request->input('nam', '')));

        if (!empty($name)) {
            $query->where(function ($query) use ($name) {
                $query->orWhere('publisher_company_profiles.company_name', '~*', $name);
                $query->orWhere('personal_profiles.firstname', '~*', $name);
                $query->orWhere('personal_profiles.lastname', '~*', $name);
            });
        }

        $query->where('users.verified', true);


        $query->where('users.active', true);

        // Order

        $query->orderBy('users.listing_level', 'ASC');

        $agents = $query->paginate(5);
        return view('mobile.agent.search', ['search' => $search, 'agents' => $agents]);
    }

    public function details(Request $request, $id) {

        $user = User::where('id', $id)->where('verified', true)->firstOrfail();

        $tab = $request->input('tab');

        $reviews = AgentReview::leftJoin('users', 'users.id', '=', 'agent_reviews.user_id')
                ->where('agent_reviews.agent_id', $id)
                ->orderBy('agent_reviews.created_at', 'desc')
                ->paginate(5);

        return view('mobile.agent.details', ['user' => $user, 'tab' => $tab, 'reviews' => $reviews]);
    }

}
