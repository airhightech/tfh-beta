<?php

namespace App\Http\Controllers\Mobile;

use App\Models\System\StaticPage;

class PageController extends BaseController {
    
    public function index($sku) {
        
        $page = StaticPage::where('sku', $sku)->firstOrFail();
        
        return view('mobile.page.index', [
            'page' => $page
        ]);
        
    }
    
}