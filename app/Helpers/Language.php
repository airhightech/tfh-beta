<?php

namespace App\Helpers;

class Language {
    
    protected $code;
    
    public function __construct($code) {
        $this->code = $code;
    }
    
    public function getSmallFlag() {
        $flags = [
            'th' => '/img/flags/th.png', 
            'en' => '/img/flags/gb.png', 
            'ja' => '/img/flags/jp.png', 
            'zh' => '/img/flags/cn.png', 
            'ko' => '/img/flags/kr.png'
        ];
        
        return isset($flags[$this->code]) ? $flags[$this->code] : false;
    }

    public static function getActives() {
        return [
            'th' => 'ภาษาไทย', 
            'en' => 'English',  
            'ja' => '日本語',
            'zh' => '中文(简体)',
            'ko' => '한국어'
        ];
    }
    
    public static function getActiveCodes() {
        return array_keys(self::getActives());
    }
}
