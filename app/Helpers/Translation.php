<?php

namespace App\Helpers;

use DB;
use App\Models\Geo\Place;

class Translation {
    
    public static function get($token, $default, $description = '') {
        if (preg_match('/(\w+)(\.)(.+)/', $token, $matches)) {

            //$locale = \App::getLocale();

            $translation = trans('_generated_' . $token);

            if ($translation != '_generated_' . $token) {
                return empty($translation) ? $default : $translation;
            }

            $text = DB::table('translations')
                    ->where('section', $matches[1])
                    ->where('label', $matches[3])
                    ->value('translation');

            if (empty($text)) {

                $langs = array_keys(Language::getActives());

                $now = date('Y-m-d H:i:s');

                $description = empty($description) ? $default : $description;

                DB::table('translations')
                        ->insert([
                            'section' => $matches[1],
                            'label' => $matches[3],
                            'translation' => json_encode(array_fill_keys($langs, '')),
                            'description' => $description,
                            'created_at' => $now,
                            'updated_at' => $now
                ]);

                return empty($default) ? $translation : $default;
            } else {
                return $default ? $default : trans($token);
            }
        } else {
            return $default ? $default : trans($token);
        }
    }
    
    public static function getFrontTranslations() {
        $trans = [
            'select-user-type' => tr('front.select-user-type', 'Please select your profile type'),
            'fill-required' => tr('front.fill-required', 'Please fill all required fields'),
            'required' => tr('front.required', 'This field is required'),
            'please-login' => tr('front.please-login', 'Please login to use this feature'),
            'listing_rent' => tr('map_search.listing.for-rent', 'Rent'),
            'listing_sale' => tr('map_search.listing.for-sale', 'Sale'),
            'condo_com' => tr('map_search.options.condo', 'Condo')
        ];
        
        $supported = Place::getSupported();
        
        foreach($supported as $place) {
            $trans['places_' . $place] = tr('places.' . $place, $place);
        }
        
        return json_encode($trans);
    }
    
}