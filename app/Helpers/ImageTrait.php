<?php

namespace App\Helpers;

use Image;

trait ImageTrait {

    public function getRelativeUrl() {
        return '/image/' . $this->getImageField();
    }

    public function getUrl($default = null) {
        if ($this->imageExists()) {
            return url('/image/' . $this->getImageField());
        } else {
            return $default;
        }
    }

    public function getThumbUrl($dimension = '100x100', $create = false, $default = null) {

        $filepath = $this->getImageField();

        if ($create) {
            $this->createThumbFromFile($dimension, $filepath);
        }

        if ($this->thumbExists($dimension)) {
            return url('/thumb/' . $dimension . '/' . $filepath);
        } else {
            return $default;
        }
    }

    protected function createThumbFromFile($dimension, $file) {

        $img = null;

        if ($file) {
            $filename = 'thumb-' . $dimension . '-' . $file;
            $filepath = storage_path('app/public/' . $filename);

            if (!file_exists($filepath)) {
                $original = storage_path('app/public/' . $file);                
                $img = $this->__createThumb($original, $filepath, $dimension);
                
            } elseif (is_readable($filepath)) {
                $img = Image::make($filepath);
            }
        }

        return $img;
    }

    protected function getImageField() {
        return isset($this->_custom_image_field) ? ($this->{$this->_custom_image_field}) : $this->filepath;
    }

    protected function imageExists() {
        return file_exists(storage_path('app/public/' . $this->getImageField()));
    }

    protected function thumbExists($dimension) {
        return file_exists(storage_path('app/public/thumb-' . $dimension . '-' . $this->getImageField()));
    }

    protected function removeImage($file) {

        $path = storage_path('app/public/*' . $file);

        foreach (glob($path) as $filepath) {
            @unlink($filepath);
        }
    }
    
    public function deleteFiles() {
        
        $path = storage_path('app/public/*' . $this->getImageField());

        foreach (glob($path) as $filepath) {
            @unlink($filepath);
        }
    }

    private function __createThumb($original, $newpath, $dimension) {
        if (file_exists($original) && is_readable($original)) {
            $img = Image::make($original);
            list($width, $height) = explode('x', $dimension);
            $img->fit($width, $height);
            $img->save($newpath);
            
            return $img;
            
        } else {
            
            return null;
        }
    }

}

?>