<?php
/**
 * File asset helper
 */
if (!function_exists('__asset')) {
    function __asset($file) {
        $version = filemtime(public_path() . $file);
        return $file . '?v=' . base_convert($version, 10, 36);
    }
}

if (!function_exists('__arr')) {
    function __arr($array, $key, $default = null) {
        return isset($array[$key]) ?  $array[$key] : $default;
    }
}

/**
 * Translation helper
 */
if (!function_exists('tr')) {

    function tr($token, $default) {
        return App\Helpers\Translation::get($token, $default);
    }

}

/**
 * Traslated fields
 */
if (!function_exists('__trfields')) {

    function __trfields($name, $object = null, $textarea = false, $row = 3, $show_all = false) {
        $langs = \App\Helpers\Language::getActives();

        $default_langs = ['th', 'en'];

        $id = uniqid();

        foreach ($langs as $code => $lang) {

            $value = '';

            if ($object) {
                if (is_array($object)) {
                    $value = array_get($object, $code);
                } else {
                    $value = $object->getTranslatable($name, $code);
                }
            }

            $value = trim($value);

            $visible = in_array($code, $default_langs) || $show_all;

            if ($textarea == true) {
                ?>
                <p class="input-lang input-lang-<?php echo $code; ?> <?php echo $visible ? '' : 'input-lang-hidden input-' . $id; ?>">
                    <label><?php echo $lang; ?></label>
                    <textarea class="form-control named-<?php echo $name; ?> <?php echo $code; ?>" rows="<?php echo $row; ?>" name="<?php echo $name, '[', $code, ']'; ?>"><?php echo $value; ?></textarea>
                </p>
                <?php
            } else {
                ?>
                <p class="input-lang input-lang-<?php echo $code; ?> <?php echo $visible ? '' : 'input-lang-hidden input-' . $id; ?>">
                    <label><?php echo $lang; ?></label>
                    <input class="form-control named-<?php echo $name; ?> <?php echo $code; ?>" name="<?php echo $name, '[', $code, ']'; ?>" value="<?php echo $value; ?>"/>
                </p>
                <?php
            }
        }

        if (!$show_all) {
            ?>
            <p class="text-right">
                <a href="#" class="btn-link show-more-lang" data-target=".input-<?php echo $id; ?>"><?php echo tr('link.show-more-translations', 'More translations'); ?></a>
            </p>
            <?php
        }
    }

}

if (!function_exists('__select')) {

    function __select($data, $expected, $value) {
        echo $data == $expected ? $value : '';
    }

}

if (!function_exists('__banner')) {

    function __banner($location) {

    }

}

if (!function_exists('__icon')) {

    function __icon($code, $default = "") {
        $filepath = DB::table('icons')->where('code', $code)->value('filepath');
        
        return $filepath ? '/image/' . $filepath : $default;
    }

}

if (!function_exists('__trimg')) {

    function __trimg($domain, $lang = null, $default_lang = 'en') {
        if ($lang == null) {
            $lang = App::getLocale();
        }

        $setting = DB::table('settings')->where('token', $domain)->value('value');

        if ($setting) {
            $data = json_decode($setting, true);
            $img = array_get($data, $lang);

            if (empty($img)) {
                $img = array_get($data, $default_lang);
            }
            $defaults = array_filter(array_values($data));
            return $img ? $img : array_shift($defaults);
        } else {
            return null;
        }
    }

}

if (!function_exists('trget')) {

    function trget($data, $lang = null, $default_lang = 'en') {

        if ($lang == null) {
            $lang = App::getLocale();
        }

        $tr = trim(array_get($data, $lang));
        return $tr ? $tr : array_get($data, $default_lang);
    }

}

if (!function_exists('__dbconf')) {

    function __dbconf($token, $translate = false) {

        $conf = DB::table('settings')
                ->where('token', $token)
                ->value('value');

        if ($translate) {
            $locale = App::getLocale();

            $translations = json_decode($conf, true);

            $conf = trim(array_get($translations, $locale));

            if (empty($conf)) {
                $conf = trim(array_get($translations, 'en'));
            }
        }

        return $conf;
    }

}

if (!function_exists('__pgdate2hr')) {

    function __pgdate2hr($date, $format = 'd/m/Y') {

        $time = strtotime($date);

        return $time > 1 ? date($format, $time) : date($format);
    }

}

if (!function_exists('__knum')) {

    function __knum($value) {
        $result = 0;
        if ($value > 999 && $value <= 999999) {
            $result = floor($value / 1000) . 'K';
        } elseif ($value > 999999) {
            $result = floor($value / 1000000) . 'M';
        } else {
            $result = $value;
        }
        return $result;
    }

}

if (!function_exists('__distance')) {

    function __distance($value) {
        $result = 0;
        if ($value > 999 && $value <= 999999) {
            $result = sprintf("%.1f", $value / 1000) . 'K';
        } elseif ($value > 999999) {
            $result = sprintf("%.1f", $value / 1000000) . 'M';
        } else {
            $result = round($value);
        }
        return $result;
    }

}

if (!function_exists('sortable_col')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function sortable_col($text, $link, $col, $default, $current) {

        if ($current[0] == $col) {
            ?>
            <a href="<?php echo $link; ?>?sortby=<?php echo $current[0]; ?>,<?php echo $current[1] == 'asc' ? 'desc' : 'asc'; ?>"
               class="sortable">
                   <?php
                   echo $text;
                   if ($current[1] == 'asc') {
                       ?>
                    <i class="fa fa-angle-up"></i>
                    <?php
                } else {
                    ?>
                    <i class="fa fa-angle-down"></i>
                    <?php
                }
                ?>
            </a>            
            <?php
        } else {
            ?>
            <a href="<?php echo $link; ?>?sortby=<?php echo $col; ?>,<?php echo $default; ?>">
                <?php echo $text; ?>
            </a>
            <?php
        }
    }

}

function max_size() {
    return formatBytes(file_upload_max_size());
}

function file_upload_max_size() {
    static $max_size = -1;

    if ($max_size < 0) {
        // Start with post_max_size.
        $max_size = parse_size(ini_get('post_max_size'));

        // If upload_max_size is less, then reduce. Except if upload_max_size is
        // zero, which indicates no limit.
        $upload_max = parse_size(ini_get('upload_max_filesize'));
        if ($upload_max > 0 && $upload_max < $max_size) {
            $max_size = $upload_max;
        }
    }
    return $max_size;
}

function parse_size($size) {
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
        // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
        return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    } else {
        return round($size);
    }
}

function formatBytes($size, $precision = 2) {
    $base = log($size, 1024);
    $suffixes = array('', 'K', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
}

function render_rating($key, $values, $max = 5) {
    $rating = array_key_exists($key, $values) ? $values[$key] : 0;
    for ($i = 1; $i <= $max; $i++) {
        if ($rating >= $i) {
            ?>
            <span class="vote full" title="<?php echo $i; ?>"><i class="fa fa-star"></i></span>
            <?php
        } else {
            ?>
            <span class="vote" title="<?php echo $i; ?>"><i class="fa fa-star-o"></i></span>
            <?php
        }
    }
}

function render_rating_mobile($key, $values, $min = 1) {
    $rating = array_key_exists($key, $values) ? $values[$key] : 1;
    for ($i = 5; $i >= $min; $i--) {
        if ($rating == $i) {
            ?>
            <input disabled checked type="radio" value="<?php echo $i; ?>" /><label class="full" title="<?php echo $i; ?>"></label>
            <?php
        } else {
            ?>
            <input disabled type="radio" value="<?php echo $i; ?>" /><label class="full" title="<?php echo $i; ?>"></label>
            <?php
        }
    }
}

function render_simple_rating($rating, $max = 5, $showall = false) {
    for ($i = 1; $i <= $rating; $i++) {
        if ($rating >= $i) {
            ?>
            <span class="vote full" title="<?php echo $i; ?>"><i class="fa fa-star"></i></span>
            <?php
        }
    }

    if ($showall && $rating < $max) {
        for ($i = $rating + 1; $i <= $max; $i++) {
            ?>
            <span class="vote" title="<?php echo $i; ?>"><i class="fa fa-star-o"></i></span>
            <?php
        }
    }
}

function updateDate($field, $options) {
    if (array_key_exists($field, $options) && !empty($options[$field])) {

        if (preg_match('/(?P<date>\d{2})\/(?P<month>\d{2})\/(?P<year>\d{4})/'
                        , $options[$field], $matches)) {
            return $field = $matches['year'] . '-' . $matches['month'] . '-' . $matches['date'];
        } else {
            return $field = date('Y-m-d');
        }
    } else {
        return $field = date('Y-m-d');
    }
}
