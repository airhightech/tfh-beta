<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Image;
use Validator;

class ImageUploader {

    protected $request;
    protected $storage = 'app/public';
    protected $errors;
    protected $names;
    protected $resize;
    protected $watermark;

    public function __construct(Request $request, $watermark = false) {
        $this->request = $request;
        $this->watermark = $watermark;
    }

    public function saveSingle($name, $resize = true) {

        $this->resize = $resize;

        $this->errors = [];
        $this->names = [];

        if ($this->request->hasFile($name)) {
            $file = $this->request->file($name);
            $this->saveFile($file);
        }

        return array_get($this->names, 0);
    }

    public function saveMultiple($name, $resize = false) {

        $this->resize = $resize;

        $this->errors = [];
        $this->names = [];

        if ($this->request->hasFile($name)) {
            $files = $this->request->file($name);

            foreach ($files as $file) {
                $this->saveFile($file);
            }
        }

        return $this->names;
    }

    protected function saveFile($file) {

        $rules = array('file' => 'required|mimes:png,gif,jpeg');

        $validator = Validator::make(array('file' => $file), $rules);

        $original = $file->getClientOriginalName();

        if ($validator->passes()) {
            $extension = $file->getClientOriginalExtension();
            $filename = $this->getUniqueFileName() . '.' . strtolower($extension);

            try {

                $this->names[] = $filename;

                if (strtolower($extension) != 'gif') {
                    $file->move(storage_path($this->storage), $filename);

                    /* 
                    if ($this->watermark) {
                      $this->addWatermark($filename);
                      } 
                     */

                    if ($this->resize) {
                        $this->resizeImage($filename);
                    }
                } else {
                    $destination = storage_path($this->storage) . '/' . $filename;
                    copy($file->getRealPath(), $destination);
                }
            } catch (FileException $ex) {
                $this->errors[] = [$original => $ex->getMessage()];
            }
        } else {
            $this->errors[] = [$original => 'File type not supported'];
        }
    }

    public function hasErrors() {
        return count($this->errors) > 0;
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getNames() {
        return $this->names;
    }

    protected function getUniqueFileName() {
        return uniqid('', true) . '-' . str_random(8);
    }

    protected function addWatermark($filepath) {

        $img = Image::make(storage_path($this->storage) . '/' . $filepath);
        $width = $img->width();
        $height = $img->height();

        $img->text('ThaiFullHouse (c) ' . date('Y'), $width / 2, $height / 2, function ($font) use ($height) {
            $font_file = public_path() . '/fonts/Signika-Bold.ttf';
            $font->file($font_file);
            $font->size($height / 20);
            $font->color(array(0, 0, 0, 0.5));
            $font->align('center');
            $font->valign('middle');
        });

        $img->save();
    }

    protected function resizeImage($filepath) {
        $img = Image::make(storage_path($this->storage) . '/' . $filepath);
        $img->resize(960, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save();
    }

}
