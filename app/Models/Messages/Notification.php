<?php

namespace App\Models\Messages;

use App\Models\BaseModel;
//use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Notification extends BaseModel {
    
    //use SoftDeletes;
    
    protected $table = 'notifications';
    
    public function send() {        
        
        $now = date('Y-m-d H:i:s');
        
        if ($this->sent == false) {
            $users = DB::table('users')->get();
            $data = [];
            foreach($users as $user) {
                $data[] = [
                    'message_id' => $this->id,
                    'agent_id' => $user->id,
                    'created_at' => $now
                ];
            }            
            DB::table('notification_receivers')->insert($data);   
            
            $this->sent = true;
            $this->save();
        }  
    }
    
}