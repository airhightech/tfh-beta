<?php

namespace App\Models\Messages;

use App\Models\BaseModel;

class NewsletterSubscription extends BaseModel {

    protected $table = 'newsletter_subscriptions';

}
