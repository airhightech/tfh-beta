<?php

namespace App\Models\Messages;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Popup extends BaseModel {
    
    use SoftDeletes;

    protected $table = 'homepage_popups';
}