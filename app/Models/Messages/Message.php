<?php

namespace App\Models\Messages;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Requests\Message\Create as CreateMessageRequest;
use Illuminate\Database\QueryException;
use Auth;
use DB;
use App\Models\Property;

class Message extends BaseModel {

    use SoftDeletes;

    protected $table = 'messages';

    public static function createFromRequest(CreateMessageRequest $request) {

        $body = $request->input('message');
        $property_id = $request->input('property_id');

        if ($property_id > 0) {
            $link = url('/property/' . $property_id);
            $body .= '<br/><br/><a href="'.$link.'" target="_blank">'.$link.'</a>';
        }

        $property = Property::find($property_id);

        $message = new self;
        $message->sender_name = $request->input('sender_name');
        $message->sender_phone = $request->input('sender_phone');
        $message->sender_email = $request->input('sender_email');
        $message->message = $body;
        $message->agent_id = $request->input('agent_id');

        if (Auth::check()) {
            $user = Auth::user();
            $message->sender_id = $user->id;
        }

        if ($request->input('subscribe') == 'yes' || $request->input('subscribe') == 'on' ) {

            try {
                DB::table('newsletter_subscriptions')
                        ->insert([
                            'email' => $request->input('sender_email')
                            , 'created_at' => DB::raw('NOW()')
                            , 'updated_at' => DB::raw('NOW()')]);
            } catch (QueryException $ex) {

            }
        }

        $message->setSecureInteger('property_id', $request);
        $message->save();

        return $message;
    }

}
