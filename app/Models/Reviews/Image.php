<?php

namespace App\Models\Reviews;

use App\Models\BaseModel;
use App\Helpers\ImageTrait;

class Image extends BaseModel {
    
    use ImageTrait;
    
    protected $table = 'manager_property_review_images';
    
    public $timestamps = false;
    
}