<?php

namespace App\Models\Reviews;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Reviews\Image as ReviewImage;
use Illuminate\Http\Request;
use App\Helpers\ImageUploader;
use DB;

class ManagerReview extends BaseModel {
    
    use SoftDeletes;
    
    protected $table = 'manager_property_reviews';
    
    public function addImageFromRequest(Request $request) {
        
        $uploader = new ImageUploader($request);
        
        $files = $uploader->saveMultiple('images');
        
        $data = [];
        
        foreach($files as $file) {
            $data[] = [
                'review_id' => $this->id,
                'filepath' => $file,
                'domain' => $request->input('domain')
            ];
        }
        
        DB::table('manager_property_review_images')->insert($data);
        
        return $uploader;
    }
    
    public function updateFromRequest(Request $request) {
        
        $options = $request->all();
        $this->updateTranslatable('comment_loc', $options);
        $this->updateTranslatable('comment_fac', $options);
        $this->updateTranslatable('comment_pla', $options);
        $this->updateTranslatable('overview', $options);
        
        $this->updateIfNotEmpty('video_link', $options);
        $this->save();        
    }
    
    /**
     * 
     * @return type
     */
    public function getLocationImages() {   
        
        return ReviewImage::where('review_id', $this->id)
                ->where('domain', 'location')->get();
        
    }
    
    /**
     * 
     * @return type
     */
    public function getFloorplanImages() {   
        
        return ReviewImage::where('review_id', $this->id)
                ->where('domain', 'floorplan')->get();
        
    }
    
    /**
     * 
     * @return type
     */
    public function getFacilityImages() {   
        
        return ReviewImage::where('review_id', $this->id)
                ->where('domain', 'facility')->get();
        
    }
    
    public function getYoutubeVideo() {
        $query = parse_url($this->video_link, PHP_URL_QUERY);
        parse_str($query, $values);
        return array_get($values, 'v', false);
    }
    
}