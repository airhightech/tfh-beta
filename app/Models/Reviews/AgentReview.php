<?php

namespace App\Models\Reviews;

use App\Models\BaseModel;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class AgentReview extends BaseModel {

    protected $table = 'agent_reviews';
    
    public function updateFromRequest(Request $request) {
        $options = $request->all();
        
        $user = Auth::user();

        $this->updateIfNotEmpty('title', $options);
        $this->updateIfNotEmpty('comment', $options);
        $this->rating = $request->input('rating');
        $this->agent_id = $request->input('agent_id');
        $this->user_id = $user->id;
        
        if ($request->input('hide_user_id') == 'yes') {
            $this->hide_user_id = true;
        } else {
            $this->hide_user_id = false;
        }
    }
    
    public function getUser() {
        return User::find($this->user_id);
    }
    
    public function getDateAgo() {
        $dt  = Carbon::parse($this->created_at);
        return $dt->diffForHumans(Carbon::now(), true);
    }
    
    public function getText($len = 50) {
        return substr($this->comment, 0, $len);
    }
    
    public function getImages() {
        return [];
    }
    
    public function updateComment(Request $request) {
        $this->comment = $request->input('comment');
        $this->status = 'APPROVED';
        $this->save();
    }
    
    public function approve() {
        $this->status = 'APPROVED';
        $this->save();
    }
    
    public function getLink() {
        return '/agent/' . $this->agent_id;
    }
}