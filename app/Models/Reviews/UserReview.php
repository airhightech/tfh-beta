<?php

namespace App\Models\Reviews;

use App\Models\BaseModel;
use App\Helpers\ImageUploader;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use DB;
use App\Models\Reviews\ReviewReply;

class UserReview extends BaseModel {

    protected $table = 'customer_reviews';

    protected $casts = [
      'rating' => 'json'
    ];

    public function updateFromRequest(Request $request) {
        $options = $request->all();

        $user = Auth::user();

        $this->updateIfNotEmpty('review', $options);
        $this->ratings = json_encode($request->input('rating', []));
        $this->property_id = $request->input('id');
        $this->customer_id = $user->id;

        $uploader = new ImageUploader($request);

        $review_photo_1 = $uploader->saveSingle('review_photo_1');
        $review_photo_2 = $uploader->saveSingle('review_photo_2');
        $review_photo_3 = $uploader->saveSingle('review_photo_3');

        $this->images = json_encode(array_filter([$review_photo_1, $review_photo_2, $review_photo_3]));
    }

    public function getImages() {
        return json_decode($this->images, true);
    }

    public function getCustomer() {
        return User::find($this->customer_id);
    }

    public function getRatings() {
        return json_decode($this->ratings, true);
    }

    public function getLikes() {
        return [
            'likes' => DB::table('customer_review_likes')->where('review_id', $this->id)->where('liked', true)->count(),
            'dislikes' => DB::table('customer_review_likes')->where('review_id', $this->id)->where('liked', false)->count()
        ];
    }

    public function getLastReplies() {
        $replies = ReviewReply::where('review_id', $this->id)
                ->orderBy('created_at', 'desc')
                ->paginate(5);

        return $replies;
    }
    
    public function getText($len = 50) {
        return substr($this->review, 0, $len);
    }
    
    public function getUser() {
        return User::find($this->customer_id);
    }
    
    public function updateComment(Request $request) {
        $this->review = $request->input('comment');
        $this->status = 'APPROVED';
        $this->save();
    }
    
    public function approve() {
        $this->status = 'APPROVED';
        $this->save();
    }
    
    public function getLink() {
        return '/property/' . $this->property_id;
    }
}
