<?php

namespace App\Models\Reviews;

use App\Models\BaseModel;
use App\Models\User;

class ReviewReply extends BaseModel {

    protected $table = 'customer_review_replies';
    
    public function getCustomer() {
        return User::find($this->user_id);
    }
    
}