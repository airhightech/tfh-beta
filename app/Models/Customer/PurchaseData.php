<?php

namespace App\Models\Customer;

use App\Models\BaseModel;
use App\Models\Listings\Package;

class PurchaseData extends BaseModel {
    
    protected $table = 'customer_purchase_data';  
    
    protected $primaryKey = 'purchase_id';
    
    public $timestamps = false;
    
    public function getPackage() {
        return Package::find($this->package_id);
    }
}