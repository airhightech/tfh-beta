<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\BaseModel;
use App\Models\User;
use App\Models\Payment\Paysbuy;

class Purchase extends BaseModel {
    
    use SoftDeletes;
    
    protected $table = 'customer_purchases';  
    
    public function getUser() {
        $account = Account::find($this->account_id);
        return User::find($account->creator_id); 
    }
    
    public function getAmount() {
        return $this->updated_amount > 0 ? $this->updated_amount : $this->total_amount;
    }
    
    public function getData() {
        return PurchaseData::find($this->id);
    }
    public function getPaybuyPayment() {
        if ($this->payment_method == 'paysbuy') {
            return Paysbuy::where('purchase_id', $this->id)->orderBy('updated_at', 'desc')->first();
        }        
        return null;
    }
    
}