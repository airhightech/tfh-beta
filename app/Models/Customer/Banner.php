<?php

namespace App\Models\Customer;

use App\Models\BaseModel;

/**
 * Condominium class
 */
class Banner extends BaseModel {

    protected $table = 'banners';
    
    public static function getLocations() {
        return [
            'home' => tr('manager_banner.location-home', 'Homepage'),
            
            'map-sale' => tr('manager_banner.location-map-sale', 'Map sale'),
            'map-rent' => tr('manager_banner.location-map-rent', 'Map rent'),
            'map-project' => tr('manager_banner.location-map-project', 'Map new project'),
            'map-condo' => tr('manager_banner.location-map-condo', 'Map condo'),
            
            'list-sale' => tr('manager_banner.location-list-sale', 'Listing Sale'),
            'list-rent' => tr('manager_banner.location-list-rent', 'Listing Rent'),
            'list-project' => tr('manager_banner.location-list-project', 'Listing New project'),
            'list-condo' => tr('manager_banner.location-list-condo', 'Listing Condo community')
        ];
    }
    
    
    public function getLocation() {
        return $this->location ? tr('manager_banner.location-' . $this->location, $this->location) : '';
    }

}
