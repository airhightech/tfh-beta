<?php

namespace App\Models\Customer;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\BaseModel;

class Account extends BaseModel {
    
    use SoftDeletes;
    
    protected $table = 'customer_accounts';  
}