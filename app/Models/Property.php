<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Helpers\ImageUploader;
use App\Models\Property\ExtendedData;
use App\Models\Property\ListingData;
use App\Models\Property\BuildingData;
use App\Models\Property\Traits\BuildingTrait;
use App\Models\Property\Traits\ImageTrait;
use App\Models\Property\Traits\ListingTrait;
use App\Models\Property\Address;
use App\Models\Property\Image;
use App\Models\Reviews\ManagerReview;
//use App\Models\Geo\Place;
use App\Models\User;
use Carbon\Carbon;
use DB;

class Property extends BaseModel {

    use SoftDeletes;
    use BuildingTrait;
    use ImageTrait;
    use ListingTrait;

    const TYPE_CONDO_BUILDING = 'cdb';
    const TYPE_APARTMENT_BUILDING = 'apb';
    const TYPE_CONDO_ROOM = 'cd';
    const TYPE_APARTMENT_ROOM = 'ap';
    const TYPE_SINGLE_HOUSE = 'sh';
    const TYPE_TOWNHOUSE = 'th';

    protected $table = 'properties';

    public function createFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->updateIfNotEmpty('ptype', $options);
        $this->updateBoolean('new_project', $options, false);
        $this->updateBoolean('user_submited', $options, false);
        $this->publication_date = date('Y-m-d H:i:s');
        $this->published = true;
    }

    public function updateFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->updateIfNotEmpty('ptype', $options);
    }

    public function addImageFromRequest(Request $request, $name = 'images', $watermark = false) {

        $uploader = new ImageUploader($request, $watermark);
        $images = $uploader->saveMultiple($name);

        if (count($images)) {
            $data = [];
            foreach ($images as $image) {
                $data[] = [
                    'property_id' => $this->id,
                    'filepath' => $image,
                    'listing_rank' => 1
                ];
            }
            DB::table('property_have_images')->insert($data);
        }

        return $uploader;
    }

    public function getImages() {
        $images = Image::where('property_id', $this->id)
                        ->orderBy('listing_rank', 'asc')->get()->all();
        return $images;
    }

    public function getMainImage() {
        return Image::where('property_id', $this->id)
                        ->orderBy('listing_rank', 'asc')->first();
    }

    public function getAddress() {
        return Address::find($this->address_id);
    }

    /**
     * Get the custom address of the property.
     * If the property is inside a building get the building's custom address
     * @return type
     */
    public function getCustomAddress() {

        /* $building = $this->getBuilding();

          if ($building) {
          return $building->getCustomAddress();
          } else {
          $extended = $this->getExtendedData();
          return $extended->getTranslatable('custom_address');
          } */

        $extended = $this->getExtendedData();
        return $extended->getCustomAddress();
    }

    public function getExtendedData() {

        $extended = ExtendedData::find($this->id);
        if (!$extended) {
            $extended = new ExtendedData;
            $extended->property_id = $this->id;
            $extended->save();
        }
        return $extended;
    }

    public function getListingData() {

        $listing = ListingData::find($this->id);

        if (!$listing) {
            $listing = new ListingData;
            $listing->property_id = $this->id;
            $listing->save();
        }

        $listing->syncData($this);

        return $listing;
    }

    public function getBuildingData() {
        $building = BuildingData::find($this->id);
        if (!$building) {
            $building = new BuildingData;
            $building->property_id = $this->id;
            $building->save();
        }
        return $building;
    }

    public function getManagerReview() {
        $review = ManagerReview::where('property_id', $this->id)->first();
        if (!$review) {
            $review = new ManagerReview;
            $review->property_id = $this->id;
            $review->save();
        }
        return $review;
    }

    public function getFacilitiesID() {

        // Check building ID

        $building_id = DB::table('property_belongs_to_building')
                        ->select('building_id')->where('property_id', $this->id)->value('building_id');

        if ($building_id > 0) {
            $building = Property::find($building_id);
            return $building->getFacilitiesID();
        } else {
            return DB::table('property_have_facilities')
                            ->where('property_id', $this->id)
                            ->pluck('facility_id')->all();
        }
    }

    public function getBuilding() {
        $building_id = DB::table('property_belongs_to_building')
                        ->select('building_id')->where('property_id', $this->id)->value('building_id');

        if ($building_id) {
            return Property::find($building_id);
        } else {
            return null;
        }
    }

    public function belongsToBuilding() {

        $building_id = DB::table('property_belongs_to_building')
                        ->select('building_id')->where('property_id', $this->id)->value('building_id');

        return $building_id;
    }

    public function getFeaturesID() {
        return DB::table('property_have_features')
                        ->where('property_id', $this->id)
                        ->pluck('feature_id')->all();
    }

    public function getNearbyPlaces() {

        $address = Address::find($this->address_id);

        if ($address) {
            $lnglat = $address->getLngLat();

            if (is_numeric($lnglat->lng) && is_numeric($lnglat->lat)) {
                return DB::table('places')
                                ->select('id', 'name', 'type'
                                        , DB::raw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$lnglat->lng} {$lnglat->lat})')) as distance")
                                        , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                                        , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                                )
                                ->whereRaw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$lnglat->lng} {$lnglat->lat})')) < " . 2000)
                                ->whereNull('deleted_at')
                                ->get();
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function getNearbyTransIdByType($type) {
        return DB::table('property_nearby_transportations')
                        ->where('type', $type)
                        ->where('property_id', $this->id)
                        ->pluck('trans_id')->all();
    }

    public function getPublicationPeriode() {

        $listing = $this->getListingData();

        if ($this->publication_date) {
            $start = date('d/m/Y', strtotime($this->publication_date));
            $end = date('d/m/Y', strtotime($listing->expiration_date));
            return $start . ' - ' . $end;
        } else {
            return 'N/A';
        }
    }

    public function getStatus() {

        $listing = $this->getListingData();

        if ($this->published) {
            return 'present';
        } else {
            if (strtotime($this->publication_date) > time()) {
                return 'pending';
            } else if (strtotime($listing->expiration_date) < time()) {
                return 'expired';
            } else {
                return 'inactive';
            }
        }
    }

    protected function getPublicationDuration() {
        return config('tfh.default_listing_period') * 3600;
    }

    public function getTypeName() {
        $type = '';
        switch ($this->ptype) {
            case self::TYPE_APARTMENT_BUILDING:
                $type = tr('property.type-apartment-building', 'Apartment');
                break;
            case self::TYPE_APARTMENT_ROOM:
                $type = tr('property.type-apartment-room', 'Apartment');
                break;
            case self::TYPE_CONDO_BUILDING:
                $type = tr('property.type-condo-building', 'Condo');
                break;
            case self::TYPE_CONDO_ROOM:
                $type = tr('property.type-condo-room', 'Condo');
                break;
            case self::TYPE_SINGLE_HOUSE:
                $type = tr('property.type-single-house', 'Single house');
                break;
            case self::TYPE_TOWNHOUSE:
                $type = tr('property.type-townhouse', 'Townhouse');
                break;
        }
        return $type;
    }

    public function isInUserFavorites($user) {

        $count = DB::table('favorite_properties')
                ->where('user_id', $user->id)
                ->where('property_id', $this->id)
                ->count();

        return $count > 0;
    }

    public function getPublisherProfile() {
        $user = User::find($this->publisher_id);
        return $user ? $user->getProfile() : null;
    }

    public function getListedOn() {
        $timezone = 7 * 3600;
        if ($this->publication_date) {
            if ($this->published) {
                return Carbon::now()->subSeconds(time() - strtotime($this->publication_date))->diffForHumans();
            } else {
                return Carbon::now()->subSeconds(time() - strtotime($this->created_at) + $timezone)->diffForHumans();
            }
        } else {
            return Carbon::now()->subSeconds(time() - strtotime($this->created_at) + $timezone)->diffForHumans();
        }
    }

    public function getGeneratedTitle() {

        $listing = $this->getListingData();
        $extended = $this->getExtendedData();
        $address = $this->getAddress();

        $building = $this->getBuilding();

        $name = '';

        $title = [];

        if ($this->ptype == 'cdb' || $this->ptype == 'apb' || $this->new_project == true) {
            $title[] = $this->getTranslatable('name');
        } else {
            $name = $this->getTypeName();

            if ($building) {
                $name = $building->getTranslatable('name') . ' ' . $name;
            }

            $title[] = $name . ' ' . tr('listing.for-' . $listing->ltype, 'for ' . ucfirst($listing->ltype));

            if ($extended->bedroom_num) {
                $title[] = $extended->bedroom_num . ' bed';
            }
        }

        if ($address) {
            $district = $address->getDistrict();
            if ($district) {
                $title[] = $district->getTranslatable('name');
            }

            $province = $address->getProvince();
            if ($province) {
                $title[] = $province->getTranslatable('name');
            }
        }

        return implode(' / ', $title);
    }

    public function getSalePrice() {
        if ($this->ptype == 'cdb' || $this->ptype == 'apb' || $this->new_project == true) {
            $extended = $this->getExtendedData();
            return $extended->starting_sales_price;
        } else {
            $listing = $this->getListingData();
            return $listing->listing_price;
        }
    }

    public function getStartingPrice() {
        if ($this->ptype == 'cdb' || $this->ptype == 'apb' || $this->new_project == true) {
            $extended = $this->getExtendedData();
            return $extended->starting_sales_price;
        } else {
            return $this->getBuildingStartingPrice();
        }
    }

    protected function getBuildingStartingPrice() {
        $building = $this->getBuilding();
        $extended = $this->getExtendedData();
        $listing = $this->getListingData();

        if ($extended->starting_sales_price > 0) {
            return $extended->starting_sales_price;
        } else if ($listing->listing_price > 0) {
            return $listing->listing_price;
        } else if ($building) {
            return $building->getStartingPrice();
        }

        return 0;
    }

    public function getTitle() {
        /* if ($this->ptype == 'cdb' || $this->ptype == 'apb' || $this->new_project == true) {
          return $this->getTranslatable('name');
          } else {
          return $this->getGeneratedTitle();
          } */

        return $this->getGeneratedTitle();
    }

    public function isBuilding() {
        return $this->ptype == 'cdb' || $this->ptype == 'apb';
    }

    public function getScore() {
        $score = 0;

        $reviews = DB::table('customer_reviews')->where('property_id', $this->id)->get();

        if (count($reviews)) {
            if ($reviews) {
                foreach ($reviews as $review) {
                    $ratings = json_decode($review->ratings, true);

                    $facilities = isset($ratings['facilities']) ? $ratings['facilities'] : 0;
                    $room = isset($ratings['room']) ? $ratings['room'] : 0;
                    $transport = isset($ratings['transport']) ? $ratings['transport'] : 0;

                    $management = isset($ratings['management']) ? $ratings['management'] : 0;
                    $secutiry = isset($ratings['security']) ? $ratings['security'] : 0;
                    $amenities = isset($ratings['amenities']) ? $ratings['amenities'] : 0;

                    $score += ($facilities + $room + $transport + $management + $secutiry + $amenities) / 4.0;
                }
            }

            $score = $score / count($reviews);
        }

        return round($score);
    }

    public function countFans() {
        return DB::table('favorite_properties')->where('property_id', $this->id)->count();
    }

    public function getRefCode() {
        $listing_type = $this->getListingData();

        $listing_types = [
            'rent' => 'RT',
            'sale' => 'SL',
        ];

        $property_types = [
            'ap' => 'AP',
            'cd' => 'CD',
            'cdb' => 'CD',
            'sh' => 'DH',
            'th' => 'TH'
        ];

        return array_get($property_types, $this->ptype, 'PT') .
                array_get($listing_types, $listing_type->ltype, 'UN') .
                sprintf('%07d', $this->id);
    }

}
