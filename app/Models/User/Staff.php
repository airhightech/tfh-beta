<?php

namespace App\Models\User;

use App\Models\BaseModel;

/**
 * Condominium class
 */
class Staff extends BaseModel {
    
    protected $primaryKey = 'staff_id';
    
    protected $table = 'business_staffs';
    
    public static function getRoles() {
        return [
            'manager' => tr('manager_staff.role-manager', 'Manager'),
            'editor' => tr('manager_staff.role-editor', 'Editor'),
            'accountant' => tr('manager_staff.role-accountant', 'Accountant'),
            'translator' => tr('manager_staff.role-translator', 'Translator')
        ];
    }
    
}