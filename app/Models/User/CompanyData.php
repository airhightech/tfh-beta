<?php

namespace App\Models\User;

use App\Models\BaseModel;
use Illuminate\Http\Request;

/**
 * Condominium class
 */
class CompanyData extends BaseModel {

    protected $table = 'publisher_company_profiles';
    
    protected $primaryKey = 'user_id';
    
    //public $timestamps = false;
    
    public function updateFromRequest(Request $request) {        
        $options = $request->all();
        $this->updateFromOptions($options);        
    }
    
    public function updateFromOptions($options) {
        $this->updateIfNotEmpty('company_name', $options);
        $this->updateIfNotEmpty('company_registration', $options);
        $this->updateIfNotEmpty('company_contact_name', $options);
    }    
}