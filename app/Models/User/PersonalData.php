<?php

namespace App\Models\User;

use App\Models\BaseModel;
use Illuminate\Http\Request;

/**
 * Condominium class
 */
class PersonalData extends BaseModel {

    protected $table = 'personal_profiles';
    
    protected $primaryKey = 'user_id';
    
    //public $timestamps = false;
    
    public function getBirthdate() {
        if ($this->birthdate) {
            list($year, $month, $day) = explode('-', $this->birthdate);
            return $day . '/' . $month . '/' . $year;
        } else {
            return '';
        }
    }
    
    public function updateFromRequest(Request $request) {
        
        $options = $request->all();
        $this->updateFromOptions($options);        
        
        // ? citizen_id_attachment
    }
    
    public function updateFromOptions($options) {
        
        if (isset($options['name'])) {
            $names = preg_split('/[\s,]+/', $options['name']);
            if (count($names) > 1) {
                $lastname = array_pop($names);
                $this->lastname = $lastname;
                $this->firstname = implode(' ', $names);
            } else if(count($names) == 1) {
                $this->firstname = $options['name'];
            }             
        } else {
            $this->updateIfNotEmpty('firstname', $options);
            $this->updateIfNotEmpty('lastname', $options);
        }
        
        $this->updateIfNotEmpty('citizen_id', $options);
        $this->updateIfNotEmpty('gender', $options);
        $this->updateIfNotEmpty('nationality', $options);
        $this->updateDate('birthdate', $options);
    }
    
}