<?php

namespace App\Models\User;

use App\Models\Property;
use DB;
use Illuminate\Database\QueryException;

trait FavoriteTrait {
    
    public function toggleFavoriteProperty($pid) {
        // Try to insert
        $result;
        try {
            DB::insert('INSERT INTO favorite_properties (user_id, property_id, created_at) VALUES (?, ?, NOW())', [$this->id, $pid]);
            $result = 'ADDED';
            // Vaforite created
        } catch (QueryException $ex) {
            $ex = null;
            DB::delete('DELETE FROM favorite_properties WHERE user_id = ? AND property_id = ?', [$this->id, $pid]);
            $result = 'REMOVED';
        }
        return $result;
    }
    
    public function toggleFavoriteAgent($aid) {
        // Try to insert
        $result;
        try {
            DB::insert('INSERT INTO favorite_agents (user_id, agent_id, created_at) VALUES (?, ?, NOW())', [$this->id, $aid]);
            $result = 'ADDED';
            // Vaforite created
        } catch (QueryException $ex) {
            $ex = null;
            DB::delete('DELETE FROM favorite_agents WHERE user_id = ? AND agent_id = ?', [$this->id, $aid]);
            $result = 'REMOVED';
        }
        return $result;
    }
    
    public function getFavoriteProperties()
    {        
        return Property::leftJoin('favorite_properties', 'favorite_properties.property_id', 'properties.id')
                ->where('favorite_properties.user_id', $this->id)->get();
    }
    
    public function getFavoriteAgents()
    {        
        return User::leftJoin('favorite_agents', 'favorite_agents.agent_id', 'users.id')
                ->where('favorite_agents.user_id', $this->id)->get();
    }
    
}