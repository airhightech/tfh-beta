<?php

namespace App\Models\User;

use DB;

trait PurchaseTrait {
    
    public function getAvailableExclusive() {
        
        return DB::table('customer_purchases')
                ->leftJoin('customer_purchase_data', 'customer_purchase_data.purchase_id', 'customer_purchases.id')
                ->leftJoin('listing_packages', 'listing_packages.id', 'customer_purchase_data.package_id')
                ->leftJoin('customer_accounts_managers', 'customer_accounts_managers.account_id', 'customer_purchases.account_id')
                ->select('customer_purchases.id', 'customer_purchase_data.listing_count', 'customer_purchases.used_listing', DB::raw('listing_packages.id AS package_id'))
                ->where('listing_packages.type', 'exclusive')
                ->where('customer_purchases.payment_status', 'paid')
                ->where('customer_accounts_managers.user_id', $this->id)
                ->whereRaw('customer_purchase_data.listing_count > customer_purchases.used_listing')
                ->get()->all();
    }
    
    
    public function countAvailableExclusive() {
        
        $purchases = $this->getAvailableExclusive();
        
        $available = 0;
        
        if (count($purchases)) {            
            foreach($purchases as $purchase) {
                $available = $available + $purchase->listing_count - $purchase->used_listing;
            }
        }  
        
        return $available;
    }
    
    public function getAvailableFeatured() {        
        return DB::table('customer_purchases')
                ->leftJoin('customer_purchase_data', 'customer_purchase_data.purchase_id', 'customer_purchases.id')
                ->leftJoin('listing_packages', 'listing_packages.id', 'customer_purchase_data.package_id')
                ->leftJoin('customer_accounts_managers', 'customer_accounts_managers.account_id', 'customer_purchases.account_id')
                ->select('customer_purchases.id', 'customer_purchase_data.listing_count', 'customer_purchases.used_listing', DB::raw('listing_packages.id AS package_id'))
                ->where('listing_packages.type', 'featured')
                ->where('customer_purchases.payment_status', 'paid')
                ->where('customer_accounts_managers.user_id', $this->id)
                ->whereRaw('customer_purchase_data.listing_count > customer_purchases.used_listing')
                ->get()->all();
    }
    
    public function getAvailableFreeFeatured() {

        return DB::table('customer_purchases')
                ->leftJoin('customer_purchase_data', 'customer_purchase_data.purchase_id', 'customer_purchases.id')
                ->leftJoin('listing_packages', 'listing_packages.id', 'customer_purchase_data.package_id')
                ->leftJoin('customer_accounts_managers', 'customer_accounts_managers.account_id', 'customer_purchases.account_id')
                ->select('customer_purchases.id', 'customer_purchase_data.free_listing_count', 'customer_purchases.used_free_listing', DB::raw('listing_packages.id AS package_id'))
                ->where('listing_packages.type', 'exclusive')
                ->where('customer_purchases.payment_status', 'paid')
                ->where('customer_accounts_managers.user_id', $this->id)
                ->whereRaw('customer_purchase_data.free_listing_count > customer_purchases.used_free_listing')
                ->get()->all();
    }
    
    public function countAvailableFeatured() {
        
        // Featured
        
        $featureds = $this->getAvailableFeatured();
        
        $featured = 0;
        
        if (count($featureds)) {            
            foreach($featureds as $purchase) {
                $featured = $featured + $purchase->listing_count - $purchase->used_listing;
            }
        }  
        
        // Exclusive
        
        $free_listings = $this->getAvailableFreeFeatured();
        
        $free_listing = 0;
        
        if (count($free_listings)) {            
            foreach($free_listings as $purchase) {
                $free_listing = $free_listing + $purchase->free_listing_count - $purchase->used_free_listing;
            }
        }    
        
        return $featured + $free_listing;
    }
    
}