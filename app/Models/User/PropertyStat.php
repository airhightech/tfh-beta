<?php

namespace App\Models\User;

use DB;
use App\Models\Property;

trait PropertyStat {

    public function countProperty($published_only = false) {
        $query = DB::table('properties')
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->where('properties.publisher_id', $this->id);

        if ($published_only) {
            $query->where('properties.published', true);
        }
        $query->whereIn('property_listing_data.ltype', ['rent', 'sale']);
        $query->whereNull('deleted_at');
        return $query->whereIn('properties.ptype', ['ap', 'cd', 'sh', 'th'])
                        ->count();
    }

    public function countGroupedProperty($published_only = false) {
        $query = DB::table('properties')
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->select('properties.ptype', DB::raw('COUNT(properties.id) AS num'))
                ->where('properties.publisher_id', $this->id);
        if ($published_only) {
            $query->where('properties.published', true);
        }
        $query->whereIn('property_listing_data.ltype', ['rent', 'sale']);
        $query->whereNull('deleted_at');
        return $query->whereIn('properties.ptype', ['ap', 'cd', 'sh', 'th'])
                        ->groupBy('properties.ptype')
                        ->pluck('num', 'properties.ptype')->all();
    }

    public function countListingStats($published_only = false) {
        $query = DB::table('properties')
                ->select('property_listing_data.ltype', DB::raw('COUNT(id) AS num'))
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->where('properties.publisher_id', $this->id);
        if ($published_only) {
            $query->where('properties.published', true);
        }
        $query->whereNull('deleted_at');
        return $query->where('properties.published', true)
                        ->groupBy('property_listing_data.ltype')
                        ->pluck('num', 'ltype')->all();
    }

    public function countExclusiveProperty($published_only = false) {

        $query = DB::table('properties')
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->where('properties.publisher_id', $this->id);
        if ($published_only) {
            $query->where('properties.published', true);
        }
        $query->whereNull('deleted_at');
        return $query->whereIn('properties.ptype', ['ap', 'cd', 'sh', 'th'])
                        ->where('property_listing_data.listing_class', 'exclusive')->count();
    }

    public function countFeaturedProperty($published_only = false) {

        $query = DB::table('properties')
                ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                ->where('properties.publisher_id', $this->id);
        if ($published_only) {
            $query->where('properties.published', true);
        }
        $query->whereNull('deleted_at');
        return $query->whereIn('properties.ptype', ['ap', 'cd', 'sh', 'th'])
                        ->where('property_listing_data.listing_class', 'featured')->count();
    }

    public function getTotalVisit() {
        return 10;
    }

    public function countNewMessage() {
        return DB::table('messages')->where('agent_id', $this->id)->where('read', false)->count();
    }

    public function countReview() {
        return DB::table('agent_reviews')->where('agent_id', $this->id)->count();
    }

    public function getProperties($limit = 0) {
        $query = Property::where('published', true)
                ->where('publisher_id', $this->id)
                ->orderBy('publication_date', 'asc');

        if ($limit > 0) {
            $query->limit($limit);
        }

        return $query->get();
    }

}
