<?php

namespace App\Models\User;

use App\Models\BaseModel;
use Illuminate\Http\Request;
use App\Helpers\ImageUploader;
use App\Models\User;
use DB;

/**
 * Condominium class
 */
class Profile extends BaseModel {

    protected $table = 'profiles';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public function getName() {
        
        $user = User::find($this->user_id);
        
        if (!$user) {
            return '';
        }
        
        $personalData = $user->getPersonalData();
        $companyData = $user->getCompanyData();

        $name = '';

        if ($user->ctype == 'developer' || $user->ctype == 'company') {
            $name = $companyData->company_name;
            if (empty($name)) {
                $name = trim($personalData->firstname . ' ' . $personalData->lastname);
            }
        } else {
            $name = trim($personalData->firstname . ' ' . $personalData->lastname);
            if (empty($name)) {
                $name = $companyData->company_name;
            }
        }

        if (empty($name)) {
            $name = $user->name;
        }
        
        if (empty($name)) {
            $name = $user->email;
        }

        return $name;
    }
    
     public function getShortName() {         
        
        $user = User::find($this->user_id);
        
        if (!$user) {
            return '';
        }
        
        $personalData = $user->getPersonalData();
        $companyData = $user->getCompanyData();

        $name = '';

        if ($user->ctype == 'developer' || $user->ctype == 'company') {
            $name = $companyData->company_name;
            if (empty($name)) {
                $name = trim($personalData->firstname);
            }
        } else {
            $name = trim($personalData->firstname);
            if (empty($name)) {
                $name = $companyData->company_name;
            }
        }

        if (empty($name)) {
            $name = $user->name;
        }
        
        if (empty($name)) {
            $name = $user->email;
        }

        return $name;
    }

    public function getCoverPicture() {
        if ($this->cover_picture_path) {
            return url('/thumb/1150x300/' . $this->cover_picture_path);
        } else {
            return '';
        }
    }
    
    public function haveProfilePicture() {
        return empty($this->profile_picture_path) == false;
    }

    public function getProfilePicture() {
        if ($this->profile_picture_path) {
            return url('/thumb/300x300/' . $this->profile_picture_path);
        } else {
            return url('/img/user.png');
        }
    }

    public function getContactPicture() {
        if ($this->profile_picture_path) {
            return url('/thumb/64x64/' . $this->profile_picture_path);
        } else {
            return url('/img/user.png');
        }
    }

    public function getTextAddress() {
        return $this->custom_address;
    }

    public function updateFromRequest(Request $request) {

        $options = $request->all();

        $this->updateFromOptions($options);

        /* ? address_id, profile_type, total_visitors, cover_picture_path, profile_picture_path
         * ? logo_picture_path, accept_quick_matching, listing_count, search_ranking_level
         * 
         */

        if ($request->hasFile('photo')) {
            $uploader = new ImageUploader($request);
            $filepath = $uploader->saveSingle('photo');
            $this->profile_picture_path = $filepath;
        }

        if ($request->hasFile('cover')) {
            $uploader = new ImageUploader($request);
            $filepath = $uploader->saveSingle('cover');
            $this->cover_picture_path = $filepath;
        }

        if ($request->hasFile('logo')) {
            $uploader = new ImageUploader($request);
            $filepath = $uploader->saveSingle('logo');
            $this->logo_picture_path = $filepath;
        }
    }
    
    public function getTags() {
        return 'House, land';
    }

    public function updateFromOptions($options) {
        $this->updateIfNotEmpty('phone', $options);
        $this->updateIfNotEmpty('mobile', $options);
        $this->updateIfNotEmpty('line_id', $options);
        $this->updateIfNotEmpty('custom_address', $options);
        $this->updateIfNotEmpty('languages', $options);
        $this->updateIfNotEmpty('introduction', $options);
    }
    
    public function getRating() {
        $avg  = DB::table('agent_reviews')
                ->where('agent_id', $this->user_id)
                ->avg('rating');
        return round($avg);
    }

}
