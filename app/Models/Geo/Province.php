<?php

namespace App\Models\Geo;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use DB;

class Province extends BaseModel {
    
    use SoftDeletes;
    use LocationTrait;
    
    protected $table = 'geo_provinces';
    
    protected function getLocationField() {
        return 'map_center';
    }
    
    public function getDistrictCount()
    {
        return DB::table('geo_districts')->where('province_id', $this->id)->count();
    }
    
    public function countProperties() {
        return DB::table('property_address')->where('province_id', $this->id)->count();
    }
    
    public function createFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->save();
    }
    
    public function updateFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->save();
    }
    
}