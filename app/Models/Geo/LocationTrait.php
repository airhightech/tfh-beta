<?php

namespace App\Models\Geo;

use DB;
use Illuminate\Http\Request;

trait LocationTrait {
    
    protected $_lnglat;
    
    protected function getLocationField() {
        return 'location';
    }

    public function getLngLat() {

        $this->_lnglat = DB::table($this->table)
                        ->select(
                                DB::raw('ST_X(ST_TRANSFORM(' . $this->getLocationField() . '::geometry, 4326)) AS lng')
                                , DB::raw('ST_Y(ST_TRANSFORM(' . $this->getLocationField() . '::geometry, 4326)) AS lat'))
                        ->where('id', $this->id)
                        ->first();
        
        return $this->_lnglat;
    }
    
    public function lng() {
        if (!$this->_lnglat) {
            $this->getLngLat();
        }
        return is_object($this->_lnglat) ? $this->_lnglat->lng : null;
    }
    
    public function lat() {
        if (!$this->_lnglat) {
            $this->getLngLat();
        }
        return is_object($this->_lnglat) ? $this->_lnglat->lat : null;
    }

    protected function setLocationFromRequest(Request $request) {

        $all = $request->all();

        if (array_key_exists('lng', $all) && array_key_exists('lat', $all) && is_numeric($all['lng']) && is_numeric($all['lat'])) {
            
            $lng = $all['lng'];
            $lat = $all['lat'];   
            
            // Validate longitude and latitude values
            
            if (abs($lng) <= 180.0 && abs($lat) <= 90.0) {
                $this->location = DB::raw("ST_GeomFromText('POINT($lng $lat 0.0)', 4326)");
            }
        }
    }

}
