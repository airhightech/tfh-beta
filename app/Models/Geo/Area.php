<?php

namespace App\Models\Geo;

use Illuminate\Http\Request;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Area extends BaseModel {
    
    use SoftDeletes;
    use LocationTrait;
    
    protected $table = 'geo_area';
    
    protected function getLocationField() {
        return 'map_center';
    }
    
    public function countProperties() {
        return DB::table('property_address')->where('area_id', $this->id)->count();
    }
    
    public function createFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->district_id = array_get($options, 'district_id');
        $this->save();
    }
    
    public function updateFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->save();
    }
    
}