<?php

namespace App\Models\Geo;

use Illuminate\Http\Request;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class District extends BaseModel {
    
    use SoftDeletes;
    use LocationTrait;
    
    protected $table = 'geo_districts';
    
    protected function getLocationField() {
        return 'map_center';
    }
    
    public function getAreaCount()
    {
        return DB::table('geo_area')->where('district_id', $this->id)->count();
    }
    
    public function countProperties() {
        return DB::table('property_address')->where('district_id', $this->id)->count();
    }
    
    public function createFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->province_id = array_get($options, 'province_id');
        $this->save();
    }
    
    public function updateFromRequest(Request $request) {
        $options = $request->all();
        $this->updateTranslatable('name', $options);
        $this->save();
    }
    
}