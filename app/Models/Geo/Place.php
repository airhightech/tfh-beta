<?php

namespace App\Models\Geo;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends BaseModel {
    
    use LocationTrait;
    use SoftDeletes;
    
    protected $table = 'places';
    
    protected static $supported = ['bts', 'mrt', 'apl', 'bank', 'dpt-store'
        , 'hospital', 'school', 'restaurant', 'spa', 'golf'];
    
    public static function getSupported() {
        return self::$supported;
    }
    
    public static function getIcon($type) {
        
        $icon = '<i class="fa fa-map-pin"></i>';
        
        if ($type == 'bts' || $type == 'mrt' || $type == 'apl') {
            $icon = '<i class="fa fa-bus"></i>';
        } else if ($type == 'bank') {
            $icon = '<i class="fa fa-bank"></i>';
        } else  if ($type == 'dpt-store') {
            $icon = '<i class="fa fa-shopping-cart"></i>';
        } else  if ($type == 'hospital') {
            $icon = '<i class="fa fa-h-square"></i>';
        } else  if ($type == 'school') {
            $icon = '<i class="fa fa-graduation-cap"></i>';
        } else  if ($type == 'restaurant') {
            $icon = '<i class="fa fa-cutlery"></i>';
        } else  if ($type == 'spa') {
            $icon = '<i class="fa fa-bath"></i>';
        } else  if ($type == 'golf') {
            $icon = '<i class="fa fa-flag-o"></i>';
        }
        
        return $icon;
    }
    
}
