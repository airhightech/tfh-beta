<?php

namespace App\Models\System;

use App\Models\BaseModel;

/**
 * Condominium class
 */
class Bank extends BaseModel {

    protected $table = 'bank_accounts';

    public static function getBankList() {
        return [
            'bbl' => 'Bangkok Bank',
            'ktb' => 'Krung Thai Bank',
            'scb' => 'Siam Commercial Bank',
            'kbank' => 'Kasikornbank',
            'bay' => 'Bank of Ayudhya',
            'tbank' => 'Thanachart Bank',
            'tmb' => 'TMB Bank'
        ];
    }
    
    public static function getBankName($code) {
        $banks = self::getBankList();        
        return isset($banks[$code]) ? $banks[$code] : 'Unknown bank';
    }

    public function getBankNameByCode() {
        $banks = self::getBankList();        
        $name = array_get($banks, $this->code, 'Unknown bank');
        return is_array($name) ? 'Unknown bank' : $name;
    }
    

}
