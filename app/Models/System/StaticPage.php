<?php

namespace App\Models\System;

use App\Models\BaseModel;

/**
 * Condominium class
 */
class StaticPage extends BaseModel {

    protected $table = 'static_pages';

}
