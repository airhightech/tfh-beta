<?php

namespace App\Models\System;

use App\Models\BaseModel;
use App\Helpers\ImageUploader;
use Illuminate\Http\Request;

/**
 * Condominium class
 */
class Icon extends BaseModel {

    protected $table = 'icons';

    public function updateFromRequest(Request $request) {

        $helper = new ImageUploader($request);

        $path = $helper->saveSingle('icon', false);

        if (!$helper->hasErrors()) {
            $this->removeOld();

            $this->code = $request->code;
            $this->filepath = $path;
            $this->save();
        }
    }

    public function removeOld() {

        if ($this->filepath) {
            $path = storage_path('app/public/*' . $this->filepath);

            foreach (glob($path) as $filepath) {
                @unlink($filepath);
            }
        }
    }

}
