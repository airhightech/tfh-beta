<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use App\Models\Geo\LocationTrait;
use App\Models\Geo\Place;
use App\Models\Geo\Province;
use App\Models\Geo\District;
use App\Models\Geo\Area;
use DB;

class Address extends BaseModel {
    
    use SoftDeletes;
    use LocationTrait;
    
    protected $table = 'property_address';
    
    protected $appends = ['lnglat'];
    
    protected $hidden = ['created_at', 'deleted_at', 'updated_at', 'location'];
    
    public function updateFromRequest(Request $request) {
        
        $options = $request->all();
        
        $this->setLocationFromRequest($request);
        $this->updateTranslatable('area_name', $options);
        $this->updateTranslatable('district_name', $options);
        $this->updateTranslatable('province_name', $options);
        $this->updateTranslatable('street_name', $options);
        
        $this->updateIfNotEmpty('street_number', $options);
        $this->updateIfNotEmpty('postal_code', $options);
        
        $this->updateIfNotEmpty('area_id', $options);
        $this->updateIfNotEmpty('district_id', $options);
        $this->updateIfNotEmpty('province_id', $options);
        
        $this->save();        
    }
    
    public function getNearbyPlaces() {
        return Place::leftJoin('address_have_nearby_places', 'address_have_nearby_places.place_id', 'places.id')
                ->where('address_have_nearby_places.address_id', $this->id)->get();
    }
    
    public function updateNearbyPlaces() {
        
        $location = 'POINT(' . $this->lng() . ' ' . $this->lat() . ')';
        
        $max_distance = config('google.nearby_place_distance');
        
        $places = DB::table('places')
                        ->select('id', DB::raw("ST_Distance(location, ST_GeogFromText('SRID=4326;$location')) as distance"))
                        ->whereRaw("ST_Distance(location, ST_GeogFromText('SRID=4326;$location')) < " . $max_distance)
                        ->get();
        
        DB::table('address_have_nearby_places')->where('address_id, $this->id')->delete();
        
        if (count($places)) {
            $data = [];            
            foreach($places as $place) {
                $data[] = [
                    'place_id' => $place->id,
                    'address_id' => $this->id,
                    'distance' => round($place->distance)
                ];
            }            
            DB::table('address_have_nearby_places')->insert($data);
        }                        
    }
    
    public function getLnglatAttribute() {
        return ['lng' => $this->lng(), 'lat' => $this->lat()];
    }
    
    public function getProvince() {
        return Province::find($this->province_id);
    }
    
    public function getProvinceName() {
        $province = Province::find($this->province_id);
        
        if ($province) {
            return $province->getTranslatable('name');
        } else {
            return '';
        }
    }
    
    public function getDistrict() {
        return District::find($this->district_id);
    }
    
    public function getDistrictName() {
        $district = District::find($this->district_id);
        
        if ($district) {
            return $district->getTranslatable('name');
        } else {
            return '';
        }
    }
    
    public function getArea() {
        return Area::find($this->area_id);
    }
    
    public function getAreaName() {
        $area = Area::find($this->area_id);
        
        if ($area) {
            return $area->getTranslatable('name');
        } else {
            return '';
        }
    }
}