<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ImageTrait;

class FavoriteProperty extends BaseModel {

    protected $table = 'favorite_properties';
    public $timestamps = false;
    
}
