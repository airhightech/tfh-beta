<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use App\Helpers\ImageTrait;

class Image extends BaseModel {
    
    use ImageTrait;
    
    protected $table = 'property_have_images';
    
    public $timestamps = false;
}