<?php

namespace App\Models\Property\Traits;

use DB;

trait BuildingTrait {

    public function countListedProperties() {
        return DB::table('property_belongs_to_building')
                        ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'property_belongs_to_building.property_id')
                        ->leftJoin('properties', 'properties.id', 'property_belongs_to_building.property_id')
                        ->select('property_listing_data.ltype', DB::raw('COUNT(property_belongs_to_building.property_id) AS pnum'))
                        ->where('property_belongs_to_building.building_id', $this->id)
                        ->where('properties.published', true)
                        ->groupBy('property_listing_data.ltype')
                        ->pluck('pnum', 'ltype')->all();
    }

    public function getListedProperties() {
        return self::leftJoin('property_belongs_to_building', 'property_belongs_to_building.property_id', 'properties.id')
                        ->leftJoin('property_listing_data', 'property_listing_data.property_id', 'property_belongs_to_building.property_id')
                        ->where('property_belongs_to_building.building_id', $this->id)
                        ->where('properties.published', true)
                        //->where('property_listing_data.ltype', $type)
                        ->where('properties.publication_date', '<=', DB::raw('NOW()'))
                        //->where('property_listing_data.expiration_date', '>=', DB::raw('NOW()'))
                        ->whereRaw('properties.deleted_at IS NULL')
                        ->get();
    }

    public function addRoomType($request) {
        
        $size = str_replace(',', '.', $request->input('size'));        
        
        DB::table('building_have_room_types')->insert([
            'property_id' => $this->id,
            'name' => $request->input('name'),
            'size' => $size
        ]);
    }

    public function deleteRoomType($id) {
        DB::delete('DELETE FROM building_have_room_types WHERE id = ?', [$id]);
    }

    public function updateRoomType($request) {
        
        $size = str_replace(',', '.', $request->input('size'));      
        
        DB::table('building_have_room_types')
                ->where('id', $request->input('id'))
                ->update([
                    'name' => $request->input('name'),
                    'size' => $size
        ]);
    }

    public function getRoomTypes() {
        return DB::select('SELECT * FROM building_have_room_types WHERE property_id = ? ORDER BY id ASC', [$this->id]);
    }

}
