<?php

namespace App\Models\Property\Traits;

use DB;

trait ImageTrait {
    
    public function countImages() {
        return DB::table('property_have_images')->where('property_id', $this->id)->count();
    }
    
}   