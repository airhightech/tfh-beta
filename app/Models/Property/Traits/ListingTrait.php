<?php

namespace App\Models\Property\Traits;

trait ListingTrait {

    public function syncPublishingDate() {

        // clean

        $now = time();

        $listingData = $this->getListingData();
        $upgrade_end = strtotime($listingData->upgrade_end);
        $listing_end = strtotime($listingData->expiration_date);

        $unpublish = false;

        if ($listingData->upgrade_end) {
            if ($upgrade_end < $now) {
                $unpublish = true;
            }
        }

        if ($listingData->expiration_date) {
            if ($listing_end < $now) {
                $unpublish = true;
            }
        }

        if ($unpublish && $upgrade_end > $listing_end) {
            $listingData->expiration_date = date('Y-m-d', $upgrade_end);
            $listingData->save();
        }

        if ($unpublish) {

            $listingData->upgrade_start = null;
            $listingData->upgrade_end = null;
            $listingData->expiration_date = null;
            $listingData->listing_class = 'standard';
            $listingData->save();

            if ($this->ptype != self::TYPE_CONDO_BUILDING 
                    && $this->ptype != self::TYPE_APARTMENT_BUILDING) {
                $this->publish = false;
                $this->save();
            }
        }
    }

}
