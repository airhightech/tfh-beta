<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use Illuminate\Http\Request;

class ListingData extends BaseModel {

    protected $table = 'property_listing_data';
    protected $primaryKey = 'property_id';
    public $timestamps = false;    

    public function syncData($property) {

        $time = 0;

        if ($property->publication_date) {
            $time = strtotime($property->publication_date) + (config('tfh.default_listing_period') * 24 * 3600);
            $this->expiration_date = date('Y-m-d', $time);
        }

        if (!$this->listing_class) {
            $this->listing_class = 'standard';
            $this->save();
        }

        if (!$this->expiration_date) {
            $this->expiration_date = date('Y-m-d', $time);
            $this->save();
        }
        
        $this->generateTitle();
    }

    public function updateFromRequest(Request $request) {

        $options = $request->all();

        $this->updateTranslatable('title', $options);

        $this->updateIfNotEmpty('ltype', $options);
        $this->updateIfNotEmpty('deposite', $options);
        $this->updateIfNotEmpty('listing_price', $options);
        $this->updateIfNotEmpty('minimal_rental_period', $options);
        $this->updateIfNotEmpty('listing_class', $options);
        $this->updateDate('movein_date', $options);
        $this->updateIfNotEmpty('ylink1', $options);
        $this->updateIfNotEmpty('ylink2', $options);

        $this->save();
    }

    public function generateTitle($force = false) {

        if (!$this->title || $force) {
            $titles = [];

            $lang = \App::getLocale();

            $prefix = '';

            if ($this->ltype) {
                $prefix = tr('listing.for-rent', 'for rent');
            } else {
                $prefix = tr('listing.for-sale', 'for sale');
            }

            $name = $prefix;

            $titles[$lang] = $name;

            if (!isset($titles['th'])) {
                $titles['th'] = $name;
            }

            $this->setTranslatable('title', $titles);
        }
    }

    public function getTypeName() {
        if ($this->ltype == 'rent') {
            return tr('listing.rent', 'Rent');
        } else {
            return tr('listing.sale', 'Sale');
        }
    }
    
    public function getAvailabilityDate() {
        return date('d/m/Y', strtotime($this->movein_date));
    }

    public function getYoutubeVideo1() {
        $query = parse_url($this->ylink1, PHP_URL_QUERY);
        parse_str($query, $values);
        return array_get($values, 'v', false);
    }

    public function getYoutubeVideo2() {
        $query = parse_url($this->ylink2, PHP_URL_QUERY);
        parse_str($query, $values);
        return array_get($values, 'v', false);
    }

}
