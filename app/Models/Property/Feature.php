<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ImageTrait;

class Feature extends BaseModel {
    
    use SoftDeletes;
    use ImageTrait;
    
    protected $_custom_image_field = 'icon_path';
    
    protected $table = 'property_features';
    
    public function removeOldIcon() {
        if (!empty($this->icon_path)) {
            $this->removeImage($this->icon_path);
        }
    }
    
}