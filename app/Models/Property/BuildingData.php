<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use Illuminate\Http\Request;
use App\Models\User\Profile;

class BuildingData extends BaseModel {
    
    protected $table = 'building_data';
    
    protected $primaryKey = 'property_id';
    
    public $timestamps = false;
    
    public function updateFromRequest(Request $request) {
        
        $options = $request->all();
        
        $this->updateIfNotEmpty('number_of_tower', $options);
        $this->updateIfNotEmpty('total_units', $options);
        $this->updateIfNotEmpty('parking_lot', $options);
        $this->setSecureInteger('developer_id', $request);
        
        $this->updateDate('project_date_start', $options);
        $this->updateDate('project_date_end', $options);
        
        $this->save();
    }
    
    public function getDeveloper() {
        return Profile::find($this->developer_id);
    }
    
}