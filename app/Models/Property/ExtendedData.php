<?php

namespace App\Models\Property;

use App\Models\BaseModel;
use Illuminate\Http\Request;
use App\Models\Property;


class ExtendedData extends BaseModel {
    
    protected $table = 'property_extended_data';
    
    protected $primaryKey = 'property_id';
    
    public $timestamps = false;
    
    public function updateFromRequest(Request $request) {
        
        $options = $request->all();
        
        $this->updateTranslatable('custom_address', $options);
        $this->updateTranslatable('details', $options);
        
        $this->updateIfNotEmpty('starting_sales_price', $options);
        $this->updateIfNotEmpty('office_hours', $options);
        $this->updateIfNotEmpty('year_built', $options);
        $this->updateIfNotEmpty('total_floors', $options);
        $this->updateIfNotEmpty('total_size', $options);
        $this->updateIfNotEmpty('land_size', $options);
        $this->updateIfNotEmpty('floor_num', $options);
        $this->updateIfNotEmpty('bedroom_num', $options);
        $this->updateIfNotEmpty('bathroom_num', $options);
        $this->updateIfNotEmpty('website', $options);
        $this->updateIfNotEmpty('psf', $options);
        $this->updateIfNotEmpty('contact_person', $options);
        
        $this->save();
    }
    
    public function getDetails() {
        $details = trim($this->getTranslatable('details'));
        
        if (empty($details)) {
            $property = Property::find($this->property_id);
            $buidling = $property->getBuilding();            
            if ($buidling) {
                $extended = $buidling->getExtendedData();
                return $extended->getDetails();
            }
        }
        
        return $details;
    }
    
    public function getCustomAddress() {
        $details = trim($this->getTranslatable('custom_address'));
        
        if (empty($details)) {
            $property = Property::find($this->property_id);
            $buidling = $property->getBuilding();            
            if ($buidling) {
                $extended = $buidling->getExtendedData();
                return $extended->getDetails();            
            }
        }
        
        return $details;
    }
}