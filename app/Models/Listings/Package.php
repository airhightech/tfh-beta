<?php

namespace App\Models\Listings;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BaseModel;
use App\Models\Customer\Purchase;
use App\Models\Customer\PurchaseData;
use Illuminate\Http\Request;
use Auth;
use DB;

class Package extends BaseModel {

    use SoftDeletes;

    const TYPE_EXCLUSIVE = 'exclusive';
    const TYPE_FEATURED = 'featured';

    protected $table = 'listing_packages';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function createPurchase(Request $request) {

        if (Auth::check()) {

            $user = Auth::user();

            $account = $user->getPurchaseAccount();

            $invoice_id = $this->getNextInvoiceId();

            $current_price = $this->discounted_price > 0 ? $this->discounted_price : $this->normal_price;

            $purchase = new Purchase;
            $purchase->account_id = $account->id;
            $purchase->invoice_id = $invoice_id;
            $purchase->bank_id = $request->input('bank');
            $purchase->used_listing = 0;
            $purchase->used_free_listing = 0;
            $purchase->total_amount = $current_price * 1.07;
            $purchase->updated_amount = 0;
            $purchase->payment_status = 'pending';
            $purchase->payment_method = $request->input('poption');
            $purchase->save();

            $this->createPurchaseData($purchase);

            return $purchase;
        }

        return null;
    }

    protected function createPurchaseData($purchase) {
        
        $current_price = $this->discounted_price > 0 ? $this->discounted_price : $this->normal_price;
        
        $data = new PurchaseData;
        $data->purchase_id = $purchase->id;
        $data->package_id = $this->id;
        $data->listing_count = $this->listing_count;
        $data->free_listing_count = $this->free_listing_count;
        $data->duration = $this->duration;
        $data->price = $current_price;
        $data->save();
    }

    protected function getNextInvoiceId() {

        $today = date('Y-m-d');

        $count = DB::table('customer_purchases')
                        ->whereRaw(DB::raw("to_char(created_at, 'YYYY-MM-DD') = '{$today}'"))->count() + 1;

        return sprintf('%04d', $count) . date('dmy');
    }

}
