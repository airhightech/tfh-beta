<?php

namespace App\Models\Listings;

use App\Models\Property;

class ExclusiveProject {

    public static function get($count = 9) {
        return Property::select('properties.*')->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                        ->where('property_listing_data.listing_class', 'exclusive')
                        ->where('properties.published', true)
                        ->where('properties.new_project', true)
                       /* ->whereRaw('property_listing_data.upgrade_start <= NOW()')
                        ->whereRaw('property_listing_data.upgrade_end >= NOW()')*/
//                        ->where(function($query) {
//                            $query->orWhereRaw('property_listing_data.expiration_date >= NOW()');
//                            $query->orWhereRaw('property_listing_data.expiration_date IS NULL');
//                        })
                        ->limit($count)->get();
    }

}
