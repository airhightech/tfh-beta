<?php

namespace App\Models\Listings;

use App\Models\Property;

class ExclusiveListing {

    public static function get($type, $count = 9) {
        return Property::select('properties.*')->leftJoin('property_listing_data', 'property_listing_data.property_id', 'properties.id')
                        ->where('property_listing_data.listing_class', 'exclusive')
                        ->where('property_listing_data.ltype', $type)
                        ->where('properties.published', true)
                        ->where('properties.new_project', false)
                        ->whereRaw('properties.publication_date <= NOW()')
//                        ->where(function($query) {
//                            $query->orWhereRaw('property_listing_data.expiration_date >= NOW()');
//                            $query->orWhereRaw('property_listing_data.expiration_date IS NULL');
//                        })
                        ->limit($count)->get();
    }

}
