<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

/**
 * Exclusive packages
 */
class Paysbuy extends Model
{
    protected $table = 'paysbuy_payments';
    
    public function getPaymentMethod() {
        $payments = [
            '00' => 'Alipay',
            '01' => 'Paysbuy',
            '02' => 'Credit Card',
            '03' => 'Paypal',
            '04' => 'American Express',
            '05' => 'Online Direct Credit',
            '06' => 'Cash channel',
            '09' => 'Installment program',
            '12' => 'Pay on Delivery'
        ];
        
        return array_get($payments, $this->method);
    }
}
