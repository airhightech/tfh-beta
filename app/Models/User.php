<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\User\Profile;
use App\Models\User\PersonalData;
use App\Models\User\CompanyData;
use App\Models\Customer\Account;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{    
    use SoftDeletes;
    use Notifiable;
    use User\PropertyStat;
    use User\FavoriteTrait;
    use User\PurchaseTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function getPublicId() {
        $types = [
            'owner' => 'O',
            'agent' => 'A',
            'company' => 'A',
            'developer' => 'L',
            'visitor' => 'U',
            'manager' => 'M'
        ];
        
        $prefix = array_get($types, $this->ctype, false);
        
        return $prefix ? $prefix . sprintf("%06d", $this->id) : $this->id;
    }
    
    public function isAgent() {
        return in_array($this->ctype, ['agent', 'company']);
    }
    
    public function getEmailValidationToken() {
        if (!$this->email_validation_token) {
            $this->email_validation_token = str_random(20);
            $this->save();
        }
        return $this->email_validation_token;
    }
    
    public function getProfile() {
        $profile = Profile::find($this->id);
        if (!$profile) {
            $profile = new Profile();
            $profile->user_id = $this->id;
            $profile->save();
        }
        return $profile;
    }
    
    public function getPersonalData() {
        $personal = PersonalData::find($this->id);
        if (!$personal) {
            $personal = new PersonalData();
            $personal->user_id = $this->id;
            $personal->save();
        }
        return $personal;
    }
    
    public function getCompanyData() {
        $company = CompanyData::find($this->id);
        if (!$company) {
            $company = new CompanyData();
            $company->user_id = $this->id;
            $company->save();
        }
        return $company;
    }
    
    public function getLoginStats() {
        $logins = DB::table('login_history')
                ->select(DB::raw('COUNT(created_at) AS logins'), 'source')
                ->where('user_id', $this->id)
                ->groupBy('source')
                ->pluck('logins', 'source')->all();
        return $logins;
    }
    
    public function countLogin() {
        return DB::table('login_history')->where('user_id', $this->id)->count();
    }
    
    public function countComments() {
        return DB::table('customer_reviews')->where('customer_id', $this->id)->count();
    }
    
    public function getLastLogin($format = 'd/m/Y', $default = '-') {
        if ($this->last_login) {
            return date($format, strtotime($this->last_login));
        } else {
            return $default;
        }
    }
    
    public function getPurchaseAccount() {
        
        $account_id = DB::table('customer_accounts_managers')->where('user_id', $this->id)->value('account_id');
        
        $account = null;
        
        if ($account_id > 0) {
            $account = Account::find($account_id);
        }
        else {
            $account = new Account;
            $account->creator_id = $this->id;
            $account->save();
            
            // Add manager
            
            DB::insert('INSERT INTO customer_accounts_managers (account_id, user_id) VALUES (?, ?)',
                    [$account->id, $this->id]);
        }
        
        return $account;
    }
    
    public function getVisitorPicture($size = '100x100') {
        if ($this->avatar) {
            return $this->avatar;
        } else {
            $personalData = $this->getPersonalData();
            if ($personalData->profile_picture_path) {
                return url('/thumb/' . $size . '/' . $personalData->profile_picture_path);
            }
        }        
        return null;
    }
    
    public function deleteAllData() {
        DB::table('agent_reviews')->where('user_id', $this->id)->delete();
        DB::table('agent_reviews')->where('agent_id', $this->id)->delete();
        
        DB::table('customer_accounts_managers')->where('user_id', $this->id)->delete();
        DB::table('customer_review_likes')->where('user_id', $this->id)->delete();
        DB::table('customer_review_replies')->where('user_id', $this->id)->delete();
        DB::table('customer_reviews')->where('customer_id', $this->id)->delete();
        
        DB::table('favorite_agents')->where('agent_id', $this->id)->delete();
        DB::table('favorite_agents')->where('user_id', $this->id)->delete();
        
        DB::table('favorite_properties')->where('user_id', $this->id)->delete();
        DB::table('login_history')->where('user_id', $this->id)->delete();
        DB::table('messages')->where('agent_id', $this->id)->delete();
        DB::table('notification_receivers')->where('agent_id', $this->id)->delete();
        
        DB::table('personal_profiles')->where('user_id', $this->id)->delete();
        DB::table('profiles')->where('user_id', $this->id)->delete();
        DB::table('publisher_company_profiles')->where('user_id', $this->id)->delete();
        DB::table('saved_searches')->where('user_id', $this->id)->delete();
        DB::table('visitor_property_reviews')->where('user_id', $this->id)->delete();
    }
    
    public function deleteProperties() {
        $property_ids = DB::table('properties')->where('publisher_id', $this->id)->pluck('id')->all();        
        
        DB::table('building_data')->whereIn('property_id', $property_ids)->delete();
        DB::table('building_have_room_types')->whereIn('property_id', $property_ids)->delete();
        DB::table('customer_reviews')->whereIn('property_id', $property_ids)->delete();
        DB::table('favorite_properties')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_belongs_to_building')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_extended_data')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_have_facilities')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_have_features')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_have_images')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_listing_data')->whereIn('property_id', $property_ids)->delete();
        DB::table('property_nearby_transportations')->whereIn('property_id', $property_ids)->delete();
        DB::table('visitor_property_reviews')->whereIn('property_id', $property_ids)->delete();
        DB::table('properties')->whereIn('id', $property_ids)->delete();
    }
}
