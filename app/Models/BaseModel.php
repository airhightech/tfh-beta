<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Language;
use Illuminate\Http\Request;

class BaseModel extends Model {

    /**
     * Set the values of a translatable field
     * @param type $field
     * @param type $values
     */
    public function setTranslatable($field, $values) {

        if ($values instanceof Request) {
            $values = $values->input($field, []);
        }
        
        $langs = Language::getActiveCodes();

        if (is_array($values)) {
            
            $old_options = json_decode($this->$field, true);
            $new_options = array_only($values, $langs);

            foreach ($new_options as $lang => $value) {
                $old_options[$lang] = $value;
                if (!array_key_exists($lang, $old_options)) {
                    $old_options[$lang] = '';
                }
            }

            $this->$field = json_encode($old_options);
        } else if (is_string($values)) {
            $lang = \App::getLocale();
            $new_options = [];
            foreach($langs as $code) {
                $new_options[$code] = $values;
            }
            $this->$field = json_encode($new_options);
        }
    }

    /**
     * Get the value of a translatable field
     * @param type $field
     * @param type $lang
     * @param type $fallback_lang
     */
    public function getTranslatable($field, $lang = null, $fallback_lang = 'en') {

        $langs = Language::getActiveCodes();
        if ($lang == null || in_array($lang, $langs) == false) {
            $lang = \App::getLocale();
        }
        
        if (!$this->$field) {
            return null;
        }

        $options = json_decode($this->$field, true);
        $tr = array_get($options, $lang);

        if (is_array($tr)) {
            $tr = array_shift($tr);
        }
        
        $tr = trim($tr);
        
        $fallback_value = null;
        
        if (is_array($options)) {
            $fallback_value = isset($options[$fallback_lang]) ? $options[$fallback_lang] : array_shift($options);
        }
        
        if (is_array($fallback_value)) {
            $fallback_value = array_shift($fallback_value);
        }

        return $tr ? $tr : trim($fallback_value);
    }

    /**
     * Shortcut for BaseModel::getTranslatable
     * @param type $field
     * @param type $lang
     * @param type $fallback_lang
     * @return type
     */
    public function tr($field, $lang = null, $fallback_lang = 'en') {
        return $this->getTranslatable($field, $lang, $fallback_lang);
    }

    protected function setSecureInteger($field, $value) {
        if ($value instanceof Request) {
            $value = $value->input($field);
        }

        if ($value) {
            $this->$field = $value;
        }
    }

    protected function updateTranslatable($field, $options) {

        if (array_key_exists($field, $options) && !empty($options[$field])) {
            $this->setTranslatable($field, $options[$field]);
        }
    }

    protected function updateIfNotEmpty($field, $options, $default = null) {
        if (array_key_exists($field, $options) && !empty($options[$field])) {
            $this->$field = $options[$field];
        } else if ($default !== null) {
            $this->$field = $default;
        }
    }
    
    protected function updateBoolean($field, $options, $default = null) {
        if (array_key_exists($field, $options) && !empty($options[$field])) {
            $this->$field = ($options[$field] == 'true');
        } else if ($default !== null) {
            $this->$field = $default;
        }
    }

    public function updateDate($field, $options) {
        if (array_key_exists($field, $options) && !empty($options[$field])) {

            if (preg_match('/(?P<date>\d{2})\/(?P<month>\d{2})\/(?P<year>\d{4})/'
                            , $options[$field], $matches)) {
                $this->$field = $matches['year'] . '-' . $matches['month'] . '-' . $matches['date'];
            } else {
                $this->$field = date('Y-m-d');
            }
        } else {
            $this->$field = date('Y-m-d');
        }
    }
    
    public function getHRDate($field, $format = 'd/m/Y') {
        return date($format, strtotime($this->$field));
    }

    /**
     * Fix Carbon bug which cannot convert date that have milliseconds
     * @param type $value
     * @return type
     */
    protected function asDateTime($value) {
        if (preg_match('/^(\d{4})-(\d{1,2})-(\d{1,2}) (\d{2}):(\d{2}):(\d{2})\.(\d+)$/', $value)) {
            $value = strtotime($value);
        }
        return parent::asDateTime($value);
    }
    
    public static function getLangCollation() {
        $lang = \App::getLocale();
        $collation = 'en_US';
        
        switch ($lang) {
            case 'en':
                $collation = 'en_US';
                break;
            case 'th':
                $collation = 'th_TH';
                break;
            case 'ja':
                $collation = 'ja_JP';
                break;
            case 'zh':
                $collation = 'zh_CN';
                break;
            case 'ko':
                $collation = 'ko_KR';
                break;
        }
        
        return $collation;
    }

}
