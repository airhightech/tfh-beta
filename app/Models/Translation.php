<?php

namespace App\Models;
/**
 * Condominium class
 */
class Translation extends BaseModel {

    protected $table = 'translations';
    
    protected $casts = ['translation' => 'json'];
    
}