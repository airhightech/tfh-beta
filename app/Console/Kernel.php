<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        /*Commands\FakeUser::class,
        Commands\FakePlace::class,
        Commands\FakeLocation::class,
        Commands\FakeAddress::class,
        Commands\FakeProperty::class,
        Commands\FakePropertyImage::class,
        Commands\FakePackage::class,
        Commands\FakeBank::class,
        Commands\FakeListed::class,
        Commands\MigratePlace::class,
        Commands\MigrateCondo::class,
        Commands\MigrateCondoData::class,
        Commands\MigrateCondoImage::class,
        Commands\MigrateLocation::class,
        Commands\MigrateFacility::class,
        Commands\MigrateFeature::class,*/
        Commands\Newsletter::class,
        Commands\ListingSync::class,
        //Commands\TestEmails::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->command('newsletters:send')
                  ->everyMinute();
        
        $schedule->command('listing:sync')
                  ->everyTenMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands() {
        require base_path('routes/console.php');
    }

}
