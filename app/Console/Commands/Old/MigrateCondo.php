<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigrateCondo extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:condo-list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        DB::table('manager_property_reviews')->whereRaw('true')->delete();
        DB::table('property_belongs_to_building')->whereRaw('true')->delete();
        DB::table('properties')->whereRaw('true')->delete();
        DB::table('property_address')->whereRaw('true')->delete();

        $handler = fopen(storage_path('app/condo_list.csv'), "r");

        $row = 1;

        $places = [];

        $now = date('Y-m-d H:i:d');

        $manager = DB::table('users')->where('ctype', 'manager')->first();

        while (($data = fgetcsv($handler, 1000, ",")) !== FALSE) {

            if ($data[0] != 'id') {

                // Create address

                if ($data[1]) {
                    $address_id = DB::table('property_address')->insertGetId([
                        'location' => $data[1],
                        'province_id' => $data[7] + 0,
                        'district_id' => $data[8] + 0,
                        'area_id' => $data[9] + 0,
                        'created_at' => $now,
                        'updated_at' => $now
                    ]);
                    
                    DB::table('properties')->where('id', $data[0])->delete();

                    $places[] = [
                        'id' => $data[0],
                        'publisher_id' => $manager->id,
                        'name' => $data[12],
                        'address_id' => $address_id,
                        'ptype' => 'cdb',
                        'new_project' => false,
                        'user_submited' => false,
                        'published' => true,
                        'publication_date' => '2016-12-31',
                        'created_at' => $now,
                        'updated_at' => $now
                    ];
                }
            }
        }
        fclose($handler);

        DB::table('properties')->insert($places);
    }

}
