<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class FakeBank extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:bank';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 4; $i++) {
            $data = [
                'code' => $faker->stateAbbr,
                'name' => $faker->company,
                'account_no' => $faker->swiftBicNumber,
                'branch' => $faker->city,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            
            DB::table('bank_accounts')->insert($data);
        }
    }

}
