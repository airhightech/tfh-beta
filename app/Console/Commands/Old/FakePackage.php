<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Helpers\Language;

class FakePackage extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $this->createPackages('exclusive');
        $this->createPackages('featured');
    }
    
    protected function createPackages($type, $count = 3) {
        
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < $count; $i++) {

            $price = $faker->numberBetween(10000, 20000);
            $listing_count = $i + 1;

            $data = [
                'name' => $this->getNames(),
                'type' => $type,
                'normal_price' => $price,
                'discounted_price' => ($price * 0.85),
                'listing_count' => $listing_count,
                'free_listing_count' => $this->getFreelistingCount($type, $listing_count),
                'duration' => 90,
                'created_at' => DB::raw('NOW()'),
                'updated_at' => DB::raw('NOW()')
            ];
            
            DB::table('listing_packages')->insert($data);
        }
    }
    
    protected function getFreelistingCount($type, $listing_count) {
        if ($type == 'featured') {
            return 0;
        } else {
            return (3 * $listing_count) + 1;
        }
    }

    protected function getNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->firstNameFemale;
        }

        return json_encode($names);
    }

}
