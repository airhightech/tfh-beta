<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigratePlace extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:place';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        DB::delete('TRUNCATE places CASCADE');

        $handler = fopen(storage_path('app/search_pois.csv'), "r");

        $row = 1;

        $places = [];

        $now = date('Y-m-d H:i:d');

        while (($data = fgetcsv($handler, 1000, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $places[] = [
                    'name' => $data[6],
                    'location' => $data[3],
                    'type' => $data[5],
                    'zoom_level' => 15,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }
        fclose($handler);

        DB::table('places')->insert($places);
    }

}
