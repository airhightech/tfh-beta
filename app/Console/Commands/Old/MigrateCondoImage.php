<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigrateCondoImage extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:condo-image';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $handler = fopen(storage_path('app/condo_have_media.csv'), "r");

        $images = [];

        while (($data = fgetcsv($handler, 4096, ",")) !== FALSE) {

            $count = DB::table('properties')->where('id', $data[0])->count();

            if ($count) {
                $images[] = [
                    'property_id' => $data[0]
                    , 'filepath' => $data[3]
                ];
            }
        }

        fclose($handler);

        DB::table('property_have_images')->insert($images);
    }

}
