<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Helpers\Language;

class FakePlace extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:place';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {


        $faker = \Faker\Factory::create();

        $data = [];
        
        $count = 100;

        for ($i = 0; $i < $count; $i++) {

            $lng = $faker->longitude(100.243783, 100.763573);
            $lat = $faker->longitude(13.603501, 13.911629);

            $location = "ST_GeomFromText('POINT($lng $lat 0.0)', 4326)";

            $type = $faker->randomElement(['bts', 'mrt', 'apl', 'bank', 'dpt-store', 'hospital', 'school']);
            
            $data[] = [
                'location' => DB::raw($location),
                'name' => $this->getNames(),
                'type' => $type
            ];
        }

        DB::table('places')->insert($data);
    }
    
    protected function getNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->city;
        }

        return json_encode($names);
    }

}
