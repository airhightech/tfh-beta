<?php

namespace App\Console\Commands;

use Mail;
use App\Emails\EmailValidation;
use Illuminate\Console\Command;
use App\Models\User;

class TestEmails extends Command {

    protected $name = 'email:test';
    protected $description = 'Test sending emails Newsletter';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $user = User::where('email', 'liva.ramarolahy@gmail.com')->first();
        Mail::to($user)->send(new EmailValidation($user));        
    }

}
