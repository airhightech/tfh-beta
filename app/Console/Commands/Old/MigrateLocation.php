<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigrateLocation extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        DB::table('geo_area')->truncate();
        DB::table('geo_districts')->truncate();
        DB::table('geo_provinces')->truncate();

        $handler = fopen(storage_path('app/provinces.csv'), "r");

        $provinces = [];

        while (($data = fgetcsv($handler, 4096, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $provinces[] = [
                    'id' => $data[0],
                    'name' => $data[3],
                    'created_at' => $data[1],
                    'updated_at' => $data[2]
                ];
            }
        }
        
        fclose($handler);
        
        DB::table('geo_provinces')->insert($provinces);
        
        // District        
        
        $handler = fopen(storage_path('app/districts.csv'), "r");

        $districts = [];

        while (($data = fgetcsv($handler, 4096, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $districts[] = [
                    'id' => $data[0],
                    'name' => $data[4],
                    'province_id' => $data[1],
                    'created_at' => $data[2],
                    'updated_at' => $data[3]
                ];
            }
        }
        
        fclose($handler);
        
        DB::table('geo_districts')->insert($districts);
        
        // Area        
        
        $handler = fopen(storage_path('app/geo_area.csv'), "r");

        $area = [];

        while (($data = fgetcsv($handler, 4096, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $area[] = [
                    'id' => $data[0],
                    'name' => $data[4],
                    'district_id' => $data[1],
                    'created_at' => $data[2],
                    'updated_at' => $data[3]
                ];
            }
        }
        
        fclose($handler);
        
        DB::table('geo_area')->insert($area);
    }

}
