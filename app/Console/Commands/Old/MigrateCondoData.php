<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigrateCondoData extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:condo-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        DB::table('manager_property_reviews')->whereRaw('true')->delete();
        DB::table('property_extended_data')->whereRaw('true')->delete();
        DB::table('property_listing_data')->whereRaw('true')->delete();
        DB::table('building_data')->whereRaw('true')->delete();

        $handler = fopen(storage_path('app/condo_profile_data.csv'), "r");

        $extended = [];
        $listing = [];
        $building = [];

        while (($data = fgetcsv($handler, 4096, ",")) !== FALSE) {



            if ($data[0] != 'condo_id') {

                $count = DB::table('properties')->where('id', $data[0])->count();
                // extended

                if ($count) {
                    $extended[] = [
                        'property_id' => $data[0],
                        'custom_address' => json_encode(['th' => $data[9]]),
                        'starting_sales_price' => $data[1] + 0,
                        'office_hours' => $data[2],
                        'year_built' => $data[3] + 0,
                        'total_floors' => $data[5] + 0,
                        'website' => $data[8],
                        'psf' => 100000,
                        'details' => $data[0],
                        'contact_person' => $data[10]
                    ];

                    // listing

                    $listing[] = [
                        'property_id' => $data[0]
                    ];

                    $building[] = [
                        'property_id' => $data[0],
                        'number_of_tower' => $data[4] + 0,
                        'total_units' => $data[6] + 0,
                        'developer_id' => 0,
                        'parking_lot' => 20
                    ];
                }
            }
        }

        fclose($handler);

        DB::table('property_extended_data')->insert($extended);
        DB::table('property_listing_data')->insert($listing);
        DB::table('building_data')->insert($building);
    }

}
