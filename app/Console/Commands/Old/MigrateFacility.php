<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigrateFacility extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:facilities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        DB::delete('TRUNCATE property_facilities CASCADE');

        $handler = fopen(storage_path('app/condo_facilities.csv'), "r");

        $row = 1;

        $places = [];

        $now = date('Y-m-d H:i:d');

        while (($data = fgetcsv($handler, 1000, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $places[] = [
                    'id' => $data[0],
                    'name' => $data[5],
                    'icon_path' => $data[3],
                    'listing_rank' => $data[4],
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }
        fclose($handler);

        DB::table('property_facilities')->insert($places);
    }

}
