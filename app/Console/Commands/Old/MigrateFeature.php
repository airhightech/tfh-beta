<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class MigrateFeature extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:features';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        DB::delete('TRUNCATE property_features CASCADE');

        $handler = fopen(storage_path('app/property_features.csv'), "r");

        $row = 1;

        $places = [];

        $now = date('Y-m-d H:i:d');

        while (($data = fgetcsv($handler, 1000, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $places[] = [
                    'name' => $data[5],
                    'icon_path' => $data[1],
                    'listing_rank' => $data[4],
                    'optional' => false,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }
        
        fclose($handler);
        
        $handler = fopen(storage_path('app/optional_features.csv'), "r");

        while (($data = fgetcsv($handler, 1000, ",")) !== FALSE) {

            if ($data[0] != 'id') {
                $places[] = [
                    'name' => $data[5],
                    'icon_path' => $data[1],
                    'listing_rank' => $data[4],
                    'optional' => true,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }
        fclose($handler);

        DB::table('property_features')->insert($places);
    }

}
