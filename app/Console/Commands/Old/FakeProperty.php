<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Language;
use DB;
use App\Models\Property;

class FakeProperty extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:property';
    protected $provinces;
    protected $districts;
    protected $areas;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $this->line('Loading geo data');
        $this->line('Loading profile data');

        $this->createPropertiesForType('owner');
        $this->createPropertiesForType('agent');
        $this->createPropertiesForType('company');
    }

    protected function createPropertiesForType($type) {

        $this->line('Create data property for type : ' . $type);

        $users = DB::table('users')->where('ctype', $type)->pluck('id');

        $bar = $this->output->createProgressBar(count($users));

        foreach ($users as $id) {
            $this->createProperty($id, Property::TYPE_CONDO_BUILDING, 3);
            $this->createProperty($id, Property::TYPE_APARTMENT_BUILDING, 3);
            $this->createProperty($id, Property::TYPE_CONDO_ROOM, 25);
            $this->createProperty($id, Property::TYPE_APARTMENT_ROOM, 25);
            $this->createProperty($id, Property::TYPE_SINGLE_HOUSE, 25);
            $this->createProperty($id, Property::TYPE_TOWNHOUSE, 25);
            $bar->advance();
        }

        $bar->finish();
        $this->line('');
    }

    protected function createProperty($user_id, $type, $count = 10) {

        $address_ids = DB::table('property_address')->inRandomOrder()->take($count)->pluck('id');

        $faker = \Faker\Factory::create();

        $now = date('Y-m-d H:i:s');

        for ($i = 0; $i < $count; $i++) {

            $_data = [
                'publisher_id' => $user_id,
                'name' => $this->getNames(),
                'address_id' => array_get($address_ids, $i),
                'ptype' => $type,
                'new_project' => $faker->randomElement([true, false]),
                'user_submited' => $faker->randomElement([true, false]),
                'published' => true,
                'publication_date' => '2016-01-01',
                'created_at' => $now,
                'updated_at' => $now
            ];

            $id = DB::table('properties')->insertGetId($_data);

            // listing data

            $lt = $faker->randomElement(['rent', 'sale']);

            $deposit = 0;
            $listing_price = 0;
            $minimal_rental_period = 0;

            if ($lt == 'rent') {
                $listing_price = $faker->numberBetween(5000, 100000);
                $deposit = $listing_price * 2;
                $minimal_rental_period = $faker->numberBetween(3, 6);
            } else {
                $listing_price = $faker->numberBetween(1000000, 20000000);
                $deposit = round($listing_price / 10);
            }

            $_data = [
                'property_id' => $id,
                'ltype' => $lt,
                'title' => $this->getTitles(),
                'deposite' => $deposit,
                'listing_price' => $listing_price,
                'minimal_rental_period' => $minimal_rental_period,
                'listing_class' => $faker->randomElement(['exclusive', 'featured', 'standard']),
                'movein_date' => '2017-01-25',
                'ylink1' => $faker->url,
                'ylink2' => $faker->url,
            ];

            DB::table('property_listing_data')->insert($_data);

            // extended data

            $total_floor = $faker->numberBetween(1, 30);

            $_data = [
                'property_id' => $id,
                'custom_address' => $this->getAddress(),
                'starting_sales_price' => $faker->numberBetween(1000000, 5000000),
                'office_hours' => '10:00 - 18:00',
                'year_built' => $faker->numberBetween(2000, 2015),
                'total_floors' => $total_floor,
                'total_size' => $faker->numberBetween(20, 120),
                'land_size' => $faker->numberBetween(20, 120),
                'floor_num' => $faker->numberBetween(1, $total_floor),
                'bedroom_num' => $faker->numberBetween(1, 6),
                'bathroom_num' => $faker->numberBetween(1, 3),
                'website' => $faker->domainName,
                'psf' => $faker->numberBetween(100000, 300000),
                'details' => $this->getDetails(),
                'contact_person' => $this->name,
            ];

            DB::table('property_extended_data')->insert($_data);

            // building data

            if ($type == Property::TYPE_CONDO_BUILDING || $type == Property::TYPE_APARTMENT_BUILDING) {

                $developer_id = DB::table('users')->where('ctype', 'developer')->inRandomOrder()->take(1)->value('id');
                $_data = [
                    'property_id' => $id,
                    'number_of_tower' => $faker->numberBetween(1, 6),
                    'total_units' => $faker->numberBetween(10, 150),
                    'developer_id' => $developer_id
                ];
                DB::table('building_data')->insert($_data);
            }
        }
    }

    protected function getNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->catchPhrase;
        }

        return json_encode($names);
    }

    protected function getTitles() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->catchPhrase;
        }

        return json_encode($names);
    }

    protected function getAddress() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->streetAddress;
        }

        return json_encode($names);
    }

    protected function getDetails() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->text();
        }

        return json_encode($names);
    }

}
