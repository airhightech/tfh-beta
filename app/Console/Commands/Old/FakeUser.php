<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Hash;
use App\Helpers\Language;

class FakeUser extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function handle() {
        
        //$user
        
        // 20 visitors
        
        $this->line('Creating visitor profile');
        $this->createUser('visitor', 20);
        
        // 20 owners
        
        $this->line('Creating owner profile');
        $this->createUser('owner', 20);
        
        // 20 freelances
        
        $this->line('Creating agent profile');
        $this->createUser('agent', 20);
        
        // 20 companies
        
        $this->line('Creating company profile');
        $this->createUser('company', 20);
        
        // 20 developers
        
        $this->line('Creating developer profile');
        $this->createUser('developer', 20);     
        
    }
    
    protected function createUser($type, $count = 20) {
        
        $faker = \Faker\Factory::create();        
        $stop = min($count, 99);        
        $data = [];        
        $password = Hash::make('123456');        
        $langs = Language::getActiveCodes();
        
        for($i = 0; $i < $stop; $i++) {
            $email = $type . '.' . $faker->safeEmail;
            
            $date = $faker->dateTimeBetween('-5 years', '-6 months');
            $login = $faker->dateTimeBetween($date);
            
            $data[] = [
                'email' => $email,
                'password' => $password,
                'ctype' => $type,
                'created_at' => $date->format('Y-m-d H:i:s'),
                'updated_at' => $date->format('Y-m-d H:i:s'),
                'last_login' => $login->format('Y-m-d H:i:s'),
                'email_validated' => true,
                'name' => $faker->name(),
                'active' => $faker->randomElement([true, false]),
                'lang' => $faker->randomElement($langs),
                'login_count' => $faker->numberBetween(1, 1000),
                'comment_count' => $faker->numberBetween(1, 1000),
            ];
        }
        
        DB::table('users')->insert($data);
    }
    
}