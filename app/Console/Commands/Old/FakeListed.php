<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class FakeListed extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:listed';
    protected $provinces;
    protected $districts;
    protected $areas;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $condos = DB::table('properties')
                ->where('ptype', 'cdb')
                ->where('new_project', false)
                ->where('published', true)
                ->pluck('id')->all();


        $properties = DB::table('properties')
                ->where(function($query) {
                    $query->orWhere('ptype', 'ap');
                    $query->orWhere('ptype', 'cd');
                    $query->orWhere('ptype', 'sh');
                    $query->orWhere('ptype', 'th');
                })
                ->where('new_project', false)
                ->where('published', true)
                ->pluck('id')->all();
                
                $size = floor(count($properties) / count($condos));

        $listed = array_chunk($properties, $size);

        foreach ($condos as $index => $condo) {
            $data = [];
            $list = $listed[$index];
            foreach($list as $prop) {
                $data[] = [
                    'building_id' => $condo,
                    'property_id' => $prop
                ];
            }
            
            DB::table('property_belongs_to_building')->insert($data);
        }
    }

}
