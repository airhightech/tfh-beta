<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class FakePropertyImage extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:property-image';
    protected $provinces;
    protected $districts;
    protected $areas;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        $images = glob(storage_path('app/public/*'));
        
        $faker = \Faker\Factory::create();
        
        $ids = DB::table('properties')->pluck('id');
        
        $bar = $this->output->createProgressBar(count($ids));
        
        foreach($ids as $id) {
            shuffle($images);
            $_images = array_slice($images, 0, 10);
            
            $data = [];
            
            foreach($_images as $image) {
                $data[] = [
                    'property_id' => $id,
                    'filepath' => str_replace(storage_path('app/public/'), '', $image)
                ];
            }
            
            DB::table('property_have_images')->insert($data);
            
            $bar->advance();
        }
        
        $bar->finish();
        $this->line('');
    }
}