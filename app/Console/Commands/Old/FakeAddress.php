<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Helpers\Language;

class FakeAddress extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {


        $faker = \Faker\Factory::create();
        
        $count = 500;
        
        $bar = $this->output->createProgressBar($count);

        for ($i = 0; $i < $count; $i++) {

            $lng = $faker->longitude(100.243783, 100.763573);
            $lat = $faker->longitude(13.603501, 13.911629);

            $location = "ST_GeomFromText('POINT($lng $lat 0.0)', 4326)";
            
            $data = [
                'location' => DB::raw($location),
                'area_name' => $this->getNames(),
                'district_name' => $this->getNames(),
                'province_name' => $this->getNames(),
                'street_number' => $faker->buildingNumber,
                'street_name' => $this->getStreetNames(),
                'postal_code' => $faker->postcode
            ];
            
            DB::table('property_address')->insert($data);
            
            $bar->advance();
            
        }
        
        $bar->finish();
        $this->line('');        
    }
    
    protected function getNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->city;
        }

        return json_encode($names);
    }
    
    protected function getStreetNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->streetName;
        }

        return json_encode($names);
    }

}
