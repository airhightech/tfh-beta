<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


class UpdateNearbyPlaces extends Command {
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'address:places';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update nearby places around address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        // use address and place dates to check updates
        
    }
    
}