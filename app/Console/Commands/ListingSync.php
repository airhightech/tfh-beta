<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class ListingSync extends Command {
    
    protected $signature = 'listing:sync';
    
    protected $description = 'Update listing dates';
    
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        $time = date('Y-m-d H:i:s');
        
        // Inactive expired listings
        
        DB::update('UPDATE properties SET published = false WHERE publication_date > ? OR id IN '
                . '(SELECT property_id FROM property_listing_data WHERE expiration_date < ?) AND ptype <> ?', [$time, $time, 'cdb']);
        
        // Reset expired upgrades
        
        DB::update('UPDATE property_listing_data SET upgrade_start = NULL, upgrade_end = NULL, listing_class = ? WHERE upgrade_end < ?'
                , ['standard', $time]);
    }
    
}