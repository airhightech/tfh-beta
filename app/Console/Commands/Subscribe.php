<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class Subscribe extends Command {
    
    protected $signature = 'subscribe:members';
    
    protected $description = 'Subscribe member users';
    
    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        
        $users = DB::select('SELECT * FROM users WHERE email NOT IN (SELECT email FROM newsletter_subscriptions WHERE true)');
        
        $emails = [];
        
        $date = date('Y-m-d H:i:s');
        
        foreach($users as $user) {            
            $lang = $user->lang ? $user->lang : 'en';            
            $emails[] = [
                'email' => $user->email,
                'created_at' => $date,
                'updated_at' => $date,
                'prefered_lang' => $lang
            ];
        }
        
        DB::table('newsletter_subscriptions')->insert($emails);
    }
    
}