<?php

namespace App\Console\Commands;

use Mail;
use Illuminate\Console\Command;
use App\Models\Messages\Newsletter as NewsData;
use App\Models\Messages\NewsletterSubscription;

class Newsletter extends Command {

    protected $name = 'newsletters:send';
    protected $description = 'Send Newsletter';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $newsletters = NewsData::where('status', 'PENDING')
                        ->whereDate('send_time', '=', date('Y-m-d'))->get()->all();
        $newsletterSubscription = NewsletterSubscription::all();

        //Browse newsletters with pending status
        foreach ($newsletters as $newsletter) {
            $mailBody = json_decode($newsletter->mail_body);
            //Browse subscriber's email
            foreach ($newsletterSubscription as $subscription) {
                $this->sendNewsletter($subscription->email, $newsletter, $subscription->prefered_lang);
            }
            $newsletter->status = 'SENT';
            $newsletter->save();
        }
    }

    public function sendNewsletter($mail, $newsletter, $prefered_lang) {
        $mailBody = json_decode($newsletter->mail_body);
        $subject = json_decode($newsletter->subject);
        $mailBody = str_replace("{{}}", $mail, $mailBody->$prefered_lang);
        Mail::send(array(), array(), function ($message) use ($mailBody, $subject, $mail, $newsletter, $prefered_lang) {
            $message->to($mail)
                    ->subject($subject->$prefered_lang)
                    ->from('no-reply@thaifullhouse.com', 'ThaiFullHouse')
                    ->setBody($mailBody, 'text/html');
        });
    }

}
