<?php

namespace App\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class ResetPasswordLink extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->subject(tr('emails.reset-password', 'Password reset'))
                        ->from('no-reply@thaifullhouse.com', 'Thai Full House')
                        ->view('emails.reset-password')
                        ->with([
                            'user' => $this->user
        ]);
    }

}
