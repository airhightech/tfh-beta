<?php

namespace App\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailAlert extends Mailable {
    
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    protected $object;
    
    protected $message;
    
    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($object, $message)
    {
        $this->object = $object;
        
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[EMAIL ALERT] - ' . $this->object)
                    ->from('info@thaifullhouse.com', 'Thai Full House')
                    ->view('emails.account-created')
                    ->with([
                        'message' => $this->message
                    ]);
    }  
}
