<?php

namespace App\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class EmailValidation extends Mailable {
    
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->user->getEmailValidationToken();
        return $this->subject(tr('emails.email-validation', 'Email validation'))
                    ->from('no-reply@thaifullhouse.com', 'Thai Full House')
                    ->view('emails.email-validation')
                    ->with(['user' => $this->user]);
    }    
}
