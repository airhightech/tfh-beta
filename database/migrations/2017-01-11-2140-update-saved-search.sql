--
--
--

ALTER TABLE saved_searches DROP COLUMN search_query;
ALTER TABLE saved_searches ADD COLUMN search_query JSONB;