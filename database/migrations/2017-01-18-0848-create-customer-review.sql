--
--
--

CREATE TABLE customer_reviews (
    id SERIAL PRIMARY KEY,
    customer_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    review TEXT,
    ratings JSONB,
    images TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);