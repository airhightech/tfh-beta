--
--
--

CREATE TABLE icons (
    id SERIAL PRIMARY KEY,
    code VARCHAR(100),
    filepath VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(code)
);