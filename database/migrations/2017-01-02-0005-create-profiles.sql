--
--
--

CREATE TABLE profiles ( 
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    phone VARCHAR(255),
    mobile VARCHAR(255),
    line_id VARCHAR(255),    
    custom_address TEXT,
    address_id INTEGER,
    languages VARCHAR(255),    
    profile_type VARCHAR(20), -- owner, freelance, company, developer
    total_visitors INTEGER,
    introduction TEXT,
    cover_picture_path VARCHAR(255),   
    profile_picture_path VARCHAR(255),   
    logo_picture_path VARCHAR(255),   
    verified BOOLEAN DEFAULT FALSE,
    accept_quick_matching BOOLEAN DEFAULT FALSE,
    listing_count INTEGER,
    search_ranking_level INTEGER,
    UNIQUE(user_id)
);

--
--
--

CREATE TABLE personal_profiles (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    firstname VARCHAR(255), 
    lastname VARCHAR(255), 
    citizen_id VARCHAR(255),
    citizen_id_attachment VARCHAR(255),
    gender VARCHAR(255),
    nationality VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(user_id)
);

--
--
-- 

CREATE TABLE publisher_company_profiles (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    company_name VARCHAR(255),
    company_registration VARCHAR(255),
    company_contact_name VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(user_id)
);