--
--
--

ALTER TABLE manager_property_reviews ADD COLUMN comment_loc JSONB;
ALTER TABLE manager_property_reviews ADD COLUMN comment_fac JSONB;
ALTER TABLE manager_property_reviews ADD COLUMN comment_pla JSONB;

ALTER TABLE manager_property_reviews DROP COLUMN comments;