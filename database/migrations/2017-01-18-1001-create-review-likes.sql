--
--
--

CREATE TABLE customer_review_likes (
    review_id INTEGER REFERENCES customer_reviews (id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    liked BOOLEAN,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(review_id, user_id)
);