--
--
--

CREATE TABLE static_pages (
    id SERIAL PRIMARY KEY,
    sku VARCHAR(50),
    title JSONB,
    html JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);