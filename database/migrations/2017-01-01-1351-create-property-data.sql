--
--
--

CREATE TABLE property_address (
    id SERIAL PRIMARY KEY,
    location geography(PointZ,4326),
    area_name JSONB,
    district_name JSONB,
    province_name JSONB,
    province_id INTEGER,
    district_id INTEGER,
    area_id INTEGER,
    street_number VARCHAR(20),
    street_name JSONB,
    postal_code VARCHAR(20),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE property_belongs_to_building (
    building_id INTEGER REFERENCES properties (id),
    property_id INTEGER REFERENCES properties (id),
    UNIQUE(building_id, property_id)
);

--
--
--

CREATE TABLE property_extended_data (
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    custom_address JSONB,
    starting_sales_price INTEGER,
    office_hours VARCHAR(255),
    year_built VARCHAR(5),    
    total_floors INTEGER,    
    total_size INTEGER,
    land_size INTEGER,
    floor_num VARCHAR(20),
    bedroom_num INTEGER,
    bathroom_num INTEGER,
    website VARCHAR(255),
    psf INTEGER,
    details JSONB,
    contact_person VARCHAR(255),
    UNIQUE(property_id)
);

--
--
--

CREATE TABLE building_data (
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    number_of_tower INTEGER,
    total_units INTEGER,
    developer_id INTEGER,
    UNIQUE(property_id)
);

--
--
--

CREATE TABLE property_listing_data (    
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    ltype VARCHAR(10), -- rent, sale
    title JSONB,    
    deposite NUMERIC(14,2),
    listing_price NUMERIC(14,2),    
    minimal_rental_period VARCHAR(255),
    listing_class VARCHAR(20),
    movein_date DATE,
    ylink1 VARCHAR(500),
    ylink2 VARCHAR(500),
    UNIQUE(property_id)
);

--
-- Replace old /condo_have_media/
--

CREATE TABLE property_have_images (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    filepath VARCHAR(255),
    listing_rank INTEGER
);

--
-- Replace old condo_have_nearby_places
--

CREATE TABLE address_have_nearby_places (
    id SERIAL PRIMARY KEY,
    address_id INTEGER REFERENCES property_address (id) ON DELETE CASCADE,
    place_id INTEGER REFERENCES places (id) ON DELETE CASCADE,
    distance INTEGER,
    UNIQUE(place_id, address_id)
);

--
-- Replace old /condo_facilities/
--

CREATE TABLE property_facilities (
    id SERIAL PRIMARY KEY,
    name JSONB,
    icon_path VARCHAR(255),
    listing_rank INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- For property_features AND replace old /optional_features/
--

CREATE TABLE property_features (
    id SERIAL PRIMARY KEY,
    name JSONB,
    icon_path VARCHAR(255),
    listing_rank INTEGER,
    optional BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- Replace old /condo_have_facilities/
--

CREATE TABLE property_have_facilities (
    facility_id INTEGER REFERENCES property_facilities (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    UNIQUE(facility_id, property_id)
);

--
-- Replace old /properties_have_features/ and /properties_have_optional_features/
--

CREATE TABLE property_have_features (
    feature_id INTEGER REFERENCES property_features (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    UNIQUE(feature_id, property_id)
);
    