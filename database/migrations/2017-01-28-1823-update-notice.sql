--
--
--

ALTER TABLE notifications ADD COLUMN sent BOOLEAN DEFAULT FALSE;
ALTER TABLE notifications ADD COLUMN updated_at TIMESTAMP;