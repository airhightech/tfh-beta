--
--
--

CREATE TABLE building_have_room_types (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES properties (id),
    name VARCHAR(255),
    size INTEGER
);