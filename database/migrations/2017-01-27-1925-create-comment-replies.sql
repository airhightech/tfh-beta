--
--
--


CREATE TABLE customer_review_replies (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES customer_reviews (id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES users (id),
    message TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);