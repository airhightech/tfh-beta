--
--
--

ALTER TABLE newsletter_subscriptions ADD COLUMN prefered_lang VARCHAR(20);

--
--
--

CREATE TABLE newsletters (
    id SERIAL PRIMARY KEY,
    subject JSONB,
    mail_body JSONB,
    status VARCHAR(20),
    send_time TIMESTAMP,    
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

