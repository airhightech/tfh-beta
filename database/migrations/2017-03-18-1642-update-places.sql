--
--
--

ALTER TABLE geo_provinces ADD COLUMN map_center geography(PointZ,4326);
ALTER TABLE geo_provinces ADD COLUMN zoom_level INTEGER;

ALTER TABLE geo_districts ADD COLUMN map_center geography(PointZ,4326);
ALTER TABLE geo_districts ADD COLUMN zoom_level INTEGER;

ALTER TABLE geo_area ADD COLUMN map_center geography(PointZ,4326);
ALTER TABLE geo_area ADD COLUMN zoom_level INTEGER;
