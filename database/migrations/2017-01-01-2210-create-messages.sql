--
--
--

CREATE TABLE messages (
    id SERIAL PRIMARY KEY,
    agent_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    sender_name VARCHAR(255),
    sender_phone VARCHAR(255),
    sender_email VARCHAR(255),
    message TEXT,
    property_id INTEGER,
    sender_id INTEGER,
    read BOOLEAN,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);


--
--
--

CREATE TABLE notifications (
    id SERIAL PRIMARY KEY,
    type VARCHAR(50),   
    message TEXT,
    created_at TIMESTAMP
);

--
--
--

CREATE TABLE notification_receivers (
    message_id INTEGER REFERENCES notifications (id) ON DELETE CASCADE,
    agent_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    read BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(message_id, agent_id)
);

--
--
--

CREATE TABLE newsletter_subscriptions (
    id INTEGER,
    email VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(email)
);