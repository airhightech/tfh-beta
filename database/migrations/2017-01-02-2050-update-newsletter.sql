--
--
--

ALTER TABLE newsletter_subscriptions DROP COLUMN id;
ALTER TABLE newsletter_subscriptions ADD COLUMN id SERIAL PRIMARY KEY;