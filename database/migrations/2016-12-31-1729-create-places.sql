--
--
--

CREATE EXTENSION postgis;

--
--
--

CREATE TABLE places (
    id SERIAL PRIMARY KEY,
    name JSONB,
    external_source VARCHAR(100),
    external_id INTEGER,
    location geography(PointZ,4326),
    zoom_level INTEGER,
    type VARCHAR(100),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
); 