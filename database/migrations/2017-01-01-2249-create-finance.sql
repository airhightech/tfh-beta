--
--
--

CREATE TABLE banners (
    id SERIAL PRIMARY KEY,
    customer VARCHAR(255),
    name VARCHAR(255),
    related_link VARCHAR(500),
    location VARCHAR(100),
    date_start TIMESTAMP,
    date_end TIMESTAMP,
    payment_status VARCHAR(10),
    print INTEGER, 
    images JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE listing_packages (
    id SERIAL PRIMARY KEY,
    name JSONB,
    type VARCHAR(20),
    normal_price NUMERIC(14,2),
    discounted_price NUMERIC(14,2),
    listing_count INTEGER,
    free_listing_count INTEGER,
    duration INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE customer_accounts (
    id SERIAL PRIMARY KEY,
    creator_id INTEGER REFERENCES users (id),
    name VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE customer_accounts_managers (
    account_id INTEGER REFERENCES customer_accounts (id),
    user_id INTEGER REFERENCES users (id),
    created_at TIMESTAMP
);

--
--
--

CREATE TABLE customer_purchases (
    id SERIAL PRIMARY KEY,
    account_id INTEGER REFERENCES customer_accounts (id) ON DELETE CASCADE,    
    invoice_id VARCHAR(255),    
    bank_id INTEGER,    
    used_listing INTEGER,
    used_free_listing INTEGER,     
    total_amount NUMERIC(14,2), -- price + vat
    updated_amount NUMERIC(14,2),
    payment_status VARCHAR(255),
    payment_method VARCHAR(255),
    payment_date TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE customer_purchase_data (
    purchase_id INTEGER REFERENCES customer_purchases (id),
    package_id INTEGER REFERENCES listing_packages (id) ON DELETE CASCADE,
    listing_count INTEGER,
    free_listing_count INTEGER,
    price NUMERIC(14,2),   
    duration INTEGER,
    UNIQUE(purchase_id) 
);

--
--
--

CREATE TABLE paysbuy_payments (
    id SERIAL PRIMARY KEY,
    purchase_id INTEGER REFERENCES customer_purchases (id) ON DELETE CASCADE,
    result VARCHAR(100),
    apcode VARCHAR(100),
    amount NUMERIC(14,2),
    fee NUMERIC(14,2),
    method VARCHAR(4),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);
  

  