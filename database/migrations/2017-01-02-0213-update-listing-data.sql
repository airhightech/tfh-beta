--
--
--

ALTER TABLE property_listing_data ALTER COLUMN minimal_rental_period TYPE INTEGER USING minimal_rental_period::integer;