--
--
--

CREATE TABLE homepage_popups (
    id SERIAL PRIMARY KEY,
    start_time TIMESTAMP,   
    end_time TIMESTAMP,
    related_link VARCHAR(500),
    content JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);