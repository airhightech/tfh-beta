--
--
--

ALTER TABLE publisher_company_profiles ADD COLUMN pinperty_link VARCHAR(255);
ALTER TABLE publisher_company_profiles ADD COLUMN twitter_link VARCHAR(255);
ALTER TABLE publisher_company_profiles ADD COLUMN facebook_link VARCHAR(255);