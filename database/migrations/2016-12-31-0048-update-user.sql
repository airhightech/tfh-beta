--
--
--

ALTER TABLE users ADD COLUMN active BOOLEAN DEFAULT FALSE;
ALTER TABLE users ADD COLUMN lang VARCHAR(2);
ALTER TABLE users ADD COLUMN email_validated BOOLEAN DEFAULT FALSE;
ALTER TABLE users ADD COLUMN email_validation_token VARCHAR(20);
ALTER TABLE users ADD COLUMN password_reset_token VARCHAR(20);
ALTER TABLE users ADD COLUMN ctype VARCHAR(20);
ALTER TABLE users ADD COLUMN last_login TIMESTAMP;


--
--
--

CREATE TABLE login_history (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    source VARCHAR(20),
    created_at TIMESTAMP
);