--
--
--

ALTER TABLE property_listing_data ADD COLUMN upgrade_start TIMESTAMP;
ALTER TABLE property_listing_data ADD COLUMN upgrade_end TIMESTAMP;