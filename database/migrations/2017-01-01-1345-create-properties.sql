--
--
--

CREATE TABLE properties (
    id SERIAL PRIMARY KEY,
    publisher_id INTEGER REFERENCES users (id),
    name JSONB,   
    address_id INTEGER,
    ptype VARCHAR(5), 
    new_project BOOLEAN,
    user_submited BOOLEAN,
    published BOOLEAN,
    publication_date DATE,    
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);