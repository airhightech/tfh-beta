--
--
--

CREATE TABLE bank_accounts (
    id SERIAL PRIMARY KEY,
    code VARCHAR(10),
    name VARCHAR(255),
    account_no VARCHAR(255),
    branch VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(account_no)
);

--
--
--

CREATE TABLE settings (
    id SERIAL PRIMARY KEY,
    token VARCHAR(255),
    value TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(token)
);

--
--
--

CREATE TABLE sessions
(
    id VARCHAR(255),
    user_id INTEGER,
    ip_address VARCHAR(45),
    user_agent TEXT,
    payload TEXT NOT NULL,
    last_activity INTEGER NOT NULL,
    UNIQUE(id)
);


CREATE TABLE translations (
    id SERIAL PRIMARY KEY,
    section VARCHAR(255),
    label VARCHAR(255),
    translation JSONB,
    description VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(section, label)
);