--
--
--

CREATE TABLE geo_provinces (
    id SERIAL PRIMARY KEY,
    name JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE geo_districts (
    id SERIAL PRIMARY KEY,
    province_id INTEGER REFERENCES geo_provinces (id) ON DELETE CASCADE, 
    name JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE geo_area (
    id SERIAL PRIMARY KEY,
    district_id INTEGER REFERENCES geo_districts (id) ON DELETE CASCADE, 
    name JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP

);