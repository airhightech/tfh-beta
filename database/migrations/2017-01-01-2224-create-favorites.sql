--
--
--

CREATE TABLE favorite_agents
(
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    agent_id  INTEGER REFERENCES users (id) ON DELETE CASCADE,
    created_at TIMESTAMP,
    UNIQUE(user_id, agent_id)
);

--
--
--

CREATE TABLE favorite_properties
(
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    created_at TIMESTAMP,
    UNIQUE(user_id, property_id)
);

--
--
--

CREATE TABLE saved_searches (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    search_query TEXT,
    created_at TIMESTAMP
);