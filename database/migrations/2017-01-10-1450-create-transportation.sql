--
--
--

CREATE TABLE property_nearby_transportations (
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    trans_id INTEGER REFERENCES places (id) ON DELETE CASCADE,
    type VARCHAR(20)
);