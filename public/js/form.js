var userIsOnline = userIsOnline || false;
var responseData = responseData || {};

$(function () {
    $('.submit-ajax-form').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        var target = $(this).data('target');
        submitGenericForm.call(this, target, $(this).data('callback'));
    });
});

var submitGenericForm = function (formId, successHandler, errorHandler) {

    var form = document.getElementById(formId);

    var requiredElements = $(form).find('[required]'); //document.querySelectorAll('[required]');

    var formError = $('.form-error');

    if ($(form).hasClass('auth-required') && userIsOnline === false) {
        modal('please-login');
        return;
    }

    if (formError.length) {
        $(formError).html('').hide();
    }

    var data = new FormData(form);

    var progress = form.dataset.progress;
    var method = form.method ? form.method : 'post';
    
    console.log('successHandler', successHandler);

    if (!successHandler) {
        successHandler = function (data, status, xhr) {

            if (data.status === 'OK') {


                if (currentModal) {

                    $(currentModal).on('hidden.bs.modal', function (e) {
                        currentModal = null;

                        if (data.modal) {
                            responseData = data.data;
                            $(data.modal).find('.modal-body').html(data.content);
                            $(data.modal).modal('show');
                        }
                    });

                    $(currentModal).modal('hide');
                }

                if (!data.modal) {
                    if (data.next && data.next !== false) {
                        window.location = data.next;
                    } else if (data.reload === true) {
                        popupSuccessOperation();
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);
                    } else if (data.message) {
                        modal(data.message);
                    } else if (data.loadajax) {
                        $(data.target).load(data.link);
                    } else {
                        popupSuccessOperation();
                    }
                }


            } else if (data.status === 'ERROR') {
                if (formError.length) {
                    $(formError).html(data.error).show();
                }
            }

            if ($(form).hasClass('clear-form')) {
                $(form).find('.clear').val('');
            }

            if (data.errors) {
                console.log(data.errors);
            }
        };
    } else {
        console.log('successHandler');
    }   

    if (!errorHandler) {
        errorHandler = function (xhr, status, error) {
            $(form).find('.input-error').html('').hide();
            for (var field in xhr.responseJSON) {
                var errors = xhr.responseJSON[field];
                $(form).find('.input-error.' + field).html(errors.join('<br/>')).show();
            }
            popupFailedOperation();
        };
    }

    var options = {
        url: form.action,
        data: data,
        processData: false,
        contentType: false,
        type: method,
        success: successHandler,
        error: errorHandler
    };

    if (progress && $(progress).length > 0) {
        options.xhr = function () {
            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = 100 * (evt.loaded / evt.total);
                    $(progress).width(percentComplete + '%');
                }
            }, false);

            xhr.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = 100 * (evt.loaded / evt.total);
                    $(progress).width(percentComplete + '%');
                }
            }, false);

            return xhr;
        };
    }

    var errors = [];

    if (requiredElements.length > 0) {
        $(form).find('.input-error:not(.keep-message)').html('').hide();
        for (var i = 0; i < requiredElements.length; i++) {
            var input = requiredElements[i];

            if (input.type === 'checkbox' || input.type === 'radio') {
                if ($('input[name="' + input.name + '"]:checked').length === 0) {
                    errors.push(input);
                    showErrorMessage(input.name);
                }
            } else if (input.value.trim() === '') {
                errors.push(input);
                showErrorMessage(input.name);
            }

        }
    }

    if (errors.length === 0) {
        if ($(form).find('.accept_condition').length > 0) {
            if ($(form).find('.accept_condition:checked').length > 0) {
                $.ajax(options);
            } else {
                show('.accept-condition-error');
            }
        } else {
            $.ajax(options);
        }

    } else {
        modal('fill-required');
    }
};

var showErrorMessage = function (name) {

    var mgs_container = $('.input-error.' + name);

    if ($(mgs_container).hasClass('keep-message')) {
        $(mgs_container).show();
    } else {
        $(mgs_container).html(tr('required')).show();
    }
};

var propertyToggleFavorite = function (pid, target) {
    
    if (!target) {
        target = $('.favorite');
    }

    if (userIsOnline) {
        $.get('/rest/user/favorite-property/' + pid, function (data, status, xhr) {
            if (data.status === 'OK') {
                if (data.result === 'ADDED') {
                    $(target).find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
                } else {
                    $(target).find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
                }
            } else {

            }
        });
    } else {
        modal('please-login');
    }

};