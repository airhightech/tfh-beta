var userIsOnline = userIsOnline || false;

$(function () {

    $('#locationSlider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 9
    });

    $("#falicitySlider").lightSlider();

    $("#floorplanSlider").lightSlider();

    $('.user-like').on('click', function () {
        var review = $(this).data('review');
        var liked = $(this).data('liked');

        if (userIsOnline) {
            $.post('/rest/review/like', {review: review, liked: liked}, function (data, status, xhr) {
                if (data.status === 'OK') {
                    $('.likes-' + review).html(data.likes);
                    $('.dislikes-' + review).html(data.dislikes);
                }
            });
        } else {
            modal('please-login');
        }

    });

});

var postUserReview = function () {
    if (userIsOnline) {
        if ($('#accept-condition').prop('checked')) {
            submitGenericForm('property-review-form');
        } else {
            $('.accept-condition-error').show();
        }
    } else {
        modal('please-login');
    }
};

var toggleReviewReply = function(rid) {
    $('#reply_review_form_'+ rid).toggle();
};