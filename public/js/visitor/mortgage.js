var CanvasJS = CanvasJS || {};
var mainPrice = mainPrice || 0;
var loan = mainPrice * 0.1;
var initLoan = 89;
var initRate = 6;
var initTerm = 30;
var chart;

var updateLoan = true;
var changeTimeout;

$(function () {
    if ($("#rate-slider").length) {
        initMortgage();
    }
});

var initMortgage = function () {
    
    var ctx = document.getElementById("mortgage-chart");

    $("#rate-slider").slider({
        min: 1,
        max: 10,
        value: initRate,
        change: updateChart
    });
    $("#term-slider").slider({
        min: 1,
        max: 30,
        value: initTerm,
        change: updateChart
    });

    var data = {
        labels: [
            "Total amount",
            "Loan amount"
        ],
        datasets: [
            {
                data: [mainPrice, loan],
                backgroundColor: [
                    "#36bedc",
                    "#343f41"
                ],
                hoverBackgroundColor: [
                    "#36bedc",
                    "#343f41"
                ]
            }]
    };

    chart = new Chart(ctx, {
        type: 'doughnut',
        data: data,
        options: {
            elements: {
                points: {
                    borderWidth: 1,
                    borderColor: 'rgb(0, 0, 0)'
                }
            },
            legend: {
                position: 'bottom'
            }

        }
    });

    updateChart();

    $('#loan-value').on('keyup', function () {
        var loan = $(this).val();

        if (loan === '') {
            loan = 0;
        }

        var that = this;

        if (changeTimeout) {
            clearTimeout(changeTimeout);
        }

        changeTimeout = setTimeout(function () {
            if (parseInt(loan) > mainPrice) {
                loan = mainPrice;
                $(that).val(mainPrice);
            }

            var loan_slider = 100 * parseInt(loan) / mainPrice;
            updateChart();

        }, 1000);
    });
};

var updateChart = function () {
    var annual_rate = parseInt($('#rate-slider').slider("value"));
    var loan_term_payment = parseInt($('#term-slider').slider("value"));

    var principal = $('#loan-value').val();

    var loan_term = loan_term_payment * 12;
    var m_rate = annual_rate / (12 * 100);
    var q = 1 + m_rate;
    var factor = Math.pow(q, loan_term);
    var Pm = m_rate * factor * principal;
    var Qm = factor - 1;
    var monthly_payment = Math.round(Pm / Qm);

    var totalAmount = monthly_payment * loan_term;
    chart.data.datasets[0].data = [totalAmount, principal];
    chart.update();

    $('#rate-value').html(annual_rate + '%');
    $('#term-value').html(loan_term_payment);

    $('#monthly').html('฿ ' + numberWithCommas(monthly_payment));


    updateLoan = true;
};