var currentHL = currentHL || 'th';
var propertyId = propertyId || null;
var searchOptions = {lt: '', np: '', pt: 'cdb', scope: 'cdb'};
var lnglat = lnglat || {};
var suggestXhr;

$(function () {

    var slideOptions = {
        item: 1,
        auto: true,
        loop: true,
        pause: 4000,
        onAfterSlide: function (elt) {
            highlightThumb(elt);
        }
    };

    $("#sale-slider").lightSlider(slideOptions);

    $("#rent-slider").lightSlider(slideOptions);

    $('.premium-item').on('click', function () {
        // open preview
        propertyId = $(this).data('id');

        lnglat.lng = $(this).data('lng');
        lnglat.lat = $(this).data('lat');

        openModalPreview();
    });

    $('.exclusive-listings .pending-listing:first-child').addClass('current');

    $('#subscribe-me').on('click', function () {

        var form = document.getElementById('subscription-form');
        var data = new FormData(form);

        $.ajax({
            url: '/rest/subscribe',
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data, status, xhr) {
                $('.default-modal-body').html(data.message);
                $('#default-modal').modal('show');
            },
            error: function (xhr, status, error) {
                if (xhr.responseJSON.email) {
                    var errors = xhr.responseJSON.email;

                    var html = '<ul class="list-unstyled">';

                    for (var i = 0; i < errors.length; i++) {
                        html = html + '<li>' + errors[i] + '</li>';
                    }

                    html = html + '</ul>';

                    $('.default-modal-body').html(html);
                    $('#default-modal').modal('show');
                }
            }
        });

    });

    $('#q').autoComplete({
        minChars: 2,
        source: function (term, response) {
            try {
                suggestXhr.abort();
            } catch (e) {
            }

            searchOptions['term'] = term;

            suggestXhr = $.getJSON('/rest/suggest', searchOptions, function (data) {
                if (data.length === 0) {
                    $('#property_id').val('');
                    //console.log('item not found');
                }
                response(data);
            });
        },
        renderItem: function (item, search) {
            var current = JSON.parse(item.name);
            var name = current[currentHL];
            if (name.length === 0) {
                name = current['en'];
            }
            if (name.length === 0) {
                name = current['th'];
            }
            var html = '<div class="autocomplete-suggestion" data-type="' + item.type
                    + '" data-id="' + item.id
                    + '" data-lng="' + item.lng
                    + '" data-lat="' + item.lat
                    + '" data-name="' + name + '">'
                    + '<span class="icon fa ' + item.type + '"></span>'
                    + '<span class="name">' + name + '</span>';
                    
            if (item.type === 'cdb' || item.type === 'apb') {
                html = html + '<span class="type">' + item.type + '</span>';
            } else if (item.type === 'cd' || item.type === 'ap' || item.type === 'th' || item.type === 'sh') {
                if (item.ltype === 'rent') {
                    html = html + '<span class="type">' + item.ltype + '</span>';
                } else if (item.ltype === 'sale') {
                    html = html + '<span class="type">' + item.ltype + '</span>';
                }
            } else {
                html = html + '<span class="type">' + item.type + '</span>';
            }
            
            html = html + '</div>';
            
            return html;
        },
        onSelect: function (e, term, item) {
            //console.log(e);
            //console.log(this);
            $('#q').val(item[0].dataset.name);
            currentId = item[0].dataset.id;

            var url = '/search?q=' + item[0].dataset.name
                    + '&rel=' + item[0].dataset.type
                    + '&lat=' + item[0].dataset.lat
                    + '&lng=' + item[0].dataset.lng
                    + '&id=' + item[0].dataset.id                    
                    + '&np=' + searchOptions.np
                    + '&pt=' + searchOptions.pt;
            
            var version = detectIE();

            if (version === false) {
                url = url + '&lt=' + searchOptions.lt;
            } else {
                url = url + '&amp;lt=' + searchOptions.lt;
            }

            //console.log(url);

            window.location = url;

        }
    });

    $('span.option').on('click', function (event) {
        event.preventDefault();
        $('span.option').removeClass('active');
        $(this).addClass('active');

        //searchOptions[$(this).data('name')] = $(this).data('value');

        console.log($(this).data('name'));

        data = {};
        var name = $(this).data('name');
        var value = $(this).data('value');

        data[name] = value;

        if (name === 'pt') {
            data['np'] = '';
            data['lt'] = '';
            data['scope'] = value;
            $('#np').val('');
            $('#lt').val('');
            $('#pt').val(value);
            $('#scope').val(value);
        } else if (name === 'np') {
            data['pt'] = '';
            data['lt'] = '';
            data['scope'] = 'project';
            $('#np').val('yes');
            $('#lt').val('');
            $('#pt').val('');
            $('#scope').val('project');
        } else {
            data['pt'] = '';
            data['np'] = '';
            data['scope'] = 'listing';
            $('#np').val('');
            $('#lt').val(value);
            $('#pt').val('');
            $('#scope').val('listing');
        }

        searchOptions = data;

        $('.main-bg').hide();
        $('.main-bg.bg-' + $(this).data('bg')).show();

        //console.log(searchOptions);
    });

    /*if ($('#promo-popup').length) {
     $('#promo-popup').modal('show');
     }*/

    $('.promo-popup-content').on('click', function () {
        var link = $(this).data('link');
        if (link) {
            window.location = link;
        }
    });

    $('#hide_popup').on('click', function (event) {
        event.stopPropagation();
        if ($(this).prop('checked') && localStorage) {
            var d = new Date();
            var n = d.getTime();
            n = n + (24 * 3600 * 1000);
            localStorage.setItem('showPopupTime', n);
        } else {
            localStorage.removeItem('showPopupTime');
        }

        /*setTimeout(function () {
            var link = $('.promo-popup-content').data('link');
            if (link) {
                window.location = link;
            }
        }, 1500);*/
    });

    var showPopup = false;

    if (localStorage) {
        if (localStorage.getItem('showPopupTime') === null) {
            showPopup = true;
        } else {
            var show_popup_time = parseInt(localStorage.getItem('showPopupTime'));

            var d = new Date();
            var n = d.getTime();

            if (show_popup_time <= n) {
                showPopup = true;
            }
        }

    }

    if (showPopup && $('#promo-popup').length) {
        $('#promo-popup').modal('show');
    }

    /*if (sessionStorage && !sessionStorage.getItem('isshow')) {
     if ($('#promo-popup').length) {
     $('#promo-popup').modal('show');
     }
     sessionStorage.setItem('isshow', true);
     }*/

});

var highlightThumb = function (elt) {
    var target0 = $(elt).data('target');
    var target1 = $(elt).children('.lslide.active').data('target');
    $(target0).removeClass('current');
    $(target1).addClass('current');
};

var showFullProperty = function () {
    window.location = '/property/' + propertyId;
};