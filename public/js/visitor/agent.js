var labels = labels || [];
var propertyData = propertyData || [];

var lfilter = 'all';
var pfilter = 'all';
var sort = 'none';

$(function () {

    $('.agent-types label').on('click', function () {
        $('.agent-types label').removeClass('active');
        $(this).addClass('active');
        $('#typ').val($(this).data('type'));
        
        $('#search_agent').submit();
    });

    var ctx = document.getElementById("property-canvas");

    var data = {
        labels: labels,
        datasets: [
            {
                data: propertyData,
                strokeWidth: 25,
                backgroundColor: [
                    "#cbff7a",
                    "#ff5d83",
                    "#179aed",
                    "#93dcb6"
                ],
                hoverBackgroundColor: [
                    "#cbff7a",
                    "#ff5d83",
                    "#179aed",
                    "#93dcb6"
                ]
            }]
    };

    if (ctx) {
        chart = new Chart(ctx, {
            type: 'doughnut',
            data: data,
            options: {
                legend: {
                    display: false
                },
                cutoutPercentage: 60
            }
        });
    }



    $('.ltype').on('change', function () {
        lfilter = $(this).val();
        applyFitler();
    });

    $('.ptype').on('change', function () {
        pfilter = $(this).val();
        applyFitler();
    });

    $('.order').on('change', function () {
        sort = $(this).val();
        if (sort !== 'none') {
            applySort();
        }
    });

    $('.rating-container').rating();

});

var applyFitler = function () {

    $('.property-list .property-item').hide();

    var lclass = '';
    var pclass = '';

    if (lfilter !== 'all') {
        lclass = '.' + lfilter;
    }

    if (pfilter !== 'all') {
        pclass = '.' + pfilter;
    }

    console.log('.property-item' + lclass + pclass);

    $('.property-item' + lclass + pclass).show();
};

var applySort = function () {
    var $wrapper = $('.property-list');

    $wrapper.find('.property-item').sort(function (a, b) {
        return parseInt($(a).data(sort)) - parseInt($(b).data(sort));
    }).appendTo($wrapper);
};

var pagerNext = function(pager){
    var current = $(pager).find('.current');
    var count = parseInt($(pager).data('count'));
    var rank = parseInt($(current).data('rank'));
    
    var next = rank + 1;
    
    if (next > count) {
        next = 1;
    }
    
    $(pager).find('.current-rank').html(next);    
    
    $(current).removeClass('current').hide();
    $(pager).find('.rank-' + next).addClass('current').show();
};

var pagerPrevious = function(pager){
    var current = $(pager).find('.current');
    var count = parseInt($(pager).data('count'));
    var rank = parseInt($(current).data('rank'));
    
    var next = rank - 1;    
    
    if (next === 0) {
        next = count;
    }  
    
    $(pager).find('.current-rank').html(next);
    
    $(current).removeClass('current').hide();
    $(pager).find('.rank-' + next).addClass('current').show();
};
