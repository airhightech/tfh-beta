var panorama;
var latlng = latlng || {};
var google = google || {};

var previewMap;
var previewMarker;
var previewPoiMarker;

var bigMap = null;

$(document).ready(function () {

    $('.social-share').socialShare({
        social: 'facebook,google,twitter'
    });

    showPropertyMap(parseFloat(latlng.lat), parseFloat(latlng.lng));

    var width = 0;

    $('.media-slider .media-group').each(function (index, value) {
        width = width + $(value).width() + 2.0;
    });
    $('.media-slider').width(width);

    console.log('width', width);

    /*
     $('.contact-box').affix({
     offset: 15
     });
     */

    $('.map-preview').on('click', function () {
        console.log('map-preview');

        $('#big-map').on('shown.bs.modal', function (e) {
            if (bigMap === null) {
                var location = {lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng)};
                bigMap = new google.maps.Map(
                        document.getElementById('big-map-container'),
                        {
                            center: location,
                            zoom: 15
                        });

                var markerIcon = '/img/pin-blue.png';

                var marker = new google.maps.Marker({
                    position: location,
                    map: bigMap,
                    draggable: false,
                    icon: {
                        url: markerIcon,
                        size: new google.maps.Size(40, 40)
                    }
                });
            } else {

            }
        });

        $('#big-map').modal('show');
    });

    $('.poi-list').on('change', function () {

        var optionSelected = $("option:selected", this);
        $('.poi-list').val(0);
        $(optionSelected).prop('selected', true);

        var options = $(optionSelected[0]).data();

        if (options.lat) {
            var poimg = getImageUrl(options.type);
            var location = {lat: parseFloat(options.lat), lng: parseFloat(options.lng)};

            var image = {
                url: poimg,
                size: new google.maps.Size(32, 37),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 37)
            };

            if (previewPoiMarker) {
                previewPoiMarker.setMap(null);
                previewPoiMarker = null;
            }

            previewPoiMarker = new google.maps.Marker({
                position: location,
                draggable: false,
                icon: image,
                map: previewMap
            });

            var bounds = new google.maps.LatLngBounds();
            bounds.extend({lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng)});
            bounds.extend(location);

            previewMap.fitBounds(bounds);
        } else {
            previewMap.panTo({lat: latlng.lat, lng: latlng.lng});
        }


    });

    $('#similar-slider').lightSlider({
        keyPress: false,
        item: 4,
        loop: true
    });



    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        console.log(e.target.dataset);

        if (e.target.dataset && e.target.dataset.ref === 'property-review') {
            initReviewSliders();
        }
    });
});


var reviewSliderLoaded = false;

var initReviewSliders = function () {

    if (!reviewSliderLoaded) {
        reviewSliderLoaded = true;
        $('#location-slider').lightSlider({
            gallery: true,
            keyPress: false,
            item: 1,
            auto: true,
            thumbItem: 8,
            loop: true
        });
        $('#floorplan-slider').lightSlider({
            gallery: true,
            keyPress: false,
            item: 1,
            auto: true,
            thumbItem: 8,
            loop: true
        });
        $('#facility-slider').lightSlider({
            gallery: true,
            keyPress: false,
            item: 1,
            auto: true,
            thumbItem: 8,
            loop: true
        });
    }

};

