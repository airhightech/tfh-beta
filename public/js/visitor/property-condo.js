$(function(){
    $('.listing-sale').hide();
    
    $('.property-listing .filters button').on('click', function(){
        var target = $(this).data('target');
        $('.listing-room').hide();
        $(target).show();
        $('.property-listing .filters button').removeClass('current');
        $(this).addClass('current');
    });
});