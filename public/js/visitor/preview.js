var propertyId = propertyId || null;
var lnglat = lnglat || {};

$(function(){
    
    $('#property-preview').on('show.bs.modal', function (e) {
        
        console.log('show preview', propertyId);
        
        $('#property-preview .modal-body').load('/preview/' + propertyId, function () {
            var width = 0;
            $('.media-slider .media-group').each(function (index, value) {
                width = width + $(value).width() + 2.0;
            });
            $('.media-slider').width(width);
            
            console.log(lnglat);
            
            showPropertyMap(parseFloat(lnglat.lat), parseFloat(lnglat.lng));
            
            initMortgage();
            
            //$('.fancybox').fancybox();
        });
    });
    
});

var openModalPreview = function(){
    $('#property-preview .modal-body').html('');
    $('#property-preview').modal('show');
};