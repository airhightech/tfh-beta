var f_lt = '';
var f_bed = '';
var f_np = '';
var f_bat = '';
var f_scope = f_scope || '';
var sortAttr = '';
var sortAsc = true;

var price_min = 0;
var price_max = 0;

var min_price_label = null;
var max_price_label = null;

var suggestXhr;

var initAutoComplete = function () {
    var currentId = currentId || 0;
    $('#q').autoComplete({
        minChars: 2,
        cache: false,
        source: function (term, response) {
            try {
                suggestXhr.abort();
            } catch (e) {
            }
            
            var dta = generateSuggestionData();
            dta['term'] = term;
            
            suggestXhr = $.getJSON('/rest/suggest', dta, function (data) {
                if (data.length === 0) {
                    $('#property_id').val('');
                    //console.log('item not found');
                }
                response(data);
            });
        },
        renderItem: function (item, search) {
            var current = JSON.parse(item.name);
            var name = current[currentHL];
            if (name.length === 0) {
                name = current['en'];
            }
            if (name.length === 0) {
                name = current['th'];
            }
            var html = '<div class="autocomplete-suggestion" data-type="' + item.type
                    + '" data-id="' + item.id
                    + '" data-lng="' + item.lng
                    + '" data-lat="' + item.lat
                    + '" data-name="' + name + '">'
                    + '<span class="icon fa ' + item.type + '"></span>'
                    + '<span class="name short">' + name + '</span>';
                    
            if (item.type === 'cdb' || item.type === 'apb') {
                html = html + '<span class="type">' + tr('condo_com') + '</span>';
            } else if (item.type === 'cd' || item.type === 'ap' || item.type === 'th' || item.type === 'sh') {
                if (item.ltype === 'rent') {
                    html = html + '<span class="type">' + tr('listing_rent') + '</span>';
                } else if (item.ltype === 'sale') {
                    html = html + '<span class="type">' + tr('listing_sale') + '</span>';
                }
            } else {
                html = html + '<span class="type">' + tr('places_' + item.type) + '</span>';
            }
            
            html = html + '</div>';
            
            return html;
        },
        onSelect: function (e, term, item) {
            $('#q').val(item[0].dataset.name);
            currentId = item[0].dataset.id;
            showUserSuggestion(item[0].dataset);
        }
    });
};

var showUserSuggestion = function (data) {

    currentId = data.id;
};

$(function () {

    updatePriceRangeVisibility();

    initAutoComplete();

    $('.btn-map-search').on('click', function () {
        currentUrl = generateSearchUrl();        
        window.location = currentUrl;
    });

    $('.max-prices').hide();

    $('.lt').on('click', function () {
        // Update price range
        updatePriceRangeVisibility();

        // disable project type
        $('.pr').prop('checked', false);
    });

    $('.pt.co').on('click', function () {

        //f_np = $(this).val();
        f_lt = '';

        $('.lt').prop('checked', false);
        $('#np').prop('checked', false);

        if ($(this).prop('checked')) {
            $('.pt.nm').prop('checked', false); //.prop('disabled', false);
            $('.lt').prop('checked', false);
            f_scope = 'cdb';
        } else {
            $('.pt.nm').prop('checked', true).prop('disabled', false);
            f_scope = 'listing';
        }

    });

    $('#np').on('click', function () {
        $('.pt.co').prop('checked', false);
        if ($(this).prop('checked')) {
            $('.pt.nm').prop('checked', false);
            $('.lt').prop('checked', false);
            f_scope = 'project';
        } else {
            $('.pt.nm').prop('checked', true).prop('disabled', false);
            f_scope = 'listing';
        }
    });

    $('.min-price').on('click', function (event) {
        event.stopPropagation();
        $('.min-prices').hide();
        $('.max-prices').show();

        price_min = parseInt($(this).data('value'));
        var label = $(this).data('label');

        if (price_min > 0) {
            $('#price_min').val(price_min);
            min_price_label = label;
        } else {
            $('#price_min').val($(this).text().trim());
            min_price_label = null;
        }

        updatePriceRangeLabel();

        // check max price value

        if (price_max > 0 && price_max < price_min) {
            // User need to select a new max price
            price_max = 0;
            $('#price_max').val('');
        }

        $('.max-price').removeClass('inactive');

        $('.max-price').each(function (idx, elt) {
            var value = parseInt($(elt).data('value'));

            if (value > 0 && value <= price_min) {
                $(elt).addClass('inactive');
            }
        });
    });

    $('.pt.nm').on('click', function () {
        $('.pt.co').prop('checked', false);
        $('#np').prop('checked', false);
    });

    $('.max-price').on('click', function (event) {

        if ($(this).hasClass('inactive')) {
            event.stopPropagation();
        } else {

            $('.min-prices').show();
            $('.max-prices').hide();

            price_max = parseInt($(this).data('value'));
            var label = $(this).data('label');

            if (price_max > 0) {
                $('#price_max').val(price_max);
                max_price_label = label;
            } else {
                $('#price_max').val($(this).text().trim());
                max_price_label = null;
            }

            updatePriceRangeLabel();
        }
    });

    //

    $('.bed-item').on('click', function () {
        $('.bed-item').removeClass('active');
        f_bed = $(this).data('filter');
        $(this).addClass('active');
        $('.bed-count').html('(' + f_bed + ')');
    });

    $('.bath-item').on('click', function () {
        $('.bath-item').removeClass('active');
        f_bat = $(this).data('filter');
        $(this).addClass('active');
        $('.bath-count').html('(' + f_bat + ')');
    });

    // Save current search

    $('.save').on('click', function () {

        if (!userIsOnline) {
            alert(onOfflineMessage);
        } else {
            var url = generateSearchUrl();

            $.post('/rest/search/save', {
                url: url
            }, function () {
                $('#saved-message').modal('show');
                updateSavedSearch();
            });
        }
    });
    
    $('.sorter').on('change', function(){
        var attr = $(this).val();
        sortby(attr);
        currentUrl = generateSearchUrl();        
        window.location = currentUrl;
    });
});

var updatePriceRangeVisibility = function () {
    $('.sale-prices').hide();
    $('.rent-prices').hide();

    f_np = '';

    if ($('#lt-sale').prop('checked') && $('#lt-rent').prop('checked')) {
        priceClass = '.sale-prices';
        $('.sale-prices').show();
        f_lt = 'rent,sale';

    } else if ($('#lt-sale').prop('checked')) {
        priceClass = '.sale-prices';
        $('.sale-prices').show();
        f_lt = 'sale';
    } else if ($('#lt-rent').prop('checked')) {
        priceClass = '.rent-prices';
        $('.rent-prices').show();
        f_lt = 'rent';
    } else {
        f_lt = '';
        $('.sale-prices').show();
    }
};

var updatePriceRangeLabel = function () {

    if (min_price_label === null && max_price_label === null) {
        $('.price-range-label').html('Any price');
    } else if (min_price_label !== null && max_price_label === null) {
        $('.price-range-label').html('Min ' + min_price_label);
    } else if (min_price_label === null && max_price_label !== null) {
        $('.price-range-label').html('Max ' + max_price_label);
    } else {
        $('.price-range-label').html(min_price_label + ' - ' + max_price_label);
    }
};

var generateSuggestionData = function(){
    
    f_np = $('#np').prop('checked') ? 'yes' : '';
    
    var tmp = [];

    $('.pt:checked').each(function (idx, elt) {
        tmp.push($(elt).val());
    });
    
    var data = {
        lt : f_lt,
        np : f_np,
        pt : tmp.join(','),
        scope : f_scope
    };
    return data;
};

var generateSearchUrl = function () {


    url = '/search-list?';

    f_np = $('#np').prop('checked') ? 'yes' : '';
    
    url = url + 'q=' + $('#q').val();

    url = url + '&lt=' + f_lt;
    url = url + '&np=' + f_np;
    url = url + '&pmin=' + price_min + '&pmax=' + price_max;

    var tmp = [];

    $('.pt:checked').each(function (idx, elt) {
        tmp.push($(elt).val());
    });

    url = url + '&pt=' + tmp.join(',');
    url = url + '&bed=' + f_bed;
    url = url + '&bath=' + f_bat;
    url = url + '&smin=' + $('#size_min').val();
    url = url + '&smax=' + $('#size_max').val();
    url = url + '&ymin=' + $('#year_min').val();
    url = url + '&ymax=' + $('#year_max').val();
    url = url + '&sort=' + sortAttr + '&dir=asc';
    url = url + '&match=true';

    /*if (sortAsc) {
        url = url + '&dir=asc';
    } else {
        url = url + '&dir=desc';
    }*/
    return url;

};

var sortby = function(attr) {

  if (sortAttr === attr) {
    sortAsc = !sortAsc;
  } else {
    sortAttr = attr;
  }
};