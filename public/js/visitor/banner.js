$(function () {
    if ($('.banner').length) {
        $.map($('.banner'), function (banner, index) {
            var location = $(banner).data('location');
            loadNextBanner(location);
        });
    } else {
        console.log('no banner');
    }
});

var currentXhr;
var timeoutHandler;

var loadNextBanner = function (location) {
    
    if (currentXhr) {
        currentXhr.abort();
    }
    
    if (timeoutHandler) {
        clearTimeout(timeoutHandler);
    }

    currentXhr = $.get('/rest/banner?location=' + location, function (data) {

        if (data.status === 'OK') {
            var img = document.createElement('img');
            $(img).addClass('img-responsive');
            $(img).prop('src', data.banner);

            var a = document.createElement('a');
            $(a).prop('href', data.link);
            $(a).prop('target', '_blank');

            $(a).append(img);

            $('.banner-' + data.location).html('');
            $('.banner-' + data.location).append(a);

            timeoutHandler = setTimeout(function () {
                loadNextBanner(data.location);
            }, 4000);

        }
    });
};


