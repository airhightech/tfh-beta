var bigMap = null;
var google = google || {};

var initMapPreview = function () {
    
    $('.map-preview').on('click', function () {
        
        var lat = $(this).data('lat');
        var lng = $(this).data('lng');

        $('#big-map').on('shown.bs.modal', function (e) {
            if (bigMap === null) {
                var location = {lat: parseFloat(lat), lng: parseFloat(lng)};
                bigMap = new google.maps.Map(
                        document.getElementById('big-map-container'),
                        {
                            center: location,
                            zoom: 15
                        });

                var markerIcon = '/img/pin-blue.png';

                var marker = new google.maps.Marker({
                    position: location,
                    map: bigMap,
                    icon: {
                        url: markerIcon,
                        size: new google.maps.Size(40, 40)
                    }
                });
            } else {

            }
        });

        $('#big-map').modal('show');
    });
};