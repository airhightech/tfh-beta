var updateMapLocation = updateMapLocation || false;
var map = map ||null;
var marker = marker || null;

var enableAndLoadDistricts = function (provinceId, callback)
{
    $('#district_id').prop('disabled', false);

    if ($('#district_id').chosen) {
        $("#district_id").chosen("destroy");
    }

    var loadingSpinner = $('.district_loading');

    if (loadingSpinner.length) {
        loadingSpinner.show();
    }

    $.getJSON('/rest/location/district/' + provinceId, function (data, status, xhr) {
        if (data.length > 0) {
            $('#district_id').html('<option value="0">Select a district</option>');

            for (var i in data) {
                $('#district_id').append('<option value="' + data[i].id + '" data-zoom="' + data[i].zoom + '" data-lat="' + data[i].lat + '" data-lng="' + data[i].lng + '">' + data[i].name + '</option>');
            }
        } else {
            $('#district_id').html('<option value="0">Sorry, no district available</option>');
        }

        if (loadingSpinner.length) {
            loadingSpinner.hide();
        }

        if (callback) {
            callback.call(this);
        }

        if ($('#district_id').chosen) {
            $('#district_id').chosen();
        }
    });
};

var disableAndClearDistricts = function ()
{
    $('#district_id').prop('disabled', true);
    $('#district_id').html('<option value="0">Please select a province</option>');
    disableAndClearArea();
};

var enableAndLoadArea = function (district_id, callback)
{
    $('#area_id').prop('disabled', false);
    
    if ($('#area_id').chosen) {
        $("#area_id").chosen("destroy");
    }

    var loadingSpinner = $('.area_loading');

    if (loadingSpinner.length) {
        loadingSpinner.show();
    }

    $.getJSON('/rest/location/area/' + district_id, function (data, status, xhr) {
        if (data.length > 0) {
            $('#area_id').html('<option value="0">Select an area</option>');

            for (var i in data) {
                $('#area_id').append('<option value="' + data[i].id + '" data-zoom="' + data[i].zoom + '" data-lat="' + data[i].lat + '" data-lng="' + data[i].lng + '">' + data[i].name + '</option>');
            }
        } else {
            $('#area_id').html('<option value="0">Sorry, no area available</option>');
        }
        if (callback) {
            callback.call(this);
        }
        if (loadingSpinner.length) {
            loadingSpinner.hide();
        }

        if ($('#area_id').chosen) {
            $('#area_id').chosen();
        }
    });
};

var disableAndClearArea = function ()
{
    $('#area_id').prop('disabled', true);
    $('#area_id').html('<option value="0">Please select a district</option>');
};


$(document).ready(function () {

    if ($('#area_id').chosen) {
        $('#area_id').chosen();
    }

    if ($('#district_id').chosen) {
        $('#district_id').chosen();
    }

    if ($('#province_id').chosen) {
        $('#province_id').chosen();
    }
    //-----

    $('.district_loading').hide();
    $('.area_loading').hide();

    $('#province_id').on('change', function () {
        var province_id = $(this).val();

        disableAndClearDistricts();

        if (parseInt(province_id) > 0) {
            $('#province_error').hide();
            enableAndLoadDistricts(province_id);
        } else {
            $('#province_error').show();
        }        
        moveMap('#province_id');        
    });

    // --------

    $('#district_id').on('change', function () {
        var district_id = $(this).val();

        disableAndClearArea();

        if (parseInt(district_id) > 0) {
            enableAndLoadArea(district_id);
            $('#district_error').hide();
        } else {
            $('#district_error').show();
        }
        
        moveMap('#district_id');
    });
    
    $('#area_id').on('change', function () {
        moveMap('#area_id');
    });


});

var moveMap = function(selector) {
    if (updateMapLocation && map) {
        var option = $(selector + ' option:selected');

        var zoom = option.data('zoom');
        var lat = parseFloat(option.data('lat'));
        var lng = parseFloat(option.data('lng'));

        if (lat && lng) {
            var position = {lat : lat, lng: lng};
            map.panTo(position);
            marker.setPosition(position);
            console.log(position);                
        }

        if (zoom) {
            map.setZoom(zoom);
        }
    }
};