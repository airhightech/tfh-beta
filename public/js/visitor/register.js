var nextStep = null;

$(function(){
    
    $('.user-selector').on('click', function(){
        nextStep = $(this).data('next');
    });
    
    $('#submit-user-type').on('click', function(){
        if (nextStep) {
            window.location = nextStep;
        } else {
            modal('select-user-type');
        }
    });
    
});