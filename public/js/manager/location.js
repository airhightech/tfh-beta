var map;
var domain = domain || '';
var id = id || 0;
var lat = lat || 13.756705;
var lng = lng || 100.505188;
var zl = zl || 15;

$(function(){
    
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: zl
    });

    map.addListener('idle', function () {
        var center = map.getCenter().toUrlValue();
        var zoom = map.getZoom();
        
        console.log('center', center);
        console.log('zoom', zoom);
        
        $.get('/rest/location/map/' + id + '?domain=' + domain + '&center=' + center + '&zoom=' + zoom, function(){
            
        });
    });
    
});