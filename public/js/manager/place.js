var placeMarkers = [];
var markerCluster;
var reloadPointsOnIdle = true;
var newMarker, currentMarker;
var google = google || {};
var supportedLangs = supportedLangs || null;
var currentId = 0;
var filters = [];

$(function () {

    map = new google.maps.Map(document.getElementById('place-map'), {
        center: {lat: 13.756705, lng: 100.505188},
        zoom: 15
    });

    map.addListener('bounds_changed', function () {
        reloadPointsOnIdle = true;
    });

    map.addListener('idle', function () {
        if (reloadPointsOnIdle) {
            loadCurrentPlaces();
        }
    });

    google.maps.event.addListener(map, 'click', function (event) {
        createNewPlace(event.latLng);
    });

    $('#place-save').on('click', saveCurrentPlace);

    $('#place-filter-apply').on('click', loadCurrentPlaces);

    $('#place-delete-confirm').on('click', function () {
        $('#place-modal').modal('show');
    });

    $('#place-delete').on('click', deleteCurrentPlace);
    
    

});

var createNewPlace = function (latlng) {
    if (newMarker) {
        newMarker.setMap(null);
    }
    newMarker = new google.maps.Marker({
        position: latlng,
        draggable: true,
        map: map
    });
    showNewPlaceDialog.call(newMarker);
    newMarker.addListener('click', showNewPlaceDialog.bind(newMarker));
    newMarker.addListener('dragend', updatePlaceLocation.bind(newMarker));

    $('#marker-error').hide();
};

var showNewPlaceDialog = function () {
    $('.place-editor').show();
    $('#place-delete').show();

    if (this.data) {
        if (newMarker) {
            newMarker.setMap(null);
            newMarker = null;
        }

        currentMarker = this;
        updateFields(this);
    } else {
        for (var i = 0; i < supportedLangs.length; i++) {
            var lang = supportedLangs[i];
            $('.named-name.' + lang).val('');
        }
        currentId = 0;

        $('#place-delete').hide();
    }

    $('#lng').val(this.position.lng());
    $('#lat').val(this.position.lat());
};

var updatePlaceLocation = function () {

    updateFields(this);

    //$('.place-editor').show();

    $('#lng').val(this.position.lng());
    $('#lat').val(this.position.lat());

    saveCurrentPlace();
};

var updateFields = function (marker) {
    for (var i = 0; i < supportedLangs.length; i++) {
        var lang = supportedLangs[i];
        $('.named-name.' + lang).val(marker.parent.names[lang]);
    }
    $('#place-id').val(marker.data.id);
    $('#type').val(marker.data.type);

    currentId = marker.data.id;
};

var resetFields = function () {
    for (var i = 0; i < supportedLangs.length; i++) {
        var lang = supportedLangs[i];
        $('.named-name.' + lang).val('');
    }
    $('#place-id').val('');
    $('#type').val(0);

    currentId = 0;

    if (currentMarker) {
        currentMarker.setMap(null);
        currentMarker = null;
    }

    $('#place-delete').hide();
};

var saveCurrentPlace = function () {
    var form = document.getElementById('place-editor-form');

    var data = new FormData(form);

    var url = currentId > 0 ? '/manager/place/update' : '/manager/place/create';

    $.ajax({
        url: url,
        data: data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data, status, xhr) {            //window.location.reload();

            loadCurrentPlaces();
            resetFields();
        },
        error: function (xhr, status, error) {

        }
    });
};

var deleteCurrentPlace = function () {
    if (currentId > 0) {
        $.get('/manager/place/delete?id=' + currentId, function () {
            $('#place-modal').modal('hide');


            resetFields();


        });
    }
};

var loadCurrentPlaces = function () {
    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    updateFilterValues();

    url = 'nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6) + '&filter=' + filters.join(',');

    $.get('/manager/place/entries?' + url, function (data, status, xhr) {
        displayPlaces(data.entries);
    });
};

var applyPlaceFilters = function () {

};

var updateFilterValues = function () {
    filters = [];

    $('.place-filter:checked').map(function (idx, filter) {
        filters.push($(filter).val());
    });
};

var displayPlaces = function (entries) {

    if (placeMarkers.length) {
        for (var i = 0; i < placeMarkers.length; i++) {
            var marker = placeMarkers[i];
            marker.setMap(null);
        }
        placeMarkers = [];
        markerInfos = [];
    }

    if (markerCluster) {
        markerCluster.clearMarkers();
    }

    if (newMarker) {
        newMarker.setMap(null);
        newMarker = null;
    }

    if (entries.length > 0) {
        for (var i = 0; i < entries.length; i++) {
            var place = new Place(entries[i]);

            place.marker.addListener('click', showNewPlaceDialog);
            place.marker.addListener('dragend', updatePlaceLocation);

            placeMarkers.push(place.marker);
        }

        if (markerCluster) {
            markerCluster.addMarkers(placeMarkers);
        } else {
            var clusterStyles = [
                {
                    textColor: 'white',
                    url: '/img/map/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/map/m1.  png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/map/m1.png',
                    height: 40,
                    width: 40
                }
            ];

            markerCluster = new MarkerClusterer(map, placeMarkers,
                    {
                        imagePath: '/img/map/m',
                        styles: clusterStyles
                    });
        }
    } else {

    }
};