$(function() {
  var dateFormat = "dd/mm/yy";
  $("#send_time")
    .datepicker({
      changeMonth: true,
      numberOfMonths: 1,
      dateFormat: dateFormat
    })

  $('#mail_body-tab a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
    currentHtml = $(this).data('content');
  });
  $('#mail_body-tab a:first').tab('show');

  currentHtml = $('#mail_body-tab a:first').data('content');

  $('.notes').summernote({
    height: 400,
    callbacks: {
      onChange: function(contents, $editable) {
        console.log('onChange:', contents, $editable);

        $(currentHtml).val(contents);
      }
    }
  });
  $('.notes').summernote('fontName', 'Signika');
});
