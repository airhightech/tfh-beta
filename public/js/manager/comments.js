$(function () {

    $('.edit-comment').on('click', openCommentModal);

});

var lastCommentId = 0;


var openCommentModal = function (event) {

    event.preventDefault();

    lastCommentId = event.currentTarget.dataset.id;
    var type = event.currentTarget.dataset.type;

    $('.modal-body').load('/manager/comment/details?id=' + lastCommentId + '&type=' + type, function (data) {

        $('.comment-details').html(data);
        
        $('#comment-modal').on('shown.bs.modal', function (e) {
            addListeners();
        });

        $('#comment-modal').modal('show');
    });
};

var addListeners = function(){
    
    $('.delete-comment').on('click', function(){
        $('#op').val('delete');
        submitGenericForm('comment-form', onUpdateComplete);
    });
    
    $('.approve-comment').on('click', function(){
        $('#op').val('approve');
        submitGenericForm('comment-form', onUpdateComplete);
    });
    
    $('.update-comment').on('click', function(){
        $('#op').val('update');
        submitGenericForm('comment-form', onUpdateComplete);
    });
};

var onUpdateComplete = function(data, status, xhr){
    window.location.reload();
};