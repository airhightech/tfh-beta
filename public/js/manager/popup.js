var currentHtml;
var currentEditor;

$(function () {

    $('#html-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        currentHtml = $(this).data('content');
        currentEditor = $(this).data('editor');
        $('.progress-bar').width(0);
    });
    $('#html-tab a:first').tab('show');

    currentHtml = $('#html-tab a:first').data('content');
    currentEditor = $('#html-tab a:first').data('editor');
    //$('.submit-ajax-form').hide();

    $('.notes').summernote({
        height: 400,
        callbacks: {
            onChange: function (contents, $editable) {
                console.log('onChange:', contents);
                $(currentHtml).val(contents);
                $('.submit-ajax-form').show();
            },
            onImageUpload: function (files) {
                sendFile(files[0]);
            }
        }

    });
    $('.notes').summernote('fontName', 'Signika');
    
    var dateFormat = "dd/mm/yy";
    var from = $("#start_time")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
            });

    var to = $("#end_time")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

});

var sendFile = function (file) {
    data = new FormData();
    data.append("image", file);
    $('.submit-ajax-form').hide();

    $.ajax({
        data: data,
        type: "POST",
        url: "/manager/page/upload",
        cache: false,
        contentType: false,
        processData: false,
        success: function (url) {
            $(currentEditor).summernote('insertImage', url);
        },
        xhr: function () {
            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener('progress', progress, false);
            xhr.addEventListener('progress', progress, false);
            return xhr;
        }
    });
};

var progress = function (e) {

    if (e.lengthComputable) {
        var max = e.total;
        var current = e.loaded;

        var percent = (current * 100) / max;
        $('.progress-bar').css('width', percent + '%');


        if (percent >= 100) {
            $('.progress-bar').css('width', '0%');
        }
    }
};