var currentHtml;

$(function () {

    $('#details-tab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
        currentHtml = $(this).data('content');
    });
    $('#details-tab a:first').tab('show');

    currentHtml = $('#details-tab a:first').data('content');

    $('.notes').summernote({
        height: 400,
        callbacks: {
            onChange: function (contents, $editable) {
                console.log('onChange:', contents, $editable);

                $(currentHtml).val(contents);
            }
        }
    });
    $('.notes').summernote('fontName', 'Signika');

});