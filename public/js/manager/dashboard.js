$(function() {

  $("#search-period").click(function() {
    refreshGraph();
  });

  var initClickFilter = function() {
    $('[id^="showby"] input').click(function() {
      var chartId = $(this).attr('for-chart');
      var filter = getPeriod();
      filter.showBy = $(this).val();
      filter.listing = $(this).parent().parent().parent().find('[id^="listing-"] input:checked').val();
      console.log(filter);
      generateChart('/manager/stat/' + chartId, chartId, filter);
    });

    $('[id^="listing-"] input').click(function() {
      var chartId = $(this).attr('for-chart');
      var filter = getPeriod();
      filter.listing = $(this).val();
      filter.showBy = $(this).parent().parent().parent().find('[id^="showby"] input:checked').val();
      generateChart('/manager/stat/' + chartId, chartId, filter);
    });
  }

  var getPeriod = function() {
    return {
      from: $('#period_from').val(),
      to: $('#period_to').val()
    };
  }

  var refreshGraph = function() {
    generateChart('/manager/stat/total_listing', 'total_listing', getPeriod());
    generateChart('/manager/stat/total_member', 'total_member', getPeriod());
    generateChart('/manager/stat/total_comments', 'total_comments', getPeriod());
    generateChart('/manager/stat/prefered_language', 'prefered_language', getPeriod());
  }

  var destroyGraph = function(idCanvas) {
    if (typeof (window[idCanvas].destroy) === 'function') {
      window[idCanvas].destroy();
    }
    var parent = $('#' + idCanvas).parent();
    $('#' + idCanvas).remove();
    parent.append('<canvas  width="500" height="400" id="' + idCanvas + '"><canvas>');
  }

  var generateChart = function(url, idCanvas, filter) {
    destroyGraph(idCanvas);
    var canvas = document.getElementById(idCanvas).getContext("2d");
    $.post(url, filter, function(data) {
      var myBarChart = new Chart(canvas, {
        type: 'bar',
        data: data,
        options: {
          barValueSpacing: 20,
          scales: {
            yAxes: [{
              ticks: {
                min: 0,
              }
            }]
          },
          legend: {
            display: false
          },
        }
      });
      window[idCanvas] = myBarChart;
    });
  }

  var initDatePicker = function() {
    var dateFormat = "dd/mm/yy";

    $("#period_from")
      .datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: dateFormat
      })

    $("#period_to")
      .datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1,
        dateFormat: dateFormat
      })
  }

  refreshGraph();
  initDatePicker();
  initClickFilter();
});
