$(function(){
    
    $('.edit-place').on('click', function(){
        currentId = $(this).data('id');
        
        $('.place-details').load('/manager/place/form/' + currentId, function(data){
            $('#place-modal').modal('show');
        });
    });
    
    $('.save-place').on('click', function(){
        submitGenericForm('place-update-form');
    });
    
    
    $('.delete-place').on('click', function(){
        currentId = $(this).data('id');
        
        $('.delete-place-details').load('/manager/place/delete-form/' + currentId, function(data){
            $('#delete-place-modal').modal('show');
        });
    });
    
    $('.remove-place').on('click', function(){
        submitGenericForm('place-delete-form');
    });
    
});