var moment = moment || function () {};

$(function () {
    var dateFormat = "dd/mm/yy";
    var from = $("#date_start")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
            });

    var to = $("#date_end")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }

    $('.set-date').on('click', function () {
        var target = $(this).data('target');
        var months = parseInt($(this).data('value'));

        var start = getDate(document.getElementById('date_start'));
        console.log(months);

        if (months === 0) {
            var end = moment();
            $(target).datepicker('setDate', end.toDate());
        } else if (start) {
            var end = moment(start).add(months, 'M');
            $(target).datepicker('setDate', end.toDate());
        }

    });
});