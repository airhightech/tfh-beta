var lat0 = lat0 || 13.746318;
var lng0 = lng0 || 100.534875;

var updateArea = function (addressComponents)
{
    console.log(addressComponents);
    area = addressComponents.streetName + ', ' + addressComponents.district;
    $('#address').val(area);
    $('#postcode').val(addressComponents.postalCode);
};
var initArea = function (addressComponents)
{
    if ($('#address').val().length === 0) {
        area = addressComponents.streetName + ', ' + addressComponents.district;
        $('#address').val(area);
        $('#postcode').val(addressComponents.postalCode);
    }
};

var togglePropertyStatus = function (id) {
    $.get('/rest/property/toggle/' + id);
};



$(function () {

    if ($('#map-picker').locationpicker) {
        $('#map-picker').locationpicker({
            location: {
                latitude: lat0,
                longitude: lng0
            },
            radius: 0,
            enableAutocomplete: true,
            autocompleteSetting: {
                componentRestrictions: {country: "th"}
            },
            inputBinding: {
                latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
                locationNameInput: $('#area')
            },
            onchanged: function (currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                updateArea(addressComponents);
            },
            oninitialized: function (component) {
                var addressComponents = $(component).locationpicker('map').location.addressComponents;
                initArea(addressComponents);
            }
        });
    }

    $('#clear-area-btn').on('change', function () {
        $('#area').val('');
    });

    $('.property-status').on('click', function () {
        console.log('dfsdfsd');
        var pid = $(this).data('id');
        $.get('/rest/property/toggle/' + pid, function () {

        });
    });
});


