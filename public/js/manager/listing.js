var currentPropertyId = 0;
var currentModal = currentModal || null;

$(function () {
    $('.edit-listing').on('click', function () {
        
        currentPropertyId = this.dataset.id;
        
        currentModal = '#listing-modal';

        $('.modal-listing').load('/rest/listing/form/' + currentPropertyId, function () {

            var dateFormat = "dd/mm/yy";
            var from = $("#from")
                    .datepicker({
                        minDate: new Date(),
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 2,
                        dateFormat: dateFormat
                    })
                    .on("change", function () {
                        to.datepicker("option", "minDate", getDate(this));
                    });

            var to = $("#to")
                    .datepicker({
                        minDate: new Date(),
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 2,
                        dateFormat: dateFormat
                    })
                    .on("change", function () {
                        from.datepicker("option", "maxDate", getDate(this));
                    });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }

            $(currentModal).on('shown.bs.modal', function(){
                if ($('.building_id').length) {
                    $('.building_id').chosen();
                }
            });
            
            $(currentModal).modal('show');
        });

        
        
    });
    
    
    $('.delete-listing').on('click', function () {
        
        currentPropertyId = this.dataset.id;
        
        currentModal = '#listing-modal-delete';

        $('.modal-listing-delete').load('/rest/listing/delete-form/' + currentPropertyId, function () {
            $(currentModal).modal('show');
        });
    });
});

var togglePropertyStatus = function(id){
    $.get('/rest/property/toggle/' + id);
};