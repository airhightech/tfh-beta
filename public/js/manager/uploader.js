var cancelDeleteImage = function (elt, target) {

};


$(function () {

    $('.image-input').on('change', function () {

        var max_size = parseInt($('input[name=MAX_FILE_SIZE]').val());

        var target = $(this).data('target');
        console.log('change', this);
        console.log('change', this.files);

        var currentSize = 0;

        if (this.files.length > 0) {
            for (var i = 0; i < this.files.length; i++) {
                currentSize += this.files[i].size;
            }
        }

        var error = $(this).data('error');

        if (currentSize > max_size) {
            $('#' + error).show();
            $(this).closest("form").reset();
        } else {
            $('#' + error).hide();
            if (this.files.length > 0) {
                for (var i = 0; i < this.files.length; i++) {
                    var file = this.files[i];
                    if (/^image\/\w+$/.test(file.type)) {
                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.target = target;
                        reader.onload = appendPreviewFromReader.bind(reader);
                    }
                }
            }
        }
    });

    $('.rm-media').on('click', function (event) {
        console.log(event);
        event.preventDefault();
        $($(this).data('target')).show();
    });

    $('.rm-media-cancel').on('click', function (event) {
        event.preventDefault();
        $($(this).data('target')).hide();
    });
});

var appendPreviewFromReader = function () {

    var div = document.createElement("div");
    $(div).addClass('img-preview');

    var target = $(this.target);

    if (target.length > 0) {
        if (target[0].tagName === 'IMG') {
            $(this.target).prop('src', this.result);
            $(this.target).show();
            $(this.target + '_placeholder').hide();
        } else {
            var img = document.createElement("img");
            $(img).prop('src', this.result);
            $(img).addClass('img-responsive');
            $(div).append(img);

            $(this.target).append(div);
        }
    }
};