var propertyId = 0;

var addRoomType = function () {
    var formData = {
        name: $('#room_name').val(),
        size: $('#room_size').val()
    };

    $.post("/rest/property/add-room/" + propertyId, formData, function (response) {
        $("#room-type-container").html(response);
        $('#room_name').val('');
        $('#room_size').val('');
    });
};

var deleteRoomType = function (roomId) {
    $.get("/rest/property/delete-room/" + propertyId + "/" + roomId, function (response) {
        $("#room-type-container").html(response);
    });
};

var updateRoom = function (roomId) {
    var formData = {
        name: $('#room_name_' + roomId).val(),
        size: $('#room_size_' + roomId).val(),
        id: roomId
    };

    $.post("/rest/property/update-room/" + propertyId + "/" + roomId, formData, function (response) {
        $("#room-type-container").html(response);
    });
};
var showUpdate = function (id)
{
    $(id).show();
};

$(function(){
    propertyId = $('.property-id').val();
});