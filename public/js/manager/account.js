$(function () {

    $('.edit-account').on('click', openAccountModal);

    $('.save-account').on('click', saveAccountDetails);
    
    $('.delete-account').on('click', showAccountDeleteDialog);
    
    $('.delete-user-account').on('click', deleteUserAccount);

});

var lastUserId = 0;


var openAccountModal = function (event) {

    event.preventDefault();

    lastUserId = event.currentTarget.dataset.id;

    $('.modal-body').load('/manager/account/profile/' + lastUserId, function (data) {

        $('.account-details').html(data);

        $('#account-modal').modal('show');
    });
};

var saveAccountDetails = function () {
    var data = new FormData(document.getElementById('account-details'));
    console.log(data);

    $.ajax({
        url: '/manager/account/update',
        data: data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data, status, xhr) {
            window.location.reload();
        },
        error: function (xhr, status, error) {

        }
    });
};

var userName;

var showAccountDeleteDialog = function(event) {
    
    event.preventDefault();

    lastUserId = event.currentTarget.dataset.id;
    userName = event.currentTarget.dataset.name;
    
    $('#user-name').html(userName);

    $('#account-delete-modal').modal('show');
};

var deleteUserAccount = function(){
    
    $.get('/rest/user/delete/' + lastUserId, function(){
        window.location.reload();
    });
    
};