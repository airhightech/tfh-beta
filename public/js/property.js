var currentHL = currentHL || 'th';

var TFHProperty = function _TFH_Property(data, type) {
    this.data = data;
    this.type = type;
    this.elt;
    this.filterPrice = 0;
};

TFHProperty.prototype.loadImage = function (data) {
    if (data.main !== null) {

    }
};

TFHProperty.prototype.addToFavorite = function () {

};

TFHProperty.prototype.getTemplate = function (favorites) {

    var html = $('#property-template').html();
    this.elt = $(html);
    //elt.find('.img-count').html(this.data.images.length + ' images');

    this.elt.find('.prop-image').prop('src', '/property-thumb/' + this.data.id + '?s=320x300');
    this.elt.prop('id', 'prop-' + this.data.id);

    if(this.data.np) {
        this.elt.find('.listing-type').addClass('project');
    } else  if(this.data.ptype === 'cdb') {
        this.elt.find('.listing-type').addClass('condo');
    } else {
        this.elt.find('.listing-type').addClass(this.data.ltype);
    }

    var price = 0;
    
    var countImages = parseInt(this.data.images);
    
    if (countImages > 1) {
        this.elt.find('.img-count').html(countImages + ' images');
    } else if(countImages === 1) {
        this.elt.find('.img-count').html('1 image');
    } else {
         this.elt.find('.img-count').html('No image');
    }
    
    this.elt.find('.address').html(this.data.address);

    if (this.data.ptype === 'cdb' || this.data.np === true) {
        
        this.elt.find('.prop-only').hide();
        
        this.filterPrice = parseInt(this.data.starting_sales_price);
        
        price = numberWithCommas(this.data.starting_sales_price);
        price = (price === null) ? 0 : price;
        this.elt.find('.listing-price').html('‎฿ ' + price);
        
        var name = $.parseJSON(this.data.name);        
        var current = name[currentHL] || '';
        
        if (current.length === 0 && name['en']) {
            current = name['en'];
        }
        
        if (current.length === 0 && name['th']) {
            current = name['th'];
        }
        
        this.elt.find('.property').html(current);                
        
    } else {
        
        this.elt.find('.beds').html(this.data.bedroom_num);
        this.elt.find('.bath').html(this.data.bathroom_num);
        this.elt.find('.size').html(this.data.total_size);
        
        this.filterPrice = parseInt(this.data.listing_price);
        
        price = numberWithCommas(this.data.listing_price);
        price = (price === null) ? 0 : price;
        this.elt.find('.listing-price').html('‎฿ ' + price);
        
        if (this.data.name) {
            var name = $.parseJSON(this.data.name);
            var current = name[currentHL] || '';        
            if (current.length === 0 && name['en']) {
                current = name['en'];
            }

            if (current.length === 0 && name['th']) {
                current = name['th'];
            }
            this.elt.find('.property').html(current);
        } else {
            
        }  
    }

    this.elt.on('click', this.showPreview.bind(this));

    // Set favorite icon

    this.elt.find('.favorite-btn').data('id', this.data.id);

    if (favorites.length > 0 && favorites.indexOf(this.data.id) >= 0) {
        this.elt.find('.favorite').addClass('fa-heart').addClass('text-danger');
    } else {
        this.elt.find('.favorite').addClass('fa-heart-o');
    }

    return this.elt;
};

TFHProperty.prototype.sortData = function () {
    console.log(this.data);

    this.elt.prop('data-rank', this.data.rank + '');
    this.elt.prop('data-year', parseInt(this.data.year_built) + '');
    this.elt.prop('data-price', this.filterPrice + '');
    this.elt.prop('data-beds', this.data.bedroom_num + '');
    this.elt.prop('data-baths', this.data.bathroom_num + '');
    this.elt.prop('data-size', this.data.total_size + '');
    this.elt.prop('data-rental', this.data.minimal_rental_period + '');
};

TFHProperty.prototype.showPreview = function () {
    showPropertyPreview(this.data);
};

