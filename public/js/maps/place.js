var google = google || {};

var Place = function (data) {
    this.data = data;
    this.names = jQuery.parseJSON(this.data.name);
    this.createMarker();
};

Place.prototype.createMarker = function () {
    var image = {
        url: this.getMarkerIcon(),
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 37)
    };
    this.marker = new google.maps.Marker({
        position: {lat: parseFloat(this.data.lat), lng: parseFloat(this.data.lng)},
        draggable: true,
        title: this.names.en + ' | ' + this.names.th,
        icon: image
    });
    
    this.marker.data = this.data;
    this.marker.parent = this;
};

Place.prototype.getMarkerIcon = function () {
    return '/img/map/places/' + this.data.type + '.png';
};