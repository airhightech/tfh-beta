var currentModal = null;
var currentHL = currentHL || 'th';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var currentDeleteLink;

$(function () {

    if ($('.show-more-lang').length) {
        $('.show-more-lang').on('click', function (event) {
            event.preventDefault();
            $(event.currentTarget.dataset.target).toggle();
        });

        $.map($('.show-more-lang'), function (elt) {
            var target = $(elt.dataset.target);

            if (target.length === 0) {
                $(elt).hide();
            }
        });
    }

    $('.silent-link').on('click', function (event) {
        event.preventDefault();
        showLoadingModal();
        $.get(this.href, function () {
            setTimeout(function () {
                window.location.reload();
            }, 400);
        });
    });

    $('.delete-action').on('click', function (event) {
        event.preventDefault();
        currentDeleteLink = this.href;
        showDeleteModal();
    });

    $('.file-value-updater').on('change', function () {
        var target = $(this).data('target');
        if (this.files.length > 0) {
            $(target).val(this.files[0].name);
        }
    });

    $('.auth-mode-switcher').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.auth-box').toggle();
    });
});

var popupSuccessOperation = function () {
    new PNotify({
        title: 'Regular Notice',
        text: 'Check me out! I\'m a notice.',
        icon: false,
        type: 'success'
    });
};

var popupFailedOperation = function () {
    new PNotify({
        title: 'Regular Notice',
        text: 'Check me out! I\'m a notice.',
        icon: false,
        type: 'error'
    });
};

var showLoadingModal = function () {
    $('#loading-modal').modal('show');
};

var showDeleteModal = function () {
    $('#delete-modal').modal('show');
};

var performDelete = function () {
    if (currentDeleteLink) {
        $.get(currentDeleteLink, function () {
            setTimeout(function () {
                window.location.reload();
            }, 400);
        });
    }
};

var _trans = _trans || {};

var tr = function (token) {
    var text = _trans[token];
    return text ? text : token;
};

var modal = function (token) {
    $('#default-modal').find('.default-modal-body').html(tr(token));
    $('#default-modal').modal('show');
};

var show = function (selector, text) {
    if (text) {
        $(selector).html(text);
    }
    $(selector).show();
};

var showPropertyMap = function (lat, lng) {
    previewMap = null;
    previewMap = new google.maps.Map(
            document.getElementById('property-map'),
            {
                center: {lat: lat, lng: lng},
                zoom: 15
            });

    previewMarker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        draggable: false,
        icon: '/img/maps/pin/pin-sale-standard.png',
        map: previewMap
    });
};

var propertyShare = function (pid) {

};

var getImageUrl = function (type) {

    var url = null;

    switch (type) {
        case 'bts':
            url = '/img/maps/selected/bts.png';
            break;
        case 'mrt':
            url = '/img/maps/selected/mrt.png';
            break;
        case 'apl':
            url = '/img/maps/selected/apl.png';
            break;
        case 'bank':
            url = '/img/maps/selected/bank.png';
            break;
        case 'dpt-store':
            url = '/img/maps/selected/departmentstore.png';
            break;
        case 'hospital':
            url = '/img/maps/selected/hospital-building.png';
            break;
        case 'school':
            url = '/img/maps/selected/school.png';
            break;
        default:
            //url = '/img/maps/selected/bts.png';
            break;
    }

    return url;
};

var numberWithCommas = function (x) {
    return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
};

var trjson = function (text) {
    var options = JSON.parse(text);

    if (options[currentHL]) {
        return options[currentHL];
    } else if (options['en']) {
        return options['en'];
    } else {
        return options['th'];
    }
};

var toggleStatus = function (id) {
    $.get('/rest/property/toggle/' + id);
};

var showPhone = function (elt) {
    var tel = $(elt).data('tel').trim();
    if (tel.length > 0) {
        $(elt).text(tel);
    } else {
        $(elt).text('Not available');
    }

};

var isFunction = function (obj) {
    return !!(obj && obj.constructor && obj.call && obj.apply);
};

var goBack = function () {
    window.history.back();
};

// Get IE or Edge browser version

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
    var ua = window.navigator.userAgent;

    // Test values; Uncomment to check result …

    // IE 10
    // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

    // IE 11
    // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

    // Edge 12 (Spartan)
    // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

    // Edge 13
    // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+) => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}