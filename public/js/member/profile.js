$(function(){
    
    if ($(".languages").length && $(".languages").chosen) {
        $(".languages").chosen({});
    }
    
    $(".birthdate").datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        beforeShow: function (input, inst) {
            $('#birthdate').data('before', $('#birthdate').val());
        },
        onClose: function (dateText, inst) {
            var data = {
                'before': $('#birthdate').data('before'),
                'after': dateText,
                'name': 'birthdate'
            };
            $.post('/rest/profile/update', data, function () {
                popupSuccessOperation();
            });
        }
    });
    
    /* Upload cover picture */

    var formData = new FormData();

    var sendCover = function () {

        $('.cover-loading').show();

        var xhr = $.ajax({
            url: '/rest/profile/upload-cover',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            complete: function (xhr) {
                $('.progress-bar').addClass('progress-bar-success');
                console.log('finished');

                $('.cover-loading').hide();

                popupSuccessOperation();
                setTimeout(function () {
                    window.location.reload(true);
                }, 2000);
            }
        });
    };

    var sendProfile = function () {

        //$('.cover-loading').show();

        var xhr = $.ajax({
            url: '/rest/profile/upload-photo',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            complete: function (xhr) {
                $('.progress-bar').addClass('progress-bar-success');
                console.log('finished');
                //$('.cover-loading').hide();
                popupSuccessOperation();
                setTimeout(function () {
                    window.location.reload(true);
                }, 2000);
            }
        });
    };

    var $coverImage = $("#cover");
    var $profileImage = $("#photo");
    if (window.FileReader) {
        $coverImage.on('change', function () {
            formData = new FormData();
            var reader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {

                formData.set('image', file);

                reader.readAsDataURL(file);
                reader.onload = function () {
                    $('#cover-img').prop('src', reader.result);
                    sendCover();
                };

            }
        });

        $profileImage.on('change', function () {
            formData = new FormData();
            var reader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {

                formData.set('image', file);

                reader.readAsDataURL(file);
                reader.onload = function () {
                    $('#profile-img').prop('src', reader.result);
                    sendProfile();
                };

            }
        });

    } else {
        $profileImage.addClass("hide");
    }

    $('.setting-data-item').on('focus', function () {
        $(this).data('before', $(this).val());
    });

    $('.setting-data-item').on('blur', function () {
        updateSetting(this);
    });

    $('select.data-select').on('change', function () {
        updateSetting(this);
    });
});

var updateSetting = function (elt) {
    if ($(elt).data('before') !== $(elt).val()) {
        $(elt).data('after', $(elt).val());
        $.post('/rest/profile/update', $(elt).data(), function () {
            popupSuccessOperation();
        });
    }
};

var updateLanguages = function (elt) {
    var $this = $(elt);

    var current = $this.val().join(',');

    if ($this.data('before') !== current) {
        $this.data('after', current);
        var data = $this.data();
        $.post('/rest/profile/update', {name: data.name, after: data.after}, function () {
            popupSuccessOperation();
        });
    }
};