
var currentModal = currentModal || null;

$(function(){
    $('#upgrade-modal-success').on('hidden.bs.modal', function(){
        window.location.reload();
    });
});

var upgradeExclusive = function (propertyId) {
    
    $.get('/member/property/exclusive-upgrade/' + propertyId, function (data) {
        
        currentModal = '#exclusive-modal';

        $('.exclusive-body').html(data);
        $('#exclusive-modal').modal('show');
        $('#exclusive-modal').on('shown.bs.modal', function(){
            enableCalendar();
        });

    });
};

var upgradeFeatured = function (propertyId) {        
   
    $.get('/member/property/featured-upgrade/' + propertyId, function (data) {
        
        currentModal = '#featured-modal';

        $('.featured-body').html(data);
        $('#featured-modal').modal('show');
        $('#featured-modal').on('shown.bs.modal', function(){
            enableCalendar();
        });

    });

};

var enableCalendar = function () {
    var dateFormat = "dd/mm/yy";
    var from = $(".upgrade_start_calendar")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat,
                onSelect: function(date, picker){
                    console.log(date);
                    $('#upgrade_start').val(date);
                }
            });
};