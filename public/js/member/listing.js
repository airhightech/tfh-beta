var reloadImageList = function (data, status, xhr) {
    var pid = $('#id').val();
    
    console.log('reloadImageList');

    $('.review-media-list').load('/member/posting/media/' + pid, function () {
        $('.img-preview-container').html('');
        $('#upload-progress').css('width', 0);

        $('.silent-link').on('click', function (event) {
            event.preventDefault();
            showLoadingModal();
            $.get(this.href, function () {
                reloadImageList();
            });
        });
    });
};

$(function () {
    $(".bts").chosen({max_selected_options: 4});
    $(".mrt").chosen({max_selected_options: 4});
    $(".apl").chosen({max_selected_options: 4});

    var dateFormat = "dd/mm/yy";

    $("#movein_date").datepicker({
        minDate: new Date(),
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat: dateFormat
    });

    $('.facility-item').on('click', function () {
        $(this).toggleClass('active');

        var ref = $(this).data('ref');

        if ($(this).hasClass('active')) {
            $(ref).prop("checked", true);
        } else {
            $(ref).prop("checked", false);
        }
    });

    $('#prop_image').on('change', function () {
        console.log('upload');
        submitGenericForm('property-image-upload', reloadImageList);
    });

    $('#listing_price').on('keyup', updatePsf);
    $('#total_size').on('keyup', updatePsf);
});

var cancelDeleteImage = function (target) {
    event.preventDefault();
    $(target).hide();
};

var showDeleteImage = function (target) {
    event.preventDefault();
    $(target).show();
};

var showFullProperty = function () {
    window.location = '/property/' + propertyId;
};

var updatePsf = function () {

    var price = parseInt($('#listing_price').val());
    var size = parseInt($('#total_size').val());

    var psf = 0;

    if (size > 0) {
        psf = Math.round(price / size);
    }

    $('#psf').val(psf);

};