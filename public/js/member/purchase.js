var packages = packages || [];
var packageId = 0;
var paymentMode = '';

$(function () {
    $('.poption').on('click', function () {

        console.log(this);

        if (this.value === 'bank') {
            paymentMode = 'bank';
            $('.package-bank-accounts').show();
            $('.package-paysbuy').hide();
        } else {
            paymentMode = 'card';
            $('.package-bank-accounts').hide();
            $('.package-paysbuy').show();
        }

    });

    $('.package-checkbox').on('click', function () {
        var ref = $(this).data('ref');
        var pid = $(this).data('package');
        setPackage(pid);
        $('.ico-choice').removeClass('fa-dot-circle-o').addClass('fa-circle');
        $('#' + ref).removeClass('fa-circle').addClass('fa-dot-circle-o');

        $('.package-price-details').show();
        $('.package-price-select').hide();
    });

    $('.package-price-details').hide();

    $('#submit-payment').on('click', function () {

        if (packageId > 0 && paymentMode !== '') {

            if (paymentMode === 'bank') {
                var form = document.getElementById('payment-form');
                var formData = new FormData(form);
                
                $('#payment-modal').on('hidden.bs.modal', function(){
                    window.location = '/member/purchase';
                });

                $.ajax({
                    url: '/rest/package/buy',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    success: function (data) {
                        console.log(data);
                        $('#order-id').html(data.orderId);
                        $('#payment-modal').modal();
                    }
                });
            } else {
                $('#payment-processing').modal();

                $.get('/rest/payment/token/' + packageId, function (data) {

                    var doc = $.parseXML(data);
                    var $xml = $(doc);
                    var code = $xml.text().substring(0, 2);

                    if (code === '00') {
                        var refid = $xml.text().substring(2);
                        window.location = 'http://www.paysbuy.com/paynow.aspx?refid=' + refid;
                    } else {
                        alert('An error occured');
                    }
                    console.log($xml.text());
                });
            }
        } else {
            alert('Please select a package and payment method');
        }

    });
});

var setPackage = function (pid) {
    var package = getPackage(pid);
    packageId = pid;

    var price = parseFloat(package.normal_price) + 0;
    var discount = parseFloat(package.discounted_price) + 0;

    price = discount > 0 ? discount : price;
    var vat = 0.07 * price;
    var total = price + vat + 0;

    $('.package-name').html(trjson(package.name) + ' (' + package.listing_count + ' listing)');
    $('.package-price').html(numberWithCommas(price.toFixed(2)) + ' Baht');
    $('.package-vat').html(numberWithCommas(vat.toFixed(2)) + ' Baht');
    $('.total-amount').html(numberWithCommas(total.toFixed(2)) + ' Baht');
};

var getPackage = function (pid) {

    var package = null;

    for (var i = 0; i < packages.length; i++) {
        if (packages[i].id === pid) {
            package = packages[i];
        }
    }

    return package;
};