var updatefilter = function () {

    var ftype = $('.filter[name=lt]:checked').val();
    var fproperty = $('.filter[name=pt]:checked').val();
    var fclass = $('.filter[name=class]:checked').val();
    var fstatus = $('.filter[name=status]:checked').val();

    window.location = '/member/listing?lt=' + ftype + '&pt=' + fproperty + '&class=' + fclass + '&status=' + fstatus;

};


$(function(){
    $('.filter').on('click', function(){
        updatefilter();
    });
});