var currentHL = currentHL || 'th';
var xhr;
var autoCompleteInitialized = false; 
var map;
var marker;
var locationPicker;
var updateMapLocation = true;
var updateAddress = false;

$(function () {
    
    $('.required-condo-name').hide();

    $('.property-types button').on('click', function () {
        $('.property-types button').removeClass('btn-selected');
        $(this).addClass('btn-selected');

        var type = $(this).data('type');

        $('#property_type').val(type);

        if (type === 'cd') {
            $('#property_name').prop('required', true);
            $('.required-condo-name').show();
            $('.mark-location-container').hide();
        } else {
            resetLocations();
            $('#property_name').prop('required', false);
            $('.required-condo-name').hide();
            $('.mark-location-container').show();
        }
        
        if (type === 'cd') {
            activatePropertyNameSuggestion();
        } else {
            if (autoCompleteInitialized) {
                $("#property_name").autoComplete( "destroy" );
            }            
        }
    });

    $('#map-picker').locationpicker({
        location: {latitude: 13.734856, longitude: 100.543396},
        radius: 0,
        enableAutocomplete: true,
        autocompleteSetting: {
            componentRestrictions: {country: "th"}
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            
            updateAddress = true;

            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            console.log(addressComponents);
            console.log(currentLocation);

            updateInputIfEmpty('#street_number', addressComponents.streetNumber);
            updateInputIfEmpty('#street_name', addressComponents.streetName);
            updateInputIfEmpty('#postal_code', addressComponents.postalCode);

            $('#lat').val(currentLocation.latitude);
            $('#lng').val(currentLocation.longitude);
        }
    });
    
    locationPicker = $('#map-picker').locationpicker('map');    
    map = locationPicker.map;
    marker = locationPicker.marker;
});

var activatePropertyNameSuggestion = function(){
    autoCompleteInitialized = true;
    $('#property_name').autoComplete({
        minChars: 2,
        source: function (term, response) {
            try {
                xhr.abort();
            } catch (e) {
            }
            xhr = $.getJSON('/rest/suggest/condo', {term: term}, function (data) {
                if (data.length === 0) {
                    $('#property_id').val('');
                    resetLocations();
                }
                response(data);
            });
        },
        renderItem: function (item, search) {
            var current = JSON.parse(item.name);
            return '<div class="autocomplete-suggestion" data-id="' + item.id + '" data-name="' + current[currentHL] + '">'
                    + current[currentHL]
                    + '</div>';
        },
        onSelect: function (e, term, item) {
            syncPropertyLocation(item[0].dataset);
            $('#property_name').val(item[0].dataset.name);
            $('#property_id').val(item[0].dataset.id);
        }
    });
};

var updateInputIfEmpty = function (input, value)
{
    if (updateAddress) {
        $(input).val(value);
    }
};

var syncPropertyLocation = function (property) {

    $.get('/rest/location/sync/' + property.id, function (data) {
        if (data.status === 'OK') {

            if (data.address.lnglat) {

                $('.location-container').hide();

                $('#lat').val(data.address.lnglat.lat);
                $('#lng').val(data.address.lnglat.lng);

                $('#map-picker').locationpicker({
                    location: {latitude: parseFloat(data.address.lnglat.lat), longitude: parseFloat(data.address.lnglat.lng)}
                });
                marker.draggable = false;
            }

        } else {
            $('.location-container').show();
        }
    });
};

var resetLocations = function () {
    $('#province_id').prop('disabled', false);
    $('#district_id').prop('disabled', false);
    $('#area_id').prop('disabled', false);

    $('#lat').val('');
    $('#lng').val('');
    
    marker.draggable = true;
    $('.location-container').show();

};
