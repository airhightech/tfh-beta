$(function() {

  $('.favorite-container').load('/user/favorites');

  $('.topbar .menu').on('click', function() {
    $('.menu-container').toggleClass('visible');
  });

  $('.topbar .favorite').on('click', function() {

    $('.menu-container').removeClass('visible');

    if (!$('.favorite-container').hasClass('visible')) {
      $('.favorite-container').load('/user/favorites');
    }
    $('.favorite-container').toggleClass('visible');
  });

  $("#submitLogin").click(function() {
    var data = {
      email: $('#email').val(),
      password: $('#password').val()
    };
    $.post("/rest/auth/login", data, function(res) {
      if (res.status == 'ERROR') {
        alert(res.error);
      } else if (res.status == 'NOT_VALIDATED') {
        alert('Please validate your email');
      } else {
        window.location = "/";
      }
    });
  });
});
