$(function() {

  selectDetailsTab('#about-tab');

  $('.agent-tab').on('click', function() {
    var tab = $(this).data('ref');
    selectDetailsTab(tab);
  });

  var ctx = document.getElementById("property-canvas");

  var data = {
    labels: labels,
    datasets: [{
      data: propertyData,
      strokeWidth: 25,
      backgroundColor: [
        "#cbff7a",
        "#ff5d83",
        "#179aed",
        "#93dcb6"
      ],
      hoverBackgroundColor: [
        "#cbff7a",
        "#ff5d83",
        "#179aed",
        "#93dcb6"
      ]
    }]
  };

  if (ctx) {
    chart = new Chart(ctx, {
      type: 'doughnut',
      data: data,
      options: {
        legend: {
          display: false
        },
        cutoutPercentage: 60
      }
    });
  }

});

$("#send-message").click(function() {
  $.post("/rest/message/send", $("#form-message").serialize(), function(data) {
    location.reload();
  });
});

$("#send-review").click(function() {
  var formData = {
    title: $('#review-title').val(),
    rating: Math.ceil(parseInt($('.rating-blue :radio:checked').val()) / 2),
    comment: $('#review-comment').val(),
    agent_id: agentId
  }
  $.post("/rest/agent/review", formData, function(data) {
    location.reload();
  });
});

var selectDetailsTab = function(tab) {
  $('.agent-tab-view').hide();
  $(tab).show();
};
