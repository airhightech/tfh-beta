var propertyMap;
var previewMap;
var previewMarker;
var previewPoiMarker;
var latlng = latlng || {};

$(function() {

    var params = {};

    if (location.search) {
        var parts = location.search.substring(1).split('&');

        for (var i = 0; i < parts.length; i++) {
            var nv = parts[i].split('=');
            if (!nv[0]) continue;
            params[nv[0]] = nv[1] || true;
        }
    }

    if (params.login == 'false') {
        alert(pleaseLogIn);
    }

    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        spaceBetween: 30,
    });

    showPropertyMap(parseFloat(latlng.lat), parseFloat(latlng.lng));
    $("#add-favorite").click(function() {
        $.get("/rest/user/favorite-property/" + idProperty, function(data) {
            if (data.result == 'ADDED') {
                $('#add-favorite').css("color", "red");
            } else if (data.result == 'REMOVED') {
                $('#add-favorite').css("color", "black");
            } else if (data.status == 'ERROR') {
                alert(pleaseLogIn);
            }
        });
    });
});

var showPropertyMap = function(lat, lng) {
    propertyMap = null;
    propertyMap = new google.maps.Map(
        document.getElementById('property-map'), {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 15
        });

    previewMap = propertyMap;

    propertyMap = new google.maps.Marker({
        position: {
            lat: lat,
            lng: lng
        },
        draggable: false,
        icon: '/img/maps/pin/pin-sale-standard.png',
        map: propertyMap
    });


    $('.poi-list').on('change', function() {
        var optionSelected = $("option:selected", this);
        $('.poi-list').val(0);
        $(optionSelected).prop('selected', true);

        var options = $(optionSelected[0]).data();

        if (options.lat) {
            var poimg = getImageUrl(options.type);
            var location = {
                lat: parseFloat(options.lat),
                lng: parseFloat(options.lng)
            };

            var image = {
                url: poimg,
                size: new google.maps.Size(32, 37),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 37)
            };

            if (previewPoiMarker) {
                previewPoiMarker.setMap(null);
                previewPoiMarker = null;
            }

            previewPoiMarker = new google.maps.Marker({
                position: location,
                draggable: false,
                icon: image,
                map: previewMap
            });

            var bounds = new google.maps.LatLngBounds();
            bounds.extend({
                lat: parseFloat(latlng.lat),
                lng: parseFloat(latlng.lng)
            });
            bounds.extend(location);

            previewMap.fitBounds(bounds);
        } else {
            previewMap.panTo({
                lat: latlng.lat,
                lng: latlng.lng
            });
        }

        $(this).blur();

    });


    var getImageUrl = function(type) {

        var url = null;

        switch (type) {
            case 'bts':
                url = '/img/maps/selected/bts.png';
                break;
            case 'mrt':
                url = '/img/maps/selected/mrt.png';
                break;
            case 'apl':
                url = '/img/maps/selected/apl.png';
                break;
            case 'bank':
                url = '/img/maps/selected/bank.png';
                break;
            case 'dpt-store':
                url = '/img/maps/selected/departmentstore.png';
                break;
            case 'hospital':
                url = '/img/maps/selected/hospital-building.png';
                break;
            case 'school':
                url = '/img/maps/selected/school.png';
                break;
            default:
                //url = '/img/maps/selected/bts.png';
                break;
        }

        return url;
    };

    /*$("#postComment").click(function() {
      var rating = {
        facilities: $("#facilities :radio:checked").val(),
        room: $("#room :radio:checked").val(),
        transport: $("#transport :radio:checked").val(),
        management: $("#management :radio:checked").val(),
        security: $("#security :radio:checked").val(),
        amenities: $("#amenities :radio:checked").val(),
      };

      var form = $('#formReview');
      var formData = new FormData(form);

      $.ajax({
        method: "POST",
        url: "/rest/review/post",
        contentType: false,
        processData: false,
        dataType: 'json',
        data: formData,
      })
        .done(function(msg) {
          alert("Data Saved: " + msg);
        });
    });
    */
};
