var rel = '';
window.tfh = {};
var currentId = currentId || 0;
$(function() {

    var rentSelected = true;
    var saleSelected;
    var ptype;
    $('#btn-rent').click(function() {
        rentSelected = !$(this).hasClass("selected");
        applyFilter();
    })

    $('#btn-sale').click(function() {
        saleSelected = !$(this).hasClass("selected");
        applyFilter();
    })

    $('#ptype').on('change', function() {
        ptype = $(this).find(":selected").val();
        applyFilter();
    })

    var applyFilter = function() {
        $('.property-item').each(function(index) {
            if ($(this).data("ltype") === 'rent') {
                if (rentSelected && ($(this).data("ptype") === ptype || !ptype)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }

            if ($(this).data("ltype") === 'sale') {
                if (saleSelected && ($(this).data("ptype") === ptype || !ptype)) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            }
        });
    };

    $('#' + currentHL).addClass('active');
    $('#search').autoComplete({
        minChars: 2,
        cache: false,
        source: function(term, response) {
            try {
                suggestXhr.abort();
            } catch (e) {}
            suggestXhr = $.getJSON('/rest/suggest', {
                lt: lt,
                pt: pt,
                scope: scope,
                np: np,
                term: term,
            }, function(data) {
                if (data.length === 0) {
                    $('#property_id').val('');
                    //console.log('item not found');
                }
                response(data);
            });
        },
        renderItem: function(item, search) {
            var current = JSON.parse(item.name);
            var name = current[currentHL];
            if (name.length === 0) {
                name = current['en'];
            }
            if (name.length === 0) {
                name = current['th'];
            }
            return '<div class="autocomplete-suggestion" data-type="' + item.type +
                '" data-id="' + item.id +
                '" data-lng="' + item.lng +
                '" data-lat="' + item.lat +
                '" data-name="' + name + '">' +
                '<span class="icon fa ' + item.type + '"></span>' +
                '<span class="name">' + name + '</span>' +
                '</div>';
        },
        onSelect: function(e, term, item) {
            //console.log(e);
            //console.log(this);
            console.log(item[0])
            console.log(item[0].dataset.name);
            $('#search').val(item[0].dataset.name);
            currentId = item[0].dataset.id;
            rel = item[0].dataset.type;
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    /*  */

    $('.error-box').hide();

    /*  */
    $('.auth-mode-switcher').on('click', function(event) {
        event.stopPropagation();
        $('.auth-box').toggle();
    });

    // Fancybox

    if ($(".fancybox").fancybox) {
        $(".fancybox").fancybox();
    }

    if ($(".single-date").datepicker) {
        $(".single-date").datepicker().datepicker("option", "dateFormat", 'dd/mm/yy');
    }

    // $( "td.single-date" ).on('focus', function(){
    //         $('<input />').appendTo(this).datepicker();
    //     });

    $('#ajax-registration-submiter').on('click', function(event) {
        event.stopPropagation();
        submitRegistration(this);
    });

    $('#ajax-login-submiter').on('click', function(event) {
        event.stopPropagation();
        submitLogin(this);
    });

    $('.ajax-form-submitter').on('click', function() {
        var target = $(this).data('target');

        var form = document.getElementById(target);
        if (form) {
            var data = new FormData(form);
            var action = $(form).prop('action');

            $.ajax({
                url: action,
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data, status, xhr) {

                    if (window.onAjaxFormSuccess !== null) {
                        window.onAjaxFormSuccess(data, xhr);
                    } else {
                        console.warn('No success handler [onAjaxFormSuccess] found');
                    }
                },
                error: function(xhr, status, error) {
                    if (window.onAjaxFormError !== null) {
                        window.onAjaxFormError(xhr);
                    } else {
                        console.warn('No error handler [onAjaxFormError] found');
                    }
                }

            });
        }
    });

});

/**/

var submitLogin = function(button) {
    var data = $('#login-form').serializeArray();
    $(button).blur();
    $(button).addClass('running');

    loginBoxHideAndClear();

    var xhr = $.post("/rest/auth/login", data, function(data) {
        $(button).removeClass('running');
        if (data.status == 'ERROR') {
            loginBoxShow();
            for (var error in data.errors) {
                loginBoxAppendError(data.errors[error]);
            }
        } else if (data.status == 'NOT_VALIDATED') {
            window.location = "/auth/validate-email/" + data.user.id;
        } else {
            window.location = "/auth/auto-redirect";
        }
    }).fail(function() {
        $(button).removeClass('running');
        loginBoxShowAndClear();
        loginBoxAppendError('<li>An error occurred.</li>');
    });
};

var loginBoxShowAndClear = function() {
    $('#login-form .error-box').show();
    $('#login-errors-list').html('');
};

var loginBoxHideAndClear = function() {
    $('#login-form .error-box').hide();
    $('#login-errors-list').html('');
};

var loginBoxShow = function() {
    $('#login-form .error-box').show();
};

var loginBoxHide = function() {
    $('#login-form .error-box').hide();
};

var loginBoxAppendError = function(error) {
    $('#login-errors-list').append(error);
};


/*
    Handle user registration
*/
var submitRegistration = function(button) {
    var data = $('#registration-form').serializeArray();

    $(button).blur();
    $(button).addClass('running');

    registrationBoxHideAndClear();

    var xhr = $.post("/rest/auth/register", data, function(data) {
        $(button).removeClass('running');
        if (data.status == 'ERROR') {
            registrationBoxShow();
            for (var error in data.errors) {
                registrationBoxAppendError(data.errors[error]);
            }
        } else {
            window.location = "/auth/validate-email/" + data.user.id;
        }
    }).fail(function() {
        $(button).removeClass('running');
        registrationBoxShowAndClear();
        registrationBoxAppendError('<li>An error occurred.</li>');
    });
};

var registrationBoxShowAndClear = function() {
    $('#registration-form .error-box').show();
    $('#registration-errors-list').html('');
};

var registrationBoxHideAndClear = function() {
    $('#registration-form .error-box').hide();
    $('#registration-errors-list').html('');
};

var registrationBoxShow = function() {
    $('#registration-form .error-box').show();
};

var registrationBoxHide = function() {
    $('#registration-form .error-box').hide();
};

var registrationBoxAppendError = function(error) {
    $('#registration-errors-list').append(error);
};

var numberWithCommas = function(x) {
    return x !== null ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
};

var showPhone = function(elt) {
    var tel = $(elt).data('tel').trim();
    if (tel.length > 0) {
        $(elt).text(tel);
    } else {
        $(elt).text('Not available');
    }

};

var propertyToggleFavorite = function(pid) {
    $.get('/rest/user/favorite-property/' + pid, function(data, status, xhr) {
        if (data.status === 'OK') {
            if (data.result === 'ADDED') {
                $('.favorite').find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
            } else {
                $('.favorite').find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
            }
        } else {

        }
    });
};
