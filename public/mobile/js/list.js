$("#add-favorite").click(function() {
  $.get("/rest/user/favorite-property/" + idProperty, function(data) {
    if (data.result == 'ADDED') {
      $('#add-favorite').css("color", "red");
    } else if (data.result == 'REMOVED') {
      $('#add-favorite').css("color", "black");
    }
  });
});

$('.apply-search-option').on('click', function() {
  //window.location.replace('/search' + generateUrlCriteria());
  var currentUrl = window.location.search
  currentUrl = removeParam("lt", currentUrl);
  currentUrl = removeParam("np", currentUrl);
  currentUrl = removeParam("pt", currentUrl);
  currentUrl = removeParam("pmin", currentUrl);
  currentUrl = removeParam("pmax", currentUrl);
  currentUrl = removeParam("bed", currentUrl);
  currentUrl = removeParam("bath", currentUrl);
  currentUrl = removeParam("smin", currentUrl);
  currentUrl = removeParam("smax", currentUrl);
  currentUrl = removeParam("ymin", currentUrl);
  currentUrl = removeParam("ymax", currentUrl);
  currentUrl = generateUrlCriteria(currentUrl);
  window.location.replace('/search' + currentUrl)
});


$("#viewMap").click(function() {
  var currentUrl = window.location.search;
  window.location.replace('/search?' + window.location.search.substr(1));
});

$("button[id^='favorite-']").click(function(e) {
  var idProp = $(this).attr('data-idprop');
  var self = this;
  $.get("/rest/user/favorite-property/" + idProp, function(data) {
    if (data.result == 'ADDED') {
      $(self).css("color", "red");
    } else if (data.result == 'REMOVED') {
      $(self).css("color", "white");
    }
    else if (data.status == 'ERROR') {
+        alert(pleaseLogIn);
    }
  });
  return false;
});

$('#q').autoComplete({
  minChars: 2,
  cache: false,
  source: function(term, response) {
    try {
      suggestXhr.abort();
    } catch (e) {}
    suggestXhr = $.getJSON('/rest/suggest', {
      scope: 'cdb,listing,cdb',
      term: term
    }, function(data) {
      if (data.length === 0) {
        $('#property_id').val('');
      //console.log('item not found');
      }
      response(data);
    });
  },
  renderItem: function(item, search) {
    var current = JSON.parse(item.name);
    var name = current[currentHL];
    if (name.length === 0) {
      name = current['en'];
    }
    if (name.length === 0) {
      name = current['th'];
    }
    return '<div class="autocomplete-suggestion" data-type="' + item.type +
      '" data-id="' + item.id +
      '" data-lng="' + item.lng +
      '" data-lat="' + item.lat +
      '" data-name="' + name + '">' +
      '<span class="icon fa ' + item.type + '"></span>' +
      '<span class="name">' + name + '</span>' +
      '</div>';
  },
  onSelect: function(e, term, item) {

    $('#q').val(item[0].dataset.name);
    currentId = item[0].dataset.id;

    var url = '/search/?q=' + item[0].dataset.name +
    '&rel=' + item[0].dataset.type +
    '&lat=' + item[0].dataset.lat +
    '&lng=' + item[0].dataset.lng +
    '&id=' + item[0].dataset.id;

    window.location = url;
  }
});

var generateUrlCriteria = function(url) {

  var url = url;

  //Get value from inputs
  var bed = $('#bed').val();
  var bath = $('#bath').val();
  var smin = $('#smin').val();
  var smax = $('#smax').val();
  var ymin = $('#ymin').val();
  var ymax = $('#ymax').val();
  var lt = $('#lt').val();
  var pt = $('#pt').val();
  var np = $('#new-project').is(":checked") ? 'yes' : 'no';
  var pmin = $('#pmin').val()
  var pmax = $('#pmax').val()

  //if user select *pt=cdb*, then *np* MUST be = NO
  if (pt === 'cdb')
    np = 'no';

  //if user select *np=yes*, then *cdb* MUST be removed from *pt* param
  if (np === 'yes')
    pt = '';

  url = url + '&lt=' + lt;
  url = url + '&np=' + np;
  url = url + '&pmin=' + pmin + '&pmax=' + pmax;
  url = url + '&pt=' + pt;
  url = url + '&bed=' + bed;
  url = url + '&bath=' + bath;
  url = url + '&smin=' + smin;
  url = url + '&smax=' + smax;
  url = url + '&ymin=' + ymin;
  url = url + '&ymax=' + ymax;

  if (currentId) {
    url = url + '&id=' + currentId;
  }

  return url;
};

function removeParam(key, sourceURL) {
  var rtn = sourceURL.split("?")[0],
    param,
    params_arr = [],
    queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
  if (queryString !== "") {
    params_arr = queryString.split("&");
    for (var i = params_arr.length - 1; i >= 0; i -= 1) {
      param = params_arr[i].split("=")[0];
      if (param === key) {
        params_arr.splice(i, 1);
      }
    }
    rtn = rtn + "?" + params_arr.join("&");
  }
  return rtn;
}
