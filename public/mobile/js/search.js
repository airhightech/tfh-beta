var priceClass = '.sale-prices';

var f_lt = '';
var f_bed = '';
var f_np = '';
var f_bat = '';
var sortAttr = '';
var sortAsc = true;

var price_min = 0;
var price_max = 0;

var min_price_label = null;
var max_price_label = null;

var map;

var reloadOnIdle = true;

var currentPage = 1;
var currentId = currentId || 0;
var domain = domain || null;
var currentPoiData = currentPoiData || null;

var lat0 = lat0 || 13.737998;
var lng0 = lng0 || 100.544032;
var zl0 = zl0 || 14;
var showCurrent = false;
var currentPoiMarker = null;
var listedProperties = [];
var maxZoomService;
var filter = false;
var firstSearch = true;

var mapContainer = {
  listingWidth: 670,
  mapHeight: 0,
  propertyListingWidth: 0,
  propertyMapWidth: 0,
  docWidth: 0,
  currentMapCenter: 0,
  haveBigMap: false
};

var searchResult;
var currentHL = currentHL || 'th';
var google = google || null;
var userIsOnline = userIsOnline || false;
var onOfflineMessage = onOfflineMessage || '';
var searchResult;

var lastClusterCount = 0;

var suggestXhr;

$("#viewList").click(function() {
  window.location.replace('/search/list' + generateSearchUrl());
});

var initAutoComplete = function() {
  var currentId = currentId || 0;
  $('#q').autoComplete({
    minChars: 2,
    cache: false,
    source: function(term, response) {
      try {
        suggestXhr.abort();
      } catch (e) {}
      suggestXhr = $.getJSON('/rest/suggest', {
        lt: searchCriteria.lt,
        pt: searchCriteria.pt,
        scope: 'cdb,listing,cdb',
        np: searchCriteria.np,
        term: term
      }, function(data) {
        if (data.length === 0) {
          $('#property_id').val('');
        //console.log('item not found');
        }
        response(data);
      });
    },
    renderItem: function(item, search) {
      var current = JSON.parse(item.name);
      var name = current[currentHL];
      if (name.length === 0) {
        name = current['en'];
      }
      if (name.length === 0) {
        name = current['th'];
      }
      return '<div class="autocomplete-suggestion" data-type="' + item.type +
        '" data-id="' + item.id +
        '" data-lng="' + item.lng +
        '" data-lat="' + item.lat +
        '" data-name="' + name + '">' +
        '<span class="icon fa ' + item.type + '"></span>' +
        '<span class="name">' + name + '</span>' +
        '</div>';
    },
    onSelect: function(e, term, item) {
      //console.log(e);
      //console.log(this);
      $('#q').val(item[0].dataset.name);
      currentId = item[0].dataset.id;
      console.log(item[0].dataset);
      showUserSuggestion(item[0].dataset);
    }
  });
};

var showUserSuggestion = function(data) {

  currentId = data.id;
  currentPoiData = data;

  currentPoiMarker = null;

  if (data.type === 'cdb' || data.type === 'apb') {
    $('#pt-cd').prop('checked', true);
    domain = 'building';
  } else if (data.type === 'cd' || data.type === 'th' || data.type === 'sh' || data.type === 'ap') {
    domain = 'listing';
  } else {
    domain = 'place';
  }

  var lat = parseFloat(data.lat);
  var lng = parseFloat(data.lng);

  map.panTo({
    lat: lat,
    lng: lng
  });

  applySearchCriteria();

  showCurrent = true;
};


$(function() {

  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: lat0,
      lng: lng0
    },
    zoom: zl0
  });

  map.addListener('bounds_changed', mapBoundsListener = function() {
    reloadOnIdle = true;
    if (currentPoiMarker) {
      currentPoiMarker.setAnimation(null);
    }
  });

  map.addListener('dragend', mapBoundsListener = function() {
    currentId = null;
  });

  map.addListener('click', mapBoundsListener = function() {
    currentId = null;
    if (currentInfo) {
      currentInfo.close();
    }
  });

  map.addListener('idle', mapIdleListener = function() {
    if (reloadOnIdle) {
      currentPage = 1;
      applySearchCriteria();
    }
  });

  initAutoComplete();

  $('.btn-map-search').on('click', function() {
    currentPage = 1;
    applySearchCriteria();
  });

  maxZoomService = new google.maps.MaxZoomService();

  $('.pt.co').on('click', function() {

    //f_np = $(this).val();
    f_lt = '';

    $('.lt').prop('checked', false);
    $('#np').prop('checked', false);

    if ($(this).prop('checked')) {
      $('.pt.nm').prop('checked', false); //.prop('disabled', false);
      $('.lt').prop('checked', false);
    } else {
      $('.pt.nm').prop('checked', true);
    }

  });

  $('#np').on('click', function() {
    $('.pt.co').prop('checked', false);
    if ($(this).prop('checked')) {
      $('.pt.nm').prop('checked', false);
      $('.lt').prop('checked', false);
    } else {
      $('.pt.nm').prop('checked', true);
    }
  });

  $('.min-price').on('click', function(event) {
    event.stopPropagation();
    $('.min-prices').hide();
    $('.max-prices').show();

    price_min = parseInt($(this).data('value'));
    var label = $(this).data('label');

    if (price_min > 0) {
      $('#price_min').val(price_min);
      min_price_label = label;
    } else {
      $('#price_min').val($(this).text().trim());
      min_price_label = null;
    }

    updatePriceRangeLabel();

    // check max price value

    if (price_max > 0 && price_max < price_min) {
      // User need to select a new max price
      price_max = 0;
      $('#price_max').val('');
    }

    $('.max-price').removeClass('inactive');

    $('.max-price').each(function(idx, elt) {
      var value = parseInt($(elt).data('value'));

      if (value > 0 && value <= price_min) {
        $(elt).addClass('inactive');
      }
    });
  });

  $('.pt.nm').on('click', function() {
    $('.pt.co').prop('checked', false);
    $('#np').prop('checked', false);
  });

  $('.max-price').on('click', function(event) {

    if ($(this).hasClass('inactive')) {
      event.stopPropagation();
    } else {

      $('.min-prices').show();
      $('.max-prices').hide();

      price_max = parseInt($(this).data('value'));
      var label = $(this).data('label');

      if (price_max > 0) {
        $('#price_max').val(price_max);
        max_price_label = label;
      } else {
        $('#price_max').val($(this).text().trim());
        max_price_label = null;
      }

      updatePriceRangeLabel();
    }
  });

  //

  $('.bed-item').on('click', function() {
    $('.bed-item').removeClass('active');
    f_bed = $(this).data('filter');
    $(this).addClass('active');
    $('.bed-count').html('(' + f_bed + ')');
  });

  $('.bath-item').on('click', function() {
    $('.bath-item').removeClass('active');
    f_bat = $(this).data('filter');
    $(this).addClass('active');
    $('.bath-count').html('(' + f_bat + ')');
  });

  //Click event when the user click on the apply button
  $('.apply-search-option').on('click', function() {
    window.location.replace('/search' + generateUrlCriteria());
  });

});

/**********************************
 *  Update price range values
 **********************************/

var updatePriceRangeVisibility = function() {
  $('.sale-prices').hide();
  $('.rent-prices').hide();

  f_np = '';

  if ($('#lt-sale').prop('checked') && $('#lt-rent').prop('checked')) {
    priceClass = '.sale-prices';
    $('.sale-prices').show();
    f_lt = 'rent,sale';

  } else if ($('#lt-sale').prop('checked')) {
    priceClass = '.sale-prices';
    $('.sale-prices').show();
    f_lt = 'sale';
  } else if ($('#lt-rent').prop('checked')) {
    priceClass = '.rent-prices';
    $('.rent-prices').show();
    f_lt = 'rent';
  } else {
    f_lt = '';
    $('.sale-prices').show();
  }
};

var updatePriceRangeLabel = function() {

  if (min_price_label === null && max_price_label === null) {
    $('.price-range-label').html('Any price');
  } else if (min_price_label !== null && max_price_label === null) {
    $('.price-range-label').html('Min ' + min_price_label);
  } else if (min_price_label === null && max_price_label !== null) {
    $('.price-range-label').html('Max ' + max_price_label);
  } else {
    $('.price-range-label').html(min_price_label + ' - ' + max_price_label);
  }
};

var generateSearchUrl = function() {

  var url = '?q=' + $('#q').val();

  var bounds = map.getBounds();
  var ne = bounds.getNorthEast();
  var sw = bounds.getSouthWest();

  url = url + '&nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6);

  f_np = $('#np').prop('checked') ? 'yes' : '';

  //Get value from inputs
  var bed = searchCriteria.bed ? searchCriteria.bed : '';
  var bath = searchCriteria.bath ? searchCriteria.bath : '';
  var smin = searchCriteria.smin ? searchCriteria.smin : '';
  var smax = searchCriteria.smax ? searchCriteria.smax : '';
  var ymin = searchCriteria.ymin ? searchCriteria.ymin : '';
  var ymax = searchCriteria.ymax ? searchCriteria.ymax : '';
  var lt = searchCriteria.lt ? searchCriteria.lt : '';
  var pt = searchCriteria.pt ? searchCriteria.pt : '';
  var np = searchCriteria.np ? searchCriteria.np : 'no';

  url = url + '&lt=' + lt;
  url = url + '&np=' + np;
  url = url + '&pmin=' + price_min + '&pmax=' + price_max;
  url = url + '&pt=' + pt;
  url = url + '&bed=' + bed;
  url = url + '&bath=' + bath;
  url = url + '&smin=' + smin;
  url = url + '&smax=' + smax;
  url = url + '&ymin=' + ymin;
  url = url + '&ymax=' + ymax;
  url = url + '&sort=' + sortAttr;

  if (sortAsc) {
    url = url + '&dir=asc';
  } else {
    url = url + '&dir=desc';
  }

  if (currentPage > 1) {
    url = url + '&page=' + currentPage;
  }
  if (currentId) {
    url = url + '&id=' + currentId;
  }

  url = url + '&rel=' + searchCriteria.rel;


  return url;

};

var generateUrlCriteria = function() {

  var url = '?q=' + $('#q').val();

  var bounds = map.getBounds();
  var ne = bounds.getNorthEast();
  var sw = bounds.getSouthWest();

  url = url + '&nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6);

  f_np = $('#np').prop('checked') ? 'yes' : '';

  //Get value from inputs
  var bed = $('#bed').val();
  var bath = $('#bath').val();
  var smin = $('#smin').val();
  var smax = $('#smax').val();
  var ymin = $('#ymin').val();
  var ymax = $('#ymax').val();
  var lt = $('#lt').val();
  var pt = $('#pt').val();
  var np = $('#new-project').is(":checked") ? 'yes' : 'no';

  //if user select *pt=cdb*, then *np* MUST be = NO
  if (pt === 'cdb')
    np = 'no';

  //if user select *np=yes*, then *cdb* MUST be removed from *pt* param
  if (np === 'yes')
    pt = '';

  url = url + '&lt=' + lt;
  url = url + '&np=' + np;
  url = url + '&pmin=' + price_min + '&pmax=' + price_max;
  url = url + '&pt=' + pt;
  url = url + '&bed=' + bed;
  url = url + '&bath=' + bath;
  url = url + '&smin=' + smin;
  url = url + '&smax=' + smax;
  url = url + '&ymin=' + ymin;
  url = url + '&ymax=' + ymax;
  url = url + '&sort=' + sortAttr;

  if (sortAsc) {
    url = url + '&dir=asc';
  } else {
    url = url + '&dir=desc';
  }

  if (currentPage > 1) {
    url = url + '&page=' + currentPage;
  }
  if (currentId) {
    url = url + '&id=' + currentId;
  }
  if (domain) {
    url = url + '&rel=' + domain;
  }

  return url;

};

var currentUrl;

var applySearchCriteria = function() {
  if (firstSearch) {
    updateMapResult(firstSearch);
    firstSearch = false;
  } else {
    updateMapResult(firstSearch);
  }
  showCurrent = false;
};

var updateMapResult = function(isFirstSearch) {

  currentUrl = generateSearchUrl();

  $.get('/rest/search/pro/map' + currentUrl, function(searchResult) {
    console.log(searchResult);
    loadPropertiesOnMap(searchResult /*data.properties*/ );
  }).fail(function() {
    $.getJSON('/rest/suggest', {
      term: $('#q').val()
    }, function(data) {
      console.log(data[0]);
      map.setCenter(new google.maps.LatLng(data[0].lat, data[0].lng));
      if (isFirstSearch) {
        alert(noResult);
      }
    });

  });;
};

var propertyMarkers = [];
var markerCluster = null;
//var markerInfos = [];
var currentInfo = null;
var currentInfoXhr;
var currentMarker = null;

var loadPropertiesOnMap = function(searchResult) {

  if (propertyMarkers.length) {

    for (var i = 0; i < propertyMarkers.length; i++) {
      var marker = propertyMarkers[i];
      marker.setMap(null);
    }
    propertyMarkers = [];
    markerInfos = [];
  }

  if (markerCluster) {
    markerCluster.clearMarkers();
  }

  currentMarker = null;

  if (searchResult.properties.length > 0) {
    for (var i = 0; i < searchResult.properties.length; i++) {
      var pdata = searchResult.properties[i];

      var markerIcon;

      if (pdata.np === true) {
        if (pdata.lclass === 'featured') {
          markerIcon = '/img/maps/pin/pin-project-featured.png';
        } else if (pdata.lclass === 'exclusive') {
          markerIcon = '/img/maps/pin/pin-project-exclusive.png';
        } else {
          markerIcon = '/img/maps/pin/pin-project-standard.png';
        }
      } else {
        if (pdata.ptype === 'cdb') {
          markerIcon = '/img/maps/pin/pin-condo.png';
        } else if (pdata.lclass === 'featured') {
          if (pdata.ltype === 'rent') {
            markerIcon = '/img/maps/pin/pin-rent-featured.png';
          } else {
            markerIcon = '/img/maps/pin/pin-sale-featured.png';
          }
        } else if (pdata.lclass === 'exclusive') {
          if (pdata.ltype === 'rent') {
            markerIcon = '/img/maps/pin/pin-rent-exclusive.png';
          } else {
            markerIcon = '/img/maps/pin/pin-sale-exclusive.png';
          }
        } else {
          if (pdata.ltype === 'rent') {
            markerIcon = '/img/maps/pin/pin-rent-standard.png';
          } else {
            markerIcon = '/img/maps/pin/pin-sale-standard.png';
          }
        }
      }

      var markerOptions = {
        position: {
          lat: parseFloat(pdata.lat),
          lng: parseFloat(pdata.lng)
        },
        icon: {
          url: markerIcon,
          size: new google.maps.Size(40, 40),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(20, 0)
        }
      };

      if (parseInt(pdata.id) === parseInt(currentId)) {
        //markerOptions['animation'] = google.maps.Animation.BOUNCE;
      }

      var marker = new google.maps.Marker(markerOptions);

      marker.data = pdata;
      marker.data.type = searchResult.type;
      marker.setDraggable(false);

      //var infowindow =

      marker.infoIndex = propertyMarkers.length;

      marker.addListener('click', function() {
        window.location = '/property/' + this.data.id;
      });

      if (parseInt(pdata.id) === parseInt(currentId)) {
        currentMarker = marker;
      }

      propertyMarkers.push(marker);
    }

    //console.log('currentId', currentId);

    if (markerCluster) {
      markerCluster.addMarkers(propertyMarkers);
    } else {
      var clusterStyles = [{
        textColor: 'white',
        url: '/img/map/m1.png',
        height: 40,
        width: 40
      },
        {
          textColor: 'white',
          url: '/img/map/m1.png',
          height: 40,
          width: 40
        },
        {
          textColor: 'white',
          url: '/img/map/m1.png',
          height: 40,
          width: 40
        }
      ];

      markerCluster = new MarkerClusterer(map, propertyMarkers, {
        imagePath: '/img/map/m',
        styles: clusterStyles
      });

      google.maps.event.addListener(markerCluster, 'clusterclick', displayClusterInfo);
    }
  }

  if (currentMarker !== null) {
    console.log('currentMarker', currentMarker);
    openMakerInfo(currentMarker);
  } else if (currentId > 0) {
    console.log('should show current property');
    openMakerInfoById(currentId);
    var coor = JSON.parse(searchResult.data);
    if(coor){
      map.setCenter(new google.maps.LatLng(coor.lat, coor.lng));
    }
  }

  if (showCurrent) {
    //console.log('Should show current item');
  }

  //console.log(currentPoiData);

  if (domain === 'place' && currentId) {

    var poimg = getImageUrl(currentPoiData.type);

    if (poimg) {

      if (currentPoiMarker === null) {
        var image = {
          url: poimg,
          size: new google.maps.Size(32, 37),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(16, 37)
        };

        currentPoiMarker = new google.maps.Marker({
          position: {
            lat: parseFloat(currentPoiData.lat),
            lng: parseFloat(currentPoiData.lng)
          },
          draggable: false,
          title: currentPoiData.title,
          animation: google.maps.Animation.BOUNCE,
          icon: image,
          map: map
        });
      } else {
        console.log('currentPoiMarker already exist');
      }
    }
  }
};

var currentMarker;

var openMakerInfoById = function(id) {
  var marker;
  //console.log('openMakerInfoById');
  currentMarker = null;
  for (var i = 0; i < propertyMarkers.length; i++) {
    var marker = propertyMarkers[i];

    if (marker.data.id === id) {
      currentMarker = marker;
      //console.log('marker', marker);
      openMakerInfo(marker);
    }
  }

  if (currentMarker === null) {
    //console.log('marker not found');
    //applySearchCriteria();
    createMarkerForCurrentData();
  }
};

var createMarkerForCurrentData = function() {};

var openMakerInfo = function(marker) {

  //console.log('openMakerInfo');

  if (currentInfo) {
    currentInfo.close();
  }
  if (currentInfoXhr) {
    currentInfoXhr.abort();
  }

  var option = {
    disableAutoPan: false,
    maxWidth: 0,
    pixelOffset: new google.maps.Size(-125, -80),
    zIndex: null,
    closeBoxURL: "",
    infoBoxClearance: new google.maps.Size(1, 1),
    isHidden: false,
    pane: "floatPane",
    enableEventPropagation: false
  };

  currentInfo = new InfoBox(option);

  //currentInfo = new google.maps.InfoWindow();

  //console.log(marker.data);

  var rand = Math.random() * (1000000) + 1000000;

  currentInfoXhr = $.get('/rest/property/map-info/' + marker.data.id + '?type=' + marker.data.ptype + '&t=' + rand, markerInfoLoader.bind(marker));
};

var markerInfoLoader = function(data) {

  console.log(this.data);

  currentInfo.setContent(data);

  if (this.data) {
    currentInfo.setPosition({
      lat: parseFloat(this.data.lat),
      lng: parseFloat(this.data.lng)
    });
  }

  currentInfo.open(map, this);

/*if (this.map !== null) {
    console.log('currentInfo - 1', currentInfo);
    currentInfo.open(map, this);
    currentInfoXhr = null;
} else if (this.data) {


}*/
};

var loadPropertiesOnList = function(result) {

  $('.search-results').html('');

  listedProperties = [];

  searchResult = result;

  if (searchResult.properties.data.length > 0) {
    $('.no-search-results').hide();
    listedProperties = searchResult.properties.data;
    reloadPropertylist();
  } else {
    $('.no-search-results').show();
    $('.property-count').html('No property found');
  }
};

var reloadPropertylist = function() {

  $('.search-results').html('');

  for (var i = 0; i < listedProperties.length; i++) {
    var pdata = listedProperties[i];
    pdata.rank = i;
    var prop = new TFHProperty(pdata, searchResult.type);
    $('.search-results').append(prop.getTemplate(searchResult.favorites));
  //prop.sortData();
  }

  $('.favorite-btn').on('click', function(event) {

    event.stopPropagation();
    var elt = $(this);
    var pid = elt.data('id');

    if (pid) {
      $.get('/rest/user/favorite-property/' + pid + '?t=' + searchResult.type, function(data, status, xhr) {
        if (data.status === 'OK') {
          if (data.result === 'ADDED') {
            elt.find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
          } else {
            elt.find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
          }
        } else {

        }
      });
    }
  });
};

var displayClusterInfo = function(cluster) {
  var markers = cluster.getMarkers();

  if (markers.length > 0) {
    var marker = markers[0];

    if (lastClusterCount === markers.length) {
      console.log('should do som');
      $('.cluster-info').show();
    } else {
      lastClusterCount = markers.length;
    }

  }
};


var sortby = function(attr, target) {

  if (sortAttr === attr) {
    sortAsc = !sortAsc;
  } else {
    sortAttr = attr;
    sortAsc = true;
  }

  $('.nav-filter li').removeClass('active');
  $('#sort-' + target).addClass('active');

  updateListResult();
};

var applySort = function() {

  /*var sorted = $('.search-results div.property-item').sort(function (a, b) {
   var contentA = parseInt($(a).data(sortAttr));
   var contentB = parseInt($(b).data(sortAttr));

   if (sortAsc) {
   return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
   } else {
   return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
   }

   });
   $('.search-results').html('');
   $('.search-results').append(sorted);
   ////console.log('applySort');*/

  reloadPropertylist();
};

var currentPropertyData;

var showPropertyPreview = function(data) {

  currentPropertyData = data;

  $('#property-preview .modal-body').html('');
  $('#property-preview').modal('show');

/*$('#property-title').html(data.property_name);*/
};

var previewMap;
var previewMarker;
var previewPoiMarker;

var showStreetView = function(lat, lng) {
  previewMap = null;
  previewMap = new google.maps.Map(
    document.getElementById('property-map'), {
      center: {
        lat: lat,
        lng: lng
      },
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

  previewMarker = new google.maps.Marker({
    position: {
      lat: lat,
      lng: lng
    },
    draggable: true,
    icon: '/img/maps/pin/pin-sale-standard.png',
    map: previewMap
  });

  $('.poi-list').on('change', function() {

    var optionSelected = $("option:selected", this);

    var options = $(optionSelected[0]).data();

    //console.log(options);

    if (options.lat) {
      var poimg = getImageUrl(options.type);
      var location = {
        lat: parseFloat(options.lat),
        lng: parseFloat(options.lng)
      };

      var image = {
        url: poimg,
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(16, 37)
      };

      if (previewPoiMarker) {
        previewPoiMarker.setMap(null);
        previewPoiMarker = null;
      }

      previewPoiMarker = new google.maps.Marker({
        position: location,
        draggable: true,
        icon: image,
        map: previewMap
      });

      var bounds = new google.maps.LatLngBounds();
      bounds.extend({
        lat: lat,
        lng: lng
      });
      bounds.extend(location);

      previewMap.fitBounds(bounds);
    } else {
      previewMap.panTo({
        lat: lat,
        lng: lng
      });
    }

  });
};

var showFullProperty = function() {
  console.log(currentPropertyData);
  window.location = '/property/' + currentPropertyData.id;
};
;
