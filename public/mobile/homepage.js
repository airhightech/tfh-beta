var lt = 'rent';
var pt = '';
var np = '';
var scope = 'listing';
(function() {
  'use strict';
  angular
    .module('myApp', [])
    .controller('myController', myController);

  myController.$inject = ['$scope'];

  function myController($scope) {
    $scope.search = function() {
      var keyword = $('input#search').val();
      if (!(keyword.length > 0)) {
        return;
      }
      var id = $('.big-button.selected').attr('id');
      switch (id) {
        case 'rent':
          window.location = '/search?q=' + keyword + '&lt=rent&id=' + currentId + "&rel=" + rel;
          break;
        case 'buy':
          window.location = '/search?q=' + keyword + '&lt=sale&id=' + currentId + "&rel=" + rel;
          break;
        case 'condo':
          window.location = '/search?q=' + keyword + '&pt=cdb&id=' + currentId + "&rel=" + rel;
          break;
        case 'new':
          window.location = '/search?q=' + keyword + '&np=yes&id=' + currentId + "&rel=" + rel;
          break;
      }
    };
    angular.element(document).ready(function() {
      var h = $('.big-button').height();
      $('.big-button').css({
        'width': h + 4
      });
      var tw = $('#inner-main').width();
      $('.spacers').css({
        'width': (tw - (2 * (h + 4)))
      });
    });

    $scope.selectButton = function(buttonId) {
      $('.big-button').removeClass('selected');
      $('#' + buttonId).addClass('selected');
      switch (buttonId) {
        case 'rent':
          lt = 'rent';
          pt = '';
          np = '';
          scope = 'listing';
          break;
        case 'buy':
          lt = 'sale';
          pt = '';
          np = '';
          scope = 'listing';
          break;
        case 'condo':
          pt = 'cdb';
          lt = '';
          np = '';
          scope = 'cdb';
          break;
        case 'new':
          np = 'yes'
          lt = '';
          pt = '';
          scope = 'project';
          break;
      }
    };
    $scope.selectedTab = 0; //default 0
    $scope.selectTab = function(tab) {
      switch (tab) {
        case 0:
          $scope.selectedTab = 1;
          $('.home-tab').removeClass('selected');
          $('#tab0').addClass('selected');
          setTimeout(function() {
            $('#content0').css('visibility', 'visible');
          }, 10);
          loadNextBanner('home');
          break;
        case 1:
          $scope.selectedTab = 2;
          $('.home-tab').removeClass('selected');
          $('#tab1').addClass('selected');
          setTimeout(function() {
            $('#content1').css('visibility', 'visible');
          }, 10);
          loadNextBanner('list-rent');
          break;
        case 2:
          $scope.selectedTab = 3;
          $('.home-tab').removeClass('selected');
          $('#tab2').addClass('selected');
          setTimeout(function() {
            $('#content2').css('visibility', 'visible');
          }, 10);
          loadNextBanner('list-sale');
          break;
      }
    };


  }
})();
