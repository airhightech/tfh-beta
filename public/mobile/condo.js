(function() {
  'use strict';

  angular
    //angular main modules
    .module('myApp', ['ngMap', 'ui.bootstrap', 'rzModule', 'chart.js', 'ngAnimate'])
    //controller
    .controller('myController', myController)
    //API service
    .service('condoApi', condoApi)
    //filter for making youtube url trusted
    .filter('trustUrl', trustUrl)
    //For giving custom background image to a div dynamically
    .directive('backImg', backImg);

  //dependency injections
  myController.$inject = ['$scope', 'NgMap', '$timeout', 'condoApi', '$location', '$anchorScroll', '$uibModal'];
  condoApi.$inject = ['$q', '$http'];
  trustUrl.$inject = ['$sce'];

  //the controller
  function myController($scope, NgMap, $timeout, condoApi, $location, $anchorScroll, $uibModal) {
    //variable declerations and initializations
    $scope.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyC9efZV5sak_Geyb6y7UMSfAaNt7fFUcfM';
    var ctrl = this;
    $scope.parseInt = parseInt;
    $scope.condoId = 1;
    $scope.commentsId = 1;
    $scope.forRent = 0;
    $scope.forSale = 0;
    $scope.replyActive = false;
    $scope.facilityStars = 3;
    $scope.likes = 33;
    $scope.dislikes = 61;
    $scope.i = 0;
    $scope.totalItems = 0;
    //get condo details from API service and apply the scope
    condoApi.getCondoDetails($scope.condoId).then(function(condoDetails) {
      $scope.condo = condoDetails.condo;
      $scope.condo.comments.currentPage = 1;
      $scope.condo.comments.itemsPerPage = 3;
      //generate new location without the lat lng values (only name)
      var newLoc = angular.copy($scope.condo.location);
      delete newLoc.lat;
      delete newLoc.lng;
      $scope.locations = newLoc;
      //initialize map
      NgMap.getMap().then(function(map) {
        ctrl.map = map;
        var lanLng = {
          lat: $scope.condo.location.lat,
          lng: $scope.condo.location.lng
        };
        ctrl.map.setCenter(lanLng);
      });
      $scope.condo.comments.totalItems = $scope.condo.comments.commentList.length;
      //initialize and/or update slider
      $scope.slider.loan = {
        value: parseInt($scope.condo.principlePrice * 0.90),
        options: {
          floor: parseInt($scope.condo.principlePrice * 0.05),
          ceil: $scope.condo.principlePrice,
          step: 100000,
          translate: function(value) {
            return parseInt((value / $scope.condo.principlePrice) * 100) + '%';
          }
        }
      }
      //count rent and sale count
      _.forEach($scope.condo.listings, function(listing) {
        if (listing.listing_type == 'rent') {
          $scope.forRent++;
        } else if (listing.listing_type == 'sale') {
          $scope.forSale++;
        }
      });
    });
    //go to the location on map when a place is selected from the dropdown
    $scope.goToLocation = function(location) {
      if (location.length > 0) {
        var latLng = location.split(' ');
        var locObj = {
          lat: Number(latLng[0]),
          lng: Number(latLng[1])
        }
        ctrl.map.setCenter(locObj);
        ctrl.map.setZoom(16);
      }
    };
    $scope.listingType = 'rent';
    $scope.condo = {
      name: 'UNIO RAMA 2',
      ranking: 4,
      principlePrice: 11000000,
      salesPrice: 3650000,
      hours: '10.00 - 18.00',
      built: 2012,
      tower: 1,
      floor: 20
    };
    //http://angular-slider.github.io/angularjs-slider/
    //slider initialization
    $scope.slider = {
      loan: {
        value: parseInt($scope.condo.principlePrice * 0.90),
        options: {
          floor: parseInt($scope.condo.principlePrice * 0.05),
          ceil: $scope.condo.principlePrice,
          step: 100000,
          translate: function(value) {
            return parseInt((value / $scope.condo.principlePrice) * 100) + '%';
          }
        }
      },
      rate: {
        value: 0.06,
        options: {
          floor: 0.01,
          ceil: 0.10,
          step: 0.01,
          precision: 2,
          translate: function(value) {
            return parseInt(100 * value) + '%';
          }
        }
      },
      term: {
        value: 30,
        options: {
          floor: 1,
          ceil: 30,
          step: 1,
          translate: function(value) {
            return value + ' year' + (value === 1 ? '' : 's');
          }
        }
      }
    }
    $scope.reply = {
      id: 0,
      likes: 0,
      dislikes: 0,
      review: '',
      created_at: '',
      ratings: {
        facility: 0,
        design: 0,
        accessibility: 0,
        management: 0,
        security: 0,
        amenities: 0
      },
      attachments: {
        first: null,
        second: null,
        third: null
      },
      agree: false
    }
    //modal contoller
    var ModalInstanceCtrl = function($scope, $uibModalInstance, data) {
      $scope.data = data;
      $scope.close = function( /*result*/ ) {
        $uibModalInstance.close($scope.data);
      };
    };

    //Open modal
    $scope.open = function(data) {
      $scope.data = data;

      var modalInstance = $uibModal.open({
        template: '<div class="modal-body" style="padding:0px">' +
          '<div class="alert alert-{{data.mode}}" style="margin-bottom:0px">' +
          '<button type="button" class="close" data-ng-click="close()">' +
          '<span class="glyphicon glyphicon-remove-circle"></span>' +
          '</button>' +
          '<strong>{{data.boldTextTitle}}</strong> {{data.textAlert}}' +
          '</div>' +
          '</div>',
        controller: ModalInstanceCtrl,
        backdrop: true,
        keyboard: true,
        backdropClick: true,
        size: 'sm',
        resolve: {
          data: function() {
            return $scope.data;
          }
        }
      });

      return modalInstance;
    }
    //send review
    $scope.submitReply = function() {
      if ($scope.reply.agree === true) {
        if (userIsOnline) {
          $scope.condo.comments.totalItems++;
          $scope.condo.comments.currentPage = Math.ceil($scope.condo.comments.totalItems / $scope.condo.comments.itemsPerPage);
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1; //January is 0!
          var hour = today.getHours();
          var min = today.getMinutes();
          var sec = today.getSeconds();
          var yyyy = today.getFullYear();
          if (dd < 10) {
            dd = '0' + dd
          }
          if (mm < 10) {
            mm = '0' + mm
          }
          if (hour < 10) {
            hour = '0' + hour
          }
          if (min < 10) {
            min = '0' + min
          }
          if (sec < 10) {
            sec = '0' + sec
          }
          var date = yyyy + '-' + mm + '-' + dd + ' ' + hour + ':' + min + ':' + sec;
          $scope.reply.created_at = date;
          $scope.reply.id = $scope.condo.comments.totalItems;
          var reply = angular.copy($scope.reply);
          //send review to server
          condoApi.postComment($scope.condoId, reply.review, reply.ratings, reply.attachments).then(function() {
            $scope.scrollTo('comments-end');
            $scope.reply = {
              id: 0,
              likes: 0,
              dislikes: 0,
              review: '',
              created_at: '',
              ratings: {
                facility: 0,
                design: 0,
                accessibility: 0,
                management: 0,
                security: 0,
                amenities: 0
              },
              attachments: {
                first: null,
                second: null,
                third: null
              },
              agree: false
            }
            $('#reply-ratings *').removeAttr('checked');
            $scope.condo.comments.commentList.push(reply);
          });
        } else {
          //display error
          var data = {
            boldTextTitle: '',
            textAlert: onOfflineMessage,
            mode: 'info'
          }
          var modal = $scope.open(data);
          $timeout(function() {
            modal.close();
          }, 3000);
        }
      } else {
        //display error
        var data = {
          boldTextTitle: '',
          textAlert: 'You must agree with the Terms and Conditions',
          mode: 'info'
        }
        var modal = $scope.open(data);
        $timeout(function() {
          modal.close();
        }, 3000);
      }
    }
    $scope.rotate = true;
    $scope.maxSize = 6;
    //force refresh slider
    $scope.refreshSlider = function() {
      $timeout(function() {
        $scope.$broadcast('rzSliderForceRender');
      });
    };
    //number functions
    $scope.getNumber = function(num) {
      return new Array(num);
    };
    $scope.beautify = function(price) {
      return parseInt(price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    $scope.formatPrice = function(num) {
      if (num >= 1000000) {
        return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
      }
      if (num >= 1000) {
        return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
      }
      return num;
    }; //}};//Don't delete this line, this is visual code coloring bug hack-fix.

    //Watch for the value changes in slider to dynamically show monthly rent
    $scope.totalAmount = 0;
    $scope.$watch(function() {
      return $scope.slider.loan.value + $scope.slider.rate.value + $scope.slider.term.value;
    }, function(newValue, oldValue) {
      //Mortgage calculation logic
      var p = $scope.condo.principlePrice;
      var r = $scope.slider.rate.value;
      var n = $scope.slider.term.value;
      var l = $scope.slider.loan.value;
      //Total amount
      $scope.totalAmount = parseInt(l * Math.pow(1 + r, n));
      //Monthly payment
      $scope.monthlyPayment = parseInt(((r * l * Math.pow(1 + r, n)) / (Math.pow(1 + r, n) - 1)) / 12);
      //Update pie chart
      $scope.data = [parseInt($scope.totalAmount - l), l]; ///
    });
    //initialize doughtnut chart
    $scope.labels = ["Total Amount", "Loan Amount"];
    $scope.data = [$scope.totalAmount, $scope.slider.loan.value];
    $scope.backgroundColor = ['#36bedc', '#343f41'];

    //scroll to div with specific id
    $scope.scrollTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    };

    //scroll functions for bottom listing bar
    $scope.scrollLeft = function() {
      var leftPos = $('#numWrap').scrollLeft();
      $("#numWrap").animate({
        scrollLeft: leftPos - 342
      }, 600);
    };

    $scope.scrollRight = function() {
      var leftPos = $('#numWrap').scrollLeft();
      $("#numWrap").animate({
        scrollLeft: leftPos + 342
      }, 600);
    };

    $scope.search = {
      type: 'list',
      index: 0,
      size: 2,
      img: [
        'http://demo.thaifullhouse.com/image/57eca77954ba6-57eca779.jpg',
        'http://demo.thaifullhouse.com/thumb/320x300/57ee03a56bac7-57ee03a5.jpg'
      ],
      favorite: false,
      favorite2: false,
      favorite3: false
    };

    //handlers for uploading image
    function handleImage(e) {
      var reader = new FileReader();
      reader.onload = function(event) {
        //$('#uploadedImg').attr('src', event.target.result);
        $scope.reply.attachments.first = event.target.result;
      }
      reader.readAsDataURL(e.target.files[0]);
    }

    function handleImage2(e) {
      var reader = new FileReader();
      reader.onload = function(event) {
        $('#uploadedImg2').attr('src', event.target.result);
        $scope.reply.attachments.second = event.target.result;
      }
      reader.readAsDataURL(e.target.files[0]);

    }

    function handleImage3(e) {
      var reader = new FileReader();
      reader.onload = function(event) {
        $('#uploadedImg3').attr('src', event.target.result);
        $scope.reply.attachments.third = event.target.result;
      }
      reader.readAsDataURL(e.target.files[0]);
    }

    angular.element(document).ready(function() {
      //$scope.refreshSlider();
      if (null != document.getElementById('photoInput')) {
        //add listeners for image upload handlers
        var imageLoader = document.getElementById('photoInput');
        //only 1 photo is allowed on mobile
        imageLoader.addEventListener('change', handleImage, false);
      /*var imageLoader = document.getElementById('filePhoto2');
      imageLoader.addEventListener('change', handleImage2, false);
      var imageLoader = document.getElementById('filePhoto3');
      imageLoader.addEventListener('change', handleImage3, false);*/
      }
    });
  }

  //Makes API calls
  function condoApi($q, $http) {
    //get condo details
    function _getCondoDetails(id) {
      var d = $q.defer();
      $http.get('http://demo.thaifullhouse.com/rest/condo/details/' + id).then(function(response) {
        return d.resolve(response.data);
      });
      return d.promise;
    }
    //post comment
    function _postComment(condo_id, review, ratings, images) {
      var d = $q.defer();
      var data = {
        condo_id: condo_id,
        review: review,
        ratings: ratings,
        images: images,
        user: currentUserId
      }
      $http.post('http://demo.thaifullhouse.com/rest/condo/review', data).then(function(response) {
        return d.resolve(response.data);
      });
      return d.promise;
    }
    return {
      getCondoDetails: _getCondoDetails,
      postComment: _postComment
    };

  }

  //filter for making youtube url trusted
  function trustUrl($sce) {
    return function(url) {
      return $sce.trustAsResourceUrl(url);
    };
  }

  //For giving custom background image to a div dynamically
  function backImg() {
    return function(scope, element, attrs) {
      attrs.$observe('backImg', function(value) {
        element.css({
          'background-image': 'linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url(' + value + ')',
          'background-position': 'center',
          'background-size': 'cover'
        });
      });
    };
  }
})();
