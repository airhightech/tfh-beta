<?php

Route::group(['namespace' => 'Rest'], function () {

    Route::post('/subscribe', 'SubscriptionController@register');
    Route::get('/unsubscribe', 'SubscriptionController@unsubscribe');

    Route::post('/auth/login', 'AuthController@login');
    Route::post('/auth/register', 'AuthController@register');
    Route::post('/auth/forgot-password', 'AuthController@forgotPassword');
    Route::post('/auth/update-password', 'AuthController@updatePassword');
    Route::post('/auth/validate-email', 'AuthController@validateEmail');

    Route::get('/user/saved-search', 'UserController@getSavedSearch');
    Route::get('/user/delete/{id}', 'UserController@deleteUser');

    Route::get('/search/save', 'SearchController@getSaved');
    Route::post('/search/save', 'SearchController@addSaved');

    Route::get('/suggest', 'SearchController@getSuggestion');
    Route::get('/suggest/condo', 'PropertyController@suggestCondo');

    Route::get('/location/district/{id}', 'LocationController@getDistrict');
    Route::get('/location/area/{id}', 'LocationController@getArea');
    Route::get('/location/sync/{id}', 'LocationController@sync');
    Route::get('/location/map/{id}', 'LocationController@updateMap');

    // Registration

    Route::post('/register/owner', 'RegistrationController@owner');
    Route::post('/register/agent', 'RegistrationController@agent');
    Route::post('/register/company', 'RegistrationController@company');
    Route::post('/register/developer', 'RegistrationController@developer');

    // Property

    Route::post('/property/info', 'PropertyController@postInfo');
    Route::post('/property/create', 'PropertyController@postCreate');
    Route::post('/property/details-info', 'PropertyController@postDetails');
    Route::post('/property/other-details', 'PropertyController@postOtherDetails');
    Route::post('/property/upload', 'PropertyController@postUpload');

    Route::post('/property/add-room/{id}', 'PropertyController@postRoom');
    Route::get('/property/delete-room/{id}/{room}', 'PropertyController@deleteRoom');
    Route::post('/property/update-room/{id}/{room}', 'PropertyController@updateRoom');


    Route::post('/property/update-facilities', 'PropertyController@updateFacilities');
    Route::post('/property/update-features', 'PropertyController@updateFeatures');
    Route::post('/property/update-optional-features', 'PropertyController@updateOptionalFeatures');

    Route::post('/property/review-upload', 'PropertyController@uploadReviewImage');
    Route::post('/property/review', 'PropertyController@updateReviewDetails');

    Route::get('/property/delete-image/{id}', 'PropertyController@deleteImage');
    Route::get('/property/delete-image-review/{id}', 'PropertyController@deleteImageReview');

    Route::get('/property/map-info/{id}', 'PropertyController@getMapInfo');
    Route::get('/property/toggle/{id}', 'PropertyController@toggle');

    Route::get('/listing/form/{id}', 'PropertyController@listingData');
    Route::get('/listing/delete-form/{id}', 'PropertyController@deleteForm');

    Route::post('/property/update-listing', 'PropertyController@updateListingData');
    Route::post('/property/delete-listing', 'PropertyController@deleteProperty');
    Route::post('/property/nearby-info', 'PropertyController@updateNearbyTransportation');

    Route::post('/profile/update', 'ProfileController@update');
    Route::post('/profile/upload-cover', 'ProfileController@uploadCover');
    Route::post('/profile/upload-photo', 'ProfileController@uploadPhoto');

    Route::post('/message/send', 'MessageController@send');
    Route::post('/message/reply', 'MessageController@reply');
    Route::get('/replies/{id}', 'MessageController@getReplies');

    Route::get('/user/favorite-property/{id}', 'PropertyController@favoriteProperty');

    Route::post('/listing/create', 'ListingController@postCreate');
    Route::post('/listing/details', 'ListingController@postDetails');

    Route::post('/listing/featured-upgrade', 'ListingController@upgradeFeatured');
    Route::post('/listing/exclusive-upgrade', 'ListingController@upgradeExclusive');

    //

    Route::get('/search/pro/map', 'SearchController@searchMap');
    Route::get('/search/pro/list', 'SearchController@searchList');

    Route::post('/province/create', 'LocationController@createProvince');
    Route::post('/district/create', 'LocationController@createDistrict');
    Route::post('/area/create', 'LocationController@createArea');

    Route::post('/province/update', 'LocationController@updateProvince');
    Route::post('/district/update', 'LocationController@updateDistrict');
    Route::post('/area/update', 'LocationController@updateArea');

    Route::get('/payment/token/{pid}', 'PaymentController@getPaysbuyToken');
    Route::post('/paysbuy/callback', 'PaymentController@handlePaysbuyCallback');
    Route::post('/package/buy', 'PaymentController@buyFromBank');

    Route::post('/review/post', 'ReviewController@postUserReview');
    Route::post('/review/like', 'ReviewController@postUserLike');

    Route::post('/agent/review', 'ReviewController@postAgentReview');


    Route::get('/banner', 'BannerController@get');

    Route::post('/notice/create', 'MessageController@createNotice');
    Route::post('/notice/update', 'MessageController@updateNotice');

    Route::post('/place/update', 'PlaceController@update');
    Route::post('/place/delete', 'PlaceController@delete');

});
