<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Route::group(['prefix' => 'member', 'namespace' => 'Member', 'middleware' => 'auth.customer'], function () {
    
    Route::get('/dashboard', 'HomeController@index');
    
    // Account management
    
    Route::get('/account', 'AccountController@index');
    
    // Listing management
    
    Route::get('/listing', 'ListingController@index');
    Route::get('/listing/select-type', 'ListingController@type');
    Route::get('/listing/details/{id}', 'ListingController@details');
    Route::get('/listing/preview/{id}', 'ListingController@preview');
    
    Route::get('/property/exclusive-upgrade/{id}', 'ListingController@getExclusiveUpgrade');
    Route::get('/property/featured-upgrade/{id}', 'ListingController@getFeaturedUpgrade');
    
    // Property management
    
    Route::get('/property/info/{id}', 'PropertyController@info');
    Route::get('/property/details/{id}', 'PropertyController@details');
    Route::get('/property/media/{id}', 'PropertyController@media');
    Route::get('/posting/media/{id}', 'PropertyController@getPostingMedia');
    
    // Purchase management
    
    Route::get('/purchase', 'PurchaseController@index');
    Route::post('purchase', 'PurchaseController@index');
    Route::get('/purchase/buy-exclusive', 'PurchaseController@getExclusive');
    Route::get('/purchase/buy-featured', 'PurchaseController@getFeatured');
    Route::get('/purchase/contact-admin', 'PurchaseController@contactAdmin');
    
    // Message management
    
    Route::get('/message', 'MessageController@index');
    
    // Notice management
    
    Route::get('/notice', 'NoticeController@index');
    
    
    // Notice management
    
    Route::get('/favorite', 'FavoriteController@index');
    
});