<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Route::group(['namespace' => 'Visitor'], function () {
    
    Route::get('/', 'HomeController@index');    
    
    // Authentication
    
    Route::get('/auth/login', 'AuthController@login');
    Route::get('/auth/logout', 'AuthController@logout');
    Route::get('/auth/register', 'AuthController@register');
    Route::get('/auth/forgot-password', 'AuthController@forgotPassword');
    Route::get('/auth/update-password', 'AuthController@updatePassword');
    Route::get('/auth/check-inbox', 'AuthController@checkInbox');
    Route::get('/auth/validate-email', 'AuthController@validateEmail');
    Route::get('/auth/request-link', 'AuthController@requestEmail');
    
    Route::post('/auth/forgot-password', 'AuthController@createRequestPassword');
    
    Route::get('/auth/register/type', 'RegistrationController@type');
    
    Route::get('/auth/register/owner', 'RegistrationController@owner');
    Route::get('/auth/register/agent', 'RegistrationController@agent');
    Route::get('/auth/register/company', 'RegistrationController@company');
    Route::get('/auth/register/developer', 'RegistrationController@developer');
    
    Route::get('/auth/facebook', 'AuthController@facebook');
    Route::get('/auth/twitter', 'AuthController@twitter');
    Route::get('/auth/google', 'AuthController@google');
    
    Route::get('/facebook-callback', 'AuthController@facebookCallback');
    Route::get('/twitter-callback', 'AuthController@twitterCallback');
    Route::get('/google-callback', 'AuthController@googleCallback');
    
    
    // Search
    
    Route::get('/search', 'PropertyController@search');
    Route::get('/search-list', 'PropertyController@searchList');
    
    // Properties
    
    Route::get('/property/{id}', 'PropertyController@details');
    Route::get('/preview/{id}', 'PropertyController@preview');    
    
    // Agent
    
    Route::get('/agent/search', 'AgentController@search');
    Route::get('/agent/{id}', 'AgentController@details'); // Regular expression defined in RouteServiceProvider
    
    
    Route::get('/page/{sku}', 'PageController@index');
    
    
    
});