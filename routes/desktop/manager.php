<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Route::group(['prefix' => 'manager', 'namespace' => 'Manager', 'middleware' => 'auth.manager'], function () {

    Route::get('/dashboard', 'HomeController@index');

    // Account management

    Route::get('/visitor/list', 'AccountController@getVisitors');
    Route::get('/owner/list', 'AccountController@getOwners');
    Route::get('/agent/list', 'AccountController@getAgents');
    Route::get('/developer/list', 'AccountController@getDevelopers');
    Route::get('/developer/create', 'AccountController@createDeveloper');
    Route::get('/account/profile/{id}', 'AccountController@getProfile');
    Route::post('/account/update', 'AccountController@updateProfile');

    // Property managers

    $buildings = ['condo' => '', 'new-condo' => 'new'];

    foreach($buildings as $building => $prefix) {
        Route::get('/'.$building.'/list', 'CondoController@'.$prefix.'index');
        Route::get('/'.$building.'/create', 'CondoController@'.$prefix.'create');
        Route::get('/'.$building.'/info/{id}', 'CondoController@'.$prefix.'info');
        Route::get('/'.$building.'/details/{id}', 'CondoController@'.$prefix.'details');
        Route::get('/'.$building.'/media/{id}', 'CondoController@'.$prefix.'media');
        Route::get('/'.$building.'/places/{id}', 'CondoController@'.$prefix.'places');
        Route::get('/'.$building.'/review/{id}', 'CondoController@'.$prefix.'review');
    }

    Route::get('/facility/list', 'FacilityController@index');
    Route::get('/facility/create', 'FacilityController@create');
    Route::get('/facility/details/{id}', 'FacilityController@details');
    Route::post('/facility/create', 'FacilityController@postCreate');
    Route::post('/facility/update', 'FacilityController@postUpdate');
    Route::post('/facility/order', 'FacilityController@order');

    Route::get('/feature/list', 'FeatureController@index');
    Route::get('/feature/create', 'FeatureController@create');
    Route::get('/feature/details/{id}', 'FeatureController@details');
    Route::post('/feature/create', 'FeatureController@postCreate');
    Route::post('/feature/update', 'FeatureController@postUpdate');
    Route::post('/feature/order', 'FeatureController@order');

    Route::get('/optional-feature/list', 'FeatureController@optionals');
    Route::get('/optional-feature/create', 'FeatureController@createOptional');
    Route::get('/optional-feature/details/{id}', 'FeatureController@detailsOptional');
    Route::get('/optional-feature/delete/{id}', 'FeatureController@deleteOptional');
    Route::post('/optional-feature/create', 'FeatureController@postCreateOptional');
    Route::post('/optional-feature/update', 'FeatureController@postUpdateOptional');
    Route::post('/optional-feature/order', 'FeatureController@order');

    // Project management

    $projects = ['condo', 'apartment', 'house', 'townhouse'];

    foreach($projects as $project) {
        Route::get('/project/'.$project.'/list', 'ProjectController@'.$project.'');
        Route::get('/project/'.$project.'/create', 'ProjectController@'.$project.'Create');
        Route::get('/project/'.$project.'/info/{id}', 'ProjectController@'.$project.'Info');
        Route::get('/project/'.$project.'/details/{id}', 'ProjectController@'.$project.'Details');
        Route::get('/project/'.$project.'/review/{id}', 'ProjectController@'.$project.'Review');
        Route::get('/project/'.$project.'/places/{id}', 'ProjectController@'.$project.'Places');
        Route::get('/project/'.$project.'/media/{id}', 'ProjectController@'.$project.'Media');
    }

    Route::get('/project/list', 'ProjectController@all');

    // Member submited


    $listings = ['condo', 'apartment', 'house', 'townhouse'];

    foreach($listings as $listing) {
        Route::get('/listing/'.$listing.'/list', 'ListingController@'.$listing.'');
        /*Route::get('/listing/'.$listing.'/info/{id}', 'ListingController@'.$listing.'Info');
        Route::get('/listing/'.$listing.'/details/{id}', 'ListingController@'.$listing.'Details');
        Route::get('/listing/'.$listing.'/review/{id}', 'ListingController@'.$listing.'Review');
        Route::get('/listing/'.$listing.'/places/{id}', 'ListingController@'.$listing.'Places');
        Route::get('/listing/'.$listing.'/media/{id}', 'ListingController@'.$listing.'Media');*/
    }

    Route::get('/listing/all', 'ListingController@all');

    Route::get('/order/list', 'OrderController@index');
    Route::get('/order/details/{id}', 'OrderController@getDetails');
    Route::post('/order/update', 'OrderController@updateOrder');

    // Geographic data

    Route::get('/province/list', 'LocationController@index');
    Route::get('/province/edit/{id}', 'LocationController@editProvince');

    Route::get('/district/list/{id}', 'LocationController@getDistrictList');
    Route::get('/district/edit/{id}', 'LocationController@editDistrict');

    Route::get('/area/list/{id}', 'LocationController@getAreaList');
    Route::get('/area/edit/{id}', 'LocationController@editArea');

    Route::get('/place/index', 'PlaceController@index');
    Route::get('/place/list', 'PlaceController@getlist');
    Route::get('/place/entries', 'PlaceController@getEntries');
    Route::post('/place/create', 'PlaceController@create');
    Route::get('/place/delete', 'PlaceController@delete');
    Route::post('/place/update', 'PlaceController@update');

    Route::get('/place/form/{id}', 'PlaceController@getForm');
    Route::get('/place/delete-form/{id}', 'PlaceController@getDeleteForm');

    // Other data

    // Popup

    Route::get('/popup/list', 'PopupController@index');
    Route::get('/popup/create', 'PopupController@create');
    Route::get('/popup/edit/{id}', 'PopupController@edit');

    Route::post('/popup/create', 'PopupController@postCreate');
    Route::post('/popup/update', 'PopupController@postUpdate');


    Route::get('/banner/list', 'BannerController@index');

    Route::get('/banner/info/{id}', 'BannerController@getInfo');
    Route::get('/banner/image/{id}', 'BannerController@getImage');
    Route::get('/banner/create', 'BannerController@getCreate');

    Route::post('/banner/create', 'BannerController@postCreate');
    Route::post('/banner/update', 'BannerController@postUpdate');
    Route::post('/banner/upload', 'BannerController@postImage');

    Route::get('/package/list', 'PackageController@getList');
    Route::get('/package/create-featured', 'PackageController@createFeatured');
    Route::get('/package/create-exclusive', 'PackageController@createExclusive');

    Route::get('/package/edit-featured/{id}', 'PackageController@editFeatured');
    Route::get('/package/edit-exclusive/{id}', 'PackageController@editExclusive');

    Route::get('/package/delete/{id}', 'PackageController@confirmDeletePackage');
    Route::get('/package/do-delete/{id}', 'PackageController@delete');

    Route::post('/package/save-featured', 'PackageController@saveFeatured');
    Route::post('/package/save-exclusive', 'PackageController@saveExclusive');

    Route::post('/package/update-featured/{id}', 'PackageController@updateFeatured');
    Route::post('/package/update-exclusive/{id}', 'PackageController@updateExclusive');

    Route::get('/staff/list', 'StaffController@getList');
    Route::get('/staff/create', 'StaffController@getCreate');
    Route::post('/staff/create', 'StaffController@postCreate');
    
    Route::get('/staff/update/{id}', 'StaffController@getUpdate');
    Route::post('/staff/update/{id}', 'StaffController@postUpdate');
    
    Route::get('/staff/delete/{id}', 'StaffController@getDelete');
    Route::get('/staff/remove/{id}', 'StaffController@delete');

    Route::get('/setting/index', 'SettingController@index');

    Route::get('/setting/contact', 'SettingController@getContact');
    Route::post('/setting/contact', 'SettingController@updateContact');
    Route::post('/setting/footer', 'SettingController@updateFooter');

    Route::get('/setting/bank', 'SettingController@getBank');
    Route::get('/setting/add-bank', 'SettingController@getAddBank');
    Route::post('/setting/add-bank', 'SettingController@addBank');

    Route::post('/setting/update', 'SettingController@update');

    Route::get('/setting/edit-bank/{id}', 'SettingController@editBank');
    Route::post('/setting/update-bank/{id}', 'SettingController@updateBank');

    Route::get('/setting/delete-bank/{id}', 'SettingController@deleteBank');


    Route::get('/setting/image', 'SettingController@getImages');
    Route::post('/setting/image-upload', 'SettingController@uploadImage');
    Route::get('/setting/image-remove', 'SettingController@removeTranslatedImage');

    Route::get('/setting/purchase', 'SettingController@getPurchase');
    Route::post('/setting/purchase', 'SettingController@postPurchase');

    Route::get('/setting/popup', 'SettingController@getPopup');
    Route::post('/setting/popup', 'SettingController@postPopup');
    
    Route::get('/setting/icon', 'SettingController@getIcon');
    Route::post('/setting/icon', 'SettingController@postIcon');
    Route::get('/setting/icon/delete', 'SettingController@removeIcon');

    Route::get('/translation/list', 'TranslationController@getList');
    Route::get('/translation/edit/{id}', 'TranslationController@getEdit');
    Route::post('/translation/update', 'TranslationController@postUpdate');
    Route::get('/translation/publish', 'TranslationController@publishTranslation');

    Route::get('/page/list', 'PageController@index');
    Route::get('/page/edit', 'PageController@edit');
    Route::post('/page/update', 'PageController@update');
    Route::post('/page/upload', 'PageController@uploadImage');

    // Dashboard
    Route::post('/stat/total_listing', 'HomeController@getTotalListingData');
    Route::post('/stat/total_member', 'HomeController@getTotalMemberData');
    Route::post('/stat/total_comments', 'HomeController@getTotalCommentData');
    Route::post('/stat/prefered_language', 'HomeController@getPreferedLanguage');

    // Notices

    Route::get('/notice/list', 'NoticeController@index');
    Route::get('/notice/create', 'NoticeController@create');
    Route::get('/notice/edit/{id}', 'NoticeController@edit');
    Route::get('/notice/send/{id}', 'NoticeController@send');


    // Newsletter

    Route::get('/newsletter/list', 'NewsletterController@index');
    Route::get('/newsletter/create', 'NewsletterController@getCreate');
    Route::post('/newsletter/create', 'NewsletterController@postCreate');
    Route::get('/newsletter/edit', 'NewsletterController@getEdit');
    Route::post('/newsletter/update', 'NewsletterController@postUpdate');

    // Newsletter Subscription

    Route::get('/newsletter-subscription/list', 'NewsletterSubController@index');
    
    // Commit
    
    Route::get('/comment/list', 'CommentController@getComments');
    Route::get('/comment/details', 'CommentController@getDetails');
    
    Route::post('/comment/update', 'CommentController@postUpdate');

});
