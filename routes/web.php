<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::get('/image/{file}', 'Common\ImageController@index');
Route::get('/thumb/{size}/{file}', 'Common\ImageController@getThumb');
Route::get('/property-thumb/{id}', 'Common\ImageController@propertyThumb');

Route::get('/access-forbidden', 'Common\ErrorController@accessForbidden');

Route::group(['domain' => 'beta.thaifullhouse.brg', 'namespace' => 'Desktop'], function () {
    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});

Route::group(['domain' => '127.0.0.1', 'namespace' => 'Desktop'], function () {
    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});

Route::group(['domain' => 'thaifullhouse.com', 'namespace' => 'Desktop'], function () {

    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});

Route::group(['domain' => 'www.thaifullhouse.com', 'namespace' => 'Desktop'], function () {
    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});

Route::group(['domain' => 'beta.thaifullhouse.com', 'namespace' => 'Desktop'], function () {
    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});

Route::group(['domain' => 'tfhbeta.brg', 'namespace' => 'Desktop'], function () {
    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});

Route::group(['domain' => 'localhost', 'namespace' => 'Desktop'], function () {
    include __DIR__ . '/desktop/admin.php';
    include __DIR__ . '/desktop/manager.php';
    include __DIR__ . '/desktop/member.php';
    include __DIR__ . '/desktop/visitor.php';
});



Route::group(['domain' => 'm.thaifullhouse.brg', 'namespace' => 'Mobile'], function () {
    include __DIR__ . '/mobile.php';
});

Route::group(['domain' => 'm.thaifullhouse.com', 'namespace' => 'Mobile'], function () {
    include __DIR__ . '/mobile.php';
});
