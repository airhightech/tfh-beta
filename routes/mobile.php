<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Route::get('/', 'HomeController@index');
Route::get('/search', 'PropertyController@search');
Route::get('/search/list', 'PropertyController@searchList');
Route::get('/property/{id}', 'PropertyController@getDetails');
Route::get('/user/favorites', 'UserController@getFavorites');
Route::post('/property/add-review', 'PropertyController@postUserReview');

Route::get('/agent/search', 'AgentController@search');
Route::get('/agent/details/{id}', 'AgentController@details');

Route::get('/user/profile', 'UserController@getProfile');
Route::get('/auth/logout', 'AuthController@logout');
Route::get('/auth/register', 'AuthController@register');
Route::get('/login', 'AuthController@getLogin');
Route::get('/facebook-login', 'AuthController@facebook');
Route::get('/auth/twitter', 'AuthController@twitter');
Route::get('/auth/google', 'AuthController@google');

Route::get('/callback-facebook', 'AuthController@facebookCallback');
Route::get('/twitter-callback', 'AuthController@twitterCallback');
Route::get('/google-callback', 'AuthController@googleCallback');

Route::get('/user/page', 'UserController@getPage');
Route::get('/user/notice', 'UserController@getNotif');
Route::get('/rest/banner', 'BannerController@get');

Route::get('/page/{sku}', 'PageController@index');
