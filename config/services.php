<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */


    'mailgun' => [
      'domain' => env('MAILGUN_DOMAIN'),
      'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '1636322470029906',
        'client_secret' => '91a3ae0491c7dcebabb5b03f35ec3cb9',
        'redirect' => env('FACEBOOK_REDIRECT'),
    ],

    'twitter' => [
        'client_id' => '138oL9YItylTGa7PftCHVelfy',
        'client_secret' => 'EL22bwySPuYYrtpYOftgHextXuJrzBGN67vwF9GxX42uX3GEeM',
        'redirect' => env('TWITTER_REDIRECT'),
    ],
    'google' => [
        'client_id' => '951550103640-8bqb10fsa7gtrmc21pjfjoe7ieo7vdno.apps.googleusercontent.com',
        'client_secret' => 'q8cBmJ6JEemUv6sVUvNdDv_I',
        'redirect' => env('GOOGLE_REDIRECT'),
    ],

];
