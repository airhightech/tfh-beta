<?php

return [
    'api_key' => env('GOOGLE_API_KEY'),
    'browser_key' => env('GOOGLE_BROWSER_KEY'),
    'nearby_place_distance' => 1500,
    'default_lat' => 13.726357, 
    'default_lng' => 100.575877,
    'default_zl' => 13
];